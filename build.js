({
    baseUrl: "www/content/script",
    mainConfigFile: 'www/content/script/main.js',
    include: ['requireLib','main'],
    name: "main",
    out: "www/content/script/main-built.js",
    paths: {
        requireLib: "../components/requirejs/require"
    }
})

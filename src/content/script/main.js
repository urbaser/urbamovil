require.config({
	baseUrl: "content/script",
	paths: {
		jQuery: "../components/jquery/dist/jquery",
		jquery: "../components/jquery/dist/jquery",
		kendo: "../components/kendo-ui/src/js",
		refr3sh: "../components/refr3sh-jslib/src/refr3sh",
		"TweenMax": "../components/gsap/src/uncompressed/TweenMax",
		"TweenLite": "../components/gsap/src/uncompressed/TweenLite",
		"TimelineMax": "../components/gsap/src/uncompressed/TimelineMax",
		"signals": "../components/signals/dist/signals",
		"kendo.repeater": "vendor/kendo.repeater",
		"gmaps": "../components/gmaps/gmaps",
		"markerCluster": "../components/gmaps/markerCluster",
		"i18next": "../components/i18next/i18next.amd.withJQuery",
		"polyfill": "../components/polyfill/dist/polyfill",
		"modernizr": "../components/modernizr/modernizr",
		"modernizrFeatures": "../components/modernizr/feature-detects",
		"es6-promise": "../components/es6-promise/es6-promise",
		"parsleyjs": "../components/parsleyjs/dist/parsley",
		"fastclick": "../components/fastclick/lib/fastclick",
	},
	shim: {
		jQuery: {
			exports: "jQuery"
		},
		kendo: {
			deps: ["jQuery"]
		},
		"kendo.repeater": {
			deps: ["kendo/kendo.core"]
		},
		"parsley": {
			deps: ["jQuery"]
		},
		"kendo/kendo.mobile.application": {
			deps: ["jQuery"]
		}
	}
});

// Function.bind polyfill
if (!Function.prototype.bind) {
	Function.prototype.bind = function(oThis) {
		if (typeof this !== 'function') {
			// closest thing possible to the ECMAScript 5
			// internal IsCallable function
			throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
		}

		var aArgs   = Array.prototype.slice.call(arguments, 1),
			fToBind = this,
			fNOP    = function() {},
			fBound  = function() {
				return fToBind.apply(this instanceof fNOP
						? this
						: oThis,
					aArgs.concat(Array.prototype.slice.call(arguments)));
			};

		fNOP.prototype = this.prototype;
		fBound.prototype = new fNOP();

		return fBound;
	};
}

var app;
function run(){
	require(["jQuery",
		"app/app",
		"app/models/user",
		"app/mapper",
		"app/config",
		"kendo/kendo.mobile.application",
		"fastclick",
		"kendo.repeater",
		"app/functions",
		"markerCluster"
	], function (j,application,user,mapper,config,kendo,fastclick) {
		window.app = app = application;
		window.app.user = user;
		window.app.viewModels = mapper;
		window.app.config = config;
		app.init();
		fastclick.attach(document.body);
	});
}

require(["jQuery","es6-promise"]);
if(window.cordova){
	console.log("Cordova environment detected");
	document.addEventListener('deviceready', function(){
		navigator.splashscreen.show();
		run();
	});
}else{
	console.log("Cordova environment NOT detected");
	setTimeout(run, 200);
}

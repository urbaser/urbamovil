
define([], function(){

  var Back = function(){}

	var obj = new Back();

  obj.back = function(){
    var url = window.app.url;
    window.app.kendoApp.pane.loader.hide();
    console.log("____________________click back button__________________");
    switch (url) {
      case "home":
        if (navigator.app) {
          navigator.app.exitApp();
        } else if (navigator.device) {
          navigator.device.exitApp();
        } else {
          window.close();
        }
        break;
      case "incidencias-home":
      case "inventory-index":
      case "planning-index":
      case "floats-index":
        window.app.kendoApp.navigate("#home");
        break;
      //Incidencias
      case "resume2":
        window.app.kendoApp.navigate("#edit2");
        break;
      case "edit2":
        window.app.kendoApp.navigate("#inventory-detailsElemento");
        break;
      case "edit":
      case "incidencias":
        window.app.kendoApp.navigate("#incidencias-home");
        break;
      case "selectLocation":
        window.app.kendoApp.navigate("#edit");
        break;
      case "selectByMap":
      case "selectByAddress":
      case "selectByInventary":
      case "selectByRFID":
      case "resume":
        window.app.kendoApp.navigate("#selectLocation");
        break;
      case "detail":
        window.app.kendoApp.navigate("#incidencias");
        break;
      case "historicoEstados":
        window.app.kendoApp.navigate("#detail");
        break;
      case "locationsMap":
        window.app.kendoApp.navigate("#detail");
        break;
      //Inventario
      case "inventory-mapsearch":
      case "inventory-search":
      case "inventory-create":
        window.app.kendoApp.navigate("#inventory-index");
        break;
      case "inventory-createElemento?tipo=1":
      case "inventory-createElemento?tipo=2":
      case "inventory-createElemento?tipo=3":
        window.app.kendoApp.navigate("#inventory-create");
        break;
      case "inventory-mapsearchCreate":
        window.app.kendoApp.navigate("#:back");
        break;
      case "inventory-results":
        window.app.kendoApp.navigate("#inventory-search");
        break;
      case "inventory-detailsElemento":
        if(kendo.history.locations[kendo.history.locations.length-2] == "inventory-mapsearch"){
          window.app.kendoApp.navigate("#inventory-mapsearch");
        }else{
          window.app.kendoApp.navigate("#inventory-results");
        }
        break;
      //Planificacion
      case "planning-search":
        window.app.kendoApp.navigate("#planning-index");
        break;
      case "planning-create":
        window.app.kendoApp.navigate("#planning-index");
        break;
      case "planning-edit":
        window.app.kendoApp.navigate("#planning-index");
        break;
      case "planning-editService":
        window.app.kendoApp.navigate("#planning-edit");
        break;
      case "planning-editStep2B":
        window.app.kendoApp.navigate("#planning-editService");
        break;
      case "planning-editStep3":
        window.app.kendoApp.navigate("#planning-editStep2B");
        break;
      case "planning-editStep4":
        window.app.kendoApp.navigate("#planning-editStep3");
        break;
      case "planning-editStep5":
        window.app.kendoApp.navigate("#planning-editStep4");
        break;
      case "planning-results":
        window.app.kendoApp.navigate("#planning-search");
        break;
      case "planning-details":
        window.app.kendoApp.navigate("#planning-results");
        break;
      //Control de flotas
      case "floats-vehicles":
        window.app.kendoApp.navigate("#floats-index");
        break;
      case "floats-routes":
        window.app.kendoApp.navigate("#floats-vehicleDetails");
        break;
      case "floats-vehicleDetails":
        window.app.kendoApp.navigate("#floats-vehicles");
        break;
      case "floats-signals":
        window.app.kendoApp.navigate("#floats-vehicleDetails");
        break;
      case "floats-signalDetails":
        window.app.kendoApp.navigate("#floats-signals");
        break;
      case "floats-alarms":
        window.app.kendoApp.navigate("#floats-vehicleDetails");
        break;
      //Otros
      case "contracts":
        window.app.kendoApp.navigate("#home");
        break;
      case "languages":
        window.app.kendoApp.navigate("#home");
        break;
      case "home-tagReaderSelector":
        window.app.kendoApp.navigate("#home");
        break;
      case "notifications":
        window.app.kendoApp.navigate("#home");
        break;

      default:
    }
  }

  return obj;
});

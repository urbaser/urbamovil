/**
 * Created by 7daysofrain on 30/7/15.
 */
define(['signals'], function(signals)
{
	var obj = {
		init: function(){
			console.log('Bluetooth initialised');
			this.bluetoothInitialised = true;
			this.deviceConnected = false;
			this.btScanning = false;
			this.btScanned = false;
		},
		scanned: new signals.Signal(),
		scannedError: new signals.Signal(),
		scan: function(){
			console.log('Started Scanning ... please wait ...');
			setTimeout(this.scanCallback.bind(this),2000);

		},
		scanCallback: function(data) {
			console.log('... finished ... scan completed');
			this.btScanning = false;
			this.btScanned = true;

			this.devices = new Array();
			this.devices.push({name:"Fake Device",address:"0000"});
			this.scanned.dispatch(this.devices);

		},
		connected: new signals.Signal(),
		connectedError: new signals.Signal(),
		connect: function(deviceAddress){
			setTimeout(this.connectionCallback.bind(this));
			this.deviceConnected = true;
			console.log('Connected to device: ' + deviceAddress || this.selectedDevice);
			this.connected.dispatch();
		},
		connectionCallback: function(data) {
			this.rfidReceived.dispatch("12345")
		},
		disconnected: new signals.Signal(),
		disconnect: function(){
			console.log('Disconnected');
			this.deviceConnected = false;
			this.disconnected.dispatch();
		},
		rfidReceived: new signals.Signal()
	};
	return obj;
});

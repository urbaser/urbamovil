define([
	'app/models/user',
	"signals",
	"app/services/BTSerialBridge",
], function(user,signals,bt)
{

	var obj = {
		scanned: new signals.Signal(),
		scannedError: new signals.Signal(),
		readTag: function(){
			if(!bt.deviceConnected)
				bt.connect(user.preferences.rfidBTId)
		},
		onConnect: function(){
			console.log("rfid reader connected");
		},
		onConnectError: function(){
			var agree = confirm(T("views.incidencias.selectByRFID.connectFail"));
			if(agree){
				bt.connect(user.preferences.rfidBTId);
			}
		},
		onRfidReceived: function(rfid){
			this.scanned.dispatch(rfid);
		},
		onRfidError: function(){
			this.error(T("views.incidencias.selectByRFID.incorrectTag"));
		},
		onDisconnected: function(){
			//bt.selectedDevice = null;
			//this.error(T("views.incidencias.selectByRFID.disconnect"));
		},
		error: function(msg){
			this.scannedError.dispatch(msg);
		}
	};


	bt.connected.add(obj.onConnect.bind(obj));
	bt.connectedError.add(obj.onConnectError.bind(obj));
	bt.rfidReceived.add(obj.onRfidReceived.bind(obj));
	bt.rfidError.add(obj.onRfidError.bind(obj));
	bt.disconnected.add(obj.onDisconnected.bind(obj));
	bt.init();
	return obj;
});

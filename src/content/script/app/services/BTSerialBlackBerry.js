/**
 * Created by 7daysofrain on 30/7/15.
 */
define(['signals'], function(signals)
{
	var obj = {
		supported: true,
		init: function(){
			this.api = com.blackberry.community.simplebtsppplugin;

			console.log('XXXX Plugin Version is: ' + this.api.pluginVersion());
			console.log('XXXX SPP Service Uuid is: ' + this.api.sppServiceUuid);

			var json = JSON.parse(this.api.initialiseBluetooth());
			console.log('XXXX json.status: ' + json.status);

			if (json.status === 'OK') {
				console.log('Bluetooth initialised');
				this.bluetoothInitialised = true;
				this.deviceConnected = false;
				this.btScanning = false;
				this.btScanned = false;
			} else {
				console.error('Bluetooth failed to initialise: ' + json.desc);
			}
		},
		scanned: new signals.Signal(),
		scannedError: new signals.Signal(),
		scan: function(){
			var json = JSON.parse(this.api.startDeviceScan(this.scanCallback.bind(this)));
			if (json.status === 'OK') {
				console.log('Started Scanning ... please wait ...');
				this.btScanning = true;
			} else {
				console.error('Failed to start scanning: ' + json.desc);
				this.scannedError.dispatch(json.desc);
			}
		},
		scanCallback: function(data) {
			console.log('... finished ... scan completed');
			var json = JSON.parse(data);
			this.btScanning = false;

			if (json.status === 'OK') {
				this.btScanned = true;
				console.log('XXXX json.devices.length ' + json.devices.length);
				this.devices = new Array();
				if (json.devices.length > 0) {
					for (var i = 0; i < json.devices.length; i++) {
						var thisDevice = {};
						thisDevice.name = json.devices[i].name;
						thisDevice.address = json.devices[i].address;
						this.devices.push(thisDevice);
						console.log('XXXX Name: ' + json.devices[i].name);
						console.log('XXXX Address: ' + json.devices[i].address);
					}
					this.scanned.dispatch(this.devices);
				} else {
					this.scannedError.dispatch(T("services.btserial.devicenotfound"));
				}
			} else {
				this.scannedError.dispatch(json.desc);
			}
		},
		connected: new signals.Signal(),
		connectedError: new signals.Signal(),
		connect: function(deviceAddress){
			this.api.bluetoothAddress = deviceAddress || this.selectedDevice;

			var json = JSON.parse(this.api.connectToDevice(this.connectionCallback.bind(this)));
			if (json.status === 'OK') {
				this.deviceConnected = true;
				console.log('Connected to device');
				this.connected.dispatch();

			} else {
				console.error('Failed to connect to device: ' + json.desc);
				this.connectedError.dispatch(json.dec);
				this.disconnect();
			}
		},
		connectionCallback: function(data) {
			var json = JSON.parse(data);

			if (json.status === 'OK') {
				if (json.event !== 'DISCONNECTION') {
					if (json.format ===  "RAW" && json.data[0] == 36) {
						console.log('XXXX data: ' + data);
						if(json.data.indexOf(36) != json.data.lastIndexOf(36)){
							var bytes = json.data.slice(5,json.data.indexOf(13))
						}
						else{
							var bytes = json.data.slice(5)
						}
						var buff = "";
						bytes.forEach(function(char){
							buff += String.fromCharCode(char);
						});
						console.log('>> ' + buff);
						if(buff.length == 16){
							this.rfidReceived.dispatch(buff);
						}
						else{
							this.rfidError.dispatch();
						}
					} else {
						//console.log('>> ' + json.data);
					}
				} else {
					console.log('Disconnect Received');
					this.disconnect();
				}
			} else {
			}
		},
		disconnected: new signals.Signal(),
		disconnect: function(){
			var json = JSON.parse(this.api.disconnectFromDevice());
			if (json.status === 'OK') {
				console.log('Disconnected');
			} else {
				console.error('Disconnect error: ' + json.desc);
			}
			this.deviceConnected = false;
			this.disconnected.dispatch();
		},
		rfidReceived: new signals.Signal(),
		rfidError: new signals.Signal()
	};
	return obj;
});

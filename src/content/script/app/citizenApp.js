define([
  "jQuery",
	"app/appbase",
	"app/models/ciudadano"
], function(j,AppBase,user) {
	'use strict'

	var VERSION = "0.8";
	var DEBUG = false;
	var NAME = "Urbamovil Ciudadano";

	var CitizenApp = function() {
		this.conf(NAME,VERSION,user,true,DEBUG);
	}
	CitizenApp.prototype = new AppBase();
	CitizenApp.prototype.constructor = CitizenApp;
	CitizenApp.prototype.onChangeRoute = function(e){
		console.log("Citizen route Changed: ", e.url);
		if(!this.online){
			if(e.url == "selectByInventary" ||
				e.url == "selectByMap"){
				console.log("Not available offline");
				$("#modal-noOffline").kendoMobileModalView("open");
				e.preventDefault();
			}
		}
		if(e.url == "login"){
			e.preventDefault();
			this.kendoApp.navigate("#loginCitizen");
		}
		if(e.url != "loginCitizen" && !window.app.user){
			this.kendoApp.navigate("#loginCitizen");
		}
	}
	CitizenApp.prototype.doneInit = function(){
		if(this.debugMode){
			$(".show-version").text("Urbamovil ciudadano versión " + this.version);
		}
		$("html").addClass("urbamovil-citizen");
	}
	CitizenApp.prototype.getStartPage = function(){
		var startPage = user.ticket ? "#home" : user.user ? "#citizenPass" : "#loginCitizen";
		console.log("Using start page: " + startPage)
		return startPage;
	};

	var obj = new CitizenApp();

	return obj;
});

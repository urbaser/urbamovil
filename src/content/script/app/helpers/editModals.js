define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,kendo)
{
	'use strict'
	var obj = {
		applySelectOne: function(name,element,datasource,callback){
			element.find(".edit-button").click(function(e){
				e.preventDefault();
				this.openSelectOne(name,element.find("[data-bind]").text(),datasource,callback);
			}.bind(this));
		},
		applyString: function(name,element,callback){
			element.find(".edit-button").click(function(e){
				e.preventDefault();
				this.openString(name,element.find("[data-bind]").text(),callback);
			}.bind(this))
		},
		openString: function(name,value,callback){
			var modal = $("#modal-editFieldInput").data("kendoMobileModalView");
			modal.open();
			modal.model.set("value",value);
			modal.model.set("name",name);
			modal.model.change = callback;
		},
		openBoolean: function(name,value,callback){
			var modal = $("#modal-editFieldBoolean").data("kendoMobileModalView");
			modal.open();
			modal.model.set("value",value);
			modal.model.set("name",name);
			modal.model.change = callback;
		},
		openNumber: function(name,value,callback){
			var modal = $("#modal-editFieldNumber").data("kendoMobileModalView");
			modal.open();
			modal.model.set("value",value);
			modal.model.set("name",name);
			modal.model.change = callback;
		},
		openDate: function(name,value,callback){
			var modal = $("#modal-editFieldDate").data("kendoMobileModalView");
			modal.open();
			modal.model.set("value",value.substr(0,value.indexOf("T")));
			modal.model.set("name",name);
			modal.model.change = callback;
		},
		openFile: function(name,callback){
			var modal = $("#modal-editFieldFile").data("kendoMobileModalView");
			modal.open();
			modal.model.change = callback;
			modal.model.set("name",name);
		},
		openSelectOne: function(name,value,datasource,callback){
			var modal = $("#modal-editFieldSelect").data("kendoMobileModalView");
			modal.open();
			modal.model.set("value",value);
			modal.model.set("source",datasource);
			modal.model.set("name",name);
			modal.model.change = callback;
		},
		openTagReader: function(callback){
			var modal = $("#modals-readTag").data("kendoMobileModalView");
			modal.open();
			modal.model.onReadHandler = callback;
		}
	};
	return obj;
});

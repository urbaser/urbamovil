define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo/kendo.mobile.buttongroup"
], function(j,kendo)
{
	'use strict'
	var obj = {
		apply: function(view, changeCallback){
			$(view).find(".buttongroup").each(function(index, element){
				var listViews = $(element).parent().find(".buttongroup-content");
				$(element).kendoMobileButtonGroup({
					select: function(e) {
						$(this)
						var current = listViews.hide().eq(e.index).show();
						if(changeCallback){
							changeCallback(current);
						}
					},
					index: index
				});
				listViews.css("display","none").eq(0).css("display","");
			});
			$(view).find(".buttongroup")
		}
	};
	return obj;
});

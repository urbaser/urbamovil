define([
	"kendo/kendo.core",
	"kendo/kendo.data",
	"app/models/masterTables"
], function(kendo,k,tables)
{
	function parameterMap(data, requestType) {
		if (requestType === 'read') {
			return JSON.stringify(data);
		}
		return data;
	}
	var obj = {

		vehiculos: {
			dataSource: new kendo.data.DataSource({ 
				transport: {
					read: {
						url: function() { return window.app.config.baseUrl + '/FlotaVehiculos' },
						dataType: "json",
						contentType: "application/json",
						type: "POST",
						data: function(){
							return obj.vehiculos.filter;
						}
					},
					parameterMap: parameterMap
				},
				schema: {
					model: {
						id: "Codigo",
						vehiculo: function() {
							return this.Codigo + " " + this.Marca + " " + this.Modelo;
						}
					},

					parse: function (response) {
						return response;
					}
				},

				requestStart: function(e) {
					if(window.app.kendoApp && window.app.kendoApp.pane && this.silent)
						window.app.kendoApp.pane.loader.show();
				},
				requestEnd: function(e){
					if(window.app.kendoApp)
						window.app.kendoApp.pane.loader.hide();
				},
				error: function (e) {
					console.error(e);
					if(window.app.kendoApp && window.app.kendoApp.pane){
						window.app.kendoApp.pane.loader.hide();
					}
					if(e.xhr.status != 401){
						throttle(error,5000);
					}
				},
			}),

			select: function(item){
				this.selected = item;
				console.log("Selected: ",obj.vehiculos.selected);
			},
			setFilter: function(filter,dontFetch){
				this.filter = filter;
				if(filter){
					console.log("select filter");
				}
				else{
					console.log("Crearing filters");
				}
				if(!dontFetch)
					this.dataSource.read();
			}
		},
		rutas: {
			dataSource: new kendo.data.DataSource({ 
				transport: {
					read: {
						url: function () {
							return window.app.config.baseUrl + '/FlotaRutas?cod=' + obj.rutas.dataSource.params
						},
						dataType: "json",
						contentType: "application/json",
						type: "POST",
						data: function(){
							return obj.rutas.filter;
						}
					},
					parameterMap: parameterMap
				},
				schema: {
					parse: function (response) {
						/*var prevVelocidad = 0;
						$.map(response, function(val, i){
							if (val.Velocidad > 0) {
								val.Duracion = Math.round((val.Distancia / val.Velocidad) * 100) / 100;
								if (val.Duracion > 0) {
									val.Aceleracion = Math.round(((val.Velocidad - prevVelocidad) / val.Duracion) * 100) / 100; //vf - vi / t;
								}
								else {
									val.Aceleracion = 0;
								}
							} else {
								val.Duracion = 0;
								val.Aceleracion = 0;
							}

							prevVelocidad = val.Velocidad;
							return val
						});*/
						return response;
					}
				},
				sort: { field: "Fecha", dir: "desc" },
				/*aggregate: [
					{ field: "Velocidad", aggregate: "min" },
					{ field: "Velocidad", aggregate: "max" },
					{ field: "Distancia", aggregate: "min" },
					{ field: "Distancia", aggregate: "max" },
					{ field: "Distancia", aggregate: "sum" },
					{ field: "Duracion", aggregate: "sum" },
					{ field: "Aceleracion", aggregate: "min" },
					{ field: "Aceleracion", aggregate: "max" },
				],*/
				requestStart: function(e) {
					if (obj.rutas.dataSource.params == undefined) { e.preventDefault(); console.log("Faltan parámetros para: " + this.transport.options.read.url()); return;  }
					if(window.app.kendoApp && window.app.kendoApp.pane)
						window.app.kendoApp.pane.loader.show();
				},
				requestEnd: function(e){
					if(window.app.kendoApp)
						window.app.kendoApp.pane.loader.hide();
				},
				error: function (e) {
					console.error(e);
					if(window.app.kendoApp && window.app.kendoApp.pane){
						window.app.kendoApp.pane.loader.hide();
					}
					if(e.xhr.status != 401){
						throttle(error,5000);
					}
				},
			}),

			setFilter: function(filter,dontFetch){
				this.filter = filter;
				if(filter){
					console.log("select filter");
				}
				else{
					console.log("Crearing filters");
				}
				if(!dontFetch)
					this.dataSource.read();
			}
		},
		categorias: {
			dataSource: new kendo.data.DataSource({ 
				transport: {
					read: {
						url: function () {
							return window.app.config.baseUrl + '/FlotaCategorias'
						},
						dataType: "json",
						type: "GET",
					}
				},
				schema: {
					parse: function (response) {
						return response;
					}
				},
				requestStart: function (e) {
					if (window.app.kendoApp && window.app.kendoApp.pane)
						window.app.kendoApp.pane.loader.show();
				},
				requestEnd: function (e) {
					if (window.app.kendoApp)
						window.app.kendoApp.pane.loader.hide();
				},
				error: function (e) {
					console.error(e);
					if (window.app.kendoApp && window.app.kendoApp.pane) {
						window.app.kendoApp.pane.loader.hide();
					}
					if (e.xhr.status != 401) {
						throttle(error, 5000);
					}
				}
			}),
		},
		alarmas: {
			dataSource: new kendo.data.DataSource({ 
				transport: {
					read: {
						url: function() { return window.app.config.baseUrl + '/FlotaAlarmas?cod=' + obj.alarmas.dataSource.params },
						dataType: "json",
						contentType: "application/json",
						type: "POST",
						data: function(){
							return obj.alarmas.filter;
						}
					},
					parameterMap: parameterMap
				},
				schema: {
					model: {
						criticidadClass: function() {
							var criticidadClass;
							switch (this.CodigoNivelCritico) {
								case "1":
									criticidadClass = "dot-green";
									break;
								case "3":
									criticidadClass = "dot-red";
									break;
								default:
									criticidadClass = "dot-grey";
									break;
							}
							return criticidadClass;
						}
					},
					parse: function (response) {
						return response;
					}
				},
				requestStart: function (e) {
					if (obj.alarmas.dataSource.params == undefined) { e.preventDefault(); console.log("Faltan parámetros para: " + this.transport.options.read.url()); return;  }
					if (window.app.kendoApp && window.app.kendoApp.pane)
						window.app.kendoApp.pane.loader.show();
				},
				requestEnd: function (e) {
					if (window.app.kendoApp)
						window.app.kendoApp.pane.loader.hide();
				},
				error: function (e) {
					console.error(e);
					if (window.app.kendoApp && window.app.kendoApp.pane) {
						window.app.kendoApp.pane.loader.hide();
					}
					if (e.xhr.status != 401) {
						throttle(error, 5000);
					}
				},
			}),

			select: function(item){
				this.selected = item;
				console.log("Selected: ",obj.alarmas.selected);
			},
			setFilter: function(filter,dontFetch){
				this.filter = filter;
				if(filter){
					console.log("select filter");
				}
				else{
					console.log("Crearing filters");
				}
				if(!dontFetch)
					this.dataSource.read();
			}
		},
		signals: {
			dataSource: new kendo.data.DataSource({ 
				transport: {
					read: {
						url: function () {
							return window.app.config.baseUrl + '/FlotaDigitalSignals?cod=' + obj.signals.dataSource.params
						},
						dataType: "json",
						contentType: "application/json",
						type: "POST",
						data: function(){
							return obj.signals.filter;
						}
					},
					parameterMap: parameterMap
				},
				schema: {
					parse: function (response) {
						return response;
					}
				},
				requestStart: function(e) {
					if (obj.signals.dataSource.params == undefined) { e.preventDefault(); console.log("Faltan parámetros para: " + this.transport.options.read.url()); return;  }
					if(window.app.kendoApp && window.app.kendoApp.pane)
						window.app.kendoApp.pane.loader.show();
				},
				requestEnd: function(e){
					if(window.app.kendoApp)
						window.app.kendoApp.pane.loader.hide();
				},
				error: function (e) {
					console.error(e);
					if(window.app.kendoApp && window.app.kendoApp.pane){
						window.app.kendoApp.pane.loader.hide();
					}
					if(e.xhr.status != 401){
						throttle(error,5000);
					}
				}
			}),

			select: function(item){
				this.selected = item;
				console.log("Selected: ",obj.signals.selected);
			},
			setFilter: function(filter,dontFetch){
				this.filter = filter;
				if(filter){
					console.log("select filter");
				}
				else{
					console.log("Crearing filters");
				}
				if(!dontFetch)
					this.dataSource.read();
			}

		},
		tiposAlarmas: {
			dataSource: new kendo.data.DataSource({ 
				transport: {
					read: {
						url: function () {
							return window.app.config.baseUrl + '/FlotaTiposAlarmas'
						},
						dataType: "json",
						type: "GET",
					}
				},
				schema: {
					parse: function (response) {
						return response;
					}
				},
				requestStart: function (e) {
					if (window.app.kendoApp && window.app.kendoApp.pane)
						window.app.kendoApp.pane.loader.show();
				},
				requestEnd: function (e) {
					if (window.app.kendoApp)
						window.app.kendoApp.pane.loader.hide();
				},
				error: function (e) {
					console.error(e);
					if (window.app.kendoApp && window.app.kendoApp.pane) {
						window.app.kendoApp.pane.loader.hide();
					}
					if (e.xhr.status != 401) {
						throttle(error, 5000);
					}
				}
			}),


		},
		criticidad: {
			dataSource: new kendo.data.DataSource({ 
				transport: {
					read: {
						url: function () {
							return window.app.config.baseUrl + '/FlotaCriticidad'
						},
						dataType: "json",
						type: "GET",
					}
				},
				schema: {
					parse: function (response) {
						return response;
					}
				},
				requestStart: function (e) {
					if (window.app.kendoApp && window.app.kendoApp.pane)
						window.app.kendoApp.pane.loader.show();
				},
				requestEnd: function (e) {
					if (window.app.kendoApp)
						window.app.kendoApp.pane.loader.hide();
				},
				error: function (e) {
					console.error(e);
					if (window.app.kendoApp && window.app.kendoApp.pane) {
						window.app.kendoApp.pane.loader.hide();
					}
					if (e.xhr.status != 401) {
						throttle(error, 5000);
					}
				}
			}),

		},
		tiposSignals: {
			dataSource: new kendo.data.DataSource({ 
				transport: {
					read: {
						url: function () {
							return window.app.config.baseUrl + '/FlotaTypeSignals'
						},
						dataType: "json",
						type: "GET",
					}
				},
				schema: {
					parse: function (response) {
						return response;
					}
				},
				requestStart: function (e) {
					if (window.app.kendoApp && window.app.kendoApp.pane)
						window.app.kendoApp.pane.loader.show();
				},
				requestEnd: function (e) {
					if (window.app.kendoApp)
						window.app.kendoApp.pane.loader.hide();
				},
				error: function (e) {
					console.error(e);
					if (window.app.kendoApp && window.app.kendoApp.pane) {
						window.app.kendoApp.pane.loader.hide();
					}
					if (e.xhr.status != 401) {
						throttle(error, 5000);
					}
				}
			}),

		}
	}


	return obj;
});

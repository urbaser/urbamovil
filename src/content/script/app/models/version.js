define([
	'jQuery',
	'signals',
	'refr3sh/net/SimpleApiCall',
	'app/config',
	"kendo/kendo.data",
	'app/models/masterTables'
], function(j,signals,SimpleApiCall,config,kendo,masterTables)
{
	var obj = {
		gotVersion: new signals.Signal(),
		getVersionFailed: new signals.Signal(),
		getVersion: function () {
			var call = new SimpleApiCall();
			call.succeed.addOnce(function (data) {
				this.gotVersion.dispatch(data);
			}.bind(this));
			call.failed.addOnce(function (data) {
				this.getVersionFailed.dispatch();
			}.bind(this));
			call.get(window.app.config.baseUrl + "/versiones", {c: window.app.version, s: window.app.os, l: window.app.detectedLanguage.toLowerCase()});
		}
	};
	return obj;
});

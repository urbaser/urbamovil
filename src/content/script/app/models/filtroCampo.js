define([
	"kendo/kendo.core",
	"kendo/kendo.data",
	"app/models/masterTables",
	"app/models/inventario"
], function(kendo,k,tables,inventary)
{
	var schema = {
		id: "IdFiltroCampo",
		fields: {
			IdFiltroCampo: { editable: false, nullable: true },
			NombreCampo: { type: "string", validation: { required: true} },
			Tipo: { type: "string", validation: { required: true} },
			Valor: { type: "string", validation: { required: true} }
		},
		title: function(){
			switch(this.NombreCampo){
				case "ESTADO_INCIDENCIA":
					return T("models.filtroCampo.state") + ": " + tables.estados.get(this.Valor).EstadoIncidencia;
					break;
				case "GRUPO_TIPO_DESCRIPCION":
					var vals = this.Valor.split(",");
					return 	tables.grupos.get(vals[0]).GrupoIncidencia + "/" +
							tables.tipos.get(vals[1]).TipoIncidencia + "/" +
							tables.descripciones.get(vals[2]).DescripcionIncidencia;
					break;
				case "FECHA_DESDE":
					return T("models.filtroCampo.date-from") + ": " + kendo.toString(this.Valor, "d/M/yyyy");
					break;
				case "FECHA_HASTA":
					return T("models.filtroCampo.date-to") + ": " + kendo.toString(this.Valor, "d/M/yyyy");
					break;
				case "ULTIMOS_DIAS":
					return T("models.filtroCampo.days",{days:this.Valor});
					break;
				case "CONTRATO":
					return T("models.filtroCampo.contract") + ": ";
					break;
				case "PRIORIDAD":
					return T("models.filtroCampo.priority") + ": " + tables.prioridades.get(this.Valor).Prioridad;
					break;
				case "PROVINCIA/MUNICIPIO":
					var vals = this.Valor.split(",");
					return 	tables.provincias.get(vals[0]).NombreProvincia + " / " +
							tables.municipios.get(vals[1]).NombreMunicipio;
					break;
				case "DIRECCION":
					return T("models.filtroCampo.address") + ": " + this.Valor;
					break;
				case "ELEMENTO_INVENTARIO":
					return T("models.filtroCampo.inventary");
					break;
				default:
					return T("models.filtroCampo.unknown");
			}
		}
	};
	return kendo.data.Model.define(schema);
});

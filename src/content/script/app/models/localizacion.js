define([
	"kendo/kendo.core",
	"kendo/kendo.data",
	"app/models/inventario"
], function(kendo,k,inventario)
{

	var schema = {
		id: "IdLocalizacion",
		fields: {
			IdLocalizacion: 		{ type: "string", editable: false, nullable: true },
			IdIncidencia: 			{ type: "string", validation: { required: true } },
			IdTipoLocalizacion: 	{ type: "number", validation: { required: true, min: 1} },
			Map_Latitud: 			{ type: "number", nullable: true },
			Map_Longitud: 			{ type: "number", nullable: true },
			Dir_Calle: 				{ type: "string", nullable: true },
			Dir_Numero: 			{ type: "number", nullable: true },
			Inv_IdDistrito: 		{ type: "number", nullable: true },
			Inv_IdEspacioUrbano: 	{ type: "number", nullable: true },
			Inv_IdZona: 			{ type: "number", nullable: true },
			Inv_IdRuta: 			{ type: "number", nullable: true },
			Inv_IdTipoElemento: 	{ type: "number", nullable: true },
			Inv_IdElemento: 		{ type: "number", nullable: true },
			Codigo: 				{ type: "string", nullable: true }
		},
		title: function(){
			if(this.Descripcion){
				return this.Descripcion;
			}
			else if(this.IdTipoLocalizacion == 2){
				return this.Dir_Calle;
			}
			else if(this.IdTipoLocalizacion == 3){
				try{
					if(this.Inv_IdTipoElemento > 0 && this.Inv_IdElemento > 0){
						return inventario.tipos.get(this.Inv_IdTipoElemento).Tipo + "/" + inventario.elementos.get(this.Inv_IdElemento).Elemento;
					}
					else if(this.Inv_IdRuta > 0){
						return inventario.rutas.get(this.Inv_IdRuta).Ruta;
					}
					else if(this.Inv_IdZona > 0 ){
						return inventario.zonas.get(this.Inv_IdZona).Zona;
					}
					else if(this.Inv_IdEspacioUrbano > 0 ){
						return inventario.espaciosUrbanos.get(this.Inv_IdEspacioUrbano).EspacioUrbano;
					}
					else if(this.Inv_IdDistrito > 0 ){
						return inventario.distritos.get(this.Inv_IdDistrito).Distrito;
					}
				}
				catch(error){}
				finally{
					return T("models.localizacion.inventary-location");
				}

			}
			else if(this.IdTipoLocalizacion == 1){
				return T("models.localizacion.map-location");
			}
			else if(this.IdTipoLocalizacion == 4){
				return T("models.localizacion.rfid-location");
			}
			else{
				return T("models.localizacion.not-implemented");
			}
		}
	};
	Object.defineProperty(schema,"distrito",{
		get: function(){
			var vo = inventario.distritos.get(this.Inv_IdDistrito);
			if(vo){
				return vo.Distrito;
			}
			return null;
		}
	});
	Object.defineProperty(schema,"espacioUrbano",{
		get: function(){
			var vo = inventario.espaciosUrbanos.get(this.Inv_IdEspacioUrbano);
			if(vo){
				return vo.EspacioUrbano;
			}
			return null;
		}
	});
	Object.defineProperty(schema,"zona",{
		get: function(){
			var vo = inventario.zonas.get(this.Inv_IdZona);
			if(vo){
				return vo.Zona;
			}
			return null;
		}
	});
	Object.defineProperty(schema,"ruta",{
		get: function(){
			var vo = inventario.rutas.get(this.Inv_IdRuta);
			if(vo){
				return vo.Ruta;
			}
			return null;
		}
	});
	Object.defineProperty(schema,"tipo",{
		get: function(){
			var vo = inventario.tipos.get(this.Inv_IdTipoElemento);
			if(vo){
				return vo.Tipo;
			}
			return null;
		}
	});
	Object.defineProperty(schema,"elemento",{
		get: function(){
			var vo = inventario.distritos.get(this.Inv_IdElemento);
			if(vo){
				return vo.Elemento;
			}
			return null;
		}
	});
	return kendo.data.Model.define(schema);
});

define([
	"kendo/kendo.core",
	"kendo/kendo.data",
	"app/models/inventario",
	"app/models/masterTables",
], function(kendo,k,inventario,tables)
{
	var schema = {
		zona: function () {
			if (this.IdZona) {
				return this.IdZona ? this.Zona : "";
			}
			if (this.IdRuta) {
				return this.IdRuta ? this.Ruta : "";
			}
			return "";
		},
		tarea: function() {
			return this.IdTarea ? tables.tareasServicio.get(this.IdTarea).Tarea : "";
		},
		linea2: function() {
			var linea2str = "";
			if (this.EspacioUrbano) linea2str = this.EspacioUrbano;
			if (this.Zona) linea2str = linea2str + " | " + this.Zona;
			else if (this.Ruta) linea2str = linea2str + " | " + this.Zona;
			return linea2str;
		}
	}
	return kendo.data.Model.define(schema);
});

define([
	"kendo/kendo.core",
	"kendo/kendo.data",
	"app/models/masterTables"
], function(kendo,k,tables)
{
	var obj = {
		planificacionDatosServicio: function () {
			var schema = {

			}
			return kendo.data.Model.define(schema);
		},
		planificacion: function () {
			var schema = {

				id: "IdOT",
				municipio: function() {
					return this.DatosServicio.IdMunicipio ? tables.municipios.get(this.DatosServicio.IdMunicipio).NombreMunicipio : "";
				},
				provincia: function() {
					return this.DatosServicio.IdProvincia ? tables.provincias.get(this.DatosServicio.IdProvincia).NombreProvincia : "";
				},

				tieneTarea: function(id) {
					if (this.Tareas.length == 0) return false;

					var result = $.grep(this.Tareas, function(e){ return parseInt(e.IdTarea) == parseInt(id); });
					return (result.length == 0) ? false : true;
				},
				instalacion: function() {
					return (app.user.isSmartUS) ? "SmarTools" : "URBAJARDIN";
				},
				fechaOTFormatted: function(){
					return this.FechaOT ? kendo.toString(this.FechaOT, "dd/MM/yyyy HH:mm") : "";
				},

				fechaFinFormatted: function(){
					if (this.DatosServicio != undefined) {
						return this.DatosServicio.FechaFin ? kendo.toString(this.DatosServicio.FechaFin, "dd/MM/yyyy HH:mm") : "";
					}
					return "";
				},
				localizacion: function() {
					var loc = "";
					if (this.instalacion() == "URBAJARDIN") {
						loc = this.municipio();
					} else {
						if (this.Localizacion.length > 0) {
							for (var i = 0; i < this.Localizacion.length; i++) {
								if (loc != "") {
									if (this.Localizacion.length - 1 == i) {
										loc = loc + " & " + this.Localizacion[i].Zona;
									} else {
										loc = loc + ", " + this.Localizacion[i].Zona;
									}
								} else {
									loc = this.Localizacion[i].Zona;
								}
							}
						}
						else loc = "(sin especificar)";
					}

					return loc;
				}
			};

			return kendo.data.Model.define(schema);
		},
		planificacionLocalizacion: function () {
			var schema = {

			};

			return kendo.data.Model.define(schema.model);
		},
		servicios: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/PlanificacionServicios/' + obj.servicios.params },
					dataType: "json",
					type: "GET",

				}
			},
			schema:{
				parse: function(response) {
					//response.unshift({IdDistrito:0,Distrito:T("models.inventario.select-district")});
					return response;
				}
			},
			requestStart: function(e) {
				if (obj.servicios.params == undefined) { e.preventDefault(); return; }
				if(window.app.kendoApp && window.app.kendoApp.pane)
					window.app.kendoApp.pane.loader.show();
			},
			requestEnd: function(e){
				if(window.app.kendoApp)
					window.app.kendoApp.pane.loader.hide();
			},
			error: function (e) {
				console.error(e);
				if(window.app.kendoApp && window.app.kendoApp.pane){
					window.app.kendoApp.pane.loader.hide();
				}
				if(e.xhr.status != 401){
					throttle(error,5000);
				}
			}
		}),
		contratosAdmin: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/PlanificacionContratosAdmv/' + obj.contratosAdmin.params },
					dataType: "json",
					type: "GET",

				}
			},
			schema:{
				parse: function(response) {
					//response.unshift({IdDistrito:0,Distrito:T("models.inventario.select-district")});
					return response;
				}
			},
			requestStart: function(e) {
				if (obj.contratosAdmin.params == undefined) { e.preventDefault(); return; }
				if(window.app.kendoApp && window.app.kendoApp.pane)
					window.app.kendoApp.pane.loader.show();
			},
			requestEnd: function(e){
				if(window.app.kendoApp)
					window.app.kendoApp.pane.loader.hide();
			},
			error: function (e) {
				console.error(e);
				if(window.app.kendoApp && window.app.kendoApp.pane){
					window.app.kendoApp.pane.loader.hide();
				}
				if(e.xhr.status != 401){
					throttle(error,5000);
				}
			}
		}),
		zonasAdmin: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/PlanificacionZonasAdmv/' + obj.zonasAdmin.params },
					dataType: "json",
					type: "GET",

				}
			},
			schema:{
				parse: function(response) {
					//response.unshift({IdDistrito:0,Distrito:T("models.inventario.select-district")});
					return response;
				}
			},
			requestStart: function(e) {
				if (obj.zonasAdmin.params == undefined) { e.preventDefault(); return; }
				if(window.app.kendoApp && window.app.kendoApp.pane)
					window.app.kendoApp.pane.loader.show();
			},
			requestEnd: function(e){
				if(window.app.kendoApp)
					window.app.kendoApp.pane.loader.hide();
			},
			error: function (e) {
				console.error(e);
				if(window.app.kendoApp && window.app.kendoApp.pane){
					window.app.kendoApp.pane.loader.hide();
				}
				if(e.xhr.status != 401){
					throttle(error,5000);
				}
			}
		}),
		rutasSector: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/PlanificacionRutaSector/' + obj.rutasSector.params },
					dataType: "json",
					type: "GET",

				}
			},
			schema:{
				parse: function(response) {
					//response.unshift({IdDistrito:0,Distrito:T("models.inventario.select-district")});
					return response;
				}
			},
			requestStart: function(e) {
				if (obj.rutasSector.params == undefined) { e.preventDefault(); return; }
				if(window.app.kendoApp && window.app.kendoApp.pane)
					window.app.kendoApp.pane.loader.show();
			},
			requestEnd: function(e){
				if(window.app.kendoApp)
					window.app.kendoApp.pane.loader.hide();
			},
			error: function (e) {
				console.error(e);
				if(window.app.kendoApp && window.app.kendoApp.pane){
					window.app.kendoApp.pane.loader.hide();
				}
				if(e.xhr.status != 401){
					throttle(error,5000);
				}
			}
		}),
		planificacionRecurso: function () {
			var schema = {
				id: "IdRecurso",
				fields: {
					IdRecurso: {type: "string", nullable: false},
					IdTipoRecurso: {type: "number", validation: {required: true, min: 1}},
					TipoRecurso: {type: "string", nullable: false},
					Recurso: {type: "string", nullable: false}
				},
				tipoRecurso: function () {
					return this.IdTipoRecurso ? planningTables.tiposRecurso.get(this.IdTipoRecurso).TipoRecurso : "";
				}
			}
			return kendo.data.Model.define(schema);
		},
		planificacionTarea: function () {
			var schema = {
				id: "IdTarea",
				fields: {
					IdTarea: {type: "number", validation: {required: true, min: 1}},
					Tarea: {type: "string", nullable: false},
					IdTipoServicio: {type: "number", validation: {required: true, min: 1}}
				},
				tipoServicio: function () {
					return this.IdTipoServicio ? planningTables.tiposServicio.get(this.IdTipoServicio).TipoServicio : "";
				}
			}
			return kendo.data.Model.define(schema);
		},
		planificacionFiltro: function () {
			var schema = {
				fields: {
					IdContrato: {type: "string", nullable: true},
					IdAreaNegocio: {type: "string", nullable: true},
					FechaDesde: {type: "date", nullable: true},
					FechaHasta: {type: "date", nullable: true},
					IdentificadorOT: {type: "string", nullable: true},
					IdentificadorServicio: {type: "string", nullable: true},
					DescripcionServicio: {type: "string", nullable: true},
					IdTipoServicio: {type: "number", validation: {required: false, min: 1}},
					IdProvincia: {type: "number", validation: {required: false, min: 1}},
					IdMunicipio: {type: "number", validation: {required: false, min: 1}},
					IdEspacioUrbano: {type: "number", validation: {required: false, min: 1}},
					IdZonaUrb: {type: "number", validation: {required: false, min: 1}},
					IdElemento: {type: "number", validation: {required: false, min: 1}},
					IdZona: {type: "number", validation: {required: false, min: 1}},
					IdRutaSector: {type: "number", validation: {required: false, min: 1}}
				}
			}
			return kendo.data.Model.define(schema);
		},
		servicioRecursos: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/PlanificacionCategoriasRecursos/' + obj.servicioRecursos.params },
					dataType: "json",
					type: "GET",

				}
			},
			schema:{
				parse: function(response) {
					//response.unshift({IdDistrito:0,Distrito:T("models.inventario.select-district")});
					return response;
				}
			},
			requestStart: function(e) {
				if (obj.servicioRecursos.params == undefined) { e.preventDefault(); return; }
				if(window.app.kendoApp && window.app.kendoApp.pane)
					window.app.kendoApp.pane.loader.show();
			},
			requestEnd: function(e){
				if(window.app.kendoApp)
					window.app.kendoApp.pane.loader.hide();
			},
			error: function (e) {
				console.error(e);
				if(window.app.kendoApp && window.app.kendoApp.pane){
					window.app.kendoApp.pane.loader.hide();
				}
				if(e.xhr.status != 401){
					throttle(error,5000);
				}
			}
		}),
	}


	return obj;
});

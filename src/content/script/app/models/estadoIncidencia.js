define([
	"kendo/kendo.core",
	"kendo/kendo.data",
	"app/models/masterTables"
], function(kendo,k,tables)
{
	var schema = {
		id: "IdEstado",
		fields: {
			IdEstado: { editable: false, nullable: true },
			Fecha: { type:"Date", nullable: false, validation: { required: true}},
			IdIncidencia: { type:"Number", nullable: false, validation: { required: true}},
			Observaciones: { type:"String", nullable: false, validation: { required: true}},
			Usuario: { type:"String", nullable: false, validation: { required: true}},
			FiltrosCampo: []
		},
		estado: function(){
			return this.IdEstado ? tables.estados.get(this.IdEstado).EstadoIncidencia : null;
		},
	};
	return kendo.data.Model.define(schema);
});

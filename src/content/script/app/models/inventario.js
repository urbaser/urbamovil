define([
	'signals',
	'kendo/kendo.data',
	'app/models/masterTables'
], function(signals,kendo, tables) {
	var obj = {
		location: new Object(),
		distritos: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/localizaciondistritos' },
					dataType: "json",
					type: "GET"
				}
			},
			schema:{
				model: {
					id: "IdDistrito",
					fields: {
						IdDistrito: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						Distrito: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				},
				parse: function(response) {
					//response.unshift({IdDistrito:0,Distrito:T("models.inventario.select-district")});
					return response;
				}
			}
		}),
		espaciosUrbanos: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/localizacionespaciourbano' },
					dataType: "json",
					type: "GET",
					data: function(){
						return {v:obj.espaciosUrbanos.params};
					}
				}
			},
			requestStart: function (e) {
				if(!obj.espaciosUrbanos.params){
					e.preventDefault();
				}
			},
			schema:{
				model: {
					id: "IdEspacioUrbano",
					fields: {
						IdEspacioUrbano: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						IdDistrito: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						EspacioUrbano: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				},
				parse: function(response) {
					//response.unshift({IdEspacioUrbano:0,EspacioUrbano:T("models.inventario.select-space")});
					return response;
				}
			}
		}),
		zonas: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/localizacionzonas' },
					dataType: "json",
					type: "GET",
					data: function(){
						return {v:obj.zonas.params};
					}
				}
			},
			requestStart: function (e) {
				if(!obj.zonas.params){
					e.preventDefault();
				}
			},
			schema:{
				model: {
					id: "IdZona",
					fields: {
						IdZona: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						IdEspacioUrbano: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						Zona: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				},
				parse: function(response) {
					var result = response.Rutas.concat(response.Zonas);
					result.forEach(function(it){
						it.Nombre = it.Zona || it.Ruta;
						it.Id = it.IdZona || it.IdRuta;
					});
					result.sort(function (a, b) {
						return a.Nombre.toLowerCase().localeCompare(b.Nombre.toLowerCase());
					})
					//result.unshift({IdZona:0,Zona:T("models.inventario.select-zone")});
					return result;
				}
			}
		}),
		rutas: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/localizacionrutas' },
					dataType: "json",
					type: "GET",
					data: function(){
						return {id:window.app.user.ticket,v:obj.rutas.params};
					}
				}
			},
			requestStart: function (e) {
				if(!this.params){
					e.preventDefault();
				}
			},
			schema:{
				model: {
					id: "IdRuta",
					fields: {
						IdRuta: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						IdEspacioUrbano: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						Ruta: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				},
				parse: function(response) {
					response.unshift({IdRuta:0,Ruta:T("models.inventario.select-route")});
					return response;
				}
			}
		}),
		tipos: tables.tiposElementos,
		elementos: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/inventario' },
					dataType: "json",
					type: "GET",
					data: function(){
						if(!obj.elementos.params){
							return {};
						}
						return {
							d: obj.elementos.params.Inv_IdDistrito,
							e: obj.elementos.params.Inv_IdEspacioUrbano,
							z: obj.elementos.params.Inv_IdZona,
							r: obj.elementos.params.Inv_IdRuta,
							t: obj.elementos.params.Inv_IdTipoElemento,
						};
					}
				}
			},
			requestStart: function (e) {
				if(!window.app.user.ticket){
					e.preventDefault();
				}
				/*if(	!this.params ||
				 !this.params.Inv_IdDistrito ||
				 !this.params.Inv_IdEspacioUrbano ||
				 (!this.params.Inv_IdZona && !this.params.Inv_IdRuta) ||
				 !this.params.Inv_IdTipoElemento){
				 e.preventDefault();
				 }*/
			},
			schema:{
				model: {
					id: "IdElemento",
					fields: {
						IdElemento: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						Elemento: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				},
				parse: function(response) {
					response = new Array();
					response.unshift({IdElemento:0,Elemento:T("models.inventario.select-element")},{IdElemento:1,Elemento:"Elemento 1"});
					return response;
				}
			}
		}),
		elementosLista: new kendo.data.DataSource({ 
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/InventarioLista' },
					dataType: "json",
					type: "POST",
					data: function(){
						return obj.elementosLista.params;
					}
				}
			},
			requestStart: function (e) {
				if(!obj.elementosLista.params){
					e.preventDefault();
				}
			},
			schema:{
				model: {
					id: "IdElemento",
					fields: {
						IdElemento: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						NombreElemento: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				},
				parse: function(response) {
					response.unshift({IdElemento:0,NombreElemento:T("models.inventario.select-element")});
					return response;
				}
			}
		})
	};
	return obj;
});

define([
	"kendo/kendo.core",
	"kendo/kendo.data",
	"app/models/masterTables",
	"app/models/inventario"
], function(kendo,k,tables, inventory)
{
	var schema = {
		zona: function () {
			if (this.IdZona) {
				return this.IdZona ? this.Zona : "";
			}
			if (this.IdRutaSector) {
				return this.IdRutaSector ? this.RutaSector : "";
			}
			return "";
		}
	};
	return kendo.data.Model.define(schema);
});

/**
	* Created by 7daysofrain on 25/3/15.
*/
define([
	"app/models/shortcut",
	"app/services/permissionManager",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"], function(Shortcut,permissionManager,kendo)
{
	var obj = {
		dataSource: new kendo.data.DataSource({
			data: [],
			transport: {
				read: function (e) {
					$.ajax({
						url: "content/data/shortcuts.json",
						dataType: "json",
						type: "GET",
						success: function(result){
							for (var i = 0; i < result.length; i++) {
								if(result[i].Label == "Incidencias"){
									result[i].Label = T("views.home.index.incidences");
								}else if(result[i].Label == "Inventario"){
									result[i].Label = T("views.home.index.inventory");
								}else if(result[i].Label == "Planificación"){
									result[i].Label = T("views.home.index.planning");
								}else{
									result[i].Label = T("views.home.index.fleetControl");
								}
							}
							e.success(result);
			    	}
					});
			 	}
			},
			error: function (e) {
				console.error(e);
				window.app.kendoApp.pane.loader.hide();
			},
			batch: false,
			schema: {
				model: Shortcut,
				parse: function (data) {
					return data.filter(function(it){
						return permissionManager.hasPermission(it.RoleVisibility);
					});
				}
			}
		})
	};

	return obj;
});

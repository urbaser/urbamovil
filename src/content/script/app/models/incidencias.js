/**
 * Created by 7daysofrain on 25/3/15.
 */
define(['jQuery',
	"app/models/incidencia",
	"app/models/localizacion",
	"kendo/kendo.core",
	"refr3sh/fn/throttle",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,Incidencia,Localizacion,kendo,throttle){
	/**
	 * Called by the DataSource to convert parameters/body into the appropriate format.
	 * @param data
	 * @param requestType One of "read", "create", "update", or "destroy"
	 * @returns {*}
	 */
	function parameterMap(data, requestType) {
		console.log("Request type: " + requestType);
		if (requestType === 'create' || requestType === 'update') {
			return JSON.stringify(data);
		}
		return data;
	}
	function error(){
		app.showMessage(T("models.incidencias.update-error"), "Error");
	}
	var filemap = new Object();
	var fileurimap = new Object();
	var closurefilemap = new Object();
	var closurefileurimap = new Object();
	var obj = {
		dataSource: new kendo.data.DataSource({
			offlineStorage: "incidencias-offline",
			data:[],
			serverPaging: true,
			pageSize: 50,
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/incidencia'},
					dataType: "json",
					type: "GET",
					data: function(){
						var o = {
						};
						if(obj.filter && obj.filter.IdFiltroUsuario){
							console.log("Pidiendo incidencias filtradas");
							o.f = obj.filter.IdFiltroUsuario;
						}
						return o;
					}
				},
				create: {
					url: function(){
						return window.app.config.baseUrl + '/incidencia'
					},
					dataType: "json",
					contentType: "application/json",
					type: "PUT",
					processData: false
				},
				update: {
					url: function(){
						return window.app.config.baseUrl + '/incidencia'
					},
					dataType: "json",
					contentType: "application/json",
					type: "POST",
					processData: false
				},
				destroy: function (e) {
					// locate item in original datasource and remove it
					sampleData.splice(getIndexById(e.data.ProductID), 1);
					// on success
					e.success();
					// on failure
					//e.error("XHR response", "status code", "error message");
				},
				parameterMap: parameterMap
			},
			error: function (e) {
				console.error(e);
				if(window.app.kendoApp && window.app.kendoApp.pane){
					window.app.kendoApp.pane.loader.hide();
				}
				if(e.xhr.status != 401 && e.xhr.status != 405){
					throttle(error,5000);
				}
			},
			batch: false,
			schema: {
				model: Incidencia,
				total: function(){
					return this.totalItems;
				},
				parse: function(data) {
					if(!data)return;
					function parseObject(it){
						if(it.Localizaciones && it.Localizaciones.length > 0){
							it.Localizaciones = it.Localizaciones.map(function(it){
								return new Localizacion(it);
							});
						}
					}
					if(data.data instanceof Array){
						this.totalItems = data.total;
						data = data.data;
						data.forEach(function(it){
							parseObject(it);
						})
						data = data.slice(0,100);
					}
					else if(data instanceof Object){
						parseObject(data);
					}

					return data;
				}
			},
			sync: function(e) {
				var data = this.data();
				for(var i = 0 ; i < data.length ; i++){
					if(filemap[data[i].uid]){
						var dactum = data[i];
						var fdata = new FormData();
						var file = filemap[dactum.uid];
						fdata.append("image"+i, file, file.name);
						delete filemap[dactum.uid];
						var url = window.app.config.baseUrl + "/incidenciaimagen?id=" + window.app.user.ticket + "&value=" + dactum.IdIncidencia;

						$.ajax({
							type: "POST",
							url: url,
							contentType: false,
							processData: false,
							data: fdata,
							success: function (results) {
								console.log(results);
								dactum.Imagen = results;
								dactum.CargandoImagen = 0;
							},
							complete: function(){
								window.app.kendoApp.pane.loader.hide();
							}
						});
						console.log("Enviado fichero adjunto");
					}
					if(fileurimap[data[i].uid]){
						var dactum = data[i];
						var uri = fileurimap[data[i].uid];
						delete fileurimap[dactum.uid];
						var url = window.app.config.baseUrl + "/incidenciaimagen?id=" + window.app.user.ticket + "&value=" + dactum.IdIncidencia;

						var options = new FileUploadOptions();
						options.fileKey = "image" + i;
						options.fileName = uri.substr(uri.lastIndexOf('/') + 1);
						options.mimeType = "image/jpeg";
						options.chunkedMode = true; //this is important to send both data and files
						options.headers = {
							"Ticket": window.app.user.ticket
						};
						var ft = new FileTransfer();

						var success =  function (results) {
							console.log(results);
							dactum.Imagen = results.response.replace(/"/g,"");
							console.log(dactum.Imagen);
							dactum.CargandoImagen = 0;
							window.app.kendoApp.pane.loader.hide();
						};
						var error = function(){
							window.app.kendoApp.pane.loader.hide();
						}
						ft.upload(uri, url, success, error, options);
					}
					if(closurefilemap[data[i].uid]){
						var dactum = data[i];
						var fdata = new FormData();
						var file = closurefilemap[dactum.uid];
						fdata.append("image"+i, file, file.name);
						delete closurefilemap[dactum.uid];
						$.ajax({
							type: "POST",
							url: window.app.config.baseUrl + "/incidenciaimagencierre?id=" + window.app.user.ticket + "&value=" + dactum.IdIncidencia,
							contentType: false,
							processData: false,
							data: fdata,
							success: function (results) {
								dactum.ImagenCierre = results;
								dactum.CargandoImagenCierre = 0;
								console.log(results);
							},
							complete: function(){
								window.app.kendoApp.pane.loader.hide();
							}
						});
						console.log("Enviado fichero adjunto de cierre");
					}
					if(closurefileurimap[data[i].uid]){
						var dactum = data[i];
						var uri = closurefileurimap[data[i].uid];
						delete closurefileurimap[dactum.uid];
						var url = window.app.config.baseUrl + "/incidenciaimagencierre?id=" + window.app.user.ticket + "&value=" + dactum.IdIncidencia;

						var options = new FileUploadOptions();
						options.fileKey = "image" + i;
						options.fileName = uri.substr(uri.lastIndexOf('/') + 1);
						options.mimeType = "image/jpeg";
						options.chunkedMode = true; //this is important to send both data and files
						options.headers = {
							"Ticket": window.app.user.ticket
						};

						var ft = new FileTransfer();

						var success =  function (results) {
							console.log(results);
							dactum.ImagenCierre = results.response.replace(/"/g,"");
							console.log(dactum.Imagen);
							dactum.CargandoImagenCierre = 0;
							window.app.kendoApp.pane.loader.hide();
						};
						var error = function(){
							window.app.kendoApp.pane.loader.hide();
						}
						ft.upload(uri, url, success, error, options);
					}
				}
				/*
				Apaño para cuando guardamos una incidencia sin internet (se guarda en localStorage).
				Al recuperar la conexion lo envia pero no lo borra del localStorage, entonces sigue
				aparenciendo en la lista, por eso lo borramos manualmente
				*/
				localStorage.removeItem("incidencias-offline");
			},
			requestStart: function(e) {
				if(window.app.kendoApp && window.app.kendoApp.pane)
					window.app.kendoApp.pane.loader.show();
				if(!window.app.user || !window.app.user.ticket){
					e.preventDefault();
				}
				if(e.type == "update" || e.type == "create"){

					var data = this.data();
					for(var i = 0 ; i < data.length ; i++){
						if(data[i].tempfile){
							filemap[data[i].uid] = data[i].tempfile;
							data[i].CargandoImagen = true;
							delete data[i].tempfile;
						}
						if(data[i].tempfileuri){
							fileurimap[data[i].uid] = data[i].tempfileuri;
							data[i].CargandoImagen = true;
							delete data[i].tempfileuri;
						}
						if(data[i].closuretempfile){
							closurefilemap[data[i].uid] = data[i].closuretempfile;
							data[i].CargandoImagenCierre = true;
							delete data[i].closuretempfile;
						}
						if(data[i].closuretempurifile){
							closurefileurimap[data[i].uid] = data[i].closuretempurifile;
							data[i].CargandoImagenCierre = true;
							delete data[i].closuretempurifile;
						}
					}
				}
			},
			requestEnd: function(e){
				if(window.app.kendoApp)
					window.app.kendoApp.pane.loader.hide();
			},
			change: function(e) {
				var data = this.data();
				console.log(data.length);
			}
		}),
		offline: function(){
			this.dataSource.online(false);
		},
		online: function(){
			this.dataSource.online(true);
			setTimeout(this.dataSource.sync,3000);
		},
		select: function(item){
			this.selected = item;
			console.log("Selected: ",this.selected);
		},
		setFilter: function(filter,dontFetch){
			this.filter = filter;
			if(filter){
				console.log("select filter " + filter.Nombre);
			}
			else{
				console.log("Crearing filters");
			}
			if(!dontFetch)
				this.dataSource.read();
		}
	};

	return obj;
});

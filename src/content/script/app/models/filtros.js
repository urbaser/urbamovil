/**
 * Created by 7daysofrain on 25/3/15.
 */
define(['jQuery',
	"app/models/filtro",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"], function(j,Filtro,kendo)
{
	/**
	 * Called by the DataSource to convert parameters/body into the appropriate format.
	 * @param data
	 * @param requestType One of "read", "create", "update", or "destroy"
	 * @returns {*}
	 */
	function parameterMap(data, requestType) {
		if (requestType === 'create' || requestType === 'update') {
			return JSON.stringify(data);
		}
		return data;
	}
	var filemap = new Object();
	var obj = {
		dataSource: new kendo.data.DataSource({ 
			data:[],
			transport: {
				read: {
					url: function(){
						return window.app.config.baseUrl + '/filtrousuario';
					},
					dataType: "json",
					type: "GET",
					data: function(){
						return {id:window.app.user.ticket};
					}
				},
				create: {
					url: function(){
						return window.app.config.baseUrl + '/filtrousuario';
					},
					dataType: "json",
					contentType: "application/json",
					type: "PUT",
					processData: false
				},
				update: {
					url: function(){
						return window.app.config.baseUrl + '/filtrousuario';
					},
					dataType: "json",
					contentType: "application/json",
					type: "POST",
					processData: false
				},
				destroy: {
					url: function(e){
						return window.app.config.baseUrl + '/filtrousuario&value=' + escape(e.IdFiltroUsuario);
					},
					dataType: "json",
					contentType: "application/json",
					type: "DELETE",
					processData: false
				},
				parameterMap: parameterMap
			},
			requestStart: function (e) {
				if(!window.app.user || !window.app.user.ticket){
					e.preventDefault();
				}
			},
			error: function (e) {
				console.error(e);
				window.app.kendoApp.pane.loader.hide();
				if(e.xhr.status != 401 && e.xhr.status != 405){
					app.showMessage(T("models.filtros.update-error"), "Error");
				}
			},
			batch: false,
			schema: {
				model: Filtro
			}
		}),
		select: function(item){
			this.selected = item;
			console.log("Selected: ",this.selected);
		}
	};

	return obj;
});

define(['jQuery',
	"app/models/masterTables",
	"app/models/planificacion",
	"kendo/kendo.core",
	"refr3sh/fn/throttle",
	"kendo/kendo.data",
	"kendo/kendo.binder"], function(j,tables,planificacion,kendo,throttle)
{
	/**
	 * Called by the DataSource to convert parameters/body into the appropriate format.
	 * @param data
	 * @param requestType One of "read", "create", "update", or "destroy"
	 * @returns {*}
	 */
	function parameterMap(data, requestType) {
		if (requestType === 'create' || requestType === 'update' || requestType === 'read') {
			return JSON.stringify(data);
		}
		return data;
	}
	function error(){
		app.showMessage(T("models.planning.update-error"), "Error");
	}
	var obj = {
		dataSource: new kendo.data.DataSource({ 
			//offlineStorage: "planificacion-offline",
			data:[],
			serverPaging:true,
			pageSize: 50,
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/PlanificacionFiltroPage'},
					dataType: "json",
					contentType: "application/json",
					type: "POST",
					data: function(){
						return obj.filter;
					}
				},
				create: {
					url: function(){
						return window.app.config.baseUrl + '/Planificacion'
					},
					dataType: "json",
					contentType: "application/json",
					type: "PUT",
					processData: false
				},
				update: {
					url: function(){
						return window.app.config.baseUrl + '/Planificacion'
					},
					dataType: "json",
					contentType: "application/json",
					type: "POST",
					processData: false
				},
				destroy: function (e) {
					// locate item in original datasource and remove it
					sampleData.splice(getIndexById(e.data.IdOT), 1);
					// on success
					e.success();
					// on failure
					//e.error("XHR response", "status code", "error message");
				},
				parameterMap: parameterMap
			},
			error: function (e) {
				console.error(e);
				if(window.app.kendoApp && window.app.kendoApp.pane){
					window.app.kendoApp.pane.loader.hide();
				}
				if(e.xhr.status != 401){
					throttle(error,5000);
				}
			},
			batch: false,
			schema: {
				model:  new planificacion.planificacion(),
				total: function(){
					return this.totalItems;
				},
				parse: function(data) {
					if(!data)return;
					this.totalItems = data.total;
					data = data.data;
					function parseObject(it){
						if(it.Localizaciones && it.Localizaciones.length > 0){
							it.Localizaciones = it.Localizaciones.map(function(it){
								return new Localizacion(it);
							});
						}
					}
					if(data instanceof Array){
						data.forEach(function(it){
							parseObject(it);
						})
					}
					else if(data instanceof Object){
						parseObject(data);
					}

					return data;
				}
			},

			requestStart: function(e) {
				if(window.app.kendoApp && window.app.kendoApp.pane)
					window.app.kendoApp.pane.loader.show();
				if(!window.app.user || !window.app.user.ticket){
					e.preventDefault();
				}
			},
			requestEnd: function(e){
				if(window.app.kendoApp)
					window.app.kendoApp.pane.loader.hide();
			}
		}),
		offline: function(){
			this.dataSource.online(false);
		},
		online: function(){
			this.dataSource.online(true);
			setTimeout(this.dataSource.sync,3000);
		},
		select: function(item){
			this.selected = item;
			console.log("Selected: ",this.selected);
		},
		setFilter: function(filter,dontFetch){
			this.filter = filter;
			if(filter){
				console.log("select filter " + filter.Nombre);
			}
			else{
				console.log("Crearing filters");
			}
			if(!dontFetch)
				this.dataSource.read();
		}
	};

	return obj;
});

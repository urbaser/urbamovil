define([
	"kendo/kendo.core",
	"kendo/kendo.data"
], function(kendo)
{
	var schema = {
		id: "IdFiltroUsuario",
		fields: {
			IdFiltroUsuario: { editable: false, nullable: true },
			Nombre: { nullable: false, validation: { required: true}},
			FiltrosCampo: []
		}
	};
	return kendo.data.Model.define(schema);
});

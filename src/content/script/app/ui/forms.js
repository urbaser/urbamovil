define([
	"jQuery"
], function() {
	var obj = {
		init: function(){
			$("input[data-icon]").each(function(i,el){
				$el = $(el);
				var path = "content/imgs/icons/" + $el.attr("data-icon")+".svg";
				var $ico = $("<img class='text-input-icon' src='" + path + "' preserveAspectRatio='xMidYMid meet'>");
				var $container = $("<div class='text-input-container' width='24' height='24'></div>");
				$el.wrap($container);
				$el.parent().append($ico);

			});
		}
	}

	return obj;
});

/**
 * Created by 7daysofrain on 21/5/15.
 */
define(['jQuery','modernizr','app/modernizr-tests'], function()
{
	var obj = {
		init: function(){
			$(".modal-frame:not(.custom-height)").height($(document).height());
		}
	};
	return obj;
});

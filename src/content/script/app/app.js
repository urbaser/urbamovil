define([
	"jQuery",
	"app/appbase",
	"app/models/user"
], function(j,AppBase,user) {
	'use strict'

	var VERSION = "Web";
	var DEBUG = false;
	var NAME = "Urbamovil";

	var MainApp = function() {
	}

	MainApp.prototype = new AppBase();
	MainApp.prototype.baseInit = MainApp.prototype.init;
	MainApp.prototype.constructor = MainApp;
	MainApp.prototype.init = function(){
		if(window.cordova && cordova.getAppVersion){
			cordova.getAppVersion.getVersionNumber().then(function (version) {
				this.conf(NAME,version,user,false,DEBUG);
				this.baseInit();
			}.bind(this));
		}else{
			this.conf(NAME,VERSION,user,false,DEBUG);
			this.baseInit();
		}
	}
	MainApp.prototype.onChangeRoute = function(e){
		console.log("Route Changed: ", e.url);
		this.url = e.url;
		if(!this.online){
			if(e.url == "selectByInventary" || e.url == "selectByMap"){
				console.log("Not available offline");
				$("#modal-noOffline").kendoMobileModalView("open");
				e.preventDefault();
			}
		}
		if(e.url != "login" && !window.app.user){
			this.kendoApp.navigate("#login");
		}
	}
	MainApp.prototype.doneInit = function(){
		if(this.debugMode){
			$(".show-version").text("Urbamovil versión " + this.version);
		}
		$("html").addClass("urbamovil-main");
	}
	MainApp.prototype.getStartPage = function(){
		var startPage = user.user ? "#home" : "#login";
		console.log("Using start page: " + startPage)
		return startPage;
	}
	MainApp.prototype.showMessage = function(message, title){
		if(this.version == VERSION){
			alert(message);
		}else{
			navigator.notification.alert(message, null, title, "Aceptar");
		}
	}
	MainApp.prototype.showConfirm = function(message, title){
		if(this.version == VERSION){
			if(confirm(message)){
				return true;
			}else{
				return false;
			}
		}else{
			navigator.notification.confirm(message, onReturn(buttonIndex), title, "Aceptar, Cancelar");
		}
	}

	function onReturn(buttonIndex){
		return buttonIndex;
	}

	var obj = new MainApp();

	return obj;
});

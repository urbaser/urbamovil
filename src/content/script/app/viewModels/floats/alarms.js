define([
	"jQuery",
	"app/models/flota",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,flota,kendo) {
	'use strict'
	var vm = {
		model: {},
		alarms: [],
		filter: {},
		tiposAlarmas: [],
		nivelesCriticidad: [],
		onViewInit: function(event){
			console.log("Init view: floats-alarms");
			this.view = event.view.element;
			this.temp = $("#alarm-list-template").html();
			$(document).on("click", "#floats-alarms .open-alarmMessage", this.showAlarm.bind(this));
			$(document).on("click", "#floats-alarms .search-btn", this.showFilters.bind(this));
			$(document).on("click", "#alarms-filter .ok-btn", this.applyFilter.bind(this));
			$(document).on("click", "#alarms-filter .reset-btn", this.resetFilter.bind(this));

			kendo.bind(event.view.header, this, kendo.mobile.ui);
			$(document).on("change", "#vehicles-filter .fecha-desde", this.setStartDate.bind(this));
			$(document).on("change", "#vehicles-filter .fecha-hasta", this.setEndDate.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: floats-alarms");
			var vehicleId = flota.vehiculos.selected.Codigo;
			this.set("model", flota.vehiculos.selected);
			flota.alarmas.dataSource.params = vehicleId;
			this.view.find(".subheader").html(T("views.floats.details.alarm")+": "+ this.model.vehiculo());
			flota.alarmas.dataSource.read();
			this.set("alarms", flota.alarmas.dataSource);
			this.view.find(".floats-list").empty();
			this.view.find(".floats-list").kendoRepeater({
				dataSource: this.alarms,
				template: this.temp
			});

			if (flota.alarmas.filter == null) {
				this.defaultDates();
			}

			this.set("tiposAlarmas", flota.tiposAlarmas.dataSource);
			this.tiposAlarmas.one("change", this.checkTiposAlarmas.bind(this));
			flota.tiposAlarmas.dataSource.read();

			this.set("nivelesCriticidad", flota.criticidad.dataSource);
			this.nivelesCriticidad.one("change", this.checkCriticidad.bind(this));
			flota.criticidad.dataSource.read();
		},
		onViewHide: function(event){
			console.log("Hide view: floats-alarms" );
		},
		defaultDates: function() {
			var currentDate = new Date();
			var toDate = new Date(currentDate.toString("yyyy-MM-ddTHH:mm:ssZ"));
			var fromDate = new Date(new Date(currentDate.setDate(currentDate.getDate()-1)).toString("yyyy-MM-ddTHH:mm:ssZ"));

			$("#alarms-filter .fecha-desde")[0].valueAsNumber = fromDate.getTime();
			$("#alarms-filter .fecha-hasta")[0].valueAsNumber = toDate.getTime();
			this.filter.FechaDesde = kendo.parseDate(fromDate, "dd/MM/yyyy HH:mm");
			this.filter.FechaHasta = kendo.parseDate(toDate, "dd/MM/yyyy HH:mm");
		},
		setStartDate: function(e) {
			var fechaHasta = new Date($("#alarms-filter .fecha-hasta").val());
			var fechaDesde = new Date($("#alarms-filter .fecha-desde").val());
			var time = fechaHasta - fechaDesde;
			var hours = time / 1000 / 60 / 60;
			if (hours > 24 || hours < 0) {
				fechaHasta = new Date(fechaDesde.getTime() + (24 * 60 * 60* 1000));
				$("#alarms-filter .fecha-hasta")[0].valueAsNumber = fechaHasta.getTime();
			}

		},
		setEndDate: function(e) {
			var fechaHasta = new Date($("#alarms-filter .fecha-hasta").val());
			var fechaDesde = new Date($("#alarms-filter .fecha-desde").val());
			var time = fechaHasta - fechaDesde;
			var hours = time / 1000 / 60 / 60;
			if (hours > 24 || hours < 0) {
				fechaDesde = new Date(fechaHasta.getTime() - (24 * 60 * 60* 1000));
				$("#alarms-filter .fecha-desde")[0].valueAsNumber = fechaDesde.getTime();
			}
		},
		showFilters: function(e) {
			e.preventDefault();
			$("#alarms-filter").toggleClass("active");
		},
		showAlarm: function(e) {
			e.preventDefault();
			var alarmId = $(e.currentTarget).attr("data-alarm-id");
			flota.alarmas.select(flota.alarmas.dataSource.getByUid(alarmId));
			$("#modal-alarmMessage").kendoMobileModalView("open");
		},
		checkTiposAlarmas: function(e) {
			if ($("#alarms-filter .select-tipoAlarma option:first").val() == 0) return;
			var nuevo = {
				TipoAlarma: "-Todos-",
				IdTipoAlarma: 0
			};
			this.tiposAlarmas.insert(0,nuevo);
		},
		checkCriticidad: function(e) {
			if ($("#alarms-filter .select-criticidad option:first").val() == "") return;
			var nuevo = {
				Nombre: "-Todos-",
				Codigo: ""
			};
			this.nivelesCriticidad.insert(0,nuevo);
		},
		applyFilter: function() {
			var filtro = {};

			var fechaDesde = $("#alarms-filter .fecha-desde").val();
			if (fechaDesde) {
				if (kendo.parseDate(fechaDesde, "yyyy-MM-ddTHH:mm:ss") != null) filtro.FechaDesde = kendo.parseDate(fechaDesde, "yyyy-MM-ddTHH:mm:ss");
				else if (kendo.parseDate(fechaDesde, "yyyy-MM-dd")) filtro.FechaDesde = kendo.parseDate(fechaDesde, "yyyy-MM-dd");
			}

			var fechaHasta = $("#alarms-filter .fecha-hasta").val();
			if (fechaHasta) {
				if (kendo.parseDate(fechaHasta, "yyyy-MM-ddTHH:mm:ss") != null) filtro.FechaHasta = kendo.parseDate(fechaHasta, "yyyy-MM-ddTHH:mm:ss");
				else if (kendo.parseDate(fechaHasta, "yyyy-MM-dd")) filtro.FechaHasta = kendo.parseDate(fechaHasta, "yyyy-MM-dd");
			}

			if (this.filter.TipoAlarma) filtro.IdTipoAlarma = (this.filter.TipoAlarma.IdTipoAlarma != 0) ? this.filter.TipoAlarma.IdTipoAlarma : "";
			if (this.filter.NivelCriticidad) filtro.Criticidad = (this.filter.NivelCriticidad.Codigo != "") ? this.filter.NivelCriticidad.Codigo : "";

			this.adjustMap = true;
			flota.alarmas.setFilter(filtro, false);
			$("#alarms-filter").removeClass("active");
		},
		resetFilter: function(e) {
			$("#alarms-filter .fecha-desde, #alarms-filter .fecha-hasta").val("");
			$("#alarms-filter .select-tipoAlarma").val($("#alarms-filter .select-tipoAlarma option:first").val());
			$("#alarms-filter .select-tipoAlarma").trigger("change");
			$("#alarms-filter .select-criticidad").val($("#alarms-filter .select-criticidad option:first").val());
			$("#alarms-filter .select-criticidad").trigger("change");
		}
	};
	return kendo.observable(vm);
});

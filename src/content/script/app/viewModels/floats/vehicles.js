define([
	"jQuery",
	"app/helpers/buttongroup",
	"app/models/flota",
	"kendo/kendo.core",
	"kendo/kendo.mobile.modalview",
	"kendo/kendo.data",
	"kendo/kendo.binder",

], function(j, buttonGroup, flota, kendo, k) {
	'use strict'
	var vm = {
		model: {},
		filter: {},
		categorias: [],
		adjustMap: true,
		bounds: {},
		vehiclesInterval: {},
		onViewInit: function(event){
			console.log("Init view: floats-vehicles");
			this.view = event.view.element;

			buttonGroup.apply(this.view,function(who) {
				setTimeout(function(){
					this.map.refresh();
					this.map.fitBounds(this.bounds);
				}.bind(this), 1000);
			}.bind(this));

			//buttonGroup.apply(this.view);
			this.temp = $("#vehicle-list-template").html();
			$(document).on("click", "#floats-vehicles .goToDetails", this.goToDetails.bind(this));
			$(document).on("click", "#floats-vehicles .search-btn", this.showFilters.bind(this));
			$(document).on("click", "#vehicles-filter .ok-btn", this.applyFilter.bind(this));
			$(document).on("click", "#vehicles-filter .reset-btn", this.resetFilter.bind(this));

			kendo.bind(event.view.header, this, kendo.mobile.ui);
			//$(document).on("change", "#vehicles-filter .fecha-desde", this.setStartDate.bind(this));
			//$(document).on("change", "#vehicles-filter .fecha-hasta", this.setEndDate.bind(this));
			buttonGroup.apply(this.view,function(who) {
				setTimeout(function(){
					this.map.refresh();
					this.map.fitBounds(this.bounds);
				}.bind(this), 1000);
			}.bind(this));

			this.mapInit();
		},
		onViewShow: function(event){
			console.log("Show view: floats-vehicles");

			this.set("categorias", flota.categorias.dataSource);
			this.categorias.one("change", this.checkCategories.bind(this));
			flota.categorias.dataSource.read();

			if (flota.vehiculos.filter == null) {
				//this.defaultDates();
			}

			this.vehiclesInterval = setInterval(function(){
				this.requestVehicles(true);
			}.bind(this), 30000);
			this.requestVehicles(false);
		},
		onViewHide: function(event){
			console.log("Hide view: floats-vehicles" );
			clearInterval(this.vehiclesInterval);
			if (this.categorias.at(0) && this.categorias.at(0).IdCategoriaRecurso == 0) this.categorias.remove(this.categorias.at(0));
		},
		defaultDates: function() {
			var currentDate = new Date();
			var toDate = new Date(currentDate.toString("yyyy-MM-ddTHH:mmZ"));
			var fromDate = new Date(new Date(currentDate.setDate(currentDate.getDate()-1)).toString("yyyy-MM-ddTHH:mmZ"));

			$("#vehicles-filter .fecha-desde")[0].valueAsNumber = fromDate.getTime();
			$("#vehicles-filter .fecha-hasta")[0].valueAsNumber = toDate.getTime();
			this.filter.FechaDesde = kendo.parseDate(fromDate, "dd/MM/yyyy HH:mm");
			this.filter.FechaHasta = kendo.parseDate(toDate, "dd/MM/yyyy HH:mm");
		},
		showFilters: function(e) {
			e.preventDefault();
			$("#vehicles-filter").toggleClass("active");
		},
		requestVehicles: function(silent) {
			this.view.find(".floats-details-list").empty();
			flota.vehiculos.dataSource.one("change", this.vehiclesChange.bind(this));
			flota.vehiculos.silent = silent;
			flota.vehiculos.dataSource.read();
		},
		vehiclesChange: function() {
			this.set("model", flota.vehiculos.dataSource);
			this.view.find(".floats-details-list").kendoRepeater({
				dataSource: this.model,
				template: this.temp
			});
			clearInterval(this.vehiclesInterval);
			if (this.model.data().length == 0) {
				app.showMessage("No se ha encontrado ningún vehículo con estos parámetros.", "Info");
			} else {
				this.vehiclesInterval = setInterval(function(){
					this.requestVehicles(true);
				}.bind(this), 30000);
			}
			this.mapShow();
		},
		goToDetails: function(event) {
			event.preventDefault();
			var vehicleId = $(event.currentTarget).attr("data-vehicle-cod");
			flota.vehiculos.select(flota.vehiculos.dataSource.get(vehicleId));

			window.app.kendoApp.navigate("#floats-vehicleDetails");
		},
		mapInit: function() {
			$('#vehicle-list-map')
				.height($(document).height() -176)
				.width($(document).width());

			this.map = new GMaps({
				div: "#vehicle-list-map",
				zoom: 18,
				width: $(document).width(),
				height: $(window).height() -280,
				zoomControl: false,
				mapTypeControl: false,
				streetViewControl: false,
				panControl: false,
				lat: 40.4378698,
				lng: -3.8196207,
				draggable: true,
			});
			this.infowindow = null;		
		},
		mapShow: function() {
			this.bounds = new google.maps.LatLngBounds();
			this.map.removeMarkers();

			for (var i = 0; i < this.model.data().length; i++) {
				var vehicle = this.model.data()[i];

				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(vehicle["Latitud"], vehicle["Longitud"]),
					icon: {
						size: new google.maps.Size(25, 25),
						scaledSize: new google.maps.Size(25, 25),
						url: 'content/imgs/flotas-camion.svg'
					},
				});
				this.map.addMarker(marker);
				this.bounds.extend(marker.position);

				google.maps.event.addListener(marker, 'click', this.mapClickListener(marker, i).bind(this));
			}

			if (this.adjustMap && this.model.data().length > 0) {
				this.map.fitBounds(this.bounds);
				this.adjustMap = false;
			}

			setTimeout(this.map.refresh.bind(this.map),1000);
		},
		mapClickListener: function(marker, i) {

			return function() {
				var vehicle = this.model.data()[i];
				if (this.infowindow) {
					this.infowindow.close();
				}
				this.infowindow = new google.maps.InfoWindow();
				this.infowindow.setContent('<p style="text-align: center">' + vehicle.vehiculo() + '</p><p style="text-align: center"><a href="" class="goToDetails" data-vehicle-cod="'+vehicle.Codigo+'">'+T("views.floats.details.viewDetails")+'</a></p>');
				this.infowindow.open(this.map, marker);
			}
		},
		checkCategories: function(e) {
			if (this.view.find(".select-categories option:first").val() == 0) return;
			var nuevo = {
				CategoriaRecurso: "-Todos-",
				IdCategoriaRecurso: 0,
				IdTipoRecurso: 0
			};
			this.categorias.insert(0,nuevo);
		},
		applyFilter: function() {
			var filtro = {};

			/*var fechaDesde = $("#vehicles-filter .fecha-desde").val();
			if (fechaDesde) {
				if (kendo.parseDate(fechaDesde, "yyyy-MM-ddTHH:mm:ss") != null) filtro.FechaDesde = kendo.parseDate(fechaDesde, "yyyy-MM-ddTHH:mm:ss");
				else if (kendo.parseDate(fechaDesde, "yyyy-MM-dd")) filtro.FechaDesde = kendo.parseDate(fechaDesde, "yyyy-MM-dd");
			}

			var fechaHasta = $("#vehicles-filter .fecha-hasta").val();
			if (fechaHasta) {
				if (kendo.parseDate(fechaHasta, "yyyy-MM-ddTHH:mm:ss") != null) filtro.FechaHasta = kendo.parseDate(fechaHasta, "yyyy-MM-ddTHH:mm:ss");
				else if (kendo.parseDate(fechaHasta, "yyyy-MM-dd")) filtro.FechaHasta = kendo.parseDate(fechaHasta, "yyyy-MM-dd");
			}*/
			filtro.FechaDesde = null;
			filtro.FechaHasta = null;

			if (this.filter.Categoria) filtro.Categoria = (this.filter.Categoria.IdCategoriaRecurso != 0) ? this.filter.Categoria.CategoriaRecurso : "";

			this.adjustMap = true;
			flota.vehiculos.setFilter(filtro, true);
			this.requestVehicles(false);
			$("#vehicles-filter").removeClass("active");
		},
		resetFilter: function(e) {
			$("#vehicles-filter .fecha-desde, #vehicles-filter .fecha-hasta").val("");
			$("#vehicles-filter .select-categories").val($("#vehicles-filter .select-categories option:first").val());
			$("#vehicles-filter .select-categories").trigger("change");
		}
	};
	return kendo.observable(vm);
});

define([
	"jQuery",
	"app/models/flota",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",

], function(j, flota, kendo) {
	'use strict'
	var vm = {
		vehicle: {},
		model: {},
		bounds: {},
		routes: [],
		adjustMap: true,
		onViewInit: function(event){
			console.log("Init view: floats-signalDetails");
			this.view = event.view.element;
			this.mapInit();
		},
		onViewShow: function(event){
			console.log("Show view: floats-signalDetails");
			//this.forDebug();
			this.set("model", flota.signals.selected);
			this.set("adjustMap", true);
			this.set("vehicle", flota.vehiculos.selected);
			this.fetchRoute();
			this.view.find(".subheader").html(this.model.TypeSignal + " / " + this.model.Descripcion + " / " + this.vehicle.vehiculo());
			//this.mapShow();
		},
		onViewHide: function(event){
			console.log("Hide view: floats-signalDetails" );
		},
		fetchRoute: function() {
			flota.rutas.dataSource.one("change", this.mapShow.bind(this));
			flota.rutas.dataSource.params =  flota.vehiculos.selected.Codigo;
			var filtro = {};
			if (flota.signals.filter && flota.signals.filter.FechaDesde) filtro.FechaDesde = flota.signals.filter.FechaDesde;
			if (flota.signals.filter && flota.signals.filter.FechaHasta) filtro.FechaHasta = flota.signals.filter.FechaHasta;
			flota.rutas.setFilter(filtro, false);
			this.set("routes", flota.rutas.dataSource);
		},
		forDebug: function() {
			flota.vehiculos.dataSource.read();
			flota.vehiculos.select(flota.vehiculos.dataSource.get("6260HGJ"));
			flota.signals.dataSource.params = flota.vehiculos.selected.Codigo;
			flota.signals.dataSource.read();
			flota.signals.select(flota.signals.dataSource.at(0));
		},
		mapInit: function() {
			$('#signalPosition-list-map')
				.height($(document).height() - 95)
				.width($(document).width());

			this.map = new GMaps({
				div: "#signalPosition-list-map",
				zoom: 18,
				width: $(document).width(),
				height: $(window).height() - 95,
				zoomControl: false, //{style:google.maps.ZoomControlStyle.LARGE},
				mapTypeControl: false,
				streetViewControl: false,
				panControl: false,
				lat: 40.4378698,
				lng: -3.8196207,
				draggable: true,
			});
		},
		/*mapShow: function() {
			this.bounds = new google.maps.LatLngBounds();
			//this.map.removeMarkers();


			var directionsService = new google.maps.DirectionsService;
			var directionsDisplay = new google.maps.DirectionsRenderer({
				suppressMarkers: true,
				suppressInfoWindows: true,
			});
			directionsDisplay.setMap(this.map.map);

			var waypts = [];

			for (var i = 0; i < Math.min(this.model.Posiciones.length -1, 8); i++) {
				var vehicle = this.model.Posiciones[i];

				waypts.push({
					location: new google.maps.LatLng(vehicle["Latitud"], vehicle["Longitud"])
				});

				this.bounds.extend(new google.maps.LatLng(vehicle["Latitud"], vehicle["Longitud"]));
			}

			var origin = new google.maps.LatLng(this.model.Posiciones[0]["Latitud"], this.model.Posiciones[0]["Longitud"]);
			var destination = new google.maps.LatLng(this.model.Posiciones[this.model.Posiciones.length - 1]["Latitud"], this.model.Posiciones[this.model.Posiciones.length - 1]["Longitud"]);
			directionsService.route({
				origin: origin,
				destination: destination,
				waypoints: waypts,
				optimizeWaypoints: true,
				travelMode: google.maps.TravelMode.DRIVING
			}, function(response, status) {
				if (status === google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
					var route = response.routes[0];
				} else {
					window.alert('Directions request failed due to ' + status);
				}
			});


			if (this.adjustMap) {
				this.map.fitBounds(this.bounds);
				this.adjustMap = false;
			}

			setTimeout(this.map.refresh.bind(this.map),1000);
		},*/
		mapShow: function() {
			this.bounds = new google.maps.LatLngBounds();
			this.map.removePolylines();
			this.map.removeMarkers();

			this.drawRoute();
			if (this.model.Posiciones.length == 0) {
				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(this.model.Latitud, this.model.Longitud),
					icon: {
						size: new google.maps.Size(20, 30),
						scaledSize: new google.maps.Size(20, 30),
						url: 'content/imgs/map-marker.svg'
					},
				});
				this.map.addMarker(marker);
				this.bounds.extend(marker.position);
			}
			else {
				var waypts = [];

				for (var i = 0; i < this.model.Posiciones.length -1; i++) {
					var vehicle = this.model.Posiciones[i];

					waypts.push([vehicle["Latitud"], vehicle["Longitud"]]);

					this.bounds.extend(new google.maps.LatLng(vehicle["Latitud"], vehicle["Longitud"]));
				}

				this.map.drawPolyline({
					path: waypts,
					strokeColor: '#159948',
					strokeOpacity: 1,
					strokeWeight: 4
				});
			}
			if (this.adjustMap) {
				this.map.fitBounds(this.bounds);
				this.adjustMap = false;
			}

			setTimeout(this.map.refresh.bind(this.map),1000);
		},
		drawRoute: function(){
			var waypts = [];

			for (var i = 0; i < this.routes.data().length -1; i++) {
				var vehicle = this.routes.data()[i];

				waypts.push([vehicle["Latitud"], vehicle["Longitud"]]);

				this.bounds.extend(new google.maps.LatLng(vehicle["Latitud"], vehicle["Longitud"]));
			}

			this.map.drawPolyline({
				path: waypts,
				strokeColor: 'red',
				strokeOpacity: 0.7,
				strokeWeight: 4
			});
		}
	};
	return kendo.observable(vm);
});

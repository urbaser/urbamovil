define([
	"jQuery",
	"app/models/flota",
	"app/helpers/buttongroup",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo/kendo.datetimepicker",

], function(j, flota, buttonGroup, kendo) {
	'use strict'
	var vm = {
		model: [],
		bounds: [],
		filter: {},
		adjustMap: true,
		onViewInit: function(event){
			console.log("Init view: floats-routes");
			this.view = event.view.element;
			buttonGroup.apply(this.view);
			//this.tempDetails = kendo.template($("#route-details-template").html());
			this.tempList = $("#route-list-template").html();
			$(document).on("click", "#floats-routes .search-btn", this.showFilters.bind(this));
			$(document).on("click", "#routes-filter .ok-btn", this.applyFilter.bind(this));
			$(document).on("click", "#routes-filter .reset-btn", this.resetFilter.bind(this));

			kendo.bind(event.view.header, this, kendo.mobile.ui);
			$(document).on("change", "#routes-filter .fecha-desde", this.setStartDate.bind(this));
			$(document).on("change", "#routes-filter .fecha-hasta", this.setEndDate.bind(this));
			buttonGroup.apply(this.view,function(who) {
				setTimeout(function(){
					this.map.refresh();
					this.map.fitBounds(this.bounds);
				}.bind(this), 1000);
			}.bind(this));
			this.mapInit();
			/*this.view.find(".fecha-desde").kendoDateTimePicker({
				value:new Date()
			});*/
		},
		onViewShow: function(event){
			console.log("Show view: floats-routes");
			this.view.find(".subheader").html("Ruta: " + flota.vehiculos.selected.vehiculo());
			var vehicleId = flota.vehiculos.selected.Codigo;
			this.defaultDates();
			flota.rutas.dataSource.params =  vehicleId;
			this.applyFilter();
		},
		defaultDates: function() {
			var currentDate = new Date();

			var toDate = new Date(currentDate.toString("yyyy-MM-ddTHH:mmZ"));
			var fromDate = new Date(new Date(currentDate.setDate(currentDate.getDate()-1)).toString("yyyy-MM-ddTHH:mmZ"));

			
			$("#routes-filter .fecha-hasta")[0].valueAsNumber = toDate.getTime();
			$("#routes-filter .fecha-desde")[0].valueAsNumber = fromDate.getTime();
			
			this.filter.FechaDesde = kendo.parseDate($("#routes-filter .fecha-desde").val(), "dd/MM/yyyy HH:mm");
			this.filter.FechaHasta = kendo.parseDate($("#routes-filter .fecha-hasta").val(), "dd/MM/yyyy HH:mm");
		},
		setStartDate: function(e) {
			var fechaHasta = new Date($("#routes-filter .fecha-hasta").val());
			var fechaDesde = new Date($("#routes-filter .fecha-desde").val());
			var time = fechaHasta - fechaDesde;
			var hours = time / 1000 / 60 / 60;
			if (hours > 24 || hours < 0) {
				fechaHasta = new Date(fechaDesde.getTime() + (24 * 60 * 60* 1000));
				$("#routes-filter .fecha-hasta")[0].valueAsNumber = fechaHasta.getTime();
			}

		},
		setEndDate: function(e) {
			var fechaHasta = new Date($("#routes-filter .fecha-hasta").val());
			var fechaDesde = new Date($("#routes-filter .fecha-desde").val());
			var time = fechaHasta - fechaDesde;
			var hours = time / 1000 / 60 / 60;
			if (hours > 24 || hours < 0) {
				fechaDesde = new Date(fechaHasta.getTime() - (24 * 60 * 60* 1000));
				$("#routes-filter .fecha-desde")[0].valueAsNumber = fechaDesde.getTime();
			}
		},
		onViewHide: function(event){
			console.log("Hide view: floats-routes" );
		},
		showFilters: function(e) {
			e.preventDefault();
			//$("#modals-vehiclesFilter").data("kendoModalView").open();
			$("#routes-filter").toggleClass("active");
		},
		mapInit: function() {
			$('#vehicle-routes-map')
				.height($(document).height() -176)
				.width($(document).width());

			this.map = new GMaps({
				div: "#vehicle-routes-map",
				zoom: 18,
				width: $(document).width(),
				height: $(window).height() -280,
				zoomControl: false, //{style:google.maps.ZoomControlStyle.LARGE},
				mapTypeControl: false,
				streetViewControl: false,
				panControl: false,
				lat: 40.4378698,
				lng: -3.8196207,
				draggable: true,
			});
		},
		mapShow: function() {
			this.bounds = new google.maps.LatLngBounds();
			this.map.removePolylines();

			var waypts = [];

			for (var i = 0; i < this.model.dataSource.data().length -1; i++) {
				var vehicle = this.model.dataSource.data()[i];

				waypts.push([vehicle["Latitud"], vehicle["Longitud"]]);

				this.bounds.extend(new google.maps.LatLng(vehicle["Latitud"], vehicle["Longitud"]));
			}

			this.map.drawPolyline({
				path: waypts,
				strokeColor: '#232156',
				strokeOpacity: 0.7,
				strokeWeight: 4
			});


			if (this.adjustMap && this.model.dataSource.data().length > 0) {
				this.map.fitBounds(this.bounds);
				this.adjustMap = false;
			}

			setTimeout(this.map.refresh.bind(this.map),1000);
		},
		checkRoutes: function(event){
			this.set("model", flota.rutas);
			//this.view.find(".route-details-container").empty().append($(this.tempDetails(this.model)).i18n());
			
			/*this.view.find(".routes-list").empty();
			this.view.find(".routes-list").kendoRepeater({
				dataSource: this.model.dataSource,
				template: this.tempList
			});*/
			this.mapShow();
		},
		applyFilter: function() {
			var filtro = {};

			var fechaDesde = $("#routes-filter .fecha-desde").val();
			if (fechaDesde) {
				if (kendo.parseDate(fechaDesde, "yyyy-MM-dd HH:mm") != null) filtro.FechaDesde = kendo.parseDate(fechaDesde, "yyyy-MM-dd HH:mm");
				else if (kendo.parseDate(fechaDesde, "yyyy-MM-dd")) filtro.FechaDesde = kendo.parseDate(fechaDesde, "yyyy-MM-dd");
			}

			var fechaHasta = $("#routes-filter .fecha-hasta").val();
			if (fechaHasta) {
				if (kendo.parseDate(fechaHasta, "yyyy-MM-dd HH:mm") != null) filtro.FechaHasta = kendo.parseDate(fechaHasta, "yyyy-MM-dd HH:mm");
				else if (kendo.parseDate(fechaHasta, "yyyy-MM-dd")) filtro.FechaHasta = kendo.parseDate(fechaHasta, "yyyy-MM-dd");
			}

			this.adjustMap = true;
			flota.rutas.dataSource.one("change", this.checkRoutes.bind(this));
			flota.rutas.setFilter(filtro, false);
			$("#routes-filter").removeClass("active");
		},
		resetFilter: function(e) {
			this.defaultDates();
		}
	};
	return kendo.observable(vm);
});

define([
	"jQuery",
	"app/models/flota",
	"app/helpers/buttongroup",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,flota,buttonGroup,kendo) {
	'use strict'
	var vm = {
		model: {},
		signals: flota.signals.dataSource,
		adjustMap: true,
		typeSignals: [],
		bounds: {},
		filter: {},
		routes: [],
		onViewInit: function(event){
			console.log("Init view: floats-signals");
			this.view = event.view.element;
			this.temp = $("#signal-list-template").html();
			$(document).on("click", "#floats-signals .goToDetails", this.goToDetails.bind(this));
			$(document).on("click", "#floats-signals .search-btn", this.showFilters.bind(this));
			$(document).on("click", "#signals-filter .ok-btn", this.applyFilter.bind(this));
			$(document).on("click", "#signals-filter .reset-btn", this.resetFilter.bind(this));

			kendo.bind(event.view.header, this, kendo.mobile.ui);
			$(document).on("change", "#signals-filter .fecha-desde", this.setStartDate.bind(this));
			$(document).on("change", "#signals-filter .fecha-hasta", this.setEndDate.bind(this));
			buttonGroup.apply(this.view,function(who) {
				setTimeout(function(){
					this.map.refresh();
					this.map.fitBounds(this.bounds);
				}.bind(this), 1000);
			}.bind(this));

			this.mapInit();
		},
		onViewShow: function(event){
			console.log("Show view: floats-signals");
			this.set("model", flota.vehiculos.selected);
			this.set("adjustMap", true);
			this.view.find(".subheader").html(T("views.floats.details.signal")+": " + this.model.vehiculo());
			var vehicleId = flota.vehiculos.selected.Codigo;

			if (flota.signals.filter == null) {
				this.defaultDates();
			}

			flota.signals.dataSource.params = vehicleId;
			this.applyFilter();

			this.set("typeSignals", flota.tiposSignals.dataSource);
			this.typeSignals.one("change", this.checkTiposSignals.bind(this));
			flota.tiposSignals.dataSource.read();


		},
		onViewHide: function(event){
			console.log("Hide view: floats-signals" );
		},
		fetchRoute: function() {
			flota.rutas.dataSource.one("change", this.mapShow.bind(this));
			flota.rutas.dataSource.params =  flota.vehiculos.selected.Codigo;
			var filtro = {};
			if (flota.signals.filter && flota.signals.filter.FechaDesde) filtro.FechaDesde = flota.signals.filter.FechaDesde;
			if (flota.signals.filter && flota.signals.filter.FechaHasta) filtro.FechaHasta = flota.signals.filter.FechaHasta;
			flota.rutas.setFilter(filtro, false);
			this.set("routes", flota.rutas.dataSource);
		},
		defaultDates: function() {
			var currentDate = new Date();
			var toDate = new Date(currentDate.toString("yyyy-MM-ddTHH:mm:ssZ"));
			var fromDate = new Date(new Date(currentDate.setDate(currentDate.getDate()-1)).toString("yyyy-MM-ddTHH:mm:ssZ"));

			$("#signals-filter .fecha-desde")[0].valueAsNumber = fromDate.getTime();
			$("#signals-filter .fecha-hasta")[0].valueAsNumber = toDate.getTime();
			this.filter.FechaDesde = kendo.parseDate(fromDate, "dd/MM/yyyy HH:mm:ss");
			this.filter.FechaHasta = kendo.parseDate(toDate, "dd/MM/yyyy HH:mm:ss");
		},
		setStartDate: function(e) {
			var fechaHasta = new Date($("#signals-filter .fecha-hasta").val());
			var fechaDesde = new Date($("#signals-filter .fecha-desde").val());
			var time = fechaHasta - fechaDesde;
			var hours = time / 1000 / 60 / 60;
			if (hours > 24 || hours < 0) {
				console.log("reset. hours: " + hours);
				fechaHasta = new Date(fechaDesde.getTime() + (24 * 60 * 60* 1000));
				$("#signals-filter .fecha-hasta")[0].valueAsNumber = fechaHasta.getTime();
			}

		},
		setEndDate: function(e) {
			var fechaHasta = new Date($("#signals-filter .fecha-hasta").val());
			var fechaDesde = new Date($("#signals-filter .fecha-desde").val());
			var time = fechaHasta - fechaDesde;
			var hours = time / 1000 / 60 / 60;
			if (hours > 24 || hours < 0) {
				console.log("reset. hours: " + hours);
				fechaDesde = new Date(fechaHasta.getTime() - (24 * 60 * 60* 1000));
				$("#signals-filter .fecha-desde")[0].valueAsNumber = fechaDesde.getTime();
			}
		},
		showFilters: function(e) {
			e.preventDefault();
			//$("#modals-vehiclesFilter").data("kendoModalView").open();
			$("#signals-filter").toggleClass("active");
		},
		checkTiposSignals: function(e) {
			if ($("#signals-filter .select-typeSignals option:first").val() == 0) return;
			var nuevo = {
				TypeSignal: "-Todos-",
				IdTypeSignal: 0
			};
			this.typeSignals.insert(0,nuevo);
		},
		signalsChange: function(e) {
			this.view.find(".floats-details-list").empty();
			this.view.find(".floats-details-list").kendoRepeater({
				dataSource: this.signals,
				template: this.temp
			});
			this.fetchRoute();
		},
		mapInit: function() {
			$('#signal-list-map')
				.height($(document).height() -176)
				.width($(document).width());

			this.map = new GMaps({
				div: "#signal-list-map",
				zoom: 18,
				width: $(document).width(),
				height: $(window).height() -280,
				zoomControl: false, //{style:google.maps.ZoomControlStyle.LARGE},
				mapTypeControl: false,
				streetViewControl: false,
				panControl: false,
				lat: 40.4378698,
				lng: -3.8196207,
				draggable: true,
			});
			this.infowindow = new google.maps.InfoWindow();
		},
		mapShow: function() {
			this.bounds = new google.maps.LatLngBounds();
			this.map.removeMarkers();
			this.map.removePolylines();

			this.drawRoute();

			for (var i = 0; i < this.signals.data().length; i++) {
				var signal = this.signals.data()[i];

				this.drawSignal(signal);

				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(signal["Latitud"], signal["Longitud"]),
					icon: {
						size: new google.maps.Size(20, 30),
						scaledSize: new google.maps.Size(20, 30),
						url: 'content/imgs/map-marker.svg'
					},
				});
				this.map.addMarker(marker);
				this.bounds.extend(marker.position);

				google.maps.event.addListener(marker, 'click', this.mapClickListener(marker, i).bind(this));
			}

			if (this.adjustMap && this.signals.data().length > 0) {
				this.map.fitBounds(this.bounds);
				this.adjustMap = false;
			}
			if (this.signals.data().length == 0) {
				app.showMessage("Este vehiculo no tiene ninguna señal.", "Info");
				if (flota.signals.filter == undefined || flota.signals.filter == {} || flota.signals.filter == null) {
					window.app.kendoApp.navigate("#:back");
					return;
				}
			}
			setTimeout(this.map.refresh.bind(this.map),1000);
		},
		mapClickListener: function(marker, i) {

			return function() {
				var signal = this.signals.data()[i];
				if (this.infowindow) {
					this.infowindow.close();
				}
				this.infowindow = new google.maps.InfoWindow();
				this.infowindow.setContent('<p style="text-align: center">' + signal.TypeSignal + ' / ' + signal.Descripcion + '</p><p style="text-align: center"><a href="" class="goToDetails" data-signal-id="'+signal.uid+'">'+T("views.floats.details.viewDetails")+'</a></p>');
				this.infowindow.open(this.map, marker);
			}
		},
		drawRoute: function(){
			var waypts = [];

			for (var i = 0; i < this.routes.data().length -1; i++) {
				var vehicle = this.routes.data()[i];

				waypts.push([vehicle["Latitud"], vehicle["Longitud"]]);

				this.bounds.extend(new google.maps.LatLng(vehicle["Latitud"], vehicle["Longitud"]));
			}

			this.map.drawPolyline({
				path: waypts,
				strokeColor: 'red',
				strokeOpacity: 0.7,
				strokeWeight: 4
			});
		},
		drawSignal: function(signal){

			if (signal.Posiciones.length > 0) {
				var waypts = [];

				for (var i = 0; i < signal.Posiciones.length -1; i++) {
					var vehicle = signal.Posiciones[i];

					waypts.push([vehicle["Latitud"], vehicle["Longitud"]]);

					this.bounds.extend(new google.maps.LatLng(vehicle["Latitud"], vehicle["Longitud"]));
				}

				this.map.drawPolyline({
					path: waypts,
					strokeColor: '#159948',
					strokeOpacity: 1,
					strokeWeight: 4
				});
			}
		},
		goToDetails: function(event) {
			event.preventDefault();
			var signalId = $(event.currentTarget).attr("data-signal-id");
			flota.signals.select(flota.signals.dataSource.getByUid(signalId));

			window.app.kendoApp.navigate("#floats-signalDetails");
		},
		applyFilter: function() {
			var filtro = {};

			var fechaDesde = $("#signals-filter .fecha-desde").val();
			if (fechaDesde) {
				if (kendo.parseDate(fechaDesde, "yyyy-MM-ddTHH:mm:ss") != null) filtro.FechaDesde = kendo.parseDate(fechaDesde, "yyyy-MM-ddTHH:mm:ss");
				else if (kendo.parseDate(fechaDesde, "yyyy-MM-dd")) filtro.FechaDesde = kendo.parseDate(fechaDesde, "yyyy-MM-dd");
			}

			var fechaHasta = $("#signals-filter .fecha-hasta").val();
			if (fechaHasta) {
				if (kendo.parseDate(fechaHasta, "yyyy-MM-ddTHH:mm:ss") != null) filtro.FechaHasta = kendo.parseDate(fechaHasta, "yyyy-MM-ddTHH:mm:ss");
				else if (kendo.parseDate(fechaHasta, "yyyy-MM-dd")) filtro.FechaHasta = kendo.parseDate(fechaHasta, "yyyy-MM-dd");
			}

			if (this.filter.TypeSignal) filtro.IdTypeSignal = (this.filter.TypeSignal.IdTypeSignal != 0) ? this.filter.TypeSignal.IdTypeSignal : "";

			this.adjustMap = true;
			flota.signals.dataSource.one("change", this.signalsChange.bind(this));
			flota.signals.setFilter(filtro, false);
			$("#signals-filter").removeClass("active");
		},
		resetFilter: function(e) {
			this.defaultDates();
			$("#signals-filter .select-typeSignals").val($("#floats-signals .select-typeSignals option:first").val());
			$("#signals-filter .select-typeSignals").trigger("change");
		}
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencias",
	"app/models/masterTables",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.mobile.modalview",
	"kendo.repeater"
], function(j,incidencias,tables,kendo) {
	'use strict'
	var vm = {
		tables: tables,
		onViewInit: function(e){
			//console.log(e.view.id + " init");
		},
		onViewShow: function(e){
			$("#stageChangeSelector").kendoRepeater({
				dataSource:tables.estados.data().filter(function(it){return it.Visible}),
				template: $("#state-change-template").html()
			});
			$("#stageChangeSelector [data-id=" + incidencias.selected.IdEstado +"] input").attr("checked","");
		},
		onClose: function(){
			$("#modal-changeState").kendoMobileModalView("close");
		},
		onSubmit: function(){
			incidencias.selected.set("IdEstado",$("#stageChangeSelector :checked").val());
			incidencias.dataSource.sync();
			$("#modal-changeState").kendoMobileModalView("close");
			if(incidencias.selected.get("IdEstado") == 4){
				window.app.kendoApp.navigate("#closure");
			}
		}
	};
	return kendo.observable(vm);
});

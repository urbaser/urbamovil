/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.mobile.modalview",
], function(j,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(e){
			//console.log(e.view.id + " init");
		},
		onViewShow: function(e){
		},
		onClose: function(){
			$("#modal-offline").kendoMobileModalView("close");
		}
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/services/BTSerialBridge",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.mobile.modalview",
	"kendo.repeater"
], function(j,bt,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(e){
			//console.log(e.view.id + " init");
		},
		onViewShow: function(e){
			$("#deviceChangeSelector").kendoRepeater({
				dataSource: bt.devices,
				template: $("#select-device-template").html()
			});
			$("#stageChangeSelector [data-id='" + bt.selectedDevice +"'] input").attr("checked","");
		},
		onClose: function(){
			$("#modal-selectBTDevice").kendoMobileModalView("close");
		},
		onSubmit: function(){
			bt.selectedDevice = $("#deviceChangeSelector :checked").val();
			$("#modal-selectBTDevice").kendoMobileModalView("close");

		}
	};
	return kendo.observable(vm);
});

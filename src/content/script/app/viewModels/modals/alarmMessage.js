define([
	"jQuery",
	"app/models/flota",
	"kendo/kendo.core",
	"kendo/kendo.mobile.modalview",
], function(j,flota,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(e){
			//console.log(e.view.id + " init");
		},
		onViewShow: function(e){
			var alarmText = flota.alarmas.selected.Observacion;
			$("#modal-alarmMessage .alarm-message").html(alarmText);
		},
		onClose: function(){
			$("#modal-alarmMessage").kendoMobileModalView("close");
			$("#modal-alarmMessage .alarm-message").html("");
		}
	};
	return kendo.observable(vm);
});

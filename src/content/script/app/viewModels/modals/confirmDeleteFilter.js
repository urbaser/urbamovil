/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.mobile.modalview",
	"app/models/filtros"
], function(j,kendo,k,filtros) {
	'use strict'
	var vm = {
		onViewInit: function(e){
			//console.log(e.view.id + " init");
		},
		onViewShow: function(e){
			if(!filtros.selected){
				$("#modal-confirmDeleteFilter").kendoMobileModalView("close");
				return;
			}
			$(e.sender.element).find(".filter-to-delete").text(filtros.selected.Nombre);
		},
		onCloseYes: function(){
			filtros.dataSource.remove(filtros.selected);
			filtros.dataSource.sync();
			filtros.select(null);
			$("#modal-confirmDeleteFilter").kendoMobileModalView("close");
		},
		onCloseNo: function(){
			$("#modal-confirmDeleteFilter").kendoMobileModalView("close");
		}
	};
	return kendo.observable(vm);
});

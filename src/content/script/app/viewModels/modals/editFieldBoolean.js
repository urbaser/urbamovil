define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,kendo) {
	'use strict'
	var vm = {
		value: null,
		onViewInit: function(event){
			console.log("Init view: modals-editFieldBoolean");
			this.view = $(event.sender.element);
		},
		onViewShow: function(event){
			console.log("Show view: modals-editFieldBoolean");
		},
		onViewHide: function(event){
			console.log("Hide view: modals-editFieldBoolean");
		},
		onCloseCancel: function(){
			$("#modal-editFieldBoolean").kendoMobileModalView("close");
		},
		onCloseSave: function(){
			$("#modal-editFieldBoolean").kendoMobileModalView("close");
			if(this.change){
				this.change(this.value);
			}
		}
	};
	return kendo.observable(vm);
});

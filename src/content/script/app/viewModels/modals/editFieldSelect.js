define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,kendo) {
	'use strict'
	var vm = {
		source: [],
		value:"",
		onViewInit: function(event){
			console.log("Init view: modals-editFieldSelect");
			this.view = $(event.sender.element);
		},
		onViewShow: function(event){
			console.log("Show view: modals-editFieldSelect");
			setTimeout(this.refresh.bind(this),100);
		},
		refresh: function(){
			if(!this.source.data) return;
			var s = -1;
			this.source.data().forEach(function(it,i){
				if(it.Text == this.value){
					s = it.Id;
				}
			}.bind(this));
			this.view.find("select").val(s);
		},
		onViewHide: function(event){
			console.log("Hide view: modals-editFieldSelect" );
		},
		onCloseCancel: function(){
			$("#modal-editFieldSelect").kendoMobileModalView("close");
		},
		onCloseSave: function(){
			$("#modal-editFieldSelect").kendoMobileModalView("close");
			if(this.change){
				this.change(this.selected);
			}
		}
	};
	var o = kendo.observable(vm);
	return o;
});

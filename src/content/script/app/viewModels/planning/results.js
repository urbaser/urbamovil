define([
	"app/models/masterTables",
	"app/models/planificacion",
	"app/models/planificaciones",
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(tables,planificacion,planificaciones,j, kendo) {
	'use strict'
	var vm = {
		model: [],
		onViewInit: function(event){
			console.log("Init view: planning-results");
			this.view = event.view.element;
			//this.temp = $("#planning-results-list-template").html();
			//$(document).on("click", "#planning-results .goToDetails", this.goToDetails.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: planning-results");
			this.view.find(".planning-list").empty();
			planificaciones.dataSource.read();
			this.set("model", planificaciones.dataSource);
			/*this.view.find(".planning-list").kendoRepeater({
				dataSource: this.model,
				template: this.temp
			});*/
		},
		onViewHide: function(event){
			console.log("Hide view: planning-results" );
		},
		goToDetails: function(event) {
			event.preventDefault();
			var idOT = event.dataItem.IdOT;
			planificaciones.select(planificaciones.dataSource.get(idOT));
			window.app.kendoApp.navigate("#planning-details");
		}
	};
	return kendo.observable(vm);
});

define([
	"jQuery",
	"../../models/planificaciones",
	"../../models/planificacion",
	"app/models/masterTables",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,planificaciones,planificacion,tables,kendo) {
	'use strict'
	var changeEvent;
	var changeEvent2;
	var vm = {
		model: {},
		tables: tables,
		editable: true,
		contratosAdmin: [],
		onViewInit: function(event){
			console.log("Init view: planning-editService");
			this.view = event.view.element;
			this.view.find(".goToStep2").click(this.goToStep2.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: planning-editService");
			this.set("model", planificaciones.selected);
			if (this.model.DatosServicio.IdServicio == null || this.model.DatosServicio.IdServicio == 0) {
				this.set("editable", true);
				this.view.find(".create-service").removeClass("create-service-hidden");
			} else {
				this.set("editable", false);
				this.view.find(".create-service").addClass("create-service-hidden");
			}


			var fechaInicio = this.model.DatosServicio.FechaInicio ? new Date(this.model.DatosServicio.FechaInicio.toString("yyyy-MM-ddTHH:mmZ")) : new Date();
			var fechaFin = this.model.DatosServicio.FechaFin ? new Date(this.model.DatosServicio.FechaFin.toString("yyyy-MM-ddTHH:mmZ")) : new Date();


			this.view.find(".servicio-fechaInicio")[0].valueAsNumber = fechaInicio.getTime();
			this.view.find(".servicio-fechaFin")[0].valueAsNumber = fechaFin.getTime();

			//this.view.find(".servicio-fechaInicio").val(this.model.DatosServicio.FechaInicio ? kendo.toString(this.model.DatosServicio.FechaInicio, "dd/MM/yyyy HH:mm") : "");
			//this.view.find(".servicio-fechaFin").val(this.model.DatosServicio.FechaFin ? kendo.toString(this.model.DatosServicio.FechaFin, "dd/MM/yyyy HH:mm") : "");


			if (this.model.instalacion() == "SmarTools") {
				planificacion.contratosAdmin.params  = this.model.DatosServicio.IdTipoServicio;
				planificacion.contratosAdmin.read();
				this.set("contratosAdmin", planificacion.contratosAdmin);
			}

			this.reset();
		},
		onViewHide: function(event){
			console.log("Hide view: planning-editService" );
			tables.municipios.filter([]);
			tables.tiposActividad.filter([]);
		},
		contratoAdminChange: function(e) {
			this.model.DatosServicio.IdContratoAdmv = parseInt(e.currentTarget.value);
			this.model.DatosServicio.ContratoAdmv = e.currentTarget.selectedOptions[0].innerHTML;

			this.enableField(".select-fasesCreacion");
			this.disableField(".select-tiposActividad");

		},
		faseCreacionChange: function(e) {
			this.model.DatosServicio.IdFaseCreacion = parseInt(e.currentTarget.value);
			this.model.DatosServicio.FaseCreacion = e.currentTarget.selectedOptions[0].innerHTML;

			tables.tiposActividad.filter({
				field: "IdFaseCreacion",
				operator: "eq",
				value: this.model.DatosServicio.IdFaseCreacion
			});

			this.enableField(".select-tiposActividad");
		},
		tipoActividadChange: function(e) {
			this.model.DatosServicio.IdTipoActividad = parseInt(e.currentTarget.value);
			this.model.DatosServicio.TipoActividad = e.currentTarget.selectedOptions[0].innerHTML;
		},

		turnosChange: function(e){
			if (this.model.DatosServicio.IdServicio == 0) {
				this.model.DatosServicio.IdTurno = parseInt(e.currentTarget.value);
				this.model.DatosServicio.Turno = e.currentTarget.selectedOptions[0].innerHTML;
			}
		},
		periodosChange: function(e){
			if (this.model.DatosServicio.IdServicio == 0) {
				this.model.DatosServicio.IdPeriodo = parseInt(e.currentTarget.value);
				this.model.DatosServicio.Periodo = e.currentTarget.selectedOptions[0].innerHTML;
			}
		},
		provinciaChange: function(e){
			//console.log("provincia change");
			this.model.DatosServicio.IdProvincia = parseInt(e.currentTarget.value);
			this.model.DatosServicio.Provincia = e.currentTarget.selectedOptions[0].innerHTML;
			this.model.DatosServicio.IdMunicipio = 0;
			if (this.model.DatosServicio.IdProvincia > 0) {
				tables.municipios.filter({
					field: "IdProvincia",
					operator: "eq",
					value: this.model.DatosServicio.IdProvincia
				});
			}
			if (this.model.DatosServicio.IdServicio == 0 || this.model.DatosServicio.IdServicio == undefined) this.enableField(".select-municipios");
			//this.view.find(".select-municipios").prepend("<option disabled selected>" + T("views.incidencias.edit.select-municipio") + "</option>");

		},
		municipioChange: function(e){
			this.model.DatosServicio.IdMunicipio = parseInt(e.currentTarget.value);
			this.model.DatosServicio.Municipio = e.currentTarget.selectedOptions[0].innerHTML;
		},
		calendarioChange: function(e){
			this.model.DatosServicio.IdCalendario = parseInt(e.currentTarget.value);
			this.model.DatosServicio.Calendario = e.currentTarget.selectedOptions[0].innerHTML;
		},
		estadoChange: function(e){
			this.model.IdEstadoOT = parseInt(e.currentTarget.value);
			this.model.EstadoOT = e.currentTarget.selectedOptions[0].innerHTML;
		},
		fechaOTChange: function(e) {
			this.model.FechaOT = parseInt(e.currentTarget.value);
		},
		fechaInicioChange: function(e) {
			this.model.DatosServicio.FechaInicio = parseInt(e.currentTarget.value);
		},
		fechaFinChange: function(e) {
			this.model.DatosServicio.FechaFin = parseInt(e.currentTarget.value);
		},
		reset: function(e) {
			if (this.editable == true) {
				this.enableField(".servicio-nombre");
				this.enableField(".select-contratosAdmin");
				if (this.model.DatosServicio.IdContratoAdmv) {
					this.enableField(".select-fasesCreacion");
				} else {
					this.disableField(".select-fasesCreacion");
				}
				if (this.model.DatosServicio.IdFaseCreacion) {
					this.enableField(".select-tiposActividad");
				} else {
					this.disableField(".select-tiposActividad");
				}
				this.enableField(".select-provincias");
				if (this.model.DatosServicio.IdProvincia) {
					this.enableField(".select-municipios");
				} else {
					this.disableField(".select-municipios");
				}
				this.disableField(".select-municipios");
				this.enableField(".select-calendarios");
				this.enableField(".select-turnos");
				this.enableField(".select-periodos");
				this.enableField(".servicio-fechaInicio");
				this.enableField(".servicio-fechaFin");
			}
			else {
				this.disableField(".select-contratosAdmin");
				this.disableField(".select-fasesCreacion");
				this.disableField(".select-tiposActividad");
				this.disableField(".select-provincias");
				this.disableField(".select-municipios");
				this.disableField(".select-calendarios");
				this.disableField(".select-turnos");
				this.disableField(".select-periodos");
				this.disableField(".servicio-fechaInicio");
				this.disableField(".servicio-fechaFin");
			}
		},
		disableField: function(field) {
			this.view.find(field).attr("disabled","").parent().addClass("disabled");
			this.view.find(field).find("option:selected").prop("selected", false);
		},
		enableField: function(field) {
			this.view.find(field).removeAttr("disabled").parent().removeClass("disabled");
			//this.view.find(field).find("option:selected").prop("selected", false);
			//this.view.find(field).val(this.view.find(field).find("option:first").val());
		},
		validate: function(e){
			var valid = true;
			var view = this.view;
			this.view.find(".error").removeClass("error");
			function checkSelect(name){
				if(!view.find(name).val()){
					view.find(name).addClass("error");
					valid = false;
				}
			}
			function checkDate(name){
				if(kendo.parseDate(new Date(view.find(name).val()), "yyyy-MM-dd HH:mm") == null){
					view.find(name).addClass("error");
					valid = false;
				}
			}
			if (this.model.instalacion() == "URBAJARDIN") {
				checkSelect(".select-provincias");
				checkSelect(".select-municipios");
			} else {
				checkSelect(".select-contratosAdmin");
				checkSelect(".select-fasesCreacion");
				checkSelect(".select-tiposActividad");
			}
			checkSelect(".select-calendarios");
			checkSelect(".select-turnos");
			checkSelect(".select-periodos");
			checkSelect(".servicio-fechaInicio");
			checkDate(".servicio-fechaInicio");
			checkSelect(".servicio-fechaFin");
			checkDate(".servicio-fechaFin");



			if (this.model.DatosServicio.IdServicio == 0) {
				checkSelect(".servicio-nombre");
			}

			if(!valid){
				app.showMessage(T("views.incidencias.edit.incomplete"), "Error");
			}
			return valid;
		},
		goToStep2: function(e) {
			var valid = this.validate();
			if (!valid)
			{
				e.preventDefault();
				return;
			}

			if (this.model.DatosServicio.IdServicio == 0 || this.model.DatosServicio.IdServicio == undefined) {
				var fechaInicio = $(this.view.find(".servicio-fechaInicio")[0]).val();
				var fechaFin = $(this.view.find(".servicio-fechaFin")[0]).val();

				if (kendo.parseDate(fechaInicio, "yyyy-MM-dd HH:mm") != null) planificaciones.selected.DatosServicio.FechaInicio = kendo.parseDate(fechaInicio, "yyyy-MM-dd HH:mm");
				else if (kendo.parseDate(fechaInicio, "yyyy-MM-dd")) planificaciones.selected.DatosServicio.FechaInicio = kendo.parseDate(fechaInicio, "yyyy-MM-dd");

				if (kendo.parseDate(fechaFin, "yyyy-MM-dd HH:mm") != null) planificaciones.selected.DatosServicio.FechaFin = kendo.parseDate(fechaFin, "yyyy-MM-dd HH:mm");
				else if (kendo.parseDate(fechaFin, "yyyy-MM-dd")) planificaciones.selected.DatosServicio.FechaFin = kendo.parseDate(fechaFin, "yyyy-MM-dd");
			}
			if (this.model.instalacion() == "SmarTools") {
				e.preventDefault();
				window.app.kendoApp.navigate("#planning-editStep2B");
			}

		}
	};
	return kendo.observable(vm);
});

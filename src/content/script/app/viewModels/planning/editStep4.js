define([
	"jQuery",
	"../../models/planificacion",
	"../../models/planificaciones",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo/kendo.mobile.listview"

], function(j,planificacion,planificaciones,kendo) {
	'use strict'

	var vm = {
		model: new planificacion.planificacion(),
		editable: true,
		onViewInit: function(event){
			console.log("Init view: planning-createStep4");
			this.view = event.view.element;
			this.temp = $("#resources-selected-list-template").html();
			this.temp2 = $("#resources2-selected-list-template").html();
			$(document).on("click", "#planning-editStep4 .remove-resource", this.removeResource.bind(this));

			$(this.view).find(".open-resources-select").click(function(e) {
				e.preventDefault();
				$("#modal-resourcesSelect").kendoMobileModalView("open");
			});
		},
		onViewShow: function(event){
			console.log("Show view: planning-createStep4");
			this.set("model",planificaciones.selected);

			if (this.model.instalacion() == "URBAJARDIN") {
				if (this.model.IdOT) {
					this.editable = false;
				}
				else {
					if (this.model.DatosServicio.IdServicio == 0 || this.model.DatosServicio.IdServicio == undefined) this.editable = true;
					else this.editable = false;
				}
			} else {
				this.editable = true;
			}

			this.view.find(".planning-details-list").kendoRepeater({
				dataSource: this.model.Recursos,
				template: this.temp
			});

			if (this.editable == true) {

				//ES EDITABLE
				if (planificaciones.selected.Recursos.length == 0) {
					this.view.find(".planning-noResources").show();
				}
				else {
					this.view.find(".planning-noResources").hide();
				}
				this.view.find(".remove-resource").show();
				this.view.find(".resources-plus").show();
			} else {

				//NO EDITABLE
				this.view.find(".remove-resource").hide();
				this.view.find(".resources-plus").hide();
				this.view.find(".planning-noResources").hide();

			}


		},

		onViewHide: function(event){
			console.log("Hide view: planning-createStep4" );

		},
		removeResource: function(event) {
			event.preventDefault();

			if (this.editable == true) {
				var element = $($(event.currentTarget).parents(".planning-details-list-item")[0]);
				var index = $("#planning-editStep4 .planning-details-list-item").index(element);
				planificaciones.selected.Recursos.splice(index, 1);
				this.view.find(".planning-details-list").kendoRepeater({
					dataSource: this.model.Recursos,
					template: this.temp
				});
				console.log("remove: " + index);

				if (planificaciones.selected.Recursos.length == 0) {
					this.view.find(".planning-noResources").show();
				}
				else {
					this.view.find(".planning-noResources").hide();
				}
			}
		}
	};
	return kendo.observable(vm);
});

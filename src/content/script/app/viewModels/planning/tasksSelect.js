define([
	"jQuery",
	"../../models/inventario",
	"../../models/inventarios",
	"../../models/planificacionTarea",
	"../../models/planificaciones",
	"app/models/masterTables",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,inventory,inventories,planificacionTarea,planificaciones,tables,kendo) {
	'use strict'
	var vm = {
		model: {},
		tables: tables,
		inventory: inventory,
		tareas: tables.tareasServicio,
		elementos: [],
		onViewInit: function(event){
			console.log("Init view: planning-tasksSelect");
			this.view = event.view.element;
			this.view.find(".add-task").click(this.addTask.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: planning-tasksSelect");
			this.set("model", {});
			this.model.IdProvincia = planificaciones.selected.DatosServicio.IdProvincia;
			this.model.IdMunicipio = planificaciones.selected.DatosServicio.IdMunicipio;
			this.tareas.filter({
				field: "IdTipoServicio",
				operator: "eq",
				value: parseInt(planificaciones.selected.DatosServicio.IdTipoServicio)
			});
			this.inventory.distritos.filter({
				field: "IdMunicipio",
				operator: "eq",
				value: this.model.IdMunicipio
			});
			this.reset();
		},
		onViewHide: function(event){
			console.log("Hide view: planning-tasksSelect" );
			tables.tareasServicio.filter([]);
			tables.municipios.filter([]);
		},
		tareaChange: function(e) {
			this.inventory.distritos.params = this.model.IdMunicipio;
			this.inventory.distritos.read();

			this.enableField(".select-distritos");
			this.disableField(".select-espacios");
			this.disableField(".select-zonas");

			this.reset();
		},
		distritoChange: function(e) {
			this.inventory.espaciosUrbanos.params = parseInt(e.currentTarget.value);
			this.inventory.espaciosUrbanos.read();

			this.enableField(".select-espacios");
			this.enableField(".select-elementos");
			this.disableField(".select-zonas");
			this.updateElements();

			this.reset();
		},
		espacioUrbanoChange: function(e){
			this.inventory.zonas.params = parseInt(e.currentTarget.value);
			this.inventory.zonas.read();

			this.enableField(".select-zonas");
			this.updateElements();

			this.reset();
		},
		zonaChange: function(e){

			this.updateElements();
			this.reset();
		},
		rutaChange: function(e){
			this.updateElements();
			this.reset();
		},
		elementoChange: function(e) {

		},
		updateElements: function() {
			var filtro = {};
			filtro.Jerarquia = {};
			filtro.Jerarquia.IdProvincia = this.model.IdProvincia;
			filtro.Jerarquia.IdMunicipio = this.model.IdMunicipio;
			if (this.model.Distrito) filtro.Jerarquia.IdDistrito = this.model.Distrito.IdDistrito;
			if (this.model.EspacioUrbano) filtro.Jerarquia.IdEspacioUrbano = this.model.EspacioUrbano.IdEspacioUrbano;

			if (this.model.ZonaRuta) {
				if (this.model.ZonaRuta.IdZona) {
					filtro.Jerarquia.IdZona = this.model.ZonaRuta.IdZona;
				}
				if (this.model.ZonaRuta.IdRuta) {
					filtro.Jerarquia.IdRuta = this.model.ZonaRuta.IdRuta;
				}
			}
			this.model.Elemento = null;

			this.view.find(".select-elementos")
				.removeAttr("disabled")
				.empty()
				.prepend("<option disabled selected>" + T("app.loading") + "</option>")
				.parent()
				.removeClass("disabled");

				/*
				Estaba agobiado
				*/
			this.inventory.elementosLista.params = filtro;
			this.inventory.elementosLista.read();
			this.set("elementos", this.inventory.elementosLista);
		},
		disableField: function(field) {
			this.view.find(field).attr("disabled","").empty().parent().addClass("disabled");
			this.view.find(field).find("option:selected").prop("selected", false);
		},
		enableField: function(field) {
			this.view.find(field).removeAttr("disabled").parent().removeClass("disabled");
			this.view.find(field).find("option:selected").prop("selected", false);
		},
		reset: function(e) {
		},
		validate: function(e){
			var valid = true;
			var view = this.view;
			this.view.find(".error").removeClass("error");
			function checkSelect(name){
				if(!view.find(name).val() || view.find(name).val() == 0){
					view.find(name).addClass("error");
					valid = false;
				}
			}
			checkSelect(".select-tareas");
			checkSelect(".select-distritos");
			checkSelect(".select-espacios");

			if(!valid){
				app.showMessage(T("views.incidencias.edit.incomplete"), "Error");
			}
			return valid;
		},
		addTask: function(e) {
			e.preventDefault();
			if (!this.validate()) {
				return;
			}

			var tarea = new planificacionTarea();
			tarea.IdTarea = this.model.Tarea.IdTarea;
			tarea.Tarea = this.model.Tarea.Tarea;
			tarea.IdEspacioUrbano = this.model.EspacioUrbano.IdEspacioUrbano;
			tarea.EspacioUrbano = this.model.EspacioUrbano.EspacioUrbano;
			if (this.model.Elemento) {
				tarea.IdElemento = this.model.Elemento.IdElemento;
				tarea.Elemento = this.model.Elemento.NombreElemento;
			}
			else {
				tarea.IdElemento = null;
				tarea.Elemento = null;
			}
			tarea.IdDistrito = this.model.Distrito.IdDistrito;
			tarea.Distrito = this.model.Distrito.Distrito;

			if (this.model.ZonaRuta) {
				if (this.model.ZonaRuta.IdZona) {
					tarea.IdZona = this.model.ZonaRuta.IdZona;
					tarea.Zona = this.model.ZonaRuta.Nombre;
					tarea.IdRuta = null;
					tarea.Ruta = null;
				}
				if (this.model.ZonaRuta.IdRuta) {
					tarea.IdRuta = this.model.ZonaRuta.IdRuta;
					tarea.Ruta = this.model.ZonaRuta.Nombre;
					tarea.IdZona = null;
					tarea.Zona = null;
				}
			}

			planificaciones.selected.Tareas.push(tarea);

			window.app.kendoApp.navigate("#:back");
		}
	};
	return kendo.observable(vm);
});

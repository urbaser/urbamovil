define([
	"app/models/masterTables",
	"app/models/planificacion",
	"app/models/planificaciones",
	"jQuery",
	"app/helpers/buttongroup",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(tables,planificacion,planificaciones,j, buttonGroup, kendo) {
	'use strict'
	var vm = {
		model: planificaciones.selected,
		onViewInit: function(event){
			console.log("Init view: planning-details");
			this.view = event.view.element;
			this.template = kendo.template($("#planning-details-template").html());

		},
		onViewShow: function(event){
			console.log("Show view: planning-details");
			this.set("model", planificaciones.selected);
			this.view.find(".planning-details-container").empty().append($(this.template(this.model)).i18n());
			buttonGroup.apply(this.view);

		},
		onViewHide: function(event){
			console.log("Hide view: planning-details" );
		},
		goToEdit: function(e) {
			window.app.kendoApp.navigate("#planning-edit");
		}
	};
	return kendo.observable(vm);
});

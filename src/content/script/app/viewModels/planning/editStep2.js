define([
	"jQuery",
	"../../models/planificacion",
	"../../models/planificaciones",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo/kendo.mobile.listview"

], function(j,planificacion,planificaciones,kendo) {
	'use strict'
	var vm = {
		model: {},
		onViewInit: function(event){
			console.log("Init view: planning-createStep2");
			this.view = event.view.element;
			this.temp = $("#tareas-list-template").html();
			this.view.find(".goToStep4").click(this.goToStep4.bind(this));
			$(document).on("click", "#planning-editStep2 .remove-tarea", this.removeTarea.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: planning-createStep2");
			if (planificaciones.selected == "undefined") {
				window.app.kendoApp.navigate("#planning-edit");
				return;
			}
			this.set("model",planificaciones.selected);
			this.view.find(".planning-details-list").kendoRepeater({
				dataSource: this.model.Tareas,
				template: this.temp
			});
			if (this.model.DatosServicio.IdServicio == 0 || this.model.DatosServicio.IdServicio == undefined) {
				if (planificaciones.selected.Tareas.length == 0) {
					this.view.find(".planning-noResources").show();
				}
				else {
					this.view.find(".planning-noResources").hide();
				}
				this.view.find(".remove-tarea").show();
				this.view.find(".resources-plus").show();
			}
			else {
				this.view.find(".planning-noResources").hide();
				this.view.find(".remove-tarea").hide();
				this.view.find(".resources-plus").hide();
			}

		},
		onViewHide: function(event){
			console.log("Hide view: planning-createStep2" );
		},
		removeTarea: function(event) {
			event.preventDefault();
			if (this.model.DatosServicio.IdServicio == 0 || this.model.DatosServicio.IdServicio == undefined) {
				var element = $($(event.currentTarget).parents(".planning-details-list-item")[0]);
				var index = $("#planning-editStep2 .planning-details-list-item").index(element);
				planificaciones.selected.Tareas.splice(index,1);
				this.view.find(".planning-details-list").kendoRepeater({
					dataSource: this.model.Tareas,
					template: this.temp
				});
				console.log("remove: "+index);
				if (planificaciones.selected.Tareas.length == 0) {
					this.view.find(".planning-noResources").show();
				}
				else {
					this.view.find(".planning-noResources").hide();
				}
			}
		},
		goToStep4: function(e) {
			if (planificaciones.selected.Tareas.length == 0)
			{
				app.showMessage("Tienes que añadir una tarea por lo menos.", "Info");
				e.preventDefault();
			}
		}
	};
	return kendo.observable(vm);
});

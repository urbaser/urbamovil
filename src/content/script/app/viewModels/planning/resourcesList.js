define([
	"jQuery",
	"../../models/planificacion",
	"../../models/planificaciones",
	"../../models/planificacionRecurso",
	"../../models/planificacionRecursos",
	"app/models/masterTables",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo/kendo.mobile.switch"

], function(j,planificacion,planificaciones,planificacionRecurso,recursos,tables,kendo) {
	'use strict'
	var changeEvent;
	var vm = {
		model: {},
		resourceCategory: {},
		resourceType: {},
		resources: [],
		onViewInit: function(event){
			console.log("Init view: planning-resourcesList");
			this.view = event.view.element;
			this.view.find(".goToStep4").click(this.addResources.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: planning-resourcesList");
			var categoria = parseInt(event.view.params["resourceCategory"]);
			this.set("resourceCategory", recursos.categorias.get(categoria));
			this.set("resourceType", tables.tiposRecurso.get(this.resourceCategory.IdTipoRecurso));
			this.set("model", planificaciones.selected);
			$("#resource-category-name").html(this.resourceCategory);
			this.set("resources", recursos.recursos);
			this.resources.one("change", changeEvent = this.checkResources.bind(this));
			this.resources.filter({
				field: "IdCategoriaRecurso",
				operator: "eq",
				value: categoria
			});
		},
		onViewHide: function(event){
			console.log("Hide view: planning-resourcesList" );
			tables.recursos.filter([]);
		},
		checkResources: function(e) {
			if (this.resources.total() == 0) {
				this.view.find(".planning-details-list").hide();
				this.view.find(".add-btn").hide();
				this.view.find(".no-resources").show();
			}
			else {
				this.view.find(".no-resources").hide();
				this.view.find(".planning-details-list").show();
				this.view.find(".add-btn").show();
			}
		},
		addResources: function(e) {
			e.preventDefault();
			var newResources = new Array();
			var resources = this.resources;
			var resourceCategory = this.resourceCategory;
			var resourceType = this.resourceType;
			var model = this.model;
			this.view.find(".resources-item:checked").each(function(index, item) {
				var newResource = new planificacionRecurso();
				newResource.IdRecurso = $(item).attr("data-field-id");
				newResource.IdCategoriaRecurso = resourceCategory.IdCategoriaRecurso;
				newResource.CategoriaRecurso = resourceCategory.CategoriaRecurso;
				newResource.IdTipoRecurso = resourceType.IdTipoRecurso;
				newResource.TipoRecurso = resourceType.TipoRecurso;
				newResource.Recurso = resources.get($(item).attr("data-field-id")).Recurso;

				model.Recursos.push(newResource);
			});
			console.log("recursos añadidos: " + this.model.Recursos.length);
			window.app.kendoApp.navigate("#:back");
		}
	};
	return kendo.observable(vm);
});

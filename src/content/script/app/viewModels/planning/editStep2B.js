define([
	"jQuery",
	"../../models/planificacion",
	"../../models/planificaciones",
	"app/models/masterTables",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo/kendo.mobile.switch"

], function(j,planificacion,planificaciones,tables,kendo) {
	'use strict'
	var changeEvent;
	var vm = {
		model: {},
		tareas: tables.tareasServicio,
		onViewInit: function(event){
			console.log("Init view: planning-createStep2");
			this.view = event.view.element;
			this.view.find(".goToStep3").click(this.goToStep3.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: planning-createStep2");
			this.set("model", planificaciones.selected);
			this.tareas.one("change", changeEvent = this.checkTasks.bind(this));
			this.tareas.filter({
				field: "IdTipoServicio",
				operator: "eq",
				value: this.model.DatosServicio.IdTipoServicio
			});
			if (this.model.DatosServicio.IdServicio != 0 || this.model.DatosServicio.IdServicio != "") this.view.find(".km-switch").hide();
			else { this.view.find(".km-switch").show(); }

		},
		checkTasks: function(e) {
			if (this.tareas.total() == 0) {
				this.view.find(".planning-details-list").hide();
				this.view.find(".no-tasks").show();
			}
			else {
				this.view.find(".no-tasks").hide();
				this.view.find(".planning-details-list").show();
			}
			for (var i = 0; i < this.model.Tareas.length; i++) {
				var tareaId = this.model.Tareas[i].IdTarea;
				if (this.view.find('[data-field-id="' + tareaId + '"]').length > 0) {
					this.view.find('[data-field-id="' + tareaId + '"]').data("kendoMobileSwitch").check(true)
				}
			}
		},
		onViewHide: function(event){
			console.log("Hide view: planning-createStep2" );
			//this.tareas.unbind("change",changeEvent);
		},
		goToStep3: function(e) {
			var tareas = new Array();
			this.view.find(".tareas-item:checked").each(function(index, item){
				var tarea = new planificacion.planificacionTarea();
				tarea.IdTarea = $(item).attr("data-field-id");
				tarea.IdTipoServicio = 0;
				tarea.Tarea = $(item).attr("data-text-field");
				tareas.push(tarea);
			});
			this.model.Tareas = tareas;
		}
	};
	return kendo.observable(vm);
});

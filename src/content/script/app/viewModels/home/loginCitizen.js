/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"app/models/ciudadano",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"jQuery",
	"kendo/kendo.core",
], function(user) {
	'use strict'
	var vm = {
		user: "",
		password: "",
		onViewInit: function(e){
			this.view = e.view.content;
		},
		onViewShow: function(e){
			if(window.app.user){
				window.app.user.clear();
			}
		},
		onViewHide: function(){
		},
		send: function(e){
			e.preventDefault();
			user.gotPassword.addOnce(function(e){
				window.app.kendoApp.pane.loader.hide();
				window.app.kendoApp.navigate("#citizenPass?thanks=0");
			}.bind(this));
			user.getPasswordFailed.addOnce(function(message){
				app.showMessage(message, "Error");
				window.app.kendoApp.pane.loader.hide();
			}.bind(this));
			if(!this.user){
				app.showMessage(T("views.home.login.incomplete"), "Error");
				console.log("error")
			}
			else{
				console.log("send: " + this.user);
				user.user = this.user;
				user.saveLocal();
				user.getPassword();

				window.app.kendoApp.pane.loader.show();
			}
		}
	};

	return kendo.observable(vm);
});

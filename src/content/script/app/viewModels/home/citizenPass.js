/**
 * Created by 7daysofrain on 8/5/15.
 */
/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"app/models/ciudadano",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"jQuery",
	"kendo/kendo.core",
], function(user) {
	'use strict'
	var vm = {
		user: "",
		password: "",
		onViewInit: function(e){
			this.view = e.view.content;
		},
		onViewShow: function(e){
			if(e.view.params.thanks == "0" || !e.view.params.thanks){
				$("#register-thanks").hide();
			}else{
				$("#register-thanks").show();
			}
		},
		onViewHide: function(){
		},
		send: function(e){
			e.preventDefault();
			user.loggedIn.addOnce(function(e){
				window.app.kendoApp.pane.loader.hide();
				window.app.kendoApp.navigate("#contracts");
			}.bind(this));
			user.loginFailed.addOnce(function(e){
				app.showMessage(T("views.home.login.incorrect"), "Error");
				window.app.kendoApp.pane.loader.hide();
			}.bind(this));
			if(!this.password){
				app.showMessage(T("views.home.login.incomplete"), "Error");
				console.log("error")
			}
			else{
				console.log("send: " + this.user);
				user.password = this.password;
				user.login();

				window.app.kendoApp.pane.loader.show();
			}
		}
	};

	return kendo.observable(vm);
});

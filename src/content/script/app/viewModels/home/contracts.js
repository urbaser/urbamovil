/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/ui/buttons",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"modernizr"
], function(j,buttons,kendo) {
	'use strict'
	var vm = {
		source: [],
		onViewInit: function(e){
			console.log(e.view.id + " init");
		},
		onViewShow: function(e){
			this.set("source",window.app.user.contracts);
			buttons.fix();
			if(window.app.user.contracts.total() == 0){
				window.app.user.getContracts();
			}
			$('.user-info .name').text(window.app.user.nombreCompleto);
			if(window.app.user.currentContract != null){
				console.log(".contract-item[data-id=" + window.app.user.currentContract.IdContrato+"]");
				$('.contract-item span').removeClass("selected");
				$(".contract-item[data-id=" + window.app.user.currentContract.IdContrato+"] span").addClass("selected");
			}
			$(".footer-contract").hide();
		},
		onSelect: function(e){
			console.log(e);
			e.preventDefault();
			window.app.user.selectContract(e.dataItem);
			$("html")
				.removeClass("contract-urbajardin")
				.removeClass("contract-smartus")
				.addClass(e.dataItem.TipoInstalacion == "URBAJARDIN" ? "contract-urbajardin" : "contract-smartus");
			window.app.kendoApp.navigate("#home");

		}
	};
	return kendo.observable(vm);
});

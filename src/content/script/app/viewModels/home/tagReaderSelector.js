define([
	"jQuery",
	"kendo/kendo.core",
	"app/models/user",
	"app/services/BTSerialBridge",
	"kendo/kendo.data",
	"kendo/kendo.binder",

], function(j,kendo,user,bt) {
	'use strict'
	var vm = {
		readers: [],
		onViewInit: function(event){
			console.log("Init view: home-tagReaderSelector");
			this.view = event.view.element;
			bt.scanned.add(this.onScanned.bind(this));
			bt.scannedError.add(this.onScannedError.bind(this));
			bt.connected.add(this.onConnect.bind(this));
			bt.connectedError.add(this.onConnectError.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: home-tagReaderSelector");
			this.relist();

		},
		relist: function(){
			var devices = [
				{title:"Lector RFID HTL Lite", code:"htl-lite", selected:user.preferences.tagReaderDevice == "htl-lite"},
				{title:"NFC Integrado", code:"nfc", selected:user.preferences.tagReaderDevice == "nfc"},
				{title:"Lector de QR", code:"qr", selected:user.preferences.tagReaderDevice == "qr"}
			]
			this.set("readers",devices);
		},
		onViewHide: function(event){
			console.log("Hide view: home-tagReaderSelector" );
		},
		onReaderSelect: function(event){
			var code = event.dataItem.code
			console.log("Reader selected: ",code)
			user.preferences.tagReaderDevice = code;
			user.saveLocal();
			this.relist();
			if(code == "htl-lite"){
				bt.scan();
			}
		},
		onScanned: function(){
			$("#modal-selectBTDevice").data("kendoMobileModalView").open();
			$("#modal-selectBTDevice").data("kendoMobileModalView").one("close", function(e) {
				console.log("closed: " + bt.selectedDevice);
				if(!bt.selectedDevice){
					this.error(T("views.incidencias.selectByRFID.notSelected"))
				}
				else{
					this.connect();
				}
			}.bind(this));
		},
		onScannedError: function(message) {
			this.error(message);
		},
		connect: function(){
			bt.connect();
		},
		onConnect: function(){
			user.preferences.rfidBTId = bt.selectedDevice;
			user.saveLocal();
		},
		onConnectError: function(){
			this.error(T("views.incidencias.selectByRFID.connectFail"));
		},
		error: function(text){
			app.showMessage(text, "Error");
		},
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"app/bootstrap/roleBasedUIProcessor",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"jQuery",
	"kendo/kendo.core",
], function(roles) {
	'use strict'
	var vm = {
		user: "",
		password: "",
		onViewInit: function(e){
			this.view = e.view.content;
		},
		onViewShow: function(e){
			if(window.app.user.ticket){
				window.app.user.clear();
			}
		},
		onViewHide: function(){
		},
		send: function(e){
			e.preventDefault();
			window.app.user.loggedIn.addOnce(function(e){
				roles.processPage();
				window.app.kendoApp.pane.loader.hide();
				window.app.kendoApp.navigate("#contracts");
			}.bind(this));
			window.app.user.loginFailed.addOnce(function(e){
				app.showMessage(T("views.home.login.incorrect"), "Error");
				window.app.kendoApp.pane.loader.hide();
			}.bind(this));
			if(!this.user || !this.password){
				console.log(this.user , this.password)
				app.showMessage(T("views.home.login.incomplete"), "Error");
				console.log("error")
			}
			else{
				console.log("send: " + this.user);
				window.app.user.user = this.user;
				window.app.user.password = this.password;
				window.app.user.login();

				window.app.kendoApp.pane.loader.show();
			}
		}
	};

	return kendo.observable(vm);
});

define([
	"jQuery",
	"app/helpers/buttongroup",
	"app/helpers/editModals",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",

], function(j, buttonGroup, editModals, kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: inventory-createPuntoRecogida");

			this.view = event.view.element;
			buttonGroup.apply(this.view);
			editModals.apply(this.view);
		},
		onViewShow: function(event){
			console.log("Show view: inventory-createPuntoRecogida");
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-createPuntoRecogida" );
		}
	};
	return kendo.observable(vm);
});

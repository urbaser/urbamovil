define([
	"jQuery",
	"app/models/inventarios",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo/kendo.mobile.switch",
], function(j, inventarios, kendo) {
	var vm = {
		onViewInit: function(event){
			console.log("Init view: inventory-mapsearchCreate");
			this.view = event.view.element;

			$('#inventory-create-map2')
				.height($(document).height() - 65)
				.width($(document).width());

			try{
				// Map
				this.template = kendo.template($("#marker-template").html());
				this.map = new GMaps({
					div:"#inventory-create-map2",
					zoom: 18,
					width: $(document).width(),
					height: $(window).height(),
					zoomControl: false, //{style:google.maps.ZoomControlStyle.LARGE},
					mapTypeControl: false,
					streetViewControl: false,
					panControl: false,
					lat: 40.416705,
					lng: -3.703582,
					draggable: true,
					click:this.onMapClick.bind(this)
				});
				this.map.map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById("inventory-create-map-search-box2"));
				this.map.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(document.getElementById("inventory-create-map-buttons2"));
				var input = document.getElementById('inventory-create-map-search2');
				var options = {
					types: ['geocode'],
					componentRestrictions: {country: 'es'}
				};
				var autocomplete = new google.maps.places.Autocomplete(input, options);
				google.maps.event.addListener(autocomplete, 'place_changed', function() {
					var place = autocomplete.getPlace();
					console.log(place);
					this.map.map.setCenter(place.geometry.location);
				}.bind(this));

				if(!Modernizr.csscalc || !Modernizr.cssvwunit){
					$('#inventory-create-map2').height($(document).height() -110).width("100%");
				}
				setTimeout(this.map.refresh.bind(this.map), 500);
			}
			catch(error){
				console.log("No se pudo cargar GMaps");
			}
		},
		onViewShow: function(event){
			console.log("Show view: inventory-mapsearchCreate");
			if(this.map){
				this.map.removeOverlays();
			}
			if(inventarios.selected){
				var lat = inventarios.selected.Latitud;
				var lng = inventarios.selected.Longitud;
				this.drawMarker(lat, lng, 1);
				this.map.map.setCenter(new google.maps.LatLng(lat, lng));
			}
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-createElemento" );
		},
		// Mapa
		onMapClick: function(e){
			this.map.removeOverlays();
			this.drawMarker(e.latLng.lat(),e.latLng.lng(),1);
			this.switchAdd();

			if(inventarios.selected){
				inventarios.selected.Latitud = e.latLng.lat();
				inventarios.selected.Longitud = e.latLng.lng();
			}
		},
		drawMarker: function(lat,lng,index){
			this.map.drawOverlay({
				lat: lat,
				lng: lng,
				content: this.template({index:index}),
				click: function(e){
					if(this.currentExpandedMarker){
						$(this.currentExpandedMarker.el).find(".options").css("visibility","hidden");
					}
					if(this.currentExpandedMarker && this.currentExpandedMarker.el == e.el){
						this.currentExpandedMarker = null;
						return;
					}
					this.currentExpandedMarker = e;
					var options = $(e.el).find(".options");
					options.css("visibility",options.css("visibility")== "visible" ? "hidden" : "visible");
					$(e.el).find(".button").click(this.removeDir.bind(this));
				}.bind(this)
			});
		},
		removeDir: function(e){
			var i = $(e.currentTarget).parent().parent().attr("data-index");
			console.log("Removing at index",i);
			this.currentExpandedMarker.setMap(null);
		},
		center: function(e){
			console.log("center");
			if(e)e.preventDefault();
			window.app.kendoApp.pane.loader.show();
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(this.showPosition.bind(this), onError);
			} else {
				onError();
			}

			function onError() {
				window.app.kendoApp.pane.loader.hide();
				console.error('Geolocation failed: '+error.message);
				app.showMessage(T("views.incidencias.selectLocation.geoloc-error"), "Error");
			}
		},
		showPosition: function(position){
			window.app.kendoApp.pane.loader.hide();
			this.map.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
		},
		switchAdd: function(e){
			if(e)e.preventDefault();
			this.addMode = !this.addMode;
			$("#map-switch-add2").find("img").attr("src",this.addMode ? "content/imgs/map-add-location-selected.svg" : "content/imgs/map-add-location.svg")
		},
		showHelp: function(e){
			e.preventDefault();
			$("#modal-mapHelp").kendoMobileModalView("open");
		},
	};
	return kendo.observable(vm);
});

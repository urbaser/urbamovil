define([
	"app/models/inventarios",
	"refr3sh/fn/debounce",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(inventarios,debounce,kendo) {
	'use strict'

	function getBoundsRadius(bounds){
		// r = radius of the earth in km
		var r = 6378.8
		// degrees to radians (divide by 57.2958)
		var ne_lat = bounds.getNorthEast().lat() / 57.2958
		var ne_lng = bounds.getNorthEast().lng() / 57.2958
		var c_lat = bounds.getCenter().lat() / 57.2958
		var c_lng = bounds.getCenter().lng() / 57.2958
		// distance = circle radius from center to Northeast corner of bounds
		var r_km = r * Math.acos(
				Math.sin(c_lat) * Math.sin(ne_lat) +
				Math.cos(c_lat) * Math.cos(ne_lat) * Math.cos(ne_lng - c_lng)
			)
		return r_km; // radius in meters
	}

	var autocomplete;
	var vm = {
		cbox_contenedor_value: true,
		cbox_ptorecogida_value: true,
		cbox_papelera_value: true,
		onViewInit: function(event){
			console.log("Init view: inventory-mapsearch");
			this.view = event.view.element;
			$(document).on("click", "#inventory-mapsearch .search-btn2", this.showFilters.bind(this));
			kendo.bind(event.view.header, this, kendo.mobile.ui);

			inventarios.location = null;
			$('#inventory-search-map')
				.height($(document).height() - 65)
				.width($(document).width());

			// Map init
			this.template = kendo.template($("#inventory-marker-template").html());
			this.map = new GMaps({
				div:"#inventory-search-map",
				width: $(document).width(),
				height: $(window).height(),
				zoom: 18,
				zoomControl: false,
        panControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        overviewMapControl: false,
				lat: 40.416705,
				lng: -3.703582,
				draggable: true,
				bounds_changed: debounce(function(){
					var coords = this.map.map.getCenter();
					var d = getBoundsRadius(this.map.map.getBounds());
					if(d>0.5){
						d = 0.5;
						this.map.removePolygons();
						this.map.drawCircle({
							lat: coords.lat(),
							lng: coords.lng(),
							radius: d*1000/2,
							strokeColor: '#ff9999',
							strokeOpacity: 1,
							strokeWeight: 1,
							fillColor: '#ffe6e6',
							fillOpacity: 0.6
						});
					}else{
						this.map.removePolygons();
					}
					if(inventarios.location == null){
						window.showLoading();
						inventarios.location = {
							x: coords.lat,
							y: coords.lng,
							d: d/2
						}
						inventarios.nearBy.read();
					}else{
						inventarios.location = {
							x: coords.lat,
							y: coords.lng,
							d: d/2
						}
					}
				}.bind(this),1),
			});
			this.map.map.setOptions({
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.LARGE,
					position: google.maps.ControlPosition.RIGHT_BOTTOM
				}
			});
			this.map.map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById("inventory-search-map-search-box"));
			this.map.map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(document.getElementById("inventory-search-map-buttons"));
			var input = document.getElementById('inventory-search-map-search');
			$(document).on('click', ".viewmoreinmap",this.showDetail.bind(this));
			// Search bar
			var options = {
				types: ['geocode'],
				componentRestrictions: {country: 'es'}
			};
			autocomplete = new google.maps.places.Autocomplete(input, options);
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
				var place = autocomplete.getPlace();
				console.log(place);
				if(place){
					this.map.map.setCenter(place.geometry.location);
					this.map.map.setZoom(17);
					window.showLoading();
					var coords = this.map.map.getCenter();
					var d = getBoundsRadius(this.map.map.getBounds());
					if(d>0.5){
						d = 0.5;
					}
					inventarios.location = {
						x: coords.lat,
						y: coords.lng,
						d: d/2
					}
					inventarios.nearBy.read();
				}
			}.bind(this));
			this.center();
			// Map update
			inventarios.nearBy.bind("change", this.drawPoints.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: inventory-mapsearch");
			this.map.removeOverlays();
			this.map.hideInfoWindows();
			autocomplete.set('place',null);
			$("#inventory-search-map-search").val("");
			this.map.refresh();
			setTimeout(this.map.refresh.bind(this.map),1000);
		},
		find: function(e){
			if(e)e.preventDefault();
			window.showLoading();
			inventarios.nearBy.read();
		},
		onChange: function(){
			this.map.removeMarkers();
			this.drawPoints();
		},
		drawPoints: function(){
			this.map.removeOverlays();
			this.map.removeMarkers();
			var direccion;
			inventarios.nearBy.data().forEach(function(it, index){
				direccion = it.CamposValor["Dirección"]+" "+it.CamposValor["Número"]+" "+it.CamposValor["Código Postal"];
				if(it.TipoElemento == "PUNTO DE RECOGIDA" && this.cbox_ptorecogida_value){
					this.drawMarker(it.Latitud, it.Longitud, it.IdElemento, it.TipoElemento, it.TipoObjeto, direccion);
				}
				if(it.TipoElemento == "CONTENEDOR" && this.cbox_contenedor_value){
					this.drawMarker(it.Latitud, it.Longitud, it.IdElemento, it.TipoElemento, it.TipoObjeto, direccion);
				}
				if(it.TipoElemento == "PAPELERA" && this.cbox_papelera_value){
					this.drawMarker(it.Latitud, it.Longitud, it.IdElemento, it.TipoElemento, it.TipoObjeto, direccion);
				}
				if(inventarios.nearBy.data().length == index+1){
					window.hideLoading();
				}
			}.bind(this))
			if(inventarios.nearBy.data().length == 0){
				window.hideLoading();
			}
		},
		drawMarker: function(lat, lng, index, tipoElem, tipoObj, direccion){
			var tml = this.template({index: index, TipoElemento: tipoElem, TipoObjeto: tipoObj, Direccion: direccion});
			var iconSrc;
			if(tipoElem == "PUNTO DE RECOGIDA"){
				iconSrc = "content/imgs/icons/ico-point-small.png";
			}else if (tipoElem == "CONTENEDOR") {
				iconSrc = "content/imgs/icons/ico-dumpster-small.png";
			}else{
				iconSrc = "content/imgs/icons/ico-trash-small.png";
			}
			this.map.addMarker({
				lat: lat,
			 	lng: lng,
				infoWindow: {content: tml},
				icon: iconSrc
			});
		},
		showDetail: function(e){
			var idElemento = $(e.target).parent().attr("data-index");
			if(idElemento == undefined){
				idElemento = $(e.target).parent().parent().attr("data-index");
			}
			inventarios.nearBy.data().forEach(function(it, index){
				if(it.IdElemento == idElemento){
					inventarios.select(it);
					window.app.kendoApp.navigate("#inventory-detailsElemento");
					return "";
				}
			}.bind(this))
		},
		center: function(e){
			console.log("center");
			if(e)e.preventDefault();
			window.showLoading();
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(pos){
					this.map.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
					var coords = this.map.map.getCenter();
					var d = getBoundsRadius(this.map.map.getBounds());
					if(d>0.5){
						d = 0.5;
					}
					inventarios.location = {
						x: coords.lat,
						y: coords.lng,
						d: d/2
					}
					inventarios.nearBy.read();
				}.bind(this), this.onError);
			} else {
				this.onError();
				window.hideLoading();
			}
		},
		onError: function() {
			window.hideLoading();
			console.error('Geolocation failed: '+ error.message);
			app.showMessage(T("views.incidencias.selectLocation.geoloc-error"), "Error");
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-mapsearch" );
		},
		showFilters: function(e){
			e.preventDefault();
			$("#modal-selectFilterMap").toggleClass("active");
		},
		apply: function(){
			$("#modal-selectFilterMap").removeClass("active");
		}
	};
	return kendo.observable(vm);
});

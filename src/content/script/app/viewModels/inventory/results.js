define([
	"app/models/inventarios",
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(inventario, j, kendo) {
	'use strict'
	var vm = {
		source: [],
		total: 0,
		totalMostrar: 0,
		onViewInit: function(event){
			console.log("Init view: inventory-results");
			this.view = event.view.element;
			inventario.dataSource.bind("change", this.dataSource_change.bind(this));
			inventario.dataSource.bind("requestStart", this.onRequestStart.bind(this));
			inventario.dataSource.bind("change", this.onRequestEnd.bind(this));
			this.view.find(".inventory-list").data("kendoMobileListView").options.messages.loadMoreText = "Cargar más";
			this.view.find(".no-result").hide();
			this.view.find(".no-conection").hide();
			this.set("source", inventario.dataSource);
		},
		onViewShow: function(event){
			console.log("Show view: inventory-results");
			this.view.find(".no-result").hide();
			this.view.find(".no-conection").hide();
			this.view.find(".inventory-list").show();
			
			if(app.online == false){
				this.view.find(".no-conection").show();
				this.view.find(".inventory-list").hide();
			}else{
				inventario.dataSource.page(0);
				kendo.mobile.application.scroller().reset();
			}
		},
		onRequestStart: function(){
			/*setTimeout(function(){
				window.hideLoading();
			},100);*/
		},
		onRequestEnd: function(){
			if(inventario.dataSource.total() == 500){
				this.set("totalMostrar", "más de 500");
			}else{
				this.set("totalMostrar", inventario.dataSource.total());
			}
			if(inventario.dataSource.total() == 0) {
				this.view.find(".no-result").show();
				this.view.find(".inventory-list").hide();
			}else{
				this.view.find(".no-result").hide();
				this.view.find(".inventory-list").show();
			}
			window.hideLoading();
		},
		onSelect: function(event){
			console.log(event);
			if(event.dataItem){
				inventario.select(event.dataItem);
			}
		},
		dataSource_change: function(event){
			this.set("total", inventario.dataSource.total())
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-results");
		}
	};
	return kendo.observable(vm);
});

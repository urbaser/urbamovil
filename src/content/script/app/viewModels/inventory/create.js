define([
	"app/models/masterTables",
	"app/models/inventarios",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(tables, inventarios, kendo) {
	'use strict'
	var vm = {
		source: new kendo.data.DataSource(),
		onViewInit: function(event){
			console.log("Init view: inventory-create");
			this.view = event.view.element;
		},
		onViewShow: function(event){
			console.log("Show view: inventory-create");
			var source = tables.tiposElementos;
			source._data[0].Tipo = T("app.tipoElem1");
			source._data[1].Tipo = T("app.tipoElem2");
			source._data[2].Tipo = T("app.tipoElem3");
			
			this.set("source", tables.tiposElementos);
			inventarios.select(null);
			$(".select-tipos-objetos").val([]);
			$(".select-municipios").val([]);
			$(".select-provincias").val([]);
			$(".select-distritos").val([]);
			$(".select-espacios").val([]);
			$(".select-zonas").val([]);
			$(".select-areas").val([]);
			$(".form-input").val("");
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-create" );
		}
	};
	return kendo.observable(vm);
});

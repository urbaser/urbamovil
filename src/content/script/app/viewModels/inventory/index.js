define([
	"app/helpers/editModals",
	"app/models/inventarios",
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(editModals,inventories,j,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: inventory-index");
			this.view = event.view.element;
		},
		onViewShow: function(event){
			console.log("Show view: inventory-index");
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-index" );
		},
		onReadTag: function (event) {
			event.preventDefault();
			editModals.openTagReader(function(tag){
				inventories.setFilter({TAG:tag});
				window.app.kendoApp.navigate("#inventory-results");
			}.bind(this))
		},
	};
	return kendo.observable(vm);
});

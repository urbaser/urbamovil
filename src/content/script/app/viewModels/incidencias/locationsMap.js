/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"app/models/localizacion",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo.repeater",
	"gmaps"
], function(j,Incidencia,incidencias,Location,kendo) {
	var vm = {
		addMode: false,
		onViewInit: function(e){
			console.log(e.view.id + " init");
			//incidencias.selected = new Incidencia();
			//incidencias.selected.Localizaciones = new Array();
			this.template = kendo.template($("#locations-marker-template").html());
			this.map = new GMaps({
				div:"#locations-map",
				zoom: 16,
				zoomControl: false, //{style:google.maps.ZoomControlStyle.LARGE},
				mapTypeControl: false,
				streetViewControl: false,
				panControl: false,
				lat: 40.416705,
				lng: -3.703582,
				draggable: true

			});
			this.map.map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(document.getElementById("locations-map-buttons"));

			if(!Modernizr.csscalc || !Modernizr.cssvwunit){
				$('#locations-map').height($(document).height() -110).width("100%");
			}

		},
		drawMarker: function(lat,lng,index){
			this.map.drawOverlay({
				lat: lat,
				lng: lng,
				content: this.template({index:index})
			});
		},
		onViewShow: function(e){
			this.map.removeOverlays();
			var total = 0;
			if(incidencias.selected && incidencias.selected.Localizaciones){
				var bounds = new google.maps.LatLngBounds();
				incidencias.selected.Localizaciones.forEach(function(it,index){
					if(it.IdTipoLocalizacion == 1){
						total++;
						bounds.extend(new google.maps.LatLng(it.Map_Latitud,it.Map_Longitud));
						this.drawMarker(it.Map_Latitud,it.Map_Longitud,index);
					}
				}.bind(this));
			}
			if(total > 0){
				this.map.map.fitBounds(bounds);
			}
			else{
				this.center();
			}
		},
		center: function(e){
			console.log("center");
			if(e)e.preventDefault();
			window.app.kendoApp.pane.loader.show();
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(this.showPosition.bind(this), onError);
			} else {
				onError();
			}

			function onError() {
				window.app.kendoApp.pane.loader.hide();
				console.error('Geolocation failed: '+error.message);
				app.showMessage(T("views.incidencias.selectLocation.geoloc-error"), "Error");
			}
		},
		showPosition: function(position){
			window.app.kendoApp.pane.loader.hide();
			this.map.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
		}
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"app/models/localizacion",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo.repeater",
	"kendo/kendo.autocomplete"
], function(j,Incidencia,incidencias,Location,kendo) {
	'use strict'
	var vm = {
		location: new Location(),
		onViewInit: function(e){
			console.log(e.view.id + " init");
			//incidencias.selected = new Incidencia();
			//incidencias.selected.Localizaciones = new Array();
			this.view = e.view.content;
			this.temp = $("#address-list-template").html();
			var input = document.getElementById('select-address-address');
			var options = {
				types: ['geocode'],
				componentRestrictions: {country: 'es'}
			};

			var autocomplete = new google.maps.places.Autocomplete(input, options);
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
			}.bind(this));
		},
		onViewShow: function(e){
			$('#select-address-address').val("");
			this.createNew();
			this.relist();
			this.view.find(".error").removeClass("error");
		},
		relist: function(){
			$("#addressList").kendoRepeater({
				dataSource: incidencias.selected.Localizaciones,
				template: this.temp
			});
			$("#addressList a").click(this.removeDir.bind(this));
		},
		addDir: function(e){
			e.preventDefault();
			if(!this.validate()) return;
			console.log("Añadida direccion");
			this.location.Dir_Calle = $("#select-address-address").val();
			$("#select-address-address").val("");
			incidencias.selected.Localizaciones.push(this.location);
			this.createNew();
			this.relist();
			this.view.find(".error").removeClass("error");
		},
		removeDir: function(e){
			e.preventDefault();
			var uid = $(e.currentTarget).attr("data-uid");
			console.log("remove",uid);
			var item = $.grep(incidencias.selected.Localizaciones, function(e){ return e.uid == uid; });
			var index = incidencias.selected.Localizaciones.indexOf(item[0]);
			if(index > -1){
				incidencias.selected.Localizaciones.splice(index,1);
				this.relist();
			}
		},
		validate: function(e){
			var valid = true;
			if(!this.view.find("#select-address-address").val()){
				this.view.find("#select-address-address").addClass("error");
				valid = false;
			}
			if(!valid && e){
				e.preventDefault();
				app.showMessage(T("views.incidencias.selectByAddress.incomplete"), "Error");
			}
			return valid;
		},
		createNew: function(){
			var loc = new Location();
			loc.IdTipoLocalizacion = 2;
			this.set("location",loc);
		}
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"app/models/masterTables",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,Incidencia,incidencias,tables,kendo) {
	'use strict'
	var vm = {
		tables: tables,
		model: new Incidencia(),
		onViewInit: function(e){
			console.log(e.view.id + " init");
			this.view = e.view.content;
			if(navigator.camera){
				$("#add-image1").click(this.onAddImageClick.bind(this));
				$("#add-image2").click(this.onAddImageClick2.bind(this));
				this.view.find("[type=file]").hide();
			}
			this.view.find(".select-grupos").prepend("<option disabled selected>" + T("views.incidencias.edit.select-group") + "</option>");
			this.view.find(".select-tipos").prepend("<option disabled selected>" + T("views.incidencias.edit.select-tipo") + "</option>");
			this.view.find(".select-descripciones").prepend("<option disabled selected>" + T("views.incidencias.edit.select-descripcion") + "</option>");
			this.view.find(".select-provincias").prepend("<option disabled selected>" + T("views.incidencias.edit.select-provincia") + "</option>");
			this.view.find(".select-municipios").prepend("<option disabled selected>" + T("views.incidencias.edit.select-municipio") + "</option>");
			this.view.find(".select-prioridades").prepend("<option disabled selected>" + T("views.incidencias.edit.select-prioridad") + "</option>");
		},
		onViewShow: function(e){
			this.view.find(".error").removeClass("error");
			if(window.app.kendoApp)
				window.app.kendoApp.scroller().reset();
			$("[type=file]").val(null);
			if(incidencias.selected == null){
				console.log("Creando nueva incidencia");
				this.set("model",new Incidencia());
				this.model.IdEstado = 1;
				this.model.Localizaciones = new Array();
				incidencias.select(this.model);
				this.view.find(".form-file img").removeClass("visible");
			}
			else{
				console.log("Editando incidencia",incidencias.selected);
				this.set("model",incidencias.selected);
				if(incidencias.selected.Imagen || this.model.tempfile){
					this.view.find(".form-file img").addClass("visible");
				}
				else{
					this.view.find(".form-file img").removeClass("visible");
				}
			}
			if(this.model.IdMunicipio > 0){
				this.provinciaChange();
				this.view.find(".select-municipios").val(this.model.IdMunicipio);
			}
			else{
				tables.municipios.filter();
				this.view.find(".select-municipios").attr("disabled","").parent().addClass(".disabled");
			}
			if(this.model.IdTipo > 0){
				this.grupoChange();
				this.view.find(".select-tipos").val(this.model.IdTipo);
			}
			else{
				this.view.find(".select-tipos").attr("disabled","").parent().addClass(".disabled");
			}
			if(this.model.IdDescripcion > 0){
				this.tipoChange();
				this.view.find(".select-descripciones").val(this.model.IdDescripcion);
			}
			else{
				this.view.find(".select-descripciones").attr("disabled","").parent().addClass(".disabled");
			}
			this.set("model",incidencias.selected)
		},
		onAddImageClick: function(e){
			e.preventDefault();
			if(navigator.camera){
				navigator.camera.getPicture(this.onCameraSuccess.bind(this), null, {
					quality : 75,
					destinationType : Camera.DestinationType.FILE_URL,
					sourceType : Camera.PictureSourceType.CAMERA,
					encodingType: Camera.EncodingType.JPEG,
					targetWidth: 1000,
					targetHeight: 1000,
					correctOrientation: true,
					saveToPhotoAlbum: false
				});
			}
			else{
				app.showMessage(T("views.incidencias.edit.select-unsupported"), "Error");
			}
		},
		onAddImageClick2: function(e){
			e.preventDefault();
			if(navigator.camera){
				navigator.camera.getPicture(this.onCameraSuccess.bind(this), null, {
					quality : 75,
					destinationType : Camera.DestinationType.FILE_URL,
					sourceType : Camera.PictureSourceType.SAVEDPHOTOALBUM,
					encodingType: Camera.EncodingType.JPEG,
					targetWidth: 1000,
					targetHeight: 1000,
					correctOrientation: true,
					saveToPhotoAlbum: false
				});
			}else{
				app.showMessage(T("views.incidencias.edit.select-unsupported"), "Error");
			}
		},
		onCameraSuccess: function(uri){
			this.model.tempfileuri = uri;
			if(this.model.tempfileuri){
				this.view.find(".form-file img").addClass("visible");
				this.model.dirty = true;
			}else{
				this.view.find(".form-file img").removeClass("visible");
			}
		},
		provinciaChange: function(e){
			tables.municipios.filter({
				field: "IdProvincia",
				operator: "eq",
				value:this.model.IdProvincia
			});
			this.view.find(".select-municipios")
				.removeAttr("disabled")
				.prepend("<option disabled selected>" + T("views.incidencias.edit.select-municipio") + "</option>")
				.parent()
				.removeClass("disabled");
		},
		grupoChange: function(e){
			tables.tipos.filter({
				field: "IdGrupoIncidencia",
				operator: "eq",
				value:this.model.IdGrupo
			});

			this.view.find(".select-tipos")
				.removeAttr("disabled")
				.prepend("<option disabled selected>" + T("views.incidencias.edit.select-tipo") + "</option>")
				.parent()
				.removeClass("disabled");
			this.view	.find(".select-descripciones")
						.attr("disabled","")
						.empty()
						.prepend("<option disabled selected>" + T("views.incidencias.edit.select-descripcion") + "</option>")
						.parent()
						.addClass("disabled");
		},
		tipoChange: function(e){
			tables.descripciones.filter({
				field: "IdTipoIncidencia",
				operator: "eq",
				value:this.model.IdTipo
			});
			this.view.find(".select-descripciones")
				.removeAttr("disabled")
				.prepend("<option disabled selected>" + T("views.incidencias.edit.select-descripcion") + "</option>")
				.parent()
				.removeClass("disabled");;
		},
		validate: function(e){
			var valid = true;
			var view = this.view;
			this.view.find(".error").removeClass("error");
			function checkSelect(name){
				if(!view.find(name).val()){
					view.find(name).addClass("error");
					valid = false;
				}
			}
			checkSelect(".select-prioridades");
			checkSelect(".select-provincias");
			checkSelect(".select-municipios");
			checkSelect(".select-grupos");
			checkSelect(".select-tipos");
			checkSelect(".select-descripciones");
			if(!valid && e){
				e.preventDefault();
				app.showMessage(T("views.incidencias.edit.incomplete"), "Error");
			}
			return valid;
		},
		imageChange: function(event){
			event.preventDefault();
			console.log("Image change")
			if(event.target.files[0].size > 8 * 1024 * 1024){
				app.showMessage(T("views.incidencias.edit.bigimage"), "Error");
				return;
			}
			this.model.tempfile = event.target.files[0];
			if(this.model.tempfile){
				this.view.find(".form-file img").addClass("visible");
				this.model.dirty = true;
				this.model.cargandoImagen = true;
			}else{
				this.view.find(".form-file img").removeClass("visible");
			}
		}
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,Incidencia,incidencias,kendo) {
	var vm = {
		model: new Incidencia(),
		onViewInit: function(e){
			this.view = e.view.content;
			console.log(e.view.id + " init");
			this.temp = $("#address-list-resume-template").html();
		},
		onViewShow: function(e){
			this.model = incidencias.selected;
			kendo.bind(this.view, this);
			this.relist();
			$('#resume-preview').attr("src","");
			if(this.model.tempfile){
				var reader = new FileReader();
				reader.onload = function(event) {
					$('#resume-preview').attr("src",event.target.result).removeAttr("style");
				};
				reader.onerror = function(event) {
					console.log(event);
					$('#resume-preview').attr("src","content/imgs/icons/ico-image.svg").css("width","25%");
				};
				reader.readAsDataURL(this.model.tempfile);
			}
			else if(this.model.tempfileuri){
				$('#resume-preview').attr("src",this.model.tempfileuri).removeAttr("style");
			}
			this.view.find(".km-scroll-container").show();
		},
		fechaFormatted: function(){
			return this.model ? kendo.toString(this.model.FechaAlta, "dd/MM/yyyy") : "";
		},
		contrato: function(){
			return window.app.user != null && window.app.user.currentContract != null ? window.app.user.currentContract.Nombre : "";
		},
		nombreGrupo: function(){
			return this.model.grupo() ? this.model.grupo().GrupoIncidencia : "";
		},
		nombreTipo: function(){
			return this.model.tipo() ? this.model.tipo().TipoIncidencia : "";
		},
		nombreDesc: function(){
			return this.model.descripcion() ? this.model.descripcion().DescripcionIncidencia : "";
		},
		nombreEstado: function(){
			return this.model.estado() ? this.model.estado().EstadoIncidencia : "";
		},
		prioridad: function(){
			return this.model.prioridad() ? this.model.prioridad().Prioridad : "";
		},
		relist: function(){
			$("#addressListResume").kendoRepeater({
				dataSource: incidencias.selected.Localizaciones,
				template: this.temp
			});
			//$("#addressList a").click(this.removeDir.bind(this));
		},
		onGuardar: function(e){
			e.preventDefault();
			if(!incidencias.selected.id){
				incidencias.dataSource.add(incidencias.selected);
			}
			incidencias.dataSource.sync();
			incidencias.selected = null;
			window.app.kendoApp.pane.history = [];
			kendo.history.locations = [];
			window.app.kendoApp.navigate("#incidencias-home");
			if(!window.app.online){
				$("#modal-offline").kendoMobileModalView("open");
			}
		}
	};
	return kendo.observable(vm);
});

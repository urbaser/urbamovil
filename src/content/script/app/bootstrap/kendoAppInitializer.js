define([
	"kendo/kendo.mobile.application",
], function(kendo)
{
	var obj = {
		process: function(app){
			return new Promise(function (resolve, reject) {
				app.kendoApp = new kendo.mobile.Application(document.body,{
					skin: "flat",
					initial: app.getStartPage(),
					init: function(e){
						e.sender.router.bind("change",app.onChangeRoute.bind(app));
						e.sender.router.bind("change",app.onChangeRoute2.bind(app));
						resolve(app);
					}.bind(this)
				});
			});
		}
	};
	return obj;
});

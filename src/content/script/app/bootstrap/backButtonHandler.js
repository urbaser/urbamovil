define([
	"app/functions",
], function(backService)
{
	var obj = {
		process: function(app){
			return new Promise(function (resolve, reject) {
				document.addEventListener("backbutton", function(ev){
					backService.back();
				}.bind(this), false);
				$("#botonAtras").click(function() {
			    backService.back();
			  });
				resolve(app);
			});
		}
	};
	return obj;
});

define([
	'app/services/permissionManager'
], function(manager)
{
	var app;
	var obj = {
		process: function(application){
			return new Promise(function (resolve, reject) {
				app = application;
				if(app.user.profile){
					this.processPage();
				}
				else{
					app.user.loggedIn.add(this.processPage.bind(this));
				}
				resolve(app);
			}.bind(this));
		},
		processPage: function(){
			console.log("Applying permissions");
			$('[data-role-visibility]').each(function(i,el){
				var permission = $(el).attr("data-role-visibility");
				$(el).css("display",manager.hasPermission(permission) ? "" : "none");
			})
			$('[data-role-enabled]').each(function(i,el){
				var permission = $(el).attr("data-role-enabled");
				if(manager.hasPermission(permission)){
					$(el).removeClass("disabled");
				}
				else{
					$(el).addClass("disabled");
				}
			})
		}
	};
	return obj;
});

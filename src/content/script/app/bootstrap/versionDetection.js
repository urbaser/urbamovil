define([
	"app/models/version",
], function(version)
{
	var resolver,rejecter,app;
	var obj = {
		process: function(application){
			return new Promise(function (resolve, reject) {
				app = application;
				resolver = resolve;
				rejecter = reject;

				console.log("Getting version from server");
				version.gotVersion.addOnce(this.onVersionDetected.bind(this));
				version.getVersionFailed.addOnce(this.onVersionDetectionFailed.bind(this));
				version.getVersion();

			}.bind(this));
		},
		onVersionDetected: function(data){
			console.log("info de la versión: " + data.IdEstadoVersion);
			if(data.IdEstadoVersion == "ACTUALIZADA"){
				// Nothing to do
			}else if(data.IdEstadoVersion == "COMPATIBLE" ){
				if(window.cordova){
		      navigator.notification.confirm(T("app.obsolete"), function(buttonIndex){
		        if(buttonIndex == 1){
		          window.open(data.Mensaje, '_system');
		        }
		      }, "Info", ["Aceptar", "Cancelar"]);
		    }else{
		      if(confirm(T("app.obsolete"))){
						window.open(data.Mensaje, '_system');
					}
		    }
			}else if(data.IdEstadoVersion == "OBSOLETA"){
				if(window.cordova){
		      navigator.notification.confirm(T("app.obsolete"), function(buttonIndex){
		        if(buttonIndex == 1){
		          window.open(data.Mensaje, '_system');
		        }
		      }, "Info", ["Aceptar", "Cancelar"]);
		    }else{
		      if(confirm(T("app.obsolete"))){
						window.open(data.Mensaje, '_system');
					}
		    }
				$(document).css("display","none");
			}else{
				console.error("Estado no reconocido: " + data.IdEstadoVersion);
			}
			resolver(app);
		},
		onVersionDetectionFailed: function(data){
			console.error("No se pudo detectar versión");
			rejecter(app);
		}
	};
	return obj;
});

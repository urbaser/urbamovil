define([
	"app/models/incidencias",
	"app/models/inventarios"
], function(incidencias, inventarios)
{
	var obj = {
		process: function(app){
			return new Promise(function (resolve, reject) {
				// Online / Offline handling
				document.addEventListener("offline", function(){
					console.log("offline");
					app.online = false;
					incidencias.offline();
					inventarios.offline();
					$("#modal-offline").kendoMobileModalView("open");
				}, false);
				document.addEventListener("online", function(){
					console.log("online");
					app.online = true;
					incidencias.online();
					inventarios.online();
				}, false);
				if(navigator.connection){
					console.log("Connection type: " + navigator.connection.type);
					if(navigator.connection.type == "none"){
						app.online = false;
					}
				}
				resolve(app);
			});
		}
	};
	return obj;
});

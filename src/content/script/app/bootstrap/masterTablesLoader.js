define([
	"app/models/masterTables",
], function(masterTables)
{
	var obj = {
		process: function(app){
			return new Promise(function (resolve, reject) {
				if(app.user.currentContract != null){
					masterTables.loadLocal(app.user.currentContract.IdContrato);
					$("html")
						.removeClass("contract-urbajardin")
						.removeClass("contract-smartus")
						.addClass(app.user.currentContract.TipoInstalacion == "URBAJARDIN" ? "contract-urbajardin" : "contract-smartus");
					// TODO load remote if empty
				}
				resolve(app);
			});
		}
	};
	return obj;
});

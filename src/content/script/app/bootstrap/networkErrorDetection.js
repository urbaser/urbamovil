define([], function()
{
	var obj = {
		process: function (app) {
			return new Promise(function (resolve, reject) {

				// Global network error handler
				$(document).ajaxError(function( event, jqxhr, settings, thrownError ) {
					// Session timeout detection
					if ( jqxhr.status == 401 ) {
						window.app.kendoApp.navigate("#login");
					}
					// Session timeout detection
					else if ( jqxhr.status == 405 ) {
						app.showMessage(T("app.nopermission"), "Error");
					}
					// Error elemento duplicado
					else if ( jqxhr.status == 500 ) {
						app.showMessage(jqxhr.responseJSON, "Error");
					}
				});
				resolve(app);
			});
		}
	};
	return obj;
});

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioComunUrbamovil;

namespace NegocioComunUrbamovil.Interfaces
{
    public class iInstalacion
    {
        public interface Incidencias 
        {
            List<clsGrupo> ObtenerGrupos(string UrlInstalacion);
            List<clsTipo> ObtenerTipos(string UrlInstalacion, int IdGrupo);
            List<clsDescripcion> ObtenerDescripcion(string UrlInstalacion, int IdTipo);
        }
    }
}

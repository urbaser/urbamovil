﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsEstado
    {
        public Guid GuidEstadoIncidencia { get; set; }
        public int IdEstado { get; set; }
        public string EstadoIncidencia { get; set; }
        public string Observaciones { get; set; }
        public Guid IdInstalacion { get; set; }
        public bool Visible { get; set; }

        public clsEstado()
        {
            Visible = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsDescripcion
    {
        public Guid GuidDescripcionIncidencia { get; set; }
        public int IdDescripcion { get; set; }
        public string DescripcionIncidencia { get; set; }
        public int IdTipoIncidencia { get; set; }
        public Guid IdInstalacion { get; set; }
    }
}

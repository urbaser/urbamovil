﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsIncidenciaHistoricoEstado
    {
        public Guid IdIncidencia { get; set; }
        public int IdEstado { get; set; }
        public DateTime Fecha { get; set; }
        public Guid IdInstalacion { get; set; }
        public string Observaciones { get; set; }
        public string Usuario { get; set; }
    }
}

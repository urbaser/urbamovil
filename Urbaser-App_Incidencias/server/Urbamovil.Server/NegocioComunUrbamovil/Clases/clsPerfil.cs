﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsPerfil
    {
        public Guid IdPerfil { get; set; }
        public string Nombre { get; set; }
        public bool EsAdministrador { get; set; }
        public bool NuevaIncidencia { get; set; }
        public bool ModificarIncidencia { get; set; }
        public bool ListadoIncidencias { get; set; }
        public bool HistoricoIncidencias { get; set; }
        public bool CambioEstadoIncidencia { get; set; }
        public bool ConsultarIncidencia { get; set; }
    }
}

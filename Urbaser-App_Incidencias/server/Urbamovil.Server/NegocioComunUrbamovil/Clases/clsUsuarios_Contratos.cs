﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsUsuarios_Contratos
    {
        public Guid Id { get; set; }
        public Guid IdUsuario { get; set; }
        public Guid IdContrato { get; set; }

        public string NombreContrato { get; set; }

        public clsUsuarios_Contratos()
        {
            NombreContrato = string.Empty;
        }
    }
}

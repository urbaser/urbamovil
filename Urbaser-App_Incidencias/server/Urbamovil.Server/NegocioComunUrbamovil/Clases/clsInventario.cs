﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsInventario
    {
        public List<clsTiposElemento> LocalizacionTiposElementos { get; set; }
        public List<clsDistritos> LocalizacionDistritos { get; set; }
        public List<clsEspaciosUrbanos> LocalizacionEspaciosUrbanos { get; set; }
        public List<clsZonas> LocalizacionZonas { get; set; }
        public List<clsRutas> LocalizacionRutas { get; set; }

        public clsInventario()
        {
            LocalizacionTiposElementos = new List<clsTiposElemento>();
            LocalizacionDistritos = new List<clsDistritos>();
            LocalizacionEspaciosUrbanos = new List<clsEspaciosUrbanos>();
            LocalizacionZonas = new List<clsZonas>();
            LocalizacionRutas = new List<clsRutas>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsLocalizacionesIncidencias
    {
        public Guid IdLocalizacion { get; set; }
        public Guid IdIncidencia { get; set; }
        public int IdTipoLocalizacion { get; set; }
        public double? Map_Latitud { get; set; }
        public double? Map_Longitud { get; set; }
        public string Dir_Calle { get; set; }
        public int? Dir_Numero { get; set; }
        public int? Inv_IdDistrito { get; set; }
        public int? Inv_IdEspacioUrbano { get; set; }
        public int? Inv_IdZona { get; set; }
        public int? Inv_IdRuta { get; set; }
        public int? Inv_IdArea { get; set; }
        public int? Inv_IdTipoElemento { get; set; }
        public int? Inv_IdElemento { get; set; }
        public string Codigo { get; set; }
        public Guid IdInstalacion { get; set; }
        public string Descripcion { get; set; }
    }
}

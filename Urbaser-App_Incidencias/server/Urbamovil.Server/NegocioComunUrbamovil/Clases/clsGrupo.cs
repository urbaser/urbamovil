﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsGrupo
    {
        public Guid GuidGrupoIncidencia { get; set; }
        public int IdGrupo { get; set; }
        public string GrupoIncidencia { get; set; }
        public Guid IdInstalacion { get; set; }
    }
}

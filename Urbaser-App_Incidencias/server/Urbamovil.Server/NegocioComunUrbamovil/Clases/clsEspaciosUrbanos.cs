﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsEspaciosUrbanos
    {
        public int IdEspacioUrbano { get; set; }
        public string EspacioUrbano { get; set; }
        public int IdDistrito { get; set; }
        public Guid IdInstalacion { get; set; }
    }
}

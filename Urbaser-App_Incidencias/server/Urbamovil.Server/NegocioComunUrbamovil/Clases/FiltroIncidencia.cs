﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class FiltroIncidencia
    {
        public int? IdEstado { get; set; }
        public int? IdGrupo { get; set; }
        public int? IdTipo { get; set; }
        public int? IdDescripcion { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public Guid? IdUsuarioAlta { get; set; }
        public Guid? IdContrato { get; set; }
        public int? IdPrioridad { get; set; }
        public int? IdProvincia { get; set; }
        public int? IdMunicipio { get; set; }
        public string Direccion { get; set; }
        public int? IdTipoElemento { get; set; }
        public int? IdElemento { get; set; }
        public int? Take { get; set; }

        public bool HasValue()
        {
            if (IdEstado.HasValue) return true;
            if (IdGrupo.HasValue) return true;
            if (IdTipo.HasValue) return true;
            if (IdDescripcion.HasValue) return true;
            if (FechaDesde.HasValue) return true;
            if (FechaHasta.HasValue) return true;
            if (IdContrato.HasValue) return true;
            if (IdUsuarioAlta.HasValue) return true;
            if (IdPrioridad.HasValue) return true;
            if (IdProvincia.HasValue) return true;
            if (IdMunicipio.HasValue) return true;
            if (!string.IsNullOrWhiteSpace(Direccion)) return true;
            if (IdTipoElemento.HasValue) return true;
            if (IdElemento.HasValue) return true;
            if (Take.HasValue) return true;
            return false;
        }

        public void SetCampo(FiltroCampo f)
        {
            switch (f.NombreCampo)
            {
                case Campos.ESTADO_INCIDENCIA:
                    if (f.Tipo == Tipos.INTEGER)
                    {
                        int i = 0;
                        if (int.TryParse(f.Valor, out i))
                        {
                            IdEstado = i;
                        }
                    }
                    break;
                case Campos.GRUPO_TIPO_DESCRIPCION:
                    string[] args = f.Valor.Split(',');
                    if (args.Length >= 1 && args.Length <= 3)
                    {
                        int i = 0;
                        if (int.TryParse(args[0],out i))
                        {
                            IdGrupo = i;
                        }
                        if (args.Length > 1)
                        {
                            if (int.TryParse(args[1], out i))
                            {
                                IdTipo = i;
                            }
                            if (args.Length == 3)
                            {
                                if (int.TryParse(args[2], out i))
                                {
                                    IdDescripcion = i;
                                }
                            }
                        }
                    }
                    break;
                case Campos.FECHA_DESDE:
                    if (f.Tipo == Tipos.DATE)
                    {
                        DateTime d = DateTime.Now;
                        if (DateTime.TryParse(f.Valor, out d))
                        {
                            FechaDesde = d;
                        }
                    }
                    break;
                case Campos.FECHA_HASTA:
                    if (f.Tipo == Tipos.DATE)
                    {
                        DateTime d = DateTime.Now;
                        if (DateTime.TryParse(f.Valor, out d))
                        {
                            FechaHasta = d;
                        }
                    }
                    break;
                case Campos.ULTIMOS_DIAS:
                    if (f.Tipo == Tipos.INTEGER)
                    {
                        int i = 0;
                        if (int.TryParse(f.Valor, out i))
                        {
                            Take = i;
                        }
                    }
                    break;
                case Campos.CONTRATO:
                    if (f.Tipo == Tipos.GUID)
                    {
                        Guid g = Guid.Empty;
                        if (Guid.TryParse(f.Valor, out g))
                        {
                            IdContrato = g;
                        }
                    }
                    break;
                case Campos.PRIORIDAD:
                    if (f.Tipo == Tipos.INTEGER)
                    {
                        int i = 0;
                        if (int.TryParse(f.Valor, out i))
                        {
                            IdPrioridad = i;
                        }
                    }
                    break;
                case Campos.PROVINCIA_MUNICIPIO:
                    string[] array = f.Valor.Split(',');
                    if (array.Length >= 1 && array.Length <= 2)
                    {
                        int i = 0;
                        if (int.TryParse(array[0], out i))
                        {
                            IdProvincia = i;
                        }
                        if (array.Length == 2)
                        {
                            if (int.TryParse(array[1], out i))
                            {
                                IdMunicipio = i;
                            }
                        }
                    }
                    break;
                case Campos.DIRECCION:
                    if (f.Tipo == Tipos.TEXTO)
                    {
                        if (!string.IsNullOrWhiteSpace(f.Valor))
                        {
                            Direccion = f.Valor;
                        }
                    }
                    break;
                case Campos.ELEMENTO_INVENTARIO:
                    string[] ar = f.Valor.Split(',');
                    if (ar.Length >= 1 && ar.Length <= 2)
                    {
                        int i = 0;
                        if (int.TryParse(ar[0], out i))
                        {
                            IdTipoElemento = i;
                        }
                        if (ar.Length == 2)
                        {
                            if (int.TryParse(ar[1], out i))
                            {
                                IdElemento = i;
                            }
                        }
                    }
                    break;
            }
        }
    }
}

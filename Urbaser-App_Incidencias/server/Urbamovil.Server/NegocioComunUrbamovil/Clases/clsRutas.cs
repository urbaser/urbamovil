﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsRutas
    {
        public int IdRuta { get; set; }
        public string Ruta { get; set; }
        public Guid IdInstalacion { get; set; }
        public int IdEspacioUrbano { get; set; }
    }
}

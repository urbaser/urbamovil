﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsVersiones
    {
        public int IdVersion { get; set; }
        public string Codigo { get; set; }
        public string Sistema { get; set; }
        public string IdEstadoVersion { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public string Mensaje { get; set; }
    }
}

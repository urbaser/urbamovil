﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsTiposElemento
    {
        public int IdTipoElemento { get; set; }
        public string Tipo { get; set; }
        public Guid IdInstalacion { get; set; }
    }
}

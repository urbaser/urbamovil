﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsIdiomas
    {
        public int IdIdioma { get; set; }
        public string Idioma { get; set; }
        public string Codigo { get; set; }
        public bool PorDefecto { get; set; }
    }
}

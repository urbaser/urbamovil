﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsPrioridad
    {
        public int IdPrioridad { get; set; }
        public string Prioridad { get; set; }
        public Guid IdInstalacion { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsMunicipio
    {
        public int IdMunicipio { get; set; }
        public string NombreMunicipio { get; set; }
        public int IdProvincia { get; set; }
        public Guid IdInstalacion { get; set; }
    }
}

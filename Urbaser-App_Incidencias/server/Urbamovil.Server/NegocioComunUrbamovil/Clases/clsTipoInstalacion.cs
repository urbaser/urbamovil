﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsTipoInstalacion
    {
        public Guid IdTipoInstalacion { get; set; }
        public string Nombre { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NegocioComunUrbamovil
{
    public class FiltroCampo
    {
        public string NombreCampo { get; set; }
        public string Tipo { get; set; }
        public string Valor { get; set; }
    }
}

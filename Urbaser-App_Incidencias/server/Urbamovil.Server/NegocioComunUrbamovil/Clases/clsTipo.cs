﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsTipo
    {
        public Guid GuidTipoIncidencia { get; set; }
        public int IdTipo { get; set; }
        public string TipoIncidencia { get; set; }
        public int IdGrupoIncidencia { get; set; }
        public Guid IdInstalacion { get; set; }
    }
}

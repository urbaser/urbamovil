﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsProvincia
    {
        public int IdProvincia { get; set; }
        public string NombreProvincia { get; set; }
        public Guid IdInstalacion { get; set; }
    }
}

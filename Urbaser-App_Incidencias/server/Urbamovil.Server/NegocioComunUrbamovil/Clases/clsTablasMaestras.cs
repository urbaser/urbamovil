﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsTablasMaestras
    {
        public List<clsGrupo> Grupos { get; set; }
        public List<clsTipo> Tipos { get; set; }
        public List<clsDescripcion> Descripciones { get; set; }
        public List<clsEstado> Estados { get; set; }
        public List<clsPrioridad> Prioridades { get; set; }
        public List<clsProvincia> Provincias { get; set; }
        public List<clsMunicipio> Municipios { get; set; }

        public clsTablasMaestras()
        {
            Grupos = new List<clsGrupo>();
            Tipos = new List<clsTipo>();
            Descripciones = new List<clsDescripcion>();
            Estados = new List<clsEstado>();
            Prioridades = new List<clsPrioridad>();
            Provincias = new List<clsProvincia>();
            Municipios = new List<clsMunicipio>();
        }
    }
}

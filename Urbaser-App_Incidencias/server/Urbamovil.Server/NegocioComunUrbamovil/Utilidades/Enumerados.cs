﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public enum Operacion
    {
        INSERT = 1,
        UPDATE = 2,
        DELETE = 3
    }

    public enum Idioma
    {
        ES = 1,
        FR = 2,
        EN = 3
    }

    public enum EstadoIncidencia
    {
        PENDIENTE = 1,
        ACEPTADA = 2,
        RECHAZADA = 3,
        CERRADA = 4,
        EN_PROCESO = 5
    }

    public enum TipoLocalizacion
    {
        MAPA = 1,
        DIRECCION = 2,
        INVENTARIO = 3,
        TAG = 4
    }
}

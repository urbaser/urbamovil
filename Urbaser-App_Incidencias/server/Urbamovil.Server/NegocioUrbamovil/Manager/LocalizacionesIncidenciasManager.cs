﻿using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioUrbamovil.Manager
{
    public class LocalizacionesIncidenciasManager : BaseManager
    {
        public LocalizacionesIncidenciasManager() : base() { }

        public LocalizacionesIncidenciasManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public List<Localizaciones_Incidencia> ObtenerPorIncidencia(Guid IdIncidencia)
        {
            var list = Contexto.Localizaciones_Incidencia.Where(l => l.IdIncidencia == IdIncidencia);

            return list.ToList();
        }

        public void Guardar(Localizaciones_Incidencia entidad)
        {
            Localizaciones_Incidencia localizacion = Contexto.Localizaciones_Incidencia.FirstOrDefault(l => l.IdLocalizacion == entidad.IdLocalizacion);

            if (localizacion == null)
            {
                Contexto.Localizaciones_Incidencia.Add(entidad);
            }
            else
            {
                localizacion.IdIncidencia = entidad.IdIncidencia;
                localizacion.IdTipoLocalizacion = entidad.IdTipoLocalizacion;
                localizacion.Map_Latitud = entidad.Map_Latitud;
                localizacion.Map_Longitud = entidad.Map_Longitud;
                localizacion.Dir_Calle = entidad.Dir_Calle;
                localizacion.Dir_Numero = entidad.Dir_Numero;
                localizacion.Inv_IdDistrito = entidad.Inv_IdDistrito;
                localizacion.Inv_IdEspacioUrbano = entidad.Inv_IdEspacioUrbano;
                localizacion.Inv_IdZona = entidad.Inv_IdZona;
                localizacion.Inv_IdRuta = entidad.Inv_IdRuta;
                localizacion.Inv_IdArea = entidad.Inv_IdArea;
                localizacion.Inv_IdTipoElemento = entidad.Inv_IdTipoElemento;
                localizacion.Inv_IdElemento = entidad.Inv_IdElemento;
                localizacion.IdInstalacion = entidad.IdInstalacion;
            }

            Contexto.SaveChanges();
        }

        public void Guardar(Guid IdIncidencia, List<Localizaciones_Incidencia> entidades)
        {
            var list = Contexto.Localizaciones_Incidencia.Where(l => l.IdIncidencia == IdIncidencia);
            if (list != null)
            {
                foreach (Localizaciones_Incidencia item in list)
                {
                    Contexto.Localizaciones_Incidencia.Remove(item);
                }
            }

            foreach(Localizaciones_Incidencia entidad in entidades)
            {
                Contexto.Localizaciones_Incidencia.Add(entidad);
            }

            Contexto.SaveChanges();
        }
    }
}

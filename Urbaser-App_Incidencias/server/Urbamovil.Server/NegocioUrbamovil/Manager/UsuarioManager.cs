﻿using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioUrbamovil.Manager
{
    public class UsuarioManager : BaseManager
    {
        #region Constructora

        public UsuarioManager() : base() { }

        public UsuarioManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        #endregion

        #region Métodos públicos

        /// <summary>
        /// Obtiene la lista de todos los usuarios
        /// </summary>
        /// <returns>Lista de usuarios</returns>
        public List<Usuarios> ObtenerUsuarios()
        {
            try
            {
                return Contexto.Usuarios.Include("TiposPerfilUsuario").Include("Idiomas").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Comprueba la existencia de un usuario con el login y la contraseña. 
        /// </summary>
        /// <param name="Login">Login del usuario</param>
        /// <param name="Pass">Contraseña del usuario</param>
        /// <param name="horas">Tiempo de aumento para la caducidad del ticket (en horas)</param>
        /// <returns>En caso de existir, se devuelve el usuario con un ticket asignado</returns>
        public Usuarios ComprobarUsuario(string Login, string Pass)
        {
            Usuarios usuario = Contexto.Usuarios.Include("TiposPerfilUsuario").Include("Idiomas").FirstOrDefault(u => u.Login.Equals(Login) && u.Pass.Equals(Pass));
                
            return usuario; 
        }

        public Usuarios ComprobarUsuarioPorCorreo(string correo)
        {
            Usuarios usuario = Contexto.Usuarios.Include("TiposPerfilUsuario").Include("Idiomas").FirstOrDefault(u => u.Email.Equals(correo));
            
            return usuario; 
        }

        public Usuarios ComprobarUsuarioPorCorreoContraClave(string correo, string contraClave)
        {
            Usuarios usuario = Contexto.Usuarios.Include("TiposPerfilUsuario").Include("Idiomas").FirstOrDefault(u => u.Email.Equals(correo) && u.ContraClave.Equals(contraClave));

            return usuario;
        }

        /// <summary>
        /// Devuleve un usuario, si existe, por su identificador
        /// </summary>
        /// <param name="id">Identificador del usuario</param>
        /// <returns>Si existe, devuelve el usuario</returns>
        public Usuarios ObtenerUsuarioPorId(Guid id)
        {
            try
            {
                Usuarios usuario = Contexto.Usuarios.FirstOrDefault(u => u.IdUsuario == id);

                if (usuario == null)
                {
                    throw new Exception("ERROR: El usuario no existe.");
                }

                return usuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Comprobamos la existencia del ticket, en caso afirmativo se verifica el usuario
        /// al que esta asignado, si el usuario existe, se actualiza la fecha de caducidad del
        /// ticket.
        /// </summary>
        /// <param name="Ticket">Ticket buscado</param>
        /// <param name="horas">Tiempo de aumento para la caducidad del ticket (en horas)</param>
        /// <returns>En caso de existir, devuelve el usuario que tiene asignado dicho ticket</returns>
        public Usuarios ObtenerUsuarioPorTicket(Guid Ticket, int horas)
        {
            try
            {
                Usuarios usuario = null;
                TicketUsuario ticket = Contexto.TicketUsuario.FirstOrDefault(t => t.Ticket == Ticket);

                if (ticket != null && DateTime.Now <= ticket.FechaCaducidad)
                {
                    usuario = Contexto.Usuarios.FirstOrDefault(u => u.IdUsuario == ticket.IdUsuario);
                    if (usuario != null)
                    {
                        ticket.FechaCaducidad = DateTime.Now.AddHours(2);
                        Contexto.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("ERROR DE AUTENTICACIÓN DEL USUARIO: No existe un usuario con dicho ticket.");
                    }
                }
                else
                {
                    throw new Exception("ERROR DE AUTENTICACIÓN DE TICKET: No existe el ticket o esta caducado.");
                }

                return usuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Buscamos al usuario que tenga asignado el Ticket
        /// </summary>
        /// <param name="Ticket">Ticket para identificar al usuario</param>
        /// <returns>El usuario que tiene asignado dicho ticket</returns>
        public Usuarios ObtenerUsuarioPorTicket(Guid Ticket)
        {
            Usuarios usuario = null;
            TicketUsuario ticket = Contexto.TicketUsuario.FirstOrDefault(t => t.Ticket == Ticket);

            if (ticket != null && DateTime.Now <= ticket.FechaCaducidad)
            {
                usuario = Contexto.Usuarios.FirstOrDefault(u => u.IdUsuario == ticket.IdUsuario);
            }

            return usuario;
        }

        /// <summary>
        /// Buscamos un usuario por el login que tiene
        /// </summary>
        /// <param name="Login">Login de usuario</param>
        /// <returns>En caso de existir, devuelve el usuario con dicho login</returns>
        public Usuarios ObtenerUsuarioPorLogin(string Login)
        {
            try
            {
                Usuarios usuario = Contexto.Usuarios.Include("TiposPerfilUsuario").FirstOrDefault(u => u.Login.Equals(Login));

                if (usuario == null)
                {
                    throw new Exception("ERROR: El usuario " + Login + " no existe en el sistema.");
                }

                return usuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Actualizamos un usuario
        /// </summary>
        /// <param name="nuevoUsuario">El usuario que actualizado</param>
        public void ActualizarUsuario(Usuarios nuevoUsuario)
        {
            try
            {
                Usuarios usuario = ObtenerUsuarioPorId(nuevoUsuario.IdUsuario);
                if ( usuario != null)
                {
                    usuario.Nombre = nuevoUsuario.Nombre;
                    usuario.Apellido1 = nuevoUsuario.Apellido1;
                    usuario.Apellido2 = nuevoUsuario.Apellido2;
                    usuario.Direccion = nuevoUsuario.Direccion;
                    usuario.Empresa = nuevoUsuario.Empresa;
                    usuario.Email = nuevoUsuario.Email;
                    usuario.Telefono = nuevoUsuario.Telefono;
                    usuario.TelefonoMovil = nuevoUsuario.TelefonoMovil;
                    usuario.Login = nuevoUsuario.Login;
                    usuario.IdPerfil = nuevoUsuario.IdPerfil;
                    usuario.IPDireccion = nuevoUsuario.IPDireccion;
                    usuario.IdIdioma = nuevoUsuario.IdIdioma;
                    usuario.Activo = nuevoUsuario.Activo;
                    usuario.FechaValidezCC = nuevoUsuario.FechaValidezCC;
                    usuario.ContraClave = nuevoUsuario.ContraClave;

                    Contexto.SaveChanges();
                }
                else
                {
                    throw new Exception("ERROR ACTUALIZAR USUARIO: el usuario que se quiere actualizar no existe.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CambiarPass(Guid idUsuario, string newPass)
        {
            try
            {
                Usuarios usuario = ObtenerUsuarioPorId(idUsuario);

                if (usuario != null)
                {
                    usuario.Pass = newPass;

                    Contexto.SaveChanges();
                }
                else
                {
                    throw new Exception("ERROR ELIMINAR: el usuario no existe.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Eliminamos el usuario con identificador idUsuario
        /// </summary>
        /// <param name="idUsuario">Identificador del usuario que queremos eliminar</param>
        public void EliminarUsuario(Guid idUsuario)
        {
            try
            {
                Usuarios usuario = ObtenerUsuarioPorId(idUsuario);

                if (usuario != null)
                {
                    Contexto.Usuarios.Remove(usuario);

                    Contexto.SaveChanges();
                }
                else
                {
                    throw new Exception("ERROR ELIMINAR: el usuario no existe.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// En caso de nos existir, guardamos un nuevo usuario
        /// </summary>
        /// <param name="usuario">Nuevo usuario</param>
        public void GuardarUsuario(Usuarios usuario)
        {
            if (!ExiteUsuario(usuario.Login, usuario.Pass))
            {
                Contexto.Usuarios.Add(usuario);
            }

            Contexto.SaveChanges();
        }

        #endregion

        #region Métodos privados

        /// <summary>
        /// Comprobamos la existencia de un usuario por su login y contraseña
        /// </summary>
        /// <param name="login">Login del usuario</param>
        /// <param name="pass">Contraseña del usuario</param>
        /// <returns>True en caso de que el usuario exista, False en caso contrario</returns>
        private bool ExiteUsuario(string login, string pass)
        {
            return Contexto.Usuarios.FirstOrDefault(u => u.Login.Equals(login) && u.Pass.Equals(pass)) != null;
        }

        #endregion
    }
}

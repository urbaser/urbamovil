﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioUrbamovil.Modelo;

namespace NegocioUrbamovil.Manager
{
    public class MensajesManager : BaseManager
    {
        public MensajesManager() : base() { }

        public MensajesManager(DbUrbamovil_DevEntities Context) : base(Context) { }

        public Mensajes ObtenerMensaje(int IdIdioma, string IdEstadoVersion)
        {
            return Contexto.Mensajes.FirstOrDefault(m => m.IdIdioma == IdIdioma && m.IdEstadoVersion == IdEstadoVersion);
        }
    }
}

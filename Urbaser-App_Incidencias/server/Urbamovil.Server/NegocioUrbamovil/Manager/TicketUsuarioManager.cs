﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioUrbamovil.Modelo;

namespace NegocioUrbamovil.Manager
{
    public class TicketUsuarioManager : BaseManager
    {
        public TicketUsuarioManager() : base() { }

        public TicketUsuarioManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public TicketUsuario Obtener(Guid id)
        {
            return Contexto.TicketUsuario.FirstOrDefault(t => t.Ticket == id);
        }

        public TicketUsuario GenerarTicketPorUsuario(Guid IdUsuario, int horas)
        {
            try
            {
                TicketUsuario ticket = new TicketUsuario();
                ticket.Ticket = Guid.NewGuid();
                ticket.IdUsuario = IdUsuario;
                ticket.FechaCaducidad = DateTime.Now.AddHours(horas);
                ticket.FechaLogin = DateTime.Now;

                Contexto.TicketUsuario.Add(ticket);
                Contexto.SaveChanges();

                return ticket;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public TicketUsuario GenerarTicketPorCiudadano(Guid IdUsuario)
        {
            TicketUsuario ticket = new TicketUsuario();
            ticket.Ticket = Guid.NewGuid();
            ticket.IdUsuario = IdUsuario;
            ticket.FechaCaducidad = DateTime.Now.AddYears(1);
            ticket.FechaLogin = DateTime.Now;

            Contexto.TicketUsuario.Add(ticket);
            Contexto.SaveChanges();

            return ticket;
        }

        public void AumentarCaducidadTicket(Guid IdTicket, int horas)
        {
            try
            {
                TicketUsuario ticket = Contexto.TicketUsuario.FirstOrDefault(t => t.Ticket == IdTicket);

                ticket.FechaCaducidad = DateTime.Now.AddHours(horas);

                Contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GuardarContratoEnTicket(Guid IdTicket, Guid IdContrato)
        {
            TicketUsuario ticket = Contexto.TicketUsuario.FirstOrDefault(t => t.Ticket == IdTicket);

            ticket.IdContrato = IdContrato;

            Contexto.SaveChanges();
        }

        public void EliminarPorUsuario(Guid IdUsuario)
        {
            try
            {
                List<TicketUsuario> listTickets = Contexto.TicketUsuario.Where(t => t.IdUsuario == IdUsuario).ToList();

                foreach (TicketUsuario ticket in listTickets)
                {
                    Contexto.TicketUsuario.Remove(ticket);
                }

                Contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

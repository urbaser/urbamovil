﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioComunUrbamovil.Interfaces;
using NegocioUrbamovil.Modelo;

namespace NegocioUrbamovil.Manager
{
    public abstract class BaseInstalacionManager
    {
       // protected iInstalacion.Incidencias _manager;
        protected string sUrlinstalacion = string.Empty;

        public BaseInstalacionManager(Guid IdInstalacion)
        {
            try
            {
                InstalacionesManager manager = new InstalacionesManager();
                Instalaciones entidad = manager.ObtenerInstalacion(IdInstalacion);

                if (entidad != null)
                {
                    sUrlinstalacion = entidad.URL;

                    //switch (entidad.TiposInstalacion.Nombre)
                    //{
                    //    case "JARDINERIA":
                    //        _manager = new NegocioURBAJARDIN.Manager();
                    //        break;
                    //    case "AWSGU":
                    //        _manager = new NegocioAWGSU.Manager();
                    //        break;
                    //    default:
                    //        throw new Exception("Aplicacion no encontrada en la lista");
                    //}
                }
                else
                {
                    throw new Exception(string.Format("ERROR: No existe la instalacion con identificador {0}", IdInstalacion));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

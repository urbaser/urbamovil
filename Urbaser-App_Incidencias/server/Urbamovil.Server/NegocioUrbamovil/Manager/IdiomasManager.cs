﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioUrbamovil.Modelo;

namespace NegocioUrbamovil.Manager
{
    public class IdiomasManager : BaseManager
    {
        public IdiomasManager() : base() { }

        public IdiomasManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public List<Idiomas> ObtenerIdiomas()
        {
            return Contexto.Idiomas.ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioUrbamovil.Modelo;

namespace NegocioUrbamovil.Manager
{
    public class IncidenciaHistoricoEstadoManager : BaseManager
    {
        public IncidenciaHistoricoEstadoManager() : base() { }

        public IncidenciaHistoricoEstadoManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public List<Incidencias_HistoricoEstado> Obtener(Guid IdIncidencia)
        {
            return Contexto.Incidencias_HistoricoEstado.Where(h => h.IdIncidencia == IdIncidencia).ToList();
        }

        public void Guardar(Incidencias_HistoricoEstado entidad)
        {
           if (!Existe(entidad))
           {
               Contexto.Incidencias_HistoricoEstado.Add(entidad);

               Contexto.SaveChanges();
           }
        }

        public bool Existe(Incidencias_HistoricoEstado entidad)
        {
            return Contexto.Incidencias_HistoricoEstado.Any(h => h.IdIncidencia == entidad.IdIncidencia && h.Fecha == entidad.Fecha);
        }
    }
}

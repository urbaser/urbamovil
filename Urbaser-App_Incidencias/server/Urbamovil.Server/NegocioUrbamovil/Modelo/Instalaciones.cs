//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NegocioUrbamovil.Modelo
{
    using System;
    using System.Collections.Generic;
    
    public partial class Instalaciones
    {
        public Instalaciones()
        {
            this.Contratos = new HashSet<Contratos>();
            this.Elementos = new HashSet<Elementos>();
            this.Incidencia_Descripciones = new HashSet<Incidencia_Descripciones>();
            this.Incidencia_Grupos = new HashSet<Incidencia_Grupos>();
            this.Incidencia_Prioridades = new HashSet<Incidencia_Prioridades>();
            this.Incidencia_Tipos = new HashSet<Incidencia_Tipos>();
            this.Incidencias = new HashSet<Incidencias>();
            this.Localizaciones_Distritos = new HashSet<Localizaciones_Distritos>();
            this.Localizaciones_Incidencia = new HashSet<Localizaciones_Incidencia>();
            this.Municipios = new HashSet<Municipios>();
            this.Provincias = new HashSet<Provincias>();
        }
    
        public System.Guid IdInstalacion { get; set; }
        public string Nombre { get; set; }
        public string URL { get; set; }
        public string URL_MAPA_AGOL { get; set; }
        public string URL_WEBSERVICES { get; set; }
        public System.Guid IdTipoInstalacion { get; set; }
        public string DistromelIMEI { get; set; }
        public string DistromelSECTION { get; set; }
    
        public virtual ICollection<Contratos> Contratos { get; set; }
        public virtual ICollection<Elementos> Elementos { get; set; }
        public virtual ICollection<Incidencia_Descripciones> Incidencia_Descripciones { get; set; }
        public virtual ICollection<Incidencia_Grupos> Incidencia_Grupos { get; set; }
        public virtual ICollection<Incidencia_Prioridades> Incidencia_Prioridades { get; set; }
        public virtual ICollection<Incidencia_Tipos> Incidencia_Tipos { get; set; }
        public virtual ICollection<Incidencias> Incidencias { get; set; }
        public virtual TiposInstalacion TiposInstalacion { get; set; }
        public virtual ICollection<Localizaciones_Distritos> Localizaciones_Distritos { get; set; }
        public virtual ICollection<Localizaciones_Incidencia> Localizaciones_Incidencia { get; set; }
        public virtual ICollection<Municipios> Municipios { get; set; }
        public virtual ICollection<Provincias> Provincias { get; set; }
    }
}

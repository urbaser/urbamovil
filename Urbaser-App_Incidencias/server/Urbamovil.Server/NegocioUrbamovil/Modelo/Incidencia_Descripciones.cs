//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NegocioUrbamovil.Modelo
{
    using System;
    using System.Collections.Generic;
    
    public partial class Incidencia_Descripciones
    {
        public Incidencia_Descripciones()
        {
            this.Incidencias = new HashSet<Incidencias>();
        }
    
        public System.Guid GuidDescripcionIncidencia { get; set; }
        public int IdDescripcion { get; set; }
        public string DescripcionIncidencia { get; set; }
        public int IdTipoIncidencia { get; set; }
        public System.Guid IdInstalacion { get; set; }
    
        public virtual Instalaciones Instalaciones { get; set; }
        public virtual ICollection<Incidencias> Incidencias { get; set; }
    }
}

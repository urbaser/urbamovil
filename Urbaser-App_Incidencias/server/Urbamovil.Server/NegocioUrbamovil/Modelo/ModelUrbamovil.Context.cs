﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NegocioUrbamovil.Modelo
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class DbUrbamovil_DevEntities : DbContext
    {
        public DbUrbamovil_DevEntities()
            : base("name=DbUrbamovil_DevEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Contratos> Contratos { get; set; }
        public virtual DbSet<Elementos> Elementos { get; set; }
        public virtual DbSet<EstadoVersion> EstadoVersion { get; set; }
        public virtual DbSet<FiltroUsuario> FiltroUsuario { get; set; }
        public virtual DbSet<HistoricoIncidencias> HistoricoIncidencias { get; set; }
        public virtual DbSet<Idiomas> Idiomas { get; set; }
        public virtual DbSet<Incidencia_Descripciones> Incidencia_Descripciones { get; set; }
        public virtual DbSet<Incidencia_Estados> Incidencia_Estados { get; set; }
        public virtual DbSet<Incidencia_Grupos> Incidencia_Grupos { get; set; }
        public virtual DbSet<Incidencia_Prioridades> Incidencia_Prioridades { get; set; }
        public virtual DbSet<Incidencia_Sources> Incidencia_Sources { get; set; }
        public virtual DbSet<Incidencia_Tipos> Incidencia_Tipos { get; set; }
        public virtual DbSet<Incidencias> Incidencias { get; set; }
        public virtual DbSet<Incidencias_HistoricoEstado> Incidencias_HistoricoEstado { get; set; }
        public virtual DbSet<Incidencias_Sicronizacion> Incidencias_Sicronizacion { get; set; }
        public virtual DbSet<Instalaciones> Instalaciones { get; set; }
        public virtual DbSet<Localizaciones_Distritos> Localizaciones_Distritos { get; set; }
        public virtual DbSet<Localizaciones_EspaciosUrbanos> Localizaciones_EspaciosUrbanos { get; set; }
        public virtual DbSet<Localizaciones_Incidencia> Localizaciones_Incidencia { get; set; }
        public virtual DbSet<Localizaciones_Rutas> Localizaciones_Rutas { get; set; }
        public virtual DbSet<Localizaciones_TiposElemento> Localizaciones_TiposElemento { get; set; }
        public virtual DbSet<Localizaciones_Zonas> Localizaciones_Zonas { get; set; }
        public virtual DbSet<Mensajes> Mensajes { get; set; }
        public virtual DbSet<Municipios> Municipios { get; set; }
        public virtual DbSet<OperacionDestino> OperacionDestino { get; set; }
        public virtual DbSet<Provincias> Provincias { get; set; }
        public virtual DbSet<TicketUsuario> TicketUsuario { get; set; }
        public virtual DbSet<TiposInstalacion> TiposInstalacion { get; set; }
        public virtual DbSet<TiposLocalizacion> TiposLocalizacion { get; set; }
        public virtual DbSet<TiposPerfilUsuario> TiposPerfilUsuario { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<Usuarios_Contratos> Usuarios_Contratos { get; set; }
        public virtual DbSet<Versiones> Versiones { get; set; }
        public virtual DbSet<VSTextos> VSTextos { get; set; }
    }
}

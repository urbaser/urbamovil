﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RepositorioUrbamovil.Repositorios;
using NegocioComunUrbamovil;

namespace RepositorioUrbamovil.Unidad
{
    public class UnitOfWork
    {
        private string URLService;
        private string APIUsuario;
        private string APIContratos;
        private string APIPerfil;
        private string APIInstalaciones;
        private string APITipoInstalaciones;
        private string APIUsuariosContratos;
        private string APIIdioma;
        private string APICambiarPass;

        private Repositorio<clsUsuarios> _UnitUsuario;
        public Repositorio<clsUsuarios> UnitUsuario
        {
            get 
            {
                if (_UnitUsuario == null) _UnitUsuario = new Repositorio<clsUsuarios>(URLService, APIUsuario);
                return _UnitUsuario;
            }
        }

        private Repositorio<clsContratos> _UnitContratos;
        public Repositorio<clsContratos> UnitContratos
        {
            get
            {
                if (_UnitContratos == null) _UnitContratos = new Repositorio<clsContratos>(URLService, APIContratos);
                return _UnitContratos;
            }
        }

        private Repositorio<clsPerfil> _UnitPerfil;
        public Repositorio<clsPerfil> UnitPerfil
        {
            get
            {
                if (_UnitPerfil == null) _UnitPerfil = new Repositorio<clsPerfil>(URLService, APIPerfil);
                return _UnitPerfil;
            }
        }

        private Repositorio<clsInstalaciones> _UnitInstalaciones;
        public Repositorio<clsInstalaciones> UnitInstalaciones
        {
            get
            {
                if (_UnitInstalaciones == null) _UnitInstalaciones = new Repositorio<clsInstalaciones>(URLService, APIInstalaciones);
                return _UnitInstalaciones;
            }
        }

        private Repositorio<clsTipoInstalacion> _UnitTipoInstalaciones;
        public Repositorio<clsTipoInstalacion> UnitTipoInstalaciones
        {
            get
            {
                if (_UnitTipoInstalaciones == null) _UnitTipoInstalaciones = new Repositorio<clsTipoInstalacion>(URLService, APITipoInstalaciones);
                return _UnitTipoInstalaciones;
            }
        }

        private Repositorio<clsUsuarios_Contratos> _UnitUsuariosContratos;
        public Repositorio<clsUsuarios_Contratos> UnitUsuariosContratos
        {
            get
            {
                if (_UnitUsuariosContratos == null) _UnitUsuariosContratos = new Repositorio<clsUsuarios_Contratos>(URLService, APIUsuariosContratos);
                return _UnitUsuariosContratos;
            }
        }

        private Repositorio<clsIdiomas> _UnitIdiomas;
        public Repositorio<clsIdiomas> UnitIdiomas
        {
            get
            {
                if (_UnitIdiomas == null) _UnitIdiomas = new Repositorio<clsIdiomas>(URLService, APIIdioma);
                return _UnitIdiomas;
            }
        }

        private Repositorio<clsPass> _UnitCambiarPass;
        public Repositorio<clsPass> UnitCambiarPass
        {
            get
            {
                if (_UnitCambiarPass == null) _UnitCambiarPass = new Repositorio<clsPass>(URLService, APICambiarPass);
                return _UnitCambiarPass;
            }
        }

        public UnitOfWork()
        {
            AppSettingsReader appReader = new AppSettingsReader();

            URLService = appReader.GetValue("KEY_URL_SERVICIO_REST", typeof(string)).ToString();
            APIUsuario = appReader.GetValue("KEY_API_USUARIO", typeof(string)).ToString();
            APIContratos = appReader.GetValue("KEY_API_CONTRATOS", typeof(string)).ToString();
            APIPerfil = appReader.GetValue("KEY_API_PERFIL", typeof(string)).ToString();
            APIInstalaciones = appReader.GetValue("KEY_API_INSTALACIONES", typeof(string)).ToString();
            APITipoInstalaciones = appReader.GetValue("KEY_API_TIPOINSTALACIONES", typeof(string)).ToString();
            APIUsuariosContratos = appReader.GetValue("KEY_API_USUARIOS_CONTRATOS", typeof(string)).ToString();
            APIIdioma = appReader.GetValue("KEY_API_IDIOMAS", typeof(string)).ToString();
            APICambiarPass = appReader.GetValue("KEY_API_CAMBIOPASS", typeof(string)).ToString();
        }
    }
}

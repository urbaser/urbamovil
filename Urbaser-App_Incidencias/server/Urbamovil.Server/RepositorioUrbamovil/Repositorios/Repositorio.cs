﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioComunUrbamovil;
using RepositorioUrbamovil.Cliente;

namespace RepositorioUrbamovil.Repositorios
{
    /// <summary>
    /// Realiza las peticiones al servico REST
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Repositorio<T> 
    {
        private RESTClient<T> Client;

        public Repositorio (string URLServicio, string ServicoAPI)
        {
            Client = new RESTClient<T>(URLServicio, ServicoAPI);
        }

        #region Métodos del repositorio

        /// <summary>
        /// Se obtiene una lista de elementos
        /// </summary>
        /// <param name="ticket">Identificador de autenticidad</param>
        /// <returns>List<typeparamref name="T"/></returns>
        public List<T> ObtenerTodos(Guid ticket)
        {
            try
            {
                return Client.GetElements(ticket);
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Se obtiene una lista de elementos que tengan en común una propiedad 
        /// determinada por un identificador
        /// </summary>
        /// <param name="ticket">Identificador de autenticidad</param>
        /// <param name="identificador">Identificador que sirve como filtro</param>
        /// <returns>List<typeparamref name="T"/></returns>
        public List<T> ObtenerTodos(Guid ticket, Guid identificador)
        {
            try
            {
                return Client.GetElements(ticket, identificador);
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Obtiene un elemento de una colección identificado por un identificador único
        /// </summary>
        /// <param name="ticket">Identificador de autenticidad</param>
        /// <param name="identificador">Identificador único del elemnto que se quiere</param>
        /// <returns></returns>
        public T Obtener(Guid ticket, Guid identificador)
        {
            try
            {
                return Client.Get(ticket, identificador);
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return default(T);
            }
        }

        /// <summary>
        /// Se actualiza las propiedades de un elemento
        /// </summary>
        /// <param name="value">Elemento que se desea actualizar</param>
        /// <param name="ticket">Identificador de autenticidad</param>
        /// <returns>True si la operación tiene éxito, False en caso contrario</returns>
        public bool Actualizar(T value, Guid ticket)
        {
            try
            {
                Client.Post(value, ticket);

                return true;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Se guarda un nuevo elemento en la colección de datos
        /// </summary>
        /// <param name="value">Nuevo elemento</param>
        /// <param name="ticket">Identificador de autenticidad</param>
        /// <returns>True si la operación tiene éxito, False en caso contrario</returns>
        public bool Guardar(T value, Guid ticket)
        {
            try
            {
                Client.Put(value, ticket);

                return true;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Se elemina un elemento de la colección de datos. Este elemento es 
        /// identificado por un identificador único
        /// </summary>
        /// <param name="identificador">Identificador único del elemento</param>
        /// <param name="ticket">Identificador de autenticidad</param>
        /// <returns>True si la operación tiene éxito, False en caso contrario</returns>
        public bool Eliminar(Guid identificador, Guid ticket)
        {
            try
            {
                Client.Delete(identificador, ticket);

                return true;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return false;
            }
        }

        #endregion
    }
}

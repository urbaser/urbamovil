﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Manager;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;
using WSUrbamovil.Encriptacion;
using System.Collections;

namespace WSUrbamovil.Controllers
{
    public class CiudadanosController : BaseController
    {
        // GET api/ciudadanos?correo=correo&cc=contra_Clave
        [HttpGet]
        public HttpResponseMessage GetByContraClave(string correo, string cc)
        {
            LogManager.AddTextLog(string.Format("RESGITROS CIUDADANO CON CORREO {0} Y CLAVE {1}", correo, cc));

            try
            {
                Usuarios usuario = managerUsuario.ComprobarUsuarioPorCorreoContraClave(correo, cc);

                if (usuario == null)
                {
                    throw HttpError(HttpStatusCode.NotFound, "No existe un usuario ");
                }

                if (!usuario.FechaValidezCC.HasValue || usuario.FechaValidezCC < DateTime.Now)
                {
                    throw HttpError(HttpStatusCode.Unauthorized, "La clave introducida esta caducada.");
                }

                usuario.Activo = true;
                usuario.FechaValidezCC = null;
                usuario.ContraClave = null;
                usuario.FechaRegistro = DateTime.Now;

                managerUsuario.ActualizarUsuario(usuario);

                TicketUsuario ticket = managerTicketUsuario.GenerarTicketPorCiudadano(usuario.IdUsuario);

                clsUsuarios user = new clsUsuarios();
                user.IdUsuario = usuario.IdUsuario;
                user.Nombre = usuario.Nombre;
                user.Apellido1 = usuario.Apellido1;
                user.Apellido2 = usuario.Apellido2;
                user.Direccion = usuario.Direccion;
                user.Empresa = usuario.Empresa;
                user.Email = usuario.Email;
                user.Telefono = usuario.Telefono;
                user.TelefonoMovil = usuario.TelefonoMovil;
                user.Login = usuario.Login;
                user.IdPerfil = usuario.IdPerfil;
                user.NombrePerfil = usuario.IdPerfil.HasValue ? usuario.TiposPerfilUsuario.Nombre : string.Empty;
                user.IPDireccion = usuario.IPDireccion;
                user.PreferenciasJSON = usuario.PreferenciasJSON;
                user.FechaRegistro = usuario.FechaRegistro;
                user.TicketSesion = ticket.Ticket;
                user.CodigoIdioma = usuario.Idiomas.Codigo;
                user.Activo = usuario.Activo;
                user.FechaValidezCC = usuario.FechaValidezCC;
                user.ContraClave = usuario.ContraClave;

                return HttpOK<clsUsuarios>(user);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // POST api/ciudadanos?correo=correo_electronico
        [HttpPost]
        public HttpResponseMessage Get(string correo)
        {
            LogManager.AddTextLog("POST CIUDADANOS. CORREO " + correo);

            try
            {
                Usuarios usuario = managerUsuario.ComprobarUsuarioPorCorreo(correo);

                if (usuario == null)
                {
                    throw HttpError(HttpStatusCode.NotFound, "No existe un usuario con la dirección de correo electrónico " + correo);
                }

                TicketUsuario ticket = managerTicketUsuario.GenerarTicketPorCiudadano(usuario.IdUsuario);

                usuario.Activo = false;
                usuario.ContraClave = Util.GenerateCode(5);
                usuario.FechaValidezCC = DateTime.Now.AddHours(horas);

                managerUsuario.ActualizarUsuario(usuario);

                EnviarCorreo(usuario.ContraClave, correo, string.Format("{0} {1} {2}", usuario.Nombre, usuario.Apellido1, usuario.Apellido2));

                return HttpOK("Validación correcta");
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // PUT api/ciudadanos
        [HttpPut]
        public HttpResponseMessage Put(clsCiudadano ciudadano)
        {
            string json = JSONManager.ConvertObjectToJSON<clsCiudadano>(ciudadano);
            LogManager.AddTextLog("PUT CIUDADANOS: " + json);

            try
            {
                Usuarios usuario = managerUsuario.ComprobarUsuarioPorCorreo(ciudadano.Email);

                if (usuario != null)
                {
                    throw HttpError(HttpStatusCode.Conflict, "Ya existe un usuario con la dirección de correo electrónico " + ciudadano.Email);
                }

                TiposPerfilUsuario perfil = managerPerfil.ObtenerPerfil(TipoPerfiles.CIUDADANO);

                Usuarios nuevoUsuario = new Usuarios();

                nuevoUsuario.IdUsuario = Guid.NewGuid();
                nuevoUsuario.Nombre = ciudadano.Nombre;
                nuevoUsuario.Apellido1 = ciudadano.Apellido1;
                nuevoUsuario.Apellido2 = ciudadano.Apellido2;
                nuevoUsuario.Direccion = string.Empty;
                nuevoUsuario.Empresa = string.Empty;
                nuevoUsuario.Email = ciudadano.Email;
                nuevoUsuario.Telefono = ciudadano.Telefono;
                nuevoUsuario.TelefonoMovil = string.Empty;
                nuevoUsuario.Login = ciudadano.Email;
                nuevoUsuario.Pass = Encriptado.EncriptarMD5(ciudadano.Email);
                nuevoUsuario.IdPerfil = perfil.IdPerfil;
                nuevoUsuario.IPDireccion = string.Empty;
                nuevoUsuario.PreferenciasJSON = string.Empty;
                nuevoUsuario.IdIdioma = (int)Idioma.ES;
                nuevoUsuario.Activo = false;
                nuevoUsuario.FechaValidezCC = DateTime.Now.AddHours(horas);
                nuevoUsuario.ContraClave = Util.GenerateCode(5);

                managerUsuario.GuardarUsuario(nuevoUsuario);

                EnviarCorreo(nuevoUsuario.ContraClave, ciudadano.Email, string.Format("{0} {1} {2}", ciudadano.Nombre, ciudadano.Apellido1, ciudadano.Apellido2));

                return HttpOK("Registro correcto");
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        [NonAction]
        private void EnviarCorreo(string cc, string email, string nombre)
        {
            Hashtable ht = Mail.GetEmailData("AVISO_CIUDADANO");

            string cuerpo = string.Format(ht["CUERPO"].ToString(), cc, nombre);
            string origen = ht["ORIGEN"].ToString();
            string titulo = ht["TITULO"].ToString();

            Mail.SendMail(titulo, cuerpo, origen, email);
        }
    }
}

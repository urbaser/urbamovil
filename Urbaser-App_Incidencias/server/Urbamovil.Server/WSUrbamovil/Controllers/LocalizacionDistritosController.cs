﻿using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class LocalizacionDistritosController : BaseController
    {
        // GET api/localizaciondistritos?id=ticket
        [HttpGet]
        public HttpResponseMessage Get(Guid id)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                List<Localizaciones_Distritos> LocalizacionDistritos = managerMaestras.ObtenerDistritos(contrato.IdInstalacion);
                List<clsDistritos> result = new List<clsDistritos>();

                foreach (var item in LocalizacionDistritos)
                {
                    clsDistritos distrito = new clsDistritos();
                    distrito.IdDistrito = item.IdDistrito;
                    distrito.Distrito = item.Distrito;
                    distrito.IdInstalacion = item.IdInstalacion;

                    result.Add(distrito);
                }

                return HttpOK<List<clsDistritos>>(result);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

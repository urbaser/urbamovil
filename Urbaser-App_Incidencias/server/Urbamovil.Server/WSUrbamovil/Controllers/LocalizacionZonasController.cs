﻿using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class LocalizacionZonasController : BaseController
    {
        // GET api/localizacionzonas?id=ticket&v=IdEspacioUrbano
        [HttpGet]
        public HttpResponseMessage Get(Guid id, int v)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                List<Localizaciones_Zonas> LocalizacionZonas = managerMaestras.ObtenerZonas(contrato.IdInstalacion, v);
                List<clsZonas> result = new List<clsZonas>();

                foreach (var item in LocalizacionZonas)
                {
                    clsZonas zona = new clsZonas();
                    zona.IdZona = item.IdZona;
                    zona.Zona = item.Zona;
                    zona.IdEspacioUrbano = item.IdEspacioUrbano;
                    zona.IdInstalacion = item.IdInstalacion;

                    result.Add(zona);
                }

                return HttpOK<List<clsZonas>>(result);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

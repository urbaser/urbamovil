﻿using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class InventarioController : BaseController
    {
        // GET api/inventario?id=ticket&IdContrato=contrato
        [HttpGet]
        public HttpResponseMessage Get(Guid Id)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(Id);

                Contratos contrato = ComprobarValidezContrato(Id);

                List<Localizaciones_TiposElemento> LocalizacionTiposElementos = managerMaestras.ObtenerTiposElementos(contrato.IdInstalacion);
                List<Localizaciones_Distritos> LocalizacionDistritos = managerMaestras.ObtenerDistritos(contrato.IdInstalacion);
                List<Localizaciones_EspaciosUrbanos> LocalizacionEspaciosUrbanos = managerMaestras.ObtenerEspaciosUrbanos(contrato.IdInstalacion);
                List<Localizaciones_Zonas> LocalizacionZonas = managerMaestras.ObtenerZonas(contrato.IdInstalacion);
                List<Localizaciones_Rutas> LocalizacionRutas = managerMaestras.ObtenerRutas(contrato.IdInstalacion);

                clsInventario inventario = new clsInventario();

                foreach (var item in LocalizacionTiposElementos)
                {
                    clsTiposElemento tipo = new clsTiposElemento();
                    tipo.IdTipoElemento = item.IdTipoElemento;
                    tipo.Tipo = item.Tipo;
                    tipo.IdInstalacion = item.IdInstalacion;

                    inventario.LocalizacionTiposElementos.Add(tipo);
                }

                foreach (var item in LocalizacionDistritos)
                {
                    clsDistritos distrito = new clsDistritos();
                    distrito.IdDistrito = item.IdDistrito;
                    distrito.Distrito = item.Distrito;
                    distrito.IdInstalacion = item.IdInstalacion;

                    inventario.LocalizacionDistritos.Add(distrito);
                }

                foreach (var item in LocalizacionEspaciosUrbanos)
                {
                    clsEspaciosUrbanos espacioUrbano = new clsEspaciosUrbanos();
                    espacioUrbano.IdEspacioUrbano = item.IdEspacioUrbano;
                    espacioUrbano.EspacioUrbano = item.EspacioUrbano;
                    espacioUrbano.IdDistrito = item.IdDistrito;
                    espacioUrbano.IdInstalacion = item.IdInstalacion;

                    inventario.LocalizacionEspaciosUrbanos.Add(espacioUrbano);
                }

                foreach (var item in LocalizacionZonas)
                {
                    clsZonas zona = new clsZonas();
                    zona.IdZona = item.IdZona;
                    zona.Zona = item.Zona;
                    zona.IdEspacioUrbano = item.IdEspacioUrbano;
                    zona.IdInstalacion = item.IdInstalacion;

                    inventario.LocalizacionZonas.Add(zona);
                }

                foreach (var item in LocalizacionRutas)
                {
                    clsRutas ruta = new clsRutas();
                    ruta.IdRuta = item.IdRuta;
                    ruta.Ruta = item.Ruta;
                    ruta.IdEspacioUrbano = item.IdEspacioUrbano;
                    ruta.IdInstalacion = item.IdInstalacion;

                    inventario.LocalizacionRutas.Add(ruta);
                }

                return HttpOK<clsInventario>(inventario);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

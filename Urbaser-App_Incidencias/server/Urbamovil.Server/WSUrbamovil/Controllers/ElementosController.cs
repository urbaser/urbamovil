﻿using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class ElementosController : BaseController
    {
        // GET api/elementos
        [HttpGet]
        public List<clsElemento> Get(Guid id, int IdTipoEl)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                List<Elementos> list = managerElementos.ObtenerElementos(contrato.IdInstalacion, IdTipoEl);
                List<clsElemento> result = null;

                if (list != null && list.Count > 0)
                {
                    result = new List<clsElemento>();

                    foreach(var item in list)
                    {
                        clsElemento elem = new clsElemento();
                        elem.IdElemento = item.IdElemento;
                        elem.NombreElemento = item.NombreElemento;
                        elem.IdInstalacion = item.IdInstalacion;
                        elem.IdTipoElemento = item.IdTipoElemento;
                        elem.IdZona = item.IdZona;
                        elem.IdRuta = item.IdRuta;

                        result.Add(elem);
                    }
                }

                return result;
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                throw hex;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw HttpError(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // GET api/elementos?id=ticket&d=distrito&e=espaciourbano&z=zona&r=ruta&t=tipo
        [HttpGet]
        public HttpResponseMessage Get(Guid id, int d, int e, int z, int r, int t)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                // TODO

                return HttpOK("OK");
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

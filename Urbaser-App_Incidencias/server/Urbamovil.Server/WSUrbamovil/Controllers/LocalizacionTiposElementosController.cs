﻿using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class LocalizacionTiposElementosController : BaseController
    {
        // GET api/localizaciontiposelementos?id=ticket
        [HttpGet]
        public HttpResponseMessage Get(Guid id)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                List<Localizaciones_TiposElemento> LocalizacionTiposElementos = managerMaestras.ObtenerTiposElementos(contrato.IdInstalacion);
                List<clsTiposElemento> result = new List<clsTiposElemento>();

                foreach (var item in LocalizacionTiposElementos)
                {
                    clsTiposElemento tipo = new clsTiposElemento();
                    tipo.IdTipoElemento = item.IdTipoElemento;
                    tipo.Tipo = item.Tipo;
                    tipo.IdInstalacion = item.IdInstalacion;

                    result.Add(tipo);
                }

                return HttpOK<List<clsTiposElemento>>(result);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

using NegocioUrbamovil.Manager;
using NegocioUrbamovil.Modelo;

namespace WSUrbamovil.Clases
{
    public class BaseController : ApiController
    {
        protected int horas;
        protected string pathHandler;
        protected string pathHandlerCierre;

        private DbUrbamovil_DevEntities Contexto = new DbUrbamovil_DevEntities();

        private UsuarioManager _managerUsuario;
        protected UsuarioManager managerUsuario
        {
            get
            {
                if (_managerUsuario == null) _managerUsuario = new UsuarioManager(Contexto);
                return _managerUsuario;
            }
        }

        private TicketUsuarioManager _managerTicketUsuario;
        protected TicketUsuarioManager managerTicketUsuario
        {
            get
            {
                if (_managerTicketUsuario == null) _managerTicketUsuario = new TicketUsuarioManager(Contexto);
                return _managerTicketUsuario;
            }
        }

        private PerfilManager _managerPerfil;
        protected PerfilManager managerPerfil
        {
            get
            {
                if (_managerPerfil == null) _managerPerfil = new PerfilManager(Contexto);
                return _managerPerfil;
            }
        }

        private FiltroUsuarioManager _managerFiltroUsuario;
        protected FiltroUsuarioManager managerFiltroUsuario
        {
            get
            {
                if (_managerFiltroUsuario == null) _managerFiltroUsuario = new FiltroUsuarioManager(Contexto);
                return _managerFiltroUsuario;
            }
        }

        private IncidenciasManager _managerIncidencias;
        protected IncidenciasManager managerIncidencias
        {
            get
            {
                if (_managerIncidencias == null) _managerIncidencias = new IncidenciasManager(Contexto);
                return _managerIncidencias;
            }
        }

        private InstalacionesManager _managerInstalaciones;
        protected InstalacionesManager managerInstalaciones
        {
            get
            {
                if (_managerInstalaciones == null) _managerInstalaciones = new InstalacionesManager(Contexto);
                return _managerInstalaciones;
            }
        }

        private TipoInstalacionManager _managerTipoInstalacion;
        protected TipoInstalacionManager managerTipoInstalacion
        {
            get
            {
                if (_managerTipoInstalacion == null) _managerTipoInstalacion = new TipoInstalacionManager(Contexto);
                return _managerTipoInstalacion;
            }
        }

        private ContratosManager _managerContratos;
        protected ContratosManager managerContratos
        {
            get
            {
                if (_managerContratos == null) _managerContratos = new ContratosManager(Contexto);
                return _managerContratos;
            }
        }

        private UsuariosContratosManager _managerUsuariosContratos;
        protected UsuariosContratosManager managerUsuariosContratos
        {
            get
            {
                if (_managerUsuariosContratos == null) _managerUsuariosContratos = new UsuariosContratosManager(Contexto);
                return _managerUsuariosContratos;
            }
        }

        private HistoricoIncidenciasManager _managerHistoricoIncidencias;
        protected HistoricoIncidenciasManager managerHistoricoIncidencias
        {
            get
            {
                if (_managerHistoricoIncidencias == null) _managerHistoricoIncidencias = new HistoricoIncidenciasManager(Contexto);
                return _managerHistoricoIncidencias;
            }
        }

        private IdiomasManager _managerIdiomas;
        protected IdiomasManager managerIdiomas
        {
            get
            {
                if (_managerIdiomas == null) _managerIdiomas = new IdiomasManager(Contexto);
                return _managerIdiomas;
            }
        }

        private MaestrasManager _managerMaestras;
        protected MaestrasManager managerMaestras 
        {
            get
            {
                if (_managerMaestras == null) _managerMaestras = new MaestrasManager(Contexto);
                return _managerMaestras;
            }
        }

        private ElementosManager _managerElementos;
        protected ElementosManager managerElementos
        {
            get
            {
                if (_managerElementos == null) _managerElementos = new ElementosManager(Contexto);
                return _managerElementos;
            }
        }

        private IncidenciaHistoricoEstadoManager _managerIncHistEstado;
        protected IncidenciaHistoricoEstadoManager managerIncHistEstado
        {
            get
            {
                if (_managerIncHistEstado == null) _managerIncHistEstado = new IncidenciaHistoricoEstadoManager(Contexto);
                return _managerIncHistEstado;
            }
        }

        private LocalizacionesIncidenciasManager _managerLocalizacionesIncidencias;
        protected LocalizacionesIncidenciasManager managerLocalizacionesIncidencias
        {
            get
            {
                if (_managerLocalizacionesIncidencias == null) _managerLocalizacionesIncidencias = new LocalizacionesIncidenciasManager(Contexto);
                return _managerLocalizacionesIncidencias;
            }
        }

        private VersionesManager _managerVersiones;
        protected VersionesManager managerVersiones
        {
            get
            {
                if (_managerVersiones == null) _managerVersiones = new VersionesManager(Contexto);
                return _managerVersiones;
            }
        }

        private MensajesManager _managerMensajes;
        protected MensajesManager managerMensajes
        {
            get
            {
                if (_managerMensajes == null) _managerMensajes = new MensajesManager(Contexto);
                return _managerMensajes;
            }
        }

        public BaseController() : base()
        {
            AppSettingsReader appReader = new AppSettingsReader();

            horas = int.Parse(appReader.GetValue("KEY_HORASCADUCIDADTICKET", typeof(string)).ToString());
            
            pathHandler = appReader.GetValue("KEY_PATH_HANDLER", typeof(string)).ToString();

            pathHandlerCierre = appReader.GetValue("KEY_PATH_HANDLER_CIERRE", typeof(string)).ToString();
        }

        protected string ToAbsolutePath(string contentPath)
        {
            var url = new System.Uri(HttpContext.Current.Request.Url, "/").AbsoluteUri;
            return url + VirtualPathUtility.ToAbsolute(contentPath);
        }

        protected Usuarios ComprobarValidezTicket(Guid id)
        {
            Usuarios usuario = managerUsuario.ObtenerUsuarioPorTicket(id);

            if (usuario != null)
            {
                managerTicketUsuario.AumentarCaducidadTicket(id, horas);
            }
            else
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Unauthorized, "El usuario no esta autorizado."));
            }
            
            return usuario;
        }

        protected Contratos ComprobarValidezContrato(Guid id)
        {
            TicketUsuario ticket = managerTicketUsuario.Obtener(id);

            if (!ticket.IdContrato.HasValue)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound, "No se hace referencia a ningun contrato."));
            }

            Contratos contrato = managerContratos.ObtenerContrato(ticket.IdContrato.Value);

            if (contrato == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound, "No existe el contrato especificado."));
            }

            return contrato;
        }

        protected HttpResponseException HttpError(HttpStatusCode status, string message)
        {
            return new HttpResponseException(Request.CreateResponse(status, message));
        }

        protected HttpResponseMessage HttpErrorMessage(HttpStatusCode status, string message)
        {
            return Request.CreateResponse(status, message);
        }

        protected HttpResponseMessage HttpOK<T>(T message)
        {
            return Request.CreateResponse<T>(HttpStatusCode.OK, message);
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Xml;

namespace WSUrbamovil.Clases
{
    public class Mail
    {
        public static void SendMail(string titulo, string cuerpo, string origen, string destino)
        {
            MailMessage email = new MailMessage(origen, destino);

            email.Subject = titulo;
            email.Body = cuerpo;
            email.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();

            client.Send(email);
        }

        public static Hashtable GetEmailData(string clave)
        {
            AppSettingsReader app = new AppSettingsReader();
            string path = app.GetValue("KEY_PathConfigMail", typeof(string)).ToString();
            path = HttpContext.Current.Server.MapPath(path);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);

            XmlNode xmlNode = xmlDoc.SelectSingleNode("Root/" + "es-ES" + "/" + clave);

            Hashtable ht = new Hashtable();

            foreach (XmlNode item in xmlNode.ChildNodes)
            {
                ht.Add(item.Name, item.InnerText);
            }

            return ht;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

using NegocioUrbamovil.Modelo;

namespace WSUrbamovil.Clases
{
    public class BaseAdminController : BaseController
    {
        public BaseAdminController() : base() { }

        protected bool ComprobarUsuarioAdministrador(Guid ticket)
        {
            Usuarios usuario = managerUsuario.ObtenerUsuarioPorTicket(ticket);
            return usuario.TiposPerfilUsuario.EsAdministrador;
        }
    }
}
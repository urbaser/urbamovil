﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.ControllersAdmin
{
    public class FiltroUsuarioAdminController : BaseAdminController
    {
        // GET api/filtrousuarioadmin?id=ticket&user=usuario
        [HttpGet]
        public List<clsFiltroUsuario> Get(Guid id, Guid user)
        {
            try
            {
                List<clsFiltroUsuario> result = null;

                if (ComprobarUsuarioAdministrador(id))
                {
                    List<FiltroUsuario> list = managerFiltroUsuario.ObtenerFiltrosPorUsuario(user);

                    result = new List<clsFiltroUsuario>();
                    foreach (var item in list)
                    {
                        clsFiltroUsuario f = new clsFiltroUsuario();
                        f.IdFiltroUsuario = item.IdFiltroUsuario;
                        f.Nombre = item.Nombre;
                        f.FiltrosCampo = JSONManager.DeserializeJSON<List<FiltroCampo>>(item.ObjetoJSON);

                        result.Add(f);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }

        // GET api/filtrousuarioadmin/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/filtrousuarioadmin
        public void Post([FromBody]string value)
        {
        }

        // PUT api/filtrousuarioadmin/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/filtrousuarioadmin/5
        public void Delete(int id)
        {
        }
    }
}

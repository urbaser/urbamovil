﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Manager;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.ControllersAdmin
{
    public class PerfilAdminController : BaseAdminController
    {
        // GET api/Perfil/id=ticket
        [HttpGet]
        public List<clsPerfil> Get(Guid id)
        {
            List<clsPerfil> perfiles = null;

            if (ComprobarUsuarioAdministrador(id))
            {
                List<TiposPerfilUsuario> list = managerPerfil.ObtenerPerfiles();
                perfiles = new List<clsPerfil>();

                foreach (var item in list)
                {
                    clsPerfil perfil = new clsPerfil();
                    perfil.EsAdministrador = item.EsAdministrador;
                    perfil.NuevaIncidencia = item.NuevaIncidencia;
                    perfil.ModificarIncidencia = item.ModificarIncidencia;
                    perfil.HistoricoIncidencias = item.HistoricoIncidencias;
                    perfil.ListadoIncidencias = item.ListadoIncidencias;
                    perfil.CambioEstadoIncidencia = item.CambioEstadoIncidencia;
                    perfil.ConsultarIncidencia = item.ConsultarIncidencia;
                    perfiles.Add(perfil);
                }
            }

            return perfiles;
        }
    }
}

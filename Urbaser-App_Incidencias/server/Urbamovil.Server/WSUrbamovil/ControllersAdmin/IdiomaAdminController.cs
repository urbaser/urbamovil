﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.ControllersAdmin
{
    public class IdiomaAdminController : BaseAdminController
    {
        // GET api/idiomaadmin?id=ticket
        [HttpGet]
        public List<clsIdiomas> Get(Guid id)
        {
            try
            {
                List<clsIdiomas> result = null;
                if (ComprobarUsuarioAdministrador(id))
                {
                    List<Idiomas> list = managerIdiomas.ObtenerIdiomas();

                    if (list != null && list.Count > 0)
                    {
                        result = new List<clsIdiomas>();
                        foreach(Idiomas i in list)
                        {
                            clsIdiomas idioma = new clsIdiomas();
                            idioma.IdIdioma = i.IdIdioma;
                            idioma.Idioma = i.Idioma;
                            idioma.Codigo = i.Codigo;

                            result.Add(idioma);
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }
    }
}

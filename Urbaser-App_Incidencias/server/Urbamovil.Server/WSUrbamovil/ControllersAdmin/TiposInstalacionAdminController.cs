﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.ControllersAdmin
{
    public class TiposInstalacionAdminController : BaseAdminController
    {
        // GET api/tiposinstalacionadmin?id=ticket
        [HttpGet]
        public List<clsTipoInstalacion> Get(Guid id)
        {
            try
            {
                List<clsTipoInstalacion> result = null;

                if (ComprobarUsuarioAdministrador(id))
                {
                    List<TiposInstalacion> list = managerTipoInstalacion.ObtenerTiposInstalaciones();
                    result = new List<clsTipoInstalacion>();

                    foreach (var item in list)
                    {
                        clsTipoInstalacion tipo = new clsTipoInstalacion();
                        tipo.IdTipoInstalacion = item.IdTipoInstalacion;
                        tipo.Nombre = item.Nombre;

                        result.Add(tipo);
                    }
                }

                return result;
            }
            catch(Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }
    }
}

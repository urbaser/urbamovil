'use strict';

module.exports = function (grunt) {


	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Configurable paths
	var config = {
		app: './src',
		dist: './www'
	};

	// Define the configuration for all the tasks
	grunt.initConfig({

		// Project settings
		config: config,

		// Watches files for changes and runs tasks based on the changed files
		watch: {
			//js: {
			//	files: ['<%= config.app %>/content/script/{,*/}*.js'],
			//	tasks: ['jshint']
			//},
			gruntfile: {
				files: ['Gruntfile.js']
			},
			//styles: {
			//	files: ['<%= config.app %>/content/styles/{,*/}*.css'],
			//	tasks: ['autoprefixer']
			//},
			sass: {
				files: '<%= config.app %>/content/styles/{,*/}*.scss',
				tasks: ['sass:dev']
			},
			html: {
				files: ['<%= config.app %>/views/**','<%= config.app %>/layouts/**','<%= config.app %>/main.html','<%= config.app %>/citizen.html'],
				tasks: ['processhtml:dev']
			}
		},
		browserSync: {
			bsFiles: {
				src: [
					'<%= config.app %>/content/script/app/**/*.js',
					'<%= config.app %>/content/styles/*.css',
					'<%= config.app %>/content/imgs/*.*',
					'<%= config.app %>/index.html'
				]
			},
			options: {
				//proxy: "local.dev", // Por si se está usando un servidor externo (apache, iis...etc)
				server: {
					baseDir: '<%= config.app %>'
				},
				watchTask: true

			}
		},
		// Empties folders to start fresh
		clean: {
			dist: {
				src: ['<%= config.dist %>/**']
			}
		},
		// The following *-min tasks produce minified files in the dist folder
		imagemin: {
			jpg: {
				options: {
					progressive: true
				},
				files: [
					{
						expand: true,
						cwd: '<%= config.app %>/content/imgs',
						src: ['**/*.jpg'],
						dest: '<%= config.dist %>/content/imgs',
						ext: '.jpg'
					}
				]
			}
		},
		svgmin: {
			dist: {
				files: [
					{
						expand: true,
						cwd: '<%= config.app %>/content/imgs',
						src: '{,*/}*.svg',
						dest: '<%= config.dist %>/content/imgs'
					}
				]
			}
		},
		requirejs: {
			dist: {
				options: {
					baseUrl: "<%= config.app %>/content/script",
					mainConfigFile: '<%= config.app %>/content/script/main.js',
					include: ['requireLib','main'],
					name: "main",
					out: "<%= config.dist %>/content/script/main-built.js",
					preserveLicenseComments: false,
					paths: {
						requireLib: "../components/requirejs/require"
					}
				}
			},
			citizen: {
				options: {
					baseUrl: "<%= config.app %>/content/script",
					mainConfigFile: '<%= config.app %>/content/script/main-citizen.js',
					include: ['requireLib','main-citizen'],
					name: "main",
					out: "<%= config.dist %>/content/script/main-built.js",
					preserveLicenseComments: false,
					paths: {
						requireLib: "../components/requirejs/require"
					}
				}
			}
		},
		sass: {
			dist: {
				options: {
					style: 'compressed',
					sourcemap: 'none'
				},
				files: {
					'<%= config.dist %>/content/styles/global.css': '<%= config.app %>/content/styles/main.scss'
				}
			},
			dev: {
				options: {
					style: 'nested',
					sourcemap: 'auto'
				},
				files: {
					'<%= config.app %>/content/styles/global.css': '<%= config.app %>/content/styles/main.scss'
				}
			}
		},
		processhtml: {
			dist: {
				options: {
				},
				files: {
					'<%= config.dist %>/index.html': ['<%= config.app %>/main.html']
				}
			},
			citizen: {
				options: {
				},
				files: {
					'<%= config.dist %>/index.html': ['<%= config.app %>/citizen.html']
				}
			},
			dev: {
				files: {
					'<%= config.app %>/index.html': ['<%= config.app %>/main.html']
				}
			},
			citizenDev: {
				files: {
					'<%= config.app %>/index.html': ['<%= config.app %>/citizen.html']
				}
			}
		},
		copy: {
			dist: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: '<%= config.app %>',
						dest: '<%= config.dist %>',
						src: [
							'*.{ico,png,txt}',
							'content/data/**',
							'content/components/kendo-ui/styles/kendo.mobile.common.min.css',
							'content/components/kendo-ui/styles/kendo.default.mobile.min.css',
							'content/components/kendo-ui/styles/kendo.mobile.all.min.css',
							'content/imgs/{,*/}*.svg',
							'content/imgs/{,*/}*.png',
							'content/fonts/{,*/}*.*',
							'content/media/**',
							'content/css',
							'res/**',
							'locales/**'
						]
					},
					 {
					 src: 'config.xml',
					 dest: '<%= config.dist %>/config.xml'
					 }
				]
			},
			debug: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: '<%= config.app %>',
						dest: '<%= config.dist %>',
						src: [
							'**'
						]
					}/*,
					 {
					 src: 'node_modules/apache-server-configs/dist/.htaccess',
					 dest: '<%= config.dist %>/.htaccess'
					 }*/
				]
			},
			android: {
				src: './platforms/android/build/outputs/apk/android-release.apk',
				dest: './dist/android-release.apk',
			},
			blackberry: {
				src: './platforms/blackberry10/build/device/bb10app.bar',
				dest: './dist/bb10app.bar',
			},
			bbicons: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: '<%= config.app %>',
						src: ['res/**'],
						dest: './platforms/blackberry10/platform_www/'
					}
				]
			}
		},

		// Run some tasks in parallel to speed up build process
		concurrent: {
			dist: [
				//'tinypng',
				'imagemin',
				'requirejs:dist',
				'sass:dist',
				'processhtml:dist'
			],
			citizen: [
				//'tinypng',
				//'svg2png',
				'imagemin',
				'requirejs:citizen',
				'sass:dist',
				'processhtml:citizen'
			]
		},
		tinypng: {
			options: {
				apiKey: "gR4YSvzh9p8qj8BY1om0-9SIpCELAQEF",
				checkSigs: true,
				sigFile: "tinypng.sig",
				summarize: true,
				showProgress: true
			},
			dist: {
				files: [
					{
						expand: true,
						cwd: '<%= config.app %>/content/imgs',
						src: ['**/*.png'],
						dest: '<%= config.dist %>/content/imgs',
						ext: '.png'
					}
				]
			}
		},
		shell: {
			bb10recreate: {
				command: [	'cordova platform remove blackberry10',
					'cordova platform add blackberry10'].join('&&')

			},
			bb10build: {
				command: ['platforms/blackberry10/cordova/build --release --keystorepass 5lh3Vs-+'].join('&&')

			},
			bb10run: {
				command: ['platforms/blackberry10/cordova/run --device --release --keystorepass 5lh3Vs-+'].join('&&')
			},
			androidDebug: {
				command: ['cordova run android'].join('&&')

			},
			androidCompileDebug: {
				command: ['cordova build android'].join('&&')

			},
			androidBuildRelease: {
				command: ['cordova build android --release'].join('&&')
			},
			androidRunRelease: {
				command: ['cordova run android --release'].join('&&')
			},
			iosrun: {
				command: ['cordova run ios'].join('&&')
			},
			iosprepare: {
				command: ['cordova prepare ios'].join('&&')
			}
		},
	});

	grunt.registerTask('serve', 'start the server and preview your app', function () {
		grunt.task.run([
			'browserSync',
			'watch'
		]);
	});

	grunt.registerTask('prepareDist', [
		'clean:dist',
		'copy:dist',
		'concurrent:dist'
	]);

	grunt.registerTask('prepareCitizen', [
		'clean:dist',
		'copy:dist',
		'concurrent:citizen'
	]);
	
	grunt.registerTask('androidBuild', [
		'prepareDist',
		'shell:androidBuildRelease',
		'copy:android'
	]);

	grunt.registerTask('androidBuildCitizen', [
		'prepareCitizen',
		'shell:androidBuildRelease',
		'copy:android'
	]);

	grunt.registerTask('androidRunProduction', [
		'prepareDist',
		'shell:androidDebug',
	]);

	grunt.registerTask('androidRunProductionCitizen', [
		'prepareCitizen',
		'shell:androidDebug',
	]);

	grunt.registerTask('androidDebug', [
		'clean:dist',
		'copy:debug',
		'shell:androidDebug'
	]);

	grunt.registerTask('androidCompileDebug', [
		'clean:dist',
		'copy:debug',
		'shell:androidCompileDebug'
	]);
	grunt.registerTask('blackberry10build', [
		'shell:bb10recreate',
		'prepareDist',
		'copy:bbicons',
		'shell:bb10build',
		'copy:blackberry'
	]);

	grunt.registerTask('blackberry10run', [
		'shell:bb10recreate',
		'prepareDist',
		'copy:bbicons',
		'shell:bb10run'
	]);

	grunt.registerTask('compile', [
		'androidBuild',
		'blackberry10build'
	]);

	grunt.registerTask('iosDebug', [
		'clean:dist',
		'copy:debug',
		'shell:iosrun'
	]);

	grunt.registerTask('iosBuild', [
		'clean:dist',
		'copy:dist',
		'concurrent:dist',
		'shell:iosprepare'
	]);

	/*
	*/
};

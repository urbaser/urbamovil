define([
	"kendo/kendo.core",
	"kendo/kendo.data",
	"app/models/masterTables"
], function(kendo,k,tables)
{
	var schema = {
		id: "IdIncidencia",
		fields: {
			IdIncidencia: { editable: false, nullable: true },
			IdGrupo: { type: "number", validation: { required: true, min: 1} },
			IdTipo: { type: "number", validation: { required: true, min: 1} },
			IdDescripcion: { type: "number", validation: { required: true, min: 1} },
			IdPrioridad: { type: "number", validation: { required: true, min: 1} },
			IdEstado: { type: "number", validation: { required: true, min: 1} },
			IdProvincia: { type: "number", validation: { required: true, min: 1} },
			IdMunicipio: { type: "number", validation: { required: true, min: 1} },
			CargandoImagen: { type: "boolean" },
			CargandoImagenCierre: { type: "boolean" },
			CodigoIncidencia: { type: "string", nullable: false},
			Imagen: { nullable: true},
			ImagenCierre: { nullable: true},
			tempfile: { nullable: true},
			Observaciones: { nullable: true},
			FechaAlta: { type: "date" },
			FechaUltimaModificacion: { type: "date" },
			Localizaciones: { nullable: true }
		},
		fechaAltaFormatted: function(){
			return this.FechaAlta ? kendo.toString(this.FechaAlta, "dd/MM/yyyy HH:mm") : "";
		},
		estado: function(){
			return this.IdEstado ? tables.estados.get(this.IdEstado) : null;
		},
		prioridad: function(){
			return tables.prioridades.get(this.IdPrioridad);
		},
		tipo: function(){
			return tables.tipos.get(this.IdTipo);
		},
		grupo: function(){
			return tables.grupos.get(this.IdGrupo);
		},
		descripcion: function(){
			return tables.descripciones.get(this.IdDescripcion);
		},
		provincia: function(){
			return this.IdProvincia ? tables.provincias.get(this.IdProvincia).NombreProvincia : "";
		},
		municipio: function(){
			return this.IdMunicipio ? tables.municipios.get(this.IdMunicipio).NombreMunicipio : "";
		},
		titulo: function(){
			try{
				if(this.descripcion()){
					return this.grupo().GrupoIncidencia + " / " + this.tipo().TipoIncidencia + " / " + this.descripcion().DescripcionIncidencia;
				}
				else if(this.tipo()){
					return this.grupo().GrupoIncidencia + " / " + this.tipo().TipoIncidencia;
				}
				else{
					return this.grupo().GrupoIncidencia;
				}
			}
			catch(e){
				return "n/a";
			}
		},
		localizaciones: function(){
			try{
				return this.Localizaciones.map(function(it){
					return it.title()
				}).join(", ");
			}
			catch(e){
				return "";
			}
		},
		hasMap: function(){
			try{
				return this.Localizaciones.filter(function(it){
					return it.IdTipoLocalizacion == 1;
				}).length > 0;
			}
			catch(e){
				return false;
			}
		}
	};
	return kendo.data.Model.define(schema);
});

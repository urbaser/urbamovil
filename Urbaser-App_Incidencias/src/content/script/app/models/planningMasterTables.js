define([
	'signals',
	'refr3sh/net/SimpleApiCall',
	'kendo/kendo.data',
], function(signals,SimpleApiCall,kendo) {
	var obj = {

		areasNegocio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdAreaNegocio",
					fields: {
						IdAreaNegocio: {
							type: "String",
							editable: false,
							nullable: false
						},
						AreaNegocio: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		tiposRecurso: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoRecurso",
					fields: {
						IdTipoRecurso: {
							type: "number",
							editable: false,
							nullable: false
						},
						TipoRecurso: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		estadosServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdEstado",
					fields: {
						IdEstado: {
							type: "number",
							editable: false,
							nullable: false
						},
						EstadoServicio: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		periodosServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdPeriodo",
					fields: {
						IdPeriodo: {
							type: "number",
							editable: false,
							nullable: false
						},
						Periodo: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		tiposServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoRecurso",
					fields: {
						IdTipoServicio: {
							type: "number",
							editable: false,
							nullable: false
						},
						TipoServicio: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdAreaNegocio: {
							type: "String",
							editable: false,
							nullable: false
						}
					},
					AreaNegocio: function() {
						return obj.areasNegocio.get(this.IdAreaNegocio);
					}
				}
			}
		}),
		turnosServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTurno",
					fields: {
						IdTurno: {
							type: "number",
							editable: false,
							nullable: false
						},
						Turno: {
							type: "String",
							editable: false,
							nullable: false
						},
						HoraInicio: {
							type: "String",
							editable: false,
							nullable: false
						},
						HoraFin: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		rutaSectorServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdRutaSector",
					fields: {
						IdRutaSector: {
							type: "number",
							editable: false,
							nullable: false
						},
						Codigo: {
							type: "String",
							editable: false,
							nullable: false
						},
						Nombre: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdAreaNegocio: {
							type: "String",
							editable: false,
							nullable: false
						}
					},
					AreaNegocio: function() {
						return obj.areasNegocio.get(this.IdAreaNegocio);
					}
				}
			}
		}),
		estadosOT: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdEstado",
					fields: {
						IdEstado: {
							type: "number",
							editable: false,
							nullable: false
						},
						Estado: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		categoriasRecursos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdCategoriaRecurso",
					fields: {
						IdCategoriaRecurso: {
							type: "number",
							editable: false,
							nullable: false
						},
						CategoriaRecurso: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdTipoRecurso: {
							type: "number",
							editable: false,
							nullable: false
						}
					},
					TipoRecurso: function() {
						return obj.tiposRecurso.get(this.IdTipoRecurso);
					}
				}
			}
		}),
		tareasServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTarea",
					fields: {
						IdTarea: {
							type: "number",
							editable: false,
							nullable: false
						},
						Tarea: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdTipoServicio: {
							type: "number",
							editable: false,
							nullable: false
						},
						IdAreaNegocio: {
							type: "String",
							editable: false,
							nullable: false
						}
					},
					TipoServicio: function() {
						return obj.tiposServicio.get(this.IdTipoServicio);
					},
					AreaNegocio: function() {
						return obj.areasNegocio.get(this.IdAreaNegocio);
					}
				}
			}
		}),
		updated: new signals.Signal(),
		update: function(contractId){
			this.loadLocal(contractId);
			var call = new SimpleApiCall();

			call.succeed.addOnce(function(data) {
				console.log(data);
				this._createSources(data);
				hasData = true;
				this.saveLocal(data,contractId);
				this.updated.dispatch();
			}.bind(this));

			call.get(window.app.config.baseUrl + "/PlanificacionMaestras",{
				idContrato: contractId
			});
		},
		_createSources: function(data){
			this.areasNegocio.data(data.AreasNegocio);
			this.tiposRecurso.data(data.TiposRecurso);
			this.estadosServicio.data(data.EstadosServicio);
			this.periodosServicio.data(data.PeriodosServicio);
			this.tiposServicio.data(data.TiposServicio);
			this.turnosServicio.data(data.TurnosServicio);
			this.rutaSectorServicio.data(data.RutaSectorServicio);
			this.estadosOT.data(data.EstadosOT);
			this.categoriasRecursos.data(data.CategoriasRecursos);
			this.tareasServicio.data(data.TareasServicio);
		},
		loadLocal: function(contractId){
			console.log("Loading planning local tables for contract: " + contractId);
			var _localdata = JSON.parse(window.localStorage.getItem("pln_tables"+contractId)) || {};
			this._createSources(_localdata);
		},
		saveLocal: function(data,contractId){
			window.localStorage.setItem("pln_tables"+contractId,JSON.stringify(data));
		}
	};
	var hasData;
	Object.defineProperty(obj,"hasData",{
		get: function(){
			return hasData;
		}
	})
	return obj;
});

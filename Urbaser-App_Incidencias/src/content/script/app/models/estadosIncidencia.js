/**
 * Created by 7daysofrain on 25/3/15.
 */
define(['jQuery',
	"app/models/estadoIncidencia",
	"app/models/incidencias",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"], function(j,EstadoIncidencia,incidencias,kendo)
{
	function parameterMap(data, requestType) {
		if (requestType === 'create' || requestType === 'update') {
			return JSON.stringify(data);
		}
		return data;
	}
	var obj = {
		dataSource: new kendo.data.DataSource({
			data:[],
			transport: {
				read: {
					url: function(){
						return window.app.config.baseUrl + '/incidenciahistoricoestado';
					},
					dataType: "json",
					type: "GET",
					data: function(){
						return {
							id:window.app.user.ticket,
							IdIncidencia: incidencias.selected.IdIncidencia
						};
					}
				},
				parameterMap: parameterMap
			},
			requestStart: function (e) {
				if(!window.app.user || !window.app.user.ticket || !incidencias.selected){
					e.preventDefault();
				}
			},
			error: function (e) {
				console.error(e);
				window.app.kendoApp.pane.loader.hide();
				if(e.xhr.status != 401){
					alert(T("models.estadosIncidencia.update-error"));
				}
				else{
				}
			},
			batch: false,
			schema: {
				model: EstadoIncidencia
			}
		}),
		select: function(item){
			this.selected = item;
			console.log("Selected: ",this.selected);
		}
	};

	return obj;
});

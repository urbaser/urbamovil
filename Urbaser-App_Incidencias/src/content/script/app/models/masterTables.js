define([
	'signals',
	'refr3sh/net/SimpleApiCall',
	'kendo/kendo.data',
], function(signals,SimpleApiCall,kendo) {
	var obj = {
		descripciones: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdDescripcion",
					fields: {
						IdDescripcion: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						DescripcionIncidencia: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdTipoIncidencia: {
							type: "number",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		estados: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdEstado",
					fields: {
						IdEstado: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						EstadoIncidencia: {
							type: "String",
							editable: false,
							nullable: false
						},
						Observaciones: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		grupos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdGrupo",
					fields: {
						IdGrupo: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						GrupoIncidencia: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		municipios: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdMunicipio",
					fields: {
						IdMunicipio: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						NombreMunicipio: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdProvincia: {
							type: "number",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		provincias: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdProvincia",
					fields: {
						IdProvincia: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						NombreProvincia: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		tipos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipo",
					fields: {
						IdTipo: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						TipoIncidencia: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdGrupoIncidencia: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
					}
				}
			}
		}),
		prioridades: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdPrioridad",
					fields: {
						IdPrioridad: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						Prioridad: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		tiposDatos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoDato",
					fields: {
						IdTipoDato: {
							type: "number",
							editable: false,
							nullable: false
						},
						TipoDato: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		tiposElementos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoElemento",
					fields: {
						IdTipoElemento: {
							type: "number",
							editable: false,
							nullable: false
						},
						Tipo: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		tiposElementosCampos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoElementoCampo"
				}
			}
		}),
		areasNegocio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdAreaNegocio",
					fields: {
						IdAreaNegocio: {
							type: "String",
							editable: false,
							nullable: false
						},
						AreaNegocio: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		tiposRecurso: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoRecurso",
					fields: {
						IdTipoRecurso: {
							type: "number",
							editable: false,
							nullable: false
						},
						TipoRecurso: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		estadosServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdEstado",
					fields: {
						IdEstado: {
							type: "number",
							editable: false,
							nullable: false
						},
						EstadoServicio: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		periodosServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdPeriodo",
					fields: {
						IdPeriodo: {
							type: "number",
							editable: false,
							nullable: false
						},
						Periodo: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		tiposServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoRecurso",
					fields: {
						IdTipoServicio: {
							type: "number",
							editable: false,
							nullable: false
						},
						TipoServicio: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdAreaNegocio: {
							type: "String",
							editable: false,
							nullable: false
						}
					},
					AreaNegocio: function() {
						return obj.areasNegocio.get(this.IdAreaNegocio);
					}
				}
			}
		}),
		turnosServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTurno",
					fields: {
						IdTurno: {
							type: "number",
							editable: false,
							nullable: false
						},
						Turno: {
							type: "String",
							editable: false,
							nullable: false
						},
						HoraInicio: {
							type: "String",
							editable: false,
							nullable: false
						},
						HoraFin: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		rutaSectorServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdRutaSector",
					fields: {
						IdRutaSector: {
							type: "number",
							editable: false,
							nullable: false
						},
						Codigo: {
							type: "String",
							editable: false,
							nullable: false
						},
						Nombre: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdAreaNegocio: {
							type: "String",
							editable: false,
							nullable: false
						}
					},
					AreaNegocio: function() {
						return obj.areasNegocio.get(this.IdAreaNegocio);
					}
				}
			}
		}),
		estadosOT: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdEstado",
					fields: {
						IdEstado: {
							type: "number",
							editable: false,
							nullable: false
						},
						Estado: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		categoriasRecursos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdCategoriaRecurso",
					fields: {
						IdCategoriaRecurso: {
							type: "number",
							editable: false,
							nullable: false
						},
						CategoriaRecurso: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdTipoRecurso: {
							type: "number",
							editable: false,
							nullable: false
						}
					},
					TipoRecurso: function() {
						return obj.tiposRecurso.get(this.IdTipoRecurso);
					}
				}
			}
		}),
		tareasServicio: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTarea",
					fields: {
						IdTarea: {
							type: "number",
							editable: false,
							nullable: false
						},
						Tarea: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdTipoServicio: {
							type: "number",
							editable: false,
							nullable: false
						}
					},
					TipoServicio: function() {
						return obj.tiposServicio.get(this.IdTipoServicio);
					}
				}
			}
		}),
		recursos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdRecurso",
					fields: {
						IdRecurso: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdCategoriaRecurso: {
							type: "number",
							editable: false,
							nullable: false
						},
						CategoriaRecurso: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdTipoRecurso: {
							type: "number",
							editable: false,
							nullable: false
						},
						Recurso: {
							type: "String",
							editable: false,
							nullable: false
						},
					},
					TipoRecurso: function() {
						return obj.tiposRecurso.get(this.IdTipoRecurso);
					}
				}
			}
		}),
		tiposRecursos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoRecurso",
					fields: {
						IdTipoRecurso: {
							type: "String",
							editable: false,
							nullable: false
						},
						TipoRecurso: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		areas: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdArea"
				}
			}
		}),
		calendarios: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdCalendario"
				}
			}
		}),
		fasesCreacion: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdFaseCreacion"
				}
			}
		}),
		tiposActividad: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoActividad"
				}
			}
		}),
		zonasAdministrativas: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdZonaAdmv"
				}
			}
		}),
		rutasSector: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdRutaSector"
				}
			}
		}),
		updated: new signals.Signal(),
		update: function(contractId){
			this.loadLocal(contractId);
			var call = new SimpleApiCall();
			call.succeed.addOnce(function(data) {
				console.log(data);
				this._createSources(data);
				this.saveLocal(data,contractId);
			}.bind(this));
			call.get(window.app.config.baseUrl + "/tablasmaestras",{
				id:	window.app.user.ticket,
				idContrato: contractId
			});
		},
		_createSources: function(data){
			this.descripciones.data(data.Descripciones);
			this.estados.data(data.Estados);
			this.grupos.data(data.Grupos);
			this.municipios.data(data.Municipios);
			this.provincias.data(data.Provincias);
			this.tipos.data(data.Tipos);
			this.prioridades.data(data.Prioridades);
			this.tiposDatos.data(data.TiposDatos);
			this.tiposElementos.data(data.TiposElementos);
			this.tiposElementosCampos.data(data.TiposElementosCampos);
			this.areasNegocio.data(data.AreasNegocio);
			this.tiposRecurso.data(data.TiposRecurso);
			this.estadosServicio.data(data.EstadosServicio);
			this.periodosServicio.data(data.PeriodosServicio);
			this.tiposServicio.data(data.TiposServicio);
			this.turnosServicio.data(data.TurnosServicio);
			this.rutaSectorServicio.data(data.RutaSectorServicio);
			this.estadosOT.data(data.EstadosOT);
			this.categoriasRecursos.data(data.CategoriasRecursos);
			this.tareasServicio.data(data.TareasServicio);
			this.recursos.data(data.Recursos);
			this.areas.data(data.Areas);
			this.calendarios.data(data.Calendarios);
			this.fasesCreacion.data(data.FasesCreacion);
			this.tiposActividad.data(data.TiposActividad);
			this.zonasAdministrativas.data(data.ZonasAdmv);
			this.rutasSector.data(data.RutasSector);
		},
		loadLocal: function(contractId){
			console.log("Loading local tables for contract: " + contractId);
			var _localdata = JSON.parse(window.localStorage.getItem("tables"+contractId)) || {};
			this._createSources(_localdata);
			console.log(this);
		},
		saveLocal: function(data,contractId){
			window.localStorage.setItem("tables"+contractId,JSON.stringify(data));
		}
	};
	return obj;
});

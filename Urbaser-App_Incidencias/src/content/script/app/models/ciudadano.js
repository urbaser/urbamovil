define([
	'jQuery',
	'signals',
	'refr3sh/net/SimpleApiCall',
	'app/config',
	"kendo/kendo.data",
	'app/models/masterTables'
], function(j,signals,SimpleApiCall,config,kendo,masterTables)
{
	var _usr = JSON.parse(window.localStorage.getItem("user")) || {};
	var _prf = JSON.parse(window.localStorage.getItem("preferences")) || {};
	var obj = {
		user: _usr.user,
		pass: _usr.pass,
		ticket: _usr.ticket,
		profile: _usr.profile,
		contracts: new kendo.data.DataSource({data:_usr.contractsData}),
		currentContract: _usr.currentContract,
		preferences: _prf,
		loggedIn: new signals.Signal(),
		loginFailed: new signals.Signal(),
		login: function(){
			var call = new SimpleApiCall();
			call.succeed.addOnce(function(data){
				console.log("login",data);
				if(!data){
					this.loginFailed.dispatch();
				}
				else{
					this.profile = data;
					this.ticket = data.TicketSesion;
					this.saveLocal();
					this.gotContracts.addOnce(function(){
						this.loggedIn.dispatch();
					}.bind(this));
					this.getContracts()
				}
			}.bind(this));
			call.failed.addOnce(function(data){
				this.loginFailed.dispatch();
			}.bind(this));
			call.get(window.app.config.baseUrl + "/ciudadanos",{correo:this.user,cc:this.password});
		},
		gotPassword: new signals.Signal(),
		getPasswordFailed: new signals.Signal(),
		getPassword: function(){
			var call = new SimpleApiCall();
			call.succeed.addOnce(function(data){
				console.log("ciudadano",data);
				this.saveLocal();
				this.gotPassword.dispatch();
			}.bind(this));
			call.failed.addOnce(function(data){
				if(data.status == 404){
					this.getPasswordFailed.dispatch(T("models.ciudadano.user-dont-exists"));
				}
				else{
					this.registeredFailed.dispatch(T("models.ciudadano.generic-error"));
				}
			}.bind(this));
			call.post(window.app.config.baseUrl + "/ciudadanos?correo=" + encodeURIComponent(this.user));
		},
		registered: new signals.Signal(),
		registeredFailed: new signals.Signal(),
		register: function(user){
			var call = new SimpleApiCall();
			call.succeed.addOnce(function(data){
				console.log("ciudadano",data);
				this.registered.dispatch();
				this.saveLocal();
			}.bind(this));
			call.failed.addOnce(function(data){
				if(data.status == 409){
					this.registeredFailed.dispatch(T("models.ciudadano.user-exists"));
				}
				else{
					this.registeredFailed.dispatch(T("models.ciudadano.generic-error"));
				}
			}.bind(this));
			this.user = user.Email;

			call.put(window.app.config.baseUrl + "/ciudadanos",user);
		},
		gotContracts: new signals.Signal(),
		gotContractsError: new signals.Signal(),
		getContracts: function(){
			var call = new SimpleApiCall();
			call.succeed.addOnce(function(data){
				this.contractsData = data;
				this.saveLocal();
				this.contracts.data(data);
				this.gotContracts.dispatch();
			}.bind(this));
			call.get(window.app.config.baseUrl + "/contratos",{id:this.ticket});
		},
		selectContract: function(item){
			console.log("Seleccionado contrato: " + item.Nombre);
			masterTables.update(item.IdContrato);
			this.currentContract = item;
			this.saveLocal();
			console.log("Seleccionado contrato: " + item.IdContrato);
		},
		saveLocal: function(){
			window.localStorage.setItem("user",JSON.stringify({
				user:this.user,
				pass:this.pass,
				profile:this.profile,
				ticket:this.ticket,
				contractsData: this.contractsData,
				currentContract: this.currentContract
			}));
			window.localStorage.setItem("preferences",JSON.stringify(this.preferences));
		},
		clear: function(){
			console.log("Clearing user");
			this.user = null;
			this.pass = null;
			this.profile = null;
			this.ticket = null;
			this.contractsData = null;
			this.currentContract = null;
			window.localStorage.removeItem("user");
		},
		isSelected: function(){
			return "a";
		}


	};
	Object.defineProperty(obj,"nombreCompleto",{
		get: function(){
			return this.profile ? this.profile.Nombre + " " + this.profile.Apellido1 : "";
		}
	});
	return obj;
});

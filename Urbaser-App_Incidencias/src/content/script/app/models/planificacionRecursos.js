define([
	'signals',
	'refr3sh/net/SimpleApiCall',
	'kendo/kendo.data',
], function(signals,SimpleApiCall,kendo) {
	var obj = {
		categorias: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdCategoriaRecurso",
					fields: {
						IdCategoriaRecurso: {
							//data type of the field {Number|String|Boolean|Date} default is String
							type: "number",
							editable: false,
							nullable: false
						},
						CategoriaRecurso: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdTipoRecurso: {
							type: "number",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		recursos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdRecurso",
					fields: {
						IdRecurso: {
							type: "number",
							editable: false,
							nullable: false
						},
						IdCategoriaRecurso: {
							type: "number",
							editable: false,
							nullable: false
						},
						CategoriaRecurso: {
							type: "String",
							editable: false,
							nullable: false
						},
						Recurso: {
							type: "String",
							editable: false,
							nullable: false
						},
						IdTipoRecurso: {
							type: "number",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		updated: new signals.Signal(),
		update: function(tipoServicioId){
			var call = new SimpleApiCall();
			call.succeed.addOnce(function(data) {
				console.log(data);
				this._createSources(data);
			}.bind(this));
			call.get(window.app.config.baseUrl + "/PlanificacionCategoriasRecursos/"+tipoServicioId);
		},
		_createSources: function(data){
			this.categorias.data(data.Categorias);
			this.recursos.data(data.Recursos);
		},
	};
	return obj;
});

/**
 * Created by 7daysofrain on 25/3/15.
 */
define([
	"app/models/masterTables",
	"app/models/inventario",
	"kendo/kendo.core",
	"refr3sh/fn/throttle",
	"kendo/kendo.data",
	"kendo/kendo.binder"], function(tablas,inventario,kendo,throttle)
{
	/**
	 * Called by the DataSource to convert parameters/body into the appropriate format.
	 * @param data
	 * @param requestType One of "read", "create", "update", or "destroy"
	 * @returns {*}
	 */
	function parameterMap(data, requestType) {
		console.log("Request type: " + requestType);
		if (requestType === 'create' || requestType === 'update' || requestType === 'read') {
			return JSON.stringify(data);
		}
		return data;
	}
	function error(){
		alert("EError");
	}

	var itemInventario = {
		id: "IdElemento",
		tipoElemento: function () {
			var i = tablas;
			var el = i.tiposElementos.get(this.IdTipoElemento);
			console.log(el)
			return el ? el.Tipo : "";
		},
		provincia: function () {
			var i = tablas;
			var el = i.provincias.get(this.IdProvincia);
			console.log(el)
			return el ? el.NombreProvincia : "";
		},
		municipio: function () {
			var i = tablas;
			var el = i.municipios.get(this.IdMunicipio);
			console.log(el)
			return el ? el.NombreMunicipio : "";
		},
		parseCampos: function () {
			var data = [];
			var campos = this.CamposValor.toJSON();
			var d = tablas.tiposElementosCampos.data();
			var t = this;
			for (var name in campos) {
				tablas.tiposElementosCampos.filter({
					IdTipoElemento: this.IdTipoElemento,
					Campo: name
				});
				data.push({
					Id: data.length,
					Nombre: name,
					Valor: campos[name],
					Definicion: d.filter(function (el) {
						return el.IdTipoElemento == t.IdTipoElemento && el.Campo == name;
					})[0]
				});
			}
			this.Campos = new kendo.data.DataSource({data: data});
			this.Campos.read();
		}
	};

	var filemap = new Object();
	var fileurimap = new Object();
	var obj = {
		dataSource: new kendo.data.DataSource({
			offlineStorage: "inventarios-offline",
			data:[],
			serverPaging:true,
			pageSize: 50,
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/InventarioFiltroPage'},
					dataType: "json",
					type: "POST",
					contentType: "application/json",
					data: function(){
						if(obj.filter){
							console.log("Pidiendo incidencias filtradas");
							return obj.filter;
						}
						return {};
					}
				},
				create: {
					url: function(){
						return window.app.config.baseUrl + '/inventario'
					},
					dataType: "json",
					contentType: "application/json",
					type: "PUT",
					processData: false
				},
				update: {
					url: function(){
						return window.app.config.baseUrl + '/inventario'
					},
					dataType: "json",
					contentType: "application/json",
					type: "POST",
					processData: false
				},
				destroy: function (e) {
					// locate item in original datasource and remove it
					sampleData.splice(getIndexById(e.data.ProductID), 1);
					// on success
					e.success();
					// on failure
					//e.error("XHR response", "status code", "error message");
				},
				parameterMap: parameterMap
			},
			error: function (e) {
				console.error(e);
				if(window.app.kendoApp && window.app.kendoApp.pane){
					window.app.kendoApp.pane.loader.hide();
				}
				if(e.xhr.status != 401){
					throttle(error,5000);
				}
			},
			batch: false,
			schema: {
				model: itemInventario,
				total: function(){
					return this.totalItems;
				},
				parse: function(data) {
					if(!data)return;
					this.totalItems = data.total;
					data = data.data;
					function parseObject(it){
						it.FechaCreacion = new Date(it.FechaCreacion);
					}
					if(data instanceof Array){
						data.forEach(function(it){
							parseObject(it);
						})
					}
					else if(data instanceof Object){
						parseObject(data);
					}

					return data;
				}
			},
			sync: function(e) {
				var data = this.data();
				for(var i = 0 ; i < data.length ; i++){
					if(filemap[data[i].uid]){
						filemap[data[i].uid].forEach(function(el){
							var dactum = data[i];
							var fdata = new FormData();
							filemap[dactum.uid].forEach(function(filedata){
								var file = filedata.file;
								var field = filedata.field;
								var fieldName = filedata.fieldName;
								fdata.append("image"+i, file, file.name);
								var url = window.app.config.baseUrl + "/InventarioFichero/" + dactum.IdElemento + "?field=" + field;

								$.ajax({
									type: "POST",
									url: url,
									contentType: false,
									processData: false,
									data: fdata,
									success: function (results) {
										console.log(results);
										dactum.CamposValor[fieldName] = results;
									},
									complete: function(){
										window.app.kendoApp.pane.loader.hide();
									}
								});
								console.log("Enviado fichero adjunto");
							})
						});

						delete filemap[dactum.uid];
					}
				}
			},
			requestStart: function(e) {
				if(window.app.kendoApp && window.app.kendoApp.pane)
					window.app.kendoApp.pane.loader.show();
				if(!window.app.user || !window.app.user.ticket){
					e.preventDefault();
				}
				if(e.type == "update" || e.type == "create"){

					var data = this.data();
					for(var i = 0 ; i < data.length ; i++){
						if(data[i].files){
							filemap[data[i].uid] = new Array();
							data[i].files.forEach(function(el,i){
								filemap[data[i].uid].push(el);
							})
						}
						delete data[i].files;
					}
				}
			},
			requestEnd: function(e){
				if(window.app.kendoApp)
					window.app.kendoApp.pane.loader.hide();
			}
		}),
		nearBy: new kendo.data.DataSource({
			data:[],
			transport: {
				read: {
					url: function(){ return window.app.config.baseUrl + '/Inventario'},
					dataType: "json",
					type: "GET",
					data: function(){
						if(obj.location){
							console.log("Pidiendo incidencias filtradas");
							return obj.location;
						}
						return {};
					}
				}
			},
			error: function (e) {
				console.error(e);
				if(window.app.kendoApp && window.app.kendoApp.pane){
					window.app.kendoApp.pane.loader.hide();
				}
				if(e.xhr.status != 401){
					throttle(error,5000);
				}
			},
			batch: false,
			schema: {
				model: itemInventario,
				parse: function(data) {
					if(!data)return;
					function parseObject(it){
					}
					if(data instanceof Array){
						data.forEach(function(it){
							parseObject(it);
						})
					}
					else if(data instanceof Object){
						parseObject(data);
					}

					return data;
				}
			},

			requestStart: function(e) {
				// if(window.app.kendoApp && window.app.kendoApp.pane)
				// 	window.app.kendoApp.pane.loader.show();
				if(!window.app.user || !window.app.user.ticket){
					e.preventDefault();
				}
			},
			requestEnd: function(e){
				if(window.app.kendoApp)
					window.app.kendoApp.pane.loader.hide();
			}
		}),
		offline: function(){
			this.dataSource.online(false);
		},
		online: function(){
			this.dataSource.online(true);
			setTimeout(this.dataSource.sync,3000);
		},
		select: function(item){
			this.selected = item;
			console.log("Selected: ",this.selected);
		},
		setFilter: function(filter,dontFetch){
			this.filter = filter;
			if(filter){
				console.log("select filter " + filter.Nombre);
			}
			else{
				console.log("Crearing filters");
			}
			if(!dontFetch)
				this.dataSource.read();
		}
	};

	return obj;
});

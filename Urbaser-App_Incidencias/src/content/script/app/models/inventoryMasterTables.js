define([
	'signals',
	'refr3sh/net/SimpleApiCall',
	'kendo/kendo.data',
], function(signals,SimpleApiCall,kendo) {
	var obj = {

		tiposDatos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoDato",
					fields: {
						IdTipoDato: {
							type: "number",
							editable: false,
							nullable: false
						},
						TipoDato: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),
		tiposElementos: new kendo.data.DataSource({
			schema:{
				model: {
					id: "IdTipoElemento",
					fields: {
						IdTipoElemento: {
							type: "number",
							editable: false,
							nullable: false
						},
						Tipo: {
							type: "String",
							editable: false,
							nullable: false
						}
					}
				}
			}
		}),

		updated: new signals.Signal(),
		update: function(contractId){
			this.loadLocal(contractId);
			var call = new SimpleApiCall();

			call.succeed.addOnce(function(data) {
				console.log(data);
				this._createSources(data);
				hasData = true;
				this.saveLocal(data,contractId);
				this.updated.dispatch();
			}.bind(this));

			call.get(window.app.config.baseUrl + "/InventarioMaestras",{
				idContrato: contractId
			});
		},
		_createSources: function(data){
			this.tiposDatos.data(data.TiposDatos);
			this.tiposElementos.data(data.TiposElementos);
		},
		loadLocal: function(contractId){
			console.log("Loading inventory local tables for contract: " + contractId);
			var _localdata = JSON.parse(window.localStorage.getItem("inv_tables"+contractId)) || {};
			this._createSources(_localdata);
		},
		saveLocal: function(data,contractId){
			window.localStorage.setItem("tables"+contractId,JSON.stringify(data));
		}
	};
	var hasData;
	Object.defineProperty(obj,"hasData",{
		get: function(){
			return hasData;
		}
	})
	return obj;
});

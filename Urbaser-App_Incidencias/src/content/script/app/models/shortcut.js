define([
	"kendo/kendo.core",
	"kendo/kendo.data"
], function(kendo,k)
{
	var schema = {
		id: "IdShortcut",
		fields: {
			IdShortcut: { editable: false, nullable: true },
			Label: { type:"String", nullable: false, validation: { required: true}},
			Path: { type:"String", nullable: false, validation: { required: true}},
			Style: { type:"String", nullable: false},

		}
	};
	return kendo.data.Model.define(schema);
});

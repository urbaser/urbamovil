define([
	'jQuery',
	'signals',
	'refr3sh/net/SimpleApiCall',
	'app/config',
	"kendo/kendo.data",
	'app/models/masterTables'
], function(j,signals,SimpleApiCall,config,kendo,masterTables)
{
	console.log("User load");
	var _usr = JSON.parse(window.localStorage.getItem("user")) || {};
	var _prf = JSON.parse(window.localStorage.getItem("preferences")) || {};
	var obj = {
		user: _usr.user,
		pass: _usr.pass,
		ticket: _usr.ticket,
		profile: _usr.profile,
		contracts: new kendo.data.DataSource({data:_usr.contractsData}),
		currentContract: _usr.currentContract,
		preferences: _prf,
		loggedIn: new signals.Signal(),
		loginFailed: new signals.Signal(),
		login: function(){
			var call = new SimpleApiCall();
			call.succeed.addOnce(function(data){
				console.log("login",data);
				if(!data){
					this.loginFailed.dispatch();
				}
				else{
					this.profile = data;
					this.ticket = data.TicketSesion;
					this.saveLocal();
					this.gotContracts.addOnce(function(){
						this.loggedIn.dispatch();
					}.bind(this));
					this.getContracts()
				}
			}.bind(this));
			call.failed.addOnce(function(data){
				this.loginFailed.dispatch();
			}.bind(this));
			call.get(window.app.config.baseUrl + "/usuario",{login:this.user,pass:this.password});
		},
		gotContracts: new signals.Signal(),
		gotContractsError: new signals.Signal(),
		getContracts: function(){
			var call = new SimpleApiCall();
			call.succeed.addOnce(function(data){
				this.contractsData = data;
				this.saveLocal();
				this.contracts.data(data);
				this.gotContracts.dispatch();
			}.bind(this));
			call.get(window.app.config.baseUrl + "/contratos");
		},
		selectContract: function(item){
			console.log("Seleccionado contrato: " + item.Nombre);
			masterTables.update(item.IdContrato);
			this.currentContract = item;
			this.saveLocal();
			console.log("Seleccionado contrato: " + item.IdContrato);
		},
		saveLocal: function(){
			window.localStorage.setItem("user",JSON.stringify({
				user:this.user,
				pass:this.pass,
				profile:this.profile,
				ticket:this.ticket,
				contractsData: this.contractsData,
				currentContract: this.currentContract
			}));
			window.localStorage.setItem("preferences",JSON.stringify(this.preferences));
		},
		clear: function(){
			this.user = null;
			this.pass = null;
			this.profile = null;
			this.ticket = null;
			this.contractsData = null;
			this.currentContract = null;
			window.localStorage.removeItem("user");
		},
		isSelected: function(){
			return "a";
		}


	};
	console.log(_usr);
	Object.defineProperty(obj,"nombreCompleto",{
		get: function(){
			if(this.profile){
				return this.profile.Nombre + " " + this.profile.Apellido1;
			}
			return "";
		}
	});
	Object.defineProperty(obj,"isSmartUS",{
		get: function(){
			if(this.currentContract && this.currentContract.TipoInstalacion == "SmarTools"){
				return true;
			}
			return false;
		}
	});
	Object.defineProperty(obj,"isUrbajardin",{
		get: function(){
			if(this.currentContract && this.currentContract.TipoInstalacion == "URBAJARDIN"){
				return true;
			}
			return false;
		}
	});
	return obj;
});

/**
	* Created by 7daysofrain on 25/3/15.
*/
define([
	"app/models/shortcut",
	"app/services/permissionManager",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"], function(Shortcut,permissionManager,kendo)
{
	var obj = {
		dataSource: new kendo.data.DataSource({
			data: [],
			transport: {
				read: {
					url: 'content/data/shortcuts.json',
					dataType: "json",
					type: "GET"
				}
			},
			error: function (e) {
				console.error(e);
				window.app.kendoApp.pane.loader.hide();
			},
			batch: false,
			schema: {
				model: Shortcut,
				parse: function (data) {
					return data.filter(function(it){
						return permissionManager.hasPermission(it.RoleVisibility);
					});
				}
			}
		})
	};

	return obj;
});

define([
	"app/viewModels/incidencias/index",
	"app/viewModels/incidencias/edit",
	"app/viewModels/incidencias/detail",
	"app/viewModels/incidencias/locationsMap",
	"app/viewModels/incidencias/selectLocation",
	"app/viewModels/incidencias/selectByAddress",
	"app/viewModels/incidencias/selectByInventary",
	"app/viewModels/incidencias/selectByMap",
	"app/viewModels/incidencias/resume",
	"app/viewModels/incidencias/stateHistory",
	"app/viewModels/incidencias/closure",
	"app/viewModels/modals/changeState",
	"app/viewModels/modals/offline",
	"app/viewModels/modals/saveFilter",
	"app/viewModels/modals/confirmDeleteFilter",
	"app/viewModels/home/index",
	"app/viewModels/home/contracts",
	"app/viewModels/home/loginCitizen",
	"app/viewModels/home/citizenPass",
	"app/viewModels/home/notifications",
	"app/viewModels/home/signin",
	"app/viewModels/filtrosDrawer"
], function()
{
	return {
		views: {
			incidencias: {
				index: require("app/viewModels/incidencias/index"),
				edit: require("app/viewModels/incidencias/edit"),
				detail: require("app/viewModels/incidencias/detail"),
				locationsMap: require("app/viewModels/incidencias/locationsMap"),
				selectLocation: require("app/viewModels/incidencias/selectLocation"),
				selectByInventary: require("app/viewModels/incidencias/selectByInventary"),
				selectByAddress: require("app/viewModels/incidencias/selectByAddress"),
				selectByMap: require("app/viewModels/incidencias/selectByMap"),
				resume: require("app/viewModels/incidencias/resume"),
				stateHistory: require("app/viewModels/incidencias/stateHistory"),
				closure: require("app/viewModels/incidencias/closure"),
			},
			home: {
				index: require("app/viewModels/home/index"),
				loginCitizen: require("app/viewModels/home/loginCitizen"),
				citizenPass: require("app/viewModels/home/citizenPass"),
				signin: require("app/viewModels/home/signin"),
				contracts: require("app/viewModels/home/contracts"),
				notifications: require("app/viewModels/home/notifications")
			},
			modals: {
				changeState: require("app/viewModels/modals/changeState"),
				offline: require("app/viewModels/modals/offline"),
				saveFilter: require("app/viewModels/modals/saveFilter"),
				confirmDeleteFilter: require("app/viewModels/modals/confirmDeleteFilter")
			},
			filtrosDrawer: require("app/viewModels/filtrosDrawer")
		},
		layouts: {
		}
	};
});

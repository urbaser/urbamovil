define([], function()
{
	// http://urbpru1234b.cloudapp.net/URBAMOVIL/Help
	var obj = {
	    //baseUrl: "https://aplicaciones.urbaser.com/URBAMOVIL/Api",
	    baseUrl: "https://aplicaciones.urbaser.com/URBAMOVIL_PRE/Api",
	    //baseUrl : "http://desarrollo.urbaser.com/URBAMOVIL/api",
		globalization:{
			supportedLanguages: ["es-es","en-en"],
			supportedLanguagesNames: [	{
											code: "es-es",
											title: "Español"
										},{
											code: "en-en",
											title: "English"
										}],
			supports: function(lang){
				return this.supportedLanguages.indexOf(lang) > -1 ||  this.supportedLanguages.indexOf(this._map[lang.toLowerCase()]) > -1;
			},
			map: function(lang){
				return this.supportedLanguages.indexOf(lang) > -1 ? lang : this._map[lang.toLowerCase()];
			},
			_map: {
				"es": "es-es",
				"en": "en-en",
				"en-gb": "en-en",
				"en-us": "en-en"
			},
			defaultLanguage: "es-es"
		}
	};
	return obj;
});

/**
 * Created by 7daysofrain on 18/5/15.
 */
define(['jQuery','TweenMax'], function()
{
	var obj = {
		init: function(){
			this.closed = true;
			$('.lang-selection').click(this.switchOpen.bind(this));
		},
		switchOpen: function(e){
			TweenMax.to(e.currentTarget,0.6,{bottom:this.closed ? 0 : -$('.lang-selection .km-listview').outerHeight(false)});
			this.closed = !this.closed;
		}
	};
	return obj;
});

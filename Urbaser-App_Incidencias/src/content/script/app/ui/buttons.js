/**
 * Created by 7daysofrain on 11/3/15.
 */
define(['jQuery','modernizr'], function()
{
	var obj = {
		init: function(){
			$(".button[data-icon]").each(function(i,el){
				$el = $(el);
				var path = "content/imgs/icons/" + ($el.attr("data-icon").indexOf(".") == -1 ? $el.attr("data-icon")+".svg" : $el.attr("data-icon") );
				var $ico = $("<span class='button-icon'><img src='" + path + "'></span><span class='label'>" + $el.text() + "</span>");
				$el.empty().append($ico);
				if(!Modernizr.flexgrow){
					$el.find(".label").css("padding-left",8);
				}
			});
		},
		fix: function(){
			return;
			if(!Modernizr.flexgrow){
				console.log("fixing button");
				$(".button[data-icon]").each(function(i,el){
					$el = $(el);
					var w = $el.width()-$el.find(".button-icon").width() - 9;
					$el.find(".label").width(w);
				});
			}
		}
	};
	return obj;
});

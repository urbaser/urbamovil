/**
 * Created by 7daysofrain on 13/5/15.
 */
define([
	"jQuery",
	"kendo/kendo.mobile.application",
	"app/initializer",
	"app/models/masterTables",
	"app/models/incidencias",
	"app/config",

	"app/bootstrap/langDetection",
	"app/bootstrap/versionDetection",
	"app/bootstrap/ticketInyection",
	"app/bootstrap/errorDetection",
	"app/bootstrap/configureKendoBinders",
	"app/bootstrap/kendoAppInitializer",
	"app/bootstrap/masterTablesLoader",
	"app/bootstrap/networkDetection",
	"app/bootstrap/networkErrorDetection",
	"app/bootstrap/backButtonHandler",
	"app/bootstrap/roleBasedUIProcessor",

	"kendo/kendo.mobile.navbar",
	"kendo/kendo.mobile.listview",
	"kendo/kendo.mobile.drawer",
	"kendo/kendo.mobile.tabstrip",
	"modernizr",
	"app/modernizr-tests",
], function(
	j,
	kendo,
	initializer,
	masterTables,
	incidencias,
	config,
	languageDetection,
	versionDetection,
	ticketInyection,
	errorDetection,
	configureKendoBinders,
	kendoAppInitializer,
	masterTablesLoader,
	networkDetection,
	networkErrorDetection,
	backButtonHandler,
	roleBasedUIProcessor
) {
	'use strict'


	var AppBase = function(){
	};
	AppBase.prototype.conf = function(name,version,user,isCitizenApp,debugMode) {
		this.online = true;
		this.name = name;
		this.version = version;
		this.debugMode = debugMode;
		this.user = user;
		this.isCitizenApp = isCitizenApp;
		this.os = !window.cordova ? "web" : window.device.platform;
		console.log("Running " + name + " v." + version +
					"\n\tdebugmode: " + debugMode +
					"\n\tis citizen: " + isCitizenApp +
					"\n\tdevice: " + this.os
		);
		$("html").addClass("platform-" + this.os);
	}
	AppBase.prototype.init = function(){
		if(window.cordova){
			this.appMode = true;
		}
		else{
			this.appMode = false;
		}
		languageDetection.process(this)
			.then(function(app){
				if(app.appMode){
					return versionDetection.process(app);
				}
				else{
					return Promise.resolve(app);
				}
			})
			.catch(function(app){
				return Promise.resolve(app);
			})
			.then(function(app){
				app.startApplication()
			})
	};
	AppBase.prototype.startApplication = function() {
		console.log("App init");

		ticketInyection.process(this)
			.then(function(app){
				return errorDetection.process(app);
			})
			.then(function(app){
				return configureKendoBinders.process(app);
			})
			.then(function(app){
				return masterTablesLoader.process(app);
			})
			.then(function(app){
				return networkDetection.process(app);
			})
			.then(function(app){
				return networkErrorDetection.process(app);
			})
			.then(function(app){
				return backButtonHandler.process(app);
			})
			.then(function(app){
				return kendoAppInitializer.process(app);
			})
			.then(function(app){
				return roleBasedUIProcessor.process(app);
			})
			.then(function(app){

				// UI Modules intitialization
				initializer.start();

				app.updatePendingCount();
				app.doneInit();
				setTimeout(function(){
					$("#body").show();
					if(navigator.splashscreen){
						navigator.splashscreen.hide();
					}
					console.log("App shown: " + (new Date().getTime() - window.ttt) + "ms.");
				},200);
			})

	}
	AppBase.prototype.changeTitle = function(tit){
		this.kendoApp
			.view()
			.header
			.find(".km-navbar")
			.data("kendoMobileNavBar")
			.title(tit);
	}
	AppBase.prototype.updatePendingCount = function(){
		var count = this.pendingOperationsCount();
		$(".user-info .baloon").text(count);
		return count;
	};
	AppBase.prototype.pendingOperationsCount = function(){
		var offlines = incidencias.dataSource.offlineData();
		var count = 0;
		if(offlines && offlines.length){
			offlines.forEach(function(d){
				if(d.__state__)count++;
			});
		}
		return count;
	};
	AppBase.prototype.getStartPage = function(){
		return "#home";
	};

	AppBase.prototype.onChangeRoute = function(){
		console.log("Route Changed: ", e.url);
	}
	AppBase.prototype.onChangeRoute2 = function(){
		// prevents the keyboard scroll bug
		window.app.kendoApp.view().element.find(".km-scroll-container").hide()
		if(window.app.kendoApp.scroller()){
			setTimeout(function(){
				var scr;
				window.app.kendoApp.view().element.find(".km-scroll-container").show()
				if(scr = window.app.kendoApp.scroller()){
					//scr.reset();
					scr.scrollTo(0,0);
				}

			}.bind(this),100);
			document.addEventListener("click",this.rejectClicks,true);
			setTimeout(function(){
				document.removeEventListener("click",this.rejectClicks,true);
			}.bind(this),300);
		}

	}
	AppBase.prototype.rejectClicks = function(event){
		console.log("click rejected");
		event.stopImmediatePropagation();
		event.preventDefault();
	}

	// Helper
	window.showLoading = function(){
		if(window.app.kendoApp && window.app.kendoApp.pane)
			window.app.kendoApp.pane.loader.show();
	}
	window.hideLoading = function(){
		if(window.app.kendoApp && window.app.kendoApp.pane)
			window.app.kendoApp.pane.loader.hide();
	}


	return AppBase;
});

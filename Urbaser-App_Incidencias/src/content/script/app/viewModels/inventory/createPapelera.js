define([
	"jQuery",
	"app/helpers/buttongroup",
	"app/helpers/editModals",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",

], function(j, buttonGroup, editModals, kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: inventory-createPapelera");

			this.view = event.view.element;
			buttonGroup.apply(this.view);
			editModals.apply(this.view);
		},
		onViewShow: function(event){
			console.log("Show view: inventory-createPapelera");
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-createPapelera" );
		}
	};
	return kendo.observable(vm);
});

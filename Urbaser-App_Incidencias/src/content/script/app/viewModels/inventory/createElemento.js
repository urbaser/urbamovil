define([
	"jQuery",
	"app/helpers/buttongroup",
	"app/helpers/editModals",
	"app/models/masterTables",
	"app/models/inventario",
	"app/models/inventarios",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo/kendo.mobile.switch",

], function(j, buttonGroup, editModals, tables, inventory, inventarios, kendo) {
	var vm = {
		model: kendo.observable({}),
		provincias: tables.provincias,
		municipios: tables.municipios,
		areas: tables.areas,
		distritos: inventory.distritos,
		espaciosUrbanos: inventory.espaciosUrbanos,
		zonas: inventory.zonas,
		onViewInit: function(event){
			console.log("Init view: inventory-createElemento");

			this.view = event.view.element;
			buttonGroup.apply(this.view,function(who) {
				console.log(who.prop("tagName"))
				if(who.prop("tagName").toLowerCase() == "div"){
					setTimeout(this.map.refresh.bind(this.map), 500);
					this.center();
				}
			}.bind(this));
			//editModals.apply(this.view);

			$('#inventory-create-map')
				.height($(document).height() - 175)
				.width($(document).width());

			try{
				// Map
				this.template = kendo.template($("#marker-template").html());
				this.map = new GMaps({
					div:"#inventory-create-map",
					zoom: 18,
					width: $(document).width(),
					height: $(window).height() -430,
					zoomControl: false, //{style:google.maps.ZoomControlStyle.LARGE},
					mapTypeControl: false,
					streetViewControl: false,
					panControl: false,
					lat: 40.416705,
					lng: -3.703582,
					draggable: true,
					click:this.onMapClick.bind(this)

				});
				this.map.map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById("inventory-create-map-search-box"));
				this.map.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(document.getElementById("inventory-create-map-buttons"));
				var input = document.getElementById('inventory-create-map-search');
				var options = {
					types: ['geocode'],
					componentRestrictions: {country: 'es'}
				};
				var autocomplete = new google.maps.places.Autocomplete(input, options);
				google.maps.event.addListener(autocomplete, 'place_changed', function() {
					var place = autocomplete.getPlace();
					console.log(place);
					this.map.map.setCenter(place.geometry.location);
				}.bind(this));

				if(!Modernizr.csscalc || !Modernizr.cssvwunit){
					$('#inventory-create-map').height($(document).height() -110).width("100%");
				}
				setTimeout(this.map.refresh.bind(this.map), 500);
			}
			catch(error){
				console.log("No se pudo cargar GMaps");
			}
		},
		onViewShow: function(event){
			console.log("Show view: inventory-createElemento");
			this.set("model",kendo.observable({}));
			var tipo = Number(event.sender.params.tipo);
			var o = tables.tiposElementos.data().find(function(el){
					return el.IdTipoElemento == tipo;
			});
			if(o){
				this.set("model.TipoElemento",o.Tipo);
			}
			this.set("model.IdTipoElemento",tipo);


			this.view.find("form > :not([data-permanent])").remove();
			tables.tiposElementosCampos
				.data()
				.filter(function(el){
					return el.IdTipoElemento == tipo && !el.SoloLectura;
				})
				.forEach(function(field,index){
					//console.log(field);
					if(field.TipoDato == "TEXTO" && field.ListaValores == null){
						this.view.find("form").eq(0).append(this.createTextField(field.Campo,field.IdTipoElementoCampo));
					}
					else if(field.TipoDato == "TEXTO" && field.ListaValores != null){
						this.view.find("form").eq(0).append(this.createDropdown(field.Campo,field.IdTipoElementoCampo,field.ListaValores.split(",")));
					}
					else if(field.TipoDato == "FECHA"){
						this.view.find("form").eq(0).append(this.createDateField(field.Campo,field.IdTipoElementoCampo));
					}
					else if(field.TipoDato == "NUMÉRICO"){
						this.view.find("form").eq(0).append(this.createNumberField(field.Campo,field.IdTipoElementoCampo));
					}
					else if(field.TipoDato == "BOOLEANO"){
						this.view.find("form").eq(0).append(this.createBooleanField(field.Campo,field.IdTipoElementoCampo));
					}
					else if(field.TipoDato == "FICHERO"){
						this.view.find("form").eq(0).append(this.createFileField(field.Campo,field.IdTipoElementoCampo));
					}
					else{
						console.warn("No se ha generado campo para tipo: " + field.TipoDato);
					}
				}.bind(this));

			if(this.map){
				this.map.removeOverlays();
			}
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-createElemento" );
		},
		validate: function(){
			if(!this.model.Longitud || !this.model.Latitud){
				alert("Debes especificar una ubicación para poder guardar el elemento.")
				return false;
			}
			return true;
		},
		populateAndSend: function(){
			this.model.CamposValor = {};
			tables.tiposElementosCampos
					.data()
					.forEach(function(field,index){
						var val = this.view.find("form [data-tipo-campo=" + field.IdTipoElementoCampo + "]").val();
						if(val){
							if(field.TipoDato == "FECHA"){
								this.model.CamposValor[field.Campo] = kendo.parseDate(val);
							}
							else if(field.TipoDato == "NUMÉRICO"){
								this.model.CamposValor[field.Campo] = Number(val);
							}
							else if(field.TipoDato == "BOOLEANO"){
								this.model.CamposValor[field.Campo] = val == "on";
							}
							else if(field.TipoDato == "FICHERO"){
								// Do nothing
							}
							else{
								this.model.CamposValor[field.Campo] = val;
							}
						}
					}.bind(this));
			if(this.validate()){
				inventarios.dataSource.add(this.model);
				inventarios.dataSource.sync();
				return true;
			}
			else{
				return false;
			}
		},
		onSave: function(event){
			event.preventDefault();
			var saved = this.populateAndSend();
			if(saved){
				window.app.kendoApp.navigate("#inventory-index");
			}
		},
		onSaveAndDuplicate: function(event){
			event.preventDefault();
			var saved = this.populateAndSend();
			if(!saved){
				return false;
			}
			var obj = this.model.toJSON();
			obj.TAG = "";
			obj.Referencia = "";
			this.set("model",kendo.observable(obj));
			window.app.kendoApp.scroller().reset();
		},
		onReadTag: function (event) {
			event.preventDefault();
			editModals.openTagReader(function(tag){
				this.model.set("TAG",tag);
			}.bind(this))
		},
		// Mapa


		onMapClick: function(e){
			if(this.addMode){
				this.model.Longitud = e.latLng.lng();
				this.model.Latitud = e.latLng.lat();
				this.map.removeOverlays();
				this.drawMarker(e.latLng.lat(),e.latLng.lng(),1);
				this.switchAdd();
			}
		},
		drawMarker: function(lat,lng,index){
			this.map.drawOverlay({
				lat: lat,
				lng: lng,
				content: this.template({index:index}),
				click: function(e){
					if(this.currentExpandedMarker){
						$(this.currentExpandedMarker.el).find(".options").css("visibility","hidden");
					}
					if(this.currentExpandedMarker && this.currentExpandedMarker.el == e.el){
						this.currentExpandedMarker = null;
						return;
					}
					this.currentExpandedMarker = e;
					var options = $(e.el).find(".options");
					options.css("visibility",options.css("visibility")== "visible" ? "hidden" : "visible");
					$(e.el).find(".button").click(this.removeDir.bind(this));
				}.bind(this)
			});
		},
		removeDir: function(e){
			var i = $(e.currentTarget).parent().parent().attr("data-index");
			console.log("Removing at index",i);
			delete this.model.Longitud;
			delete this.model.Latitud;
			this.currentExpandedMarker.setMap(null);
		},
		center: function(e){
			console.log("cetner");
			if(e)e.preventDefault();
			window.app.kendoApp.pane.loader.show();
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(this.showPosition.bind(this), onError);
			} else {
				onError();
			}

			function onError() {
				window.app.kendoApp.pane.loader.hide();
				console.error('Geolocation failed: '+error.message);
				alert(T("views.incidencias.selectLocation.geoloc-error"));
			}
		},
		showPosition: function(position){
			window.app.kendoApp.pane.loader.hide();
			this.map.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
		},
		switchAdd: function(e){
			if(e)e.preventDefault();
			this.addMode = !this.addMode;
			$("#map-switch-add").find("img").attr("src",this.addMode ? "content/imgs/map-add-location-selected.svg" : "content/imgs/map-add-location.svg")
		},
		showHelp: function(e){
			e.preventDefault();
			$("#modal-mapHelp").kendoMobileModalView("open");
		},

		// Propiedades
		provinciaChange: function(e){
			this.model.IdProvincia = this.Provincia.IdProvincia;
			if (this.hasOwnProperty('Provincia')) {
				tables.municipios.filter({
					field: "IdProvincia",
					operator: "eq",
					value: this.model.IdProvincia
				});
			}
			this.view.find(".select-municipios")
				.removeAttr("disabled")
				//.prepend("<option disabled selected>" + T("views.incidencias.edit.select-municipio") + "</option>")
				.parent()
				.removeClass("disabled");
			delete this.model.IdMunicipio;

			this.view.find(".select-distritos")
				.val([])
				.attr("disabled","")
				.find("option:selected").prop("selected", 0);
			delete this.model.IdDistrito;

			this.view.find(".select-espacios")
				.val([])
				.attr("disabled","")
				.find("option:selected").prop("selected", 0);
			delete this.model.IdEspacioUrbano;

			this.view.find(".select-zonas")
				.val([])
				.attr("disabled","")
				.find("option:selected").prop("selected", 0);
			delete this.model.IdZona;

		},
		municipioChange: function(e){
			this.model.IdMunicipio = this.Municipio.IdMunicipio;
			if (this.hasOwnProperty('Municipio')) {
				inventory.distritos.filter({
					field: "IdMunicipio",
					operator: "eq",
					value: this.model.IdMunicipio
				});
			}
			this.view.find(".select-distritos")
				.removeAttr("disabled")
				//.prepend("<option disabled selected>" + T("views.incidencias.edit.select-distrito") + "</option>")
				.parent()
				.removeClass("disabled");
			delete this.model.IdDistrito;

			this.view.find(".select-espacios")
				.val([])
				.attr("disabled","")
				.find("option:selected").prop("selected", 0);
			delete this.model.IdEspacioUrbano;

			this.view.find(".select-zonas")
				.val([])
				.attr("disabled","")
				.find("option:selected").prop("selected", 0);
			delete this.model.IdZona;
		},
		distritoChange: function(e){
			this.model.IdDistrito = this.Distrito.IdDistrito;
			if (inventory.espaciosUrbanos.params != this.model.IdDistrito) {
				inventory.espaciosUrbanos.params = this.model.IdDistrito;
				inventory.espaciosUrbanos.read();
				this.view.find(".select-espacios")
					.removeAttr("disabled")
					.empty()
					.prepend("<option disabled selected>" + T("app.loading") + "</option>")
					.parent()
					.removeClass("disabled");
				delete this.model.IdEspacioUrbano;

				this.view.find(".select-zonas")
					.val([])
					.attr("disabled","")
					.find("option:selected").prop("selected", 0);
				delete this.model.IdZona;
			}
		},
		espacioChange: function(e){
			this.model.IdEspacioUrbano = this.EspacioUrbano.IdEspacioUrbano;
			if (inventory.zonas.params != this.model.IdEspacioUrbano) {
				inventory.zonas.params = this.model.IdEspacioUrbano;
				inventory.zonas.read();
				this.view.find(".select-zonas")
					.removeAttr("disabled")
					.empty()
					.prepend("<option disabled selected>" + T("app.loading") + "</option>")
					.parent()
					.removeClass("disabled");
				delete this.model.IdZona;
			}
		},
		zonaChange: function(e){
			delete this.model.IdZona;
			delete this.model.IdRuta;
			if(this.Zona.IdZona){
				this.model.IdZona = this.Zona.IdZona
			}
			if(this.Zona.IdRuta){
				this.model.IdRuta = this.Zona.IdRuta
			}
		},
		areaChange: function(e){
			this.model.IdArea = this.Area.IdArea;
		},
		imageChange: function(event){
			event.preventDefault();
			console.log("Image change")
			if(event.target.files[0].size > 8 * 1024 * 1024){
				alert(T("views.incidencias.edit.bigimage"));
				return;
			}
			if(!this.model.files){
				this.model.files = new Array();
			}
			if(event.target.files[0]){
				this.model.files.push({
					file: event.target.files[0],
					field: $(event.target).attr("data-tipo-campo"),
					fieldName:  $(event.target).attr("data-label-campo"),
				});
				this.model.dirty = true;
			}
		},
		// Creacion dinámica de campos
		createTextField: function(label,idTipoCampo){
			label = label.substr(0,20);
			return $('<div><label>' + label + ':</label><input class="form-input" type="text" data-tipo-campo="' + idTipoCampo + '" /></div>');
		},

		createDateField: function(label,idTipoCampo){
			label = label.substr(0,20);
			return $('<div><label>' + label + ':</label><input class="form-input" type="date" data-tipo-campo="' + idTipoCampo + '" /></div>');
		},

		createFileField: function(label,idTipoCampo){
			label = label.substr(0,20);
			var el = $('<div><label>' + label + ':</label><input class="form-input" type="file" data-bind="events: { change: imageChange }" accept="image/x-png, image/gif, image/jpeg" data-tipo-campo="' + idTipoCampo + '" data-label-campo="' + label + '" /></div>');
			kendo.init(el, this, kendo.mobile.ui);
			el.find("input").on("change",this.imageChange.bind(this));
			return el;
		},

		createNumberField: function(label,idTipoCampo){
			label = label.substr(0,20);
			return $('<div><label>' + label + ':</label><input class="form-input" type="number" data-tipo-campo="' + idTipoCampo + '" /></div>');
		},

		createBooleanField: function(label,idTipoCampo){
			label = label.substr(0,20);
			var swit = $('<div><label>' + label + ':</label><input type="checkbox" data-role="switch" data-off-label="No" data-on-label="Si" data-tipo-campo="' + idTipoCampo + '" /></div>');
			kendo.init(swit, this, kendo.mobile.ui);
			return swit;
		},
		createDropdown: function(label,idTipoCampo,valores){
			label = label.substr(0,20);
			var el = $('<div><label>' + label + ':</label><span class="form-select"><select data-tipo-campo="' + idTipoCampo + '"></select></span></div>');
			el.find("select").append("<option> </option>")
			valores.forEach(function (valor){
				el.find("select").append("<option>" + valor + "</option>")
			});
			return el;
		}
	};
	return kendo.observable(vm);
});

define([
	"app/models/inventario",
	"app/models/inventarios",
	"app/models/masterTables",
	"jQuery",
	"app/helpers/buttongroup",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(inventory, inventories, tables, j, buttonGroup,kendo) {
	'use strict'
	var vm = {
		tiposElementos: tables.tiposElementos,
		provincias: tables.provincias,
		municipios: tables.municipios,
		areas: tables.areas,
		distritos: inventory.distritos,
		espaciosUrbanos: inventory.espaciosUrbanos,
		zonas: inventory.zonas,
		dataSource: inventories,
		filtrada: new kendo.data.DataSource(),
		model: {  },
		onViewInit: function(event){
			console.log("Init view: inventory-search");
			this.view = event.view.element;
			var view = $(this.view);

			var listviews = view.find(".cat-1 > *");

			view.find(".inventory-category .buttongroup").kendoMobileButtonGroup({
				select: function(e) {
					listviews.hide()
						.eq(e.index)
						.show();
				},
				index: 0
			});
			/*this.view.find(".select-inventory-categories").prepend("<option disabled selected>" + T("models.inventario.select-type") + "</option>");
			this.view.find(".select-provincias").prepend("<option disabled selected>" + T("views.incidencias.edit.select-provincia") + "</option>");
			this.view.find(".select-municipios").prepend("<option disabled selected>" + T("views.incidencias.edit.select-municipio") + "</option>");
			this.view.find(".select-distritos").prepend("<option disabled selected>" + T("views.incidencias.edit.select-distrito") + "</option>");
			this.view.find(".select-espacios").prepend("<option disabled selected>" + T("views.incidencias.edit.select-espacio") + "</option>");
			this.view.find(".select-zonas").prepend("<option disabled selected>" + T("views.incidencias.edit.select-zona") + "</option>");
			*/
			inventory.espaciosUrbanos.params = null;
			setTimeout(this.onReset.bind(this),100);
		},
		onViewShow: function(event){
			console.log("Show view: inventory-search");
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-search" );
		},
		createModel: function(){
			this.model = {
				Jerarquia: {}
			};
		},
		provinciaChange: function(e){
			this.model.Jerarquia.IdProvincia = this.model.Provincia.IdProvincia;
			if (this.model.hasOwnProperty('Provincia')) {
				tables.municipios.filter({
					field: "IdProvincia",
					operator: "eq",
					value: this.model.Jerarquia.IdProvincia
				});
			}
			this.view.find(".select-municipios")
				.removeAttr("disabled")
				//.prepend("<option disabled selected>" + T("app.loading") + "</option>")
				.parent()
				.removeClass("disabled");
			delete this.model.Jerarquia.IdMunicipio;

			this.view.find(".select-distritos")
				.val([])
				.attr("disabled","")
				.find("option:selected").prop("selected", 0);
			delete this.model.Jerarquia.IdDistrito;

			this.view.find(".select-espacios")
				.val([])
				.attr("disabled","")
				.find("option:selected").prop("selected", 0);
			delete this.model.Jerarquia.IdEspacioUrbano;

			this.view.find(".select-zonas")
				.val([])
				.attr("disabled","")
				.find("option:selected").prop("selected", 0);
			delete this.model.Jerarquia.IdZona;

		},
		municipioChange: function(e){
			this.model.Jerarquia.IdMunicipio = this.model.Municipio.IdMunicipio;
			if (this.model.hasOwnProperty('Municipio')) {
				inventory.distritos.filter({
					field: "IdMunicipio",
					operator: "eq",
					value: this.model.Jerarquia.IdMunicipio
				});
			}
			this.view.find(".select-distritos")
				.removeAttr("disabled")
				//.prepend("<option disabled selected>" + T("views.incidencias.edit.select-distrito") + "</option>")
				.parent()
				.removeClass("disabled");
			delete this.model.Jerarquia.IdDistrito;

			this.view.find(".select-espacios")
				.val([])
				.attr("disabled","")
				.find("option:selected").prop("selected", 0);
			delete this.model.Jerarquia.IdEspacioUrbano;

			this.view.find(".select-zonas")
				.val([])
				.attr("disabled","")
				.find("option:selected").prop("selected", 0);
			delete this.model.Jerarquia.IdZona;
		},
		distritoChange: function(e){
			console.log("Distrito change",this.model.Distrito)
			this.model.Jerarquia.IdDistrito = this.model.Distrito.IdDistrito;
			if (inventory.espaciosUrbanos.params != this.model.Jerarquia.IdDistrito) {
				inventory.espaciosUrbanos.params = this.model.Jerarquia.IdDistrito;
				inventory.espaciosUrbanos.read();
				this.view.find(".select-espacios")
						.removeAttr("disabled")
						.empty()
						.prepend("<option disabled selected>" + T("app.loading") + "</option>")
						.parent()
						.removeClass("disabled");
				delete this.model.Jerarquia.IdEspacioUrbano;

				this.view.find(".select-zonas")
					.val([])
					.attr("disabled","")
					.find("option:selected").prop("selected", 0);
				delete this.model.Jerarquia.IdZona;
			}
		},
		espacioChange: function(e){
			this.model.Jerarquia.IdEspacioUrbano = this.model.EspacioUrbano.IdEspacioUrbano;
			if (inventory.zonas.params != this.model.Jerarquia.IdEspacioUrbano) {
				inventory.zonas.params = this.model.Jerarquia.IdEspacioUrbano;
				inventory.zonas.read();
				this.view.find(".select-zonas")
						.removeAttr("disabled")
						.empty()
						.prepend("<option disabled selected>" + T("app.loading") + "</option>")
						.parent()
						.removeClass("disabled");
				delete this.model.Jerarquia.IdZona;
			}
		},
		zonaChange: function(e){
			delete this.model.Jerarquia.IdZona;
			delete this.model.Jerarquia.IdRuta;
			if(this.model.Zona.IdZona){
				this.model.Jerarquia.IdZona = this.model.Zona.IdZona
			}
			if(this.model.Zona.IdRuta){
				this.model.Jerarquia.IdRuta = this.model.Zona.IdRuta
			}
		},
		areaChange: function(e){
			this.model.Jerarquia.IdArea = this.model.Area.IdArea;
		},
		tipoChange: function(e) {
			var categoryId = $('option:selected', e.currentTarget).attr('value');
			this.view.find(".cat-1 form:first-child > div:not([data-permanent])").remove();
			this.model.IdTipoElemento = categoryId;
			tables.tiposElementosCampos
				.data()
				.filter(function(el){
					return el.IdTipoElemento == categoryId;
				})
				.forEach(function(field,index){
					console.log(field);
					if(field.TipoDato == "TEXTO" && field.ListaValores == null){
						this.view.find(".cat-1 form").eq(0).append(this.createTextField(field.Campo,field.IdTipoElementoCampo));
					}
					else if(field.TipoDato == "TEXTO" && field.ListaValores != null){
						this.view.find(".cat-1 form").eq(0).append(this.createDropdown(field.Campo,field.IdTipoElementoCampo,field.ListaValores.split(",")));
					}
					else if(field.TipoDato == "FECHA"){
						this.view.find(".cat-1 form").eq(0).append(this.createDateField(field.Campo,field.IdTipoElementoCampo));
					}
				}.bind(this))
		},
		onBuscar: function(e){
			console.log("filtrando");
			this.model.Propiedades = [];
			tables.tiposElementosCampos
				.data()
				.forEach(function(field,index){
					var val = this.view.find(".cat-1 form [data-tipo-campo=" + field.IdTipoElementoCampo + "]").val();
					if(val){
						var p = {
							IdTipoElementoCampo: field.IdTipoElementoCampo,
							Valor: val
						}
						this.model.Propiedades.push(p);
					}
				}.bind(this));
			inventories.setFilter(this.model);
		},
		onReset: function(e){
			if(e){
				e.preventDefault();
			}
			this.view.find("form").each(function(i,el){
				el.reset();
			});
			this.view.find("select").val(0);
			this.createModel();
		},
		tipoLoad: function(e) {
			console.log("loaded");
		},

		createTextField: function(label,idTipoCampo){
			return $('<div><label>' + label + ':</label><input class="form-input" type="text" data-tipo-campo="' + idTipoCampo + '" /></div>');
		},

		createDateField: function(label,idTipoCampo){
			return $('<div><label>' + label + ':</label><input class="form-input" type="date" data-tipo-campo="' + idTipoCampo + '" /></div>');
		},
		createDropdown: function(label,idTipoCampo,valores){
			var el = $('<div><label>' + label + ':</label><span class="form-select"><select data-tipo-campo="' + idTipoCampo + '"></select></span></div>');
			el.find("select").append("<option> </option>")
			valores.forEach(function (valor){
				el.find("select").append("<option>" + valor + "</option>")
			});
			return el;
		}
	};

	return kendo.observable(vm);
});

define([
	"app/models/inventarios",
	"refr3sh/fn/debounce",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(inventarios,debounce,kendo) {
	'use strict'

	function getBoundsRadius(bounds){
		// r = radius of the earth in km
		var r = 6378.8
		// degrees to radians (divide by 57.2958)
		var ne_lat = bounds.getNorthEast().lat() / 57.2958
		var ne_lng = bounds.getNorthEast().lng() / 57.2958
		var c_lat = bounds.getCenter().lat() / 57.2958
		var c_lng = bounds.getCenter().lng() / 57.2958
		// distance = circle radius from center to Northeast corner of bounds
		var r_km = r * Math.acos(
				Math.sin(c_lat) * Math.sin(ne_lat) +
				Math.cos(c_lat) * Math.cos(ne_lat) * Math.cos(ne_lng - c_lng)
			)
		return r_km; // radius in meters
	}
	var autocomplete;
	var vm = {
		onViewInit: function(event){
			console.log("Init view: inventory-mapsearch");
			this.view = event.view.element;

			// Map init
			this.template = kendo.template($("#inventory-marker-template").html());
			this.map = new GMaps({
				div:"#inventory-search-map",
				width: $(document).width(),
				height: $(window).height() - 110,
				zoom: 18,
				zoomControl: true,
				zoomControlPosition:"BOTTOM_RIGHT",
				mapTypeControl: false,
				streetViewControl: false,
				panControl: false,
				lat: 40.416705,
				lng: -3.703582,
				draggable: true,
				bounds_changed: debounce(function(){
					var coords = this.map.map.getCenter();
					console.log("Showing position",coords);
					window.app.kendoApp.pane.loader.hide();
					inventarios.location = {
						x: coords.lat,
						y: coords.lng,
						d: getBoundsRadius(this.map.map.getBounds())
					}
					inventarios.nearBy.read();
				}.bind(this),1000)

			});
			this.map.map.setOptions({
				zoomControlOptions: {
					style:google.maps.ZoomControlStyle.LARGE,
					position: google.maps.ControlPosition.RIGHT_BOTTOM
				}
			});
			this.map.map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById("inventory-search-map-search-box"));
			this.map.map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(document.getElementById("inventory-search-map-buttons"));
			var input = document.getElementById('inventory-search-map-search');

			$(document).on('click', ".viewmoreinmap",this.showDetail.bind(this));

			// Search bar
			var options = {
				types: ['geocode'],
				componentRestrictions: {country: 'es'}
			};
			autocomplete = new google.maps.places.Autocomplete(input, options);
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
				var place = autocomplete.getPlace();
				console.log(place);
				if(place){
					this.map.map.setCenter(place.geometry.location);
				}
			}.bind(this));

			this.center();

			// Map update
			inventarios.nearBy.bind("change", this.drawPoints.bind(this));
		},
		drawPoints: function(){
			this.map.removeOverlays();
			inventarios.nearBy.data().forEach(function(it,index){
				this.drawMarker(it.Latitud,it.Longitud,index,it.TipoElemento + " / " + it.NombreElemento);
			}.bind(this))
		},
		drawMarker: function(lat,lng,index,nombre){
			var tml = this.template({index:index,NombreElemento:nombre});
			//console.log(tml.find("a"));
			this.map.addMarker({
				lat: lat,
				lng: lng,
				infoWindow: {content: tml }
			});
		},
		showDetail: function(e){
			//e.preventDefault();
			var index = Number($(e.target).parent().attr("data-index"));
			var it = inventarios.nearBy.at(index);
			inventarios.select(it);
			console.log(it);
		},
		onViewShow: function(event){
			console.log("Show view: inventory-mapsearch");
			this.map.removeOverlays();
			autocomplete.set('place',null);
			$("#inventory-search-map-search").val("");
			this.map.refresh();
			setTimeout(this.map.refresh.bind(this.map),1000);

			/*var total = 0;
			if(incidencias.selected && incidencias.selected.Localizaciones){
				var bounds = new google.maps.LatLngBounds();
				incidencias.selected.Localizaciones.forEach(function(it,index){
					if(it.IdTipoLocalizacion == 1){
						total++;
						bounds.extend(new google.maps.LatLng(it.Map_Latitud,it.Map_Longitud));
						this.drawMarker(it.Map_Latitud,it.Map_Longitud,index);
					}
				}.bind(this));
			}
			if(total > 0){
				this.map.map.fitBounds(bounds);
			}
			else{
				this.center();
			}*/
		},
		center: function(e){
			console.log("center");
			if(e)e.preventDefault();
			window.showLoading();
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(pos){
					this.map.map.setCenter(new google.maps.LatLng(pos.coords.latitude,pos.coords.longitude));
				}.bind(this), this.onError);
			} else {
				this.onError();
			}
		},
		onError: function() {
			window.hideLoading();
			console.error('Geolocation failed: '+ error.message);
			alert(T("views.incidencias.selectLocation.geoloc-error"));
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-mapsearch" );
		}
	};
	return kendo.observable(vm);
});

define([
	"app/models/inventarios",
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(inventario, j, kendo) {
	'use strict'
	var vm = {
		source: [],
		total: 0,
		onViewInit: function(event){
			console.log("Init view: inventory-results");
			this.view = event.view.element;
			inventario.dataSource.bind("change", this.dataSource_change.bind(this));
			inventario.dataSource.bind("requestStart", this.onRequestStart.bind(this));
			inventario.dataSource.bind("change", this.onRequestEnd.bind(this));
			this.view.find(".inventory-list").data("kendoMobileListView").options.messages.loadMoreText = "Cargar mas";
			this.view.find(".no-result").hide();
		},
		onRequestStart: function(){
			setTimeout(function(){
				window.hideLoading();
			},100);

		},
		onRequestEnd: function(){
			if(inventario.dataSource.total() == 0) {
				this.view.find(".no-result").show();
				this.view.find(".inventory-list").hide();
			}
			else{
				this.view.find(".no-result").hide();
				this.view.find(".inventory-list").show();
			}
			//window.hideLoading();
			// if(inventario.dataSource.total() == 1){
			// 	inventario.select(inventario.dataSource.at(0));
			// 	window.app.kendoApp.navigate("#inventory-detailsElemento");
			// }
			// else if(inventario.dataSource.total() == 0){
			// 	console.log("No hay");
			// 	this.set("source",inventario.dataSource);
			// 	//alert("No hay resultados para esa búsqueda");
			// 	//history.back();
			// }
			// else{
			// 	this.set("source",inventario.dataSource);
			// }

			this.set("source",inventario.dataSource);
		},
		onViewShow: function(event){
			console.log("Show view: inventory-results");
			this.set("source",inventario.dataSource);
			this.view.find(".no-result").hide();
			this.view.find(".inventory-list").show();
			/*if(window.app.pendingOperationsCount() == 0){
				console.log("fetch");
				inventario.dataSource.fetch();
			}*/
		},
		onSelect: function(event){
			console.log(event);
			if(event.dataItem){
				inventario.select(event.dataItem);
			}
		},
		dataSource_change: function(event){
			this.set("total",inventario.dataSource.total())
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-results" );
		}
	};
	return kendo.observable(vm);
});

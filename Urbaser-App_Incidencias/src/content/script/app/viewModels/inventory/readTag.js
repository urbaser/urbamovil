define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: inventory-readTag");
			this.view = event.view.element;
		},
		onViewShow: function(event){
			console.log("Show view: inventory-readTag");
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-readTag" );
		}
	};
	return kendo.observable(vm);
});

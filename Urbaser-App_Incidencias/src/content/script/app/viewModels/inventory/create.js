define([
	"app/models/masterTables",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(tables,kendo) {
	'use strict'
	var vm = {
		source: new kendo.data.DataSource(),
		onViewInit: function(event){
			console.log("Init view: inventory-create");
			this.view = event.view.element;
		},
		onViewShow: function(event){
			console.log("Show view: inventory-create");
			this.set("source",tables.tiposElementos);
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-create" );
		}
	};
	return kendo.observable(vm);
});

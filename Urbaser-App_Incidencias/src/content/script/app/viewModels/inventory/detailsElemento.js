define([
	"app/models/inventarios",
	"app/models/masterTables",
	"app/helpers/buttongroup",
	"app/helpers/editModals",
	"app/services/permissionManager",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"gmaps",

], function(inventario, tables,buttonGroup, editModals, permissionManager, kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: inventory-detailsElemento");

			this.view = event.view.element;
			buttonGroup.apply(this.view,function(who) {
				setTimeout(function(){
					this.map.refresh();
					this.map.map.setCenter(new google.maps.LatLng(inventario.selected.Latitud,inventario.selected.Longitud));
				}.bind(this), 1000);
			}.bind(this));

			this.configureEdits();

			$('#inventory-element-detail-map')
				.height($(document).height() -176)
				.width($(document).width());

			this.map = new GMaps({
				div: "#inventory-element-detail-map",
				zoom: 18,
				width: $(document).width(),
				height: $(window).height() -280,
				zoomControl: false, //{style:google.maps.ZoomControlStyle.LARGE},
				mapTypeControl: false,
				streetViewControl: false,
				panControl: false,
				lat: 40.416705,
				lng: -3.703582,
				draggable: true,
				load: function(){
					this.map.map.setCenter(new google.maps.LatLng(inventario.selected.Latitud,inventario.selected.Longitud));
				}.bind(this)
				//click:this.onMapClick.bind(this)

			});
			this.view.find(".espacioUrbano").click(this.onEspacioClick.bind(this));
			this.view.find(".ruta").click(this.onRutaClick.bind(this));
			this.view.find(".zona").click(this.onZonaClick.bind(this));
			this.view.find(".puntoRecogida").click(this.onPuntoRecogidaClick.bind(this));
			this.view.find(".contenedores").click(this.onContenedoresClick.bind(this));

			this.view.find(".properties-list").data("kendoMobileListView").bind("dataBound",function(){
				this.configureDynamicEdits();
				this.checkPermissions();
			}.bind(this));
		},
		configureEdits: function(){

			function convertDataSource(ds){
				var provs = new kendo.data.DataSource()
				provs.data(ds.data().map(function(e){ return {Id:e.IdProvincia,Text:e.NombreProvincia}}));
				return provs;
			}

			// Nombre
			editModals.applyString("Nombre",this.view.find(".nombre-elemento"),function(value){
				this.model.set("NombreElemento",value);
				inventario.dataSource.sync();
			}.bind(this));

			// Provincias
			editModals.applySelectOne(
				"Provincia",
				this.view.find(".provincia-elemento"),
				convertDataSource(tables.provincias),
				function(value){
					if(this.model.IdProvincia != value.Id){
						this.model.set("IdProvincia",value.Id);
						this.model.set("provincia",value.Text);
						inventario.dataSource.sync();
					}
				}.bind(this)
			);
		},
		configureDynamicEdits: function(){
			this.view.find(".properties-list li > div").each(function(index,element){
				$(element).find(".edit-button").click(function(e){
					e.preventDefault();
					var id = Number($(element).attr("data-definition"));
					if(id){
						console.log(id,tables.tiposElementosCampos.get(id));

						var def = tables.tiposElementosCampos.get(id);
						var defValue = $(element).find("[data-value]").attr("data-value");
						var defName = def.Campo;

						if(def.TipoDato == "TEXTO" && !def.ListaValores){
							editModals.openString(defName,defValue,function(value){
								var name = $(element).attr("data-name");
								inventario.selected.CamposValor[name] = value;
								inventario.selected.dirty = true;
								inventario.selected.parseCampos();
								this.set("campos",inventario.selected.Campos);
								this.view.find(".properties-list").data("kendoMobileListView").refresh();
								inventario.dataSource.sync();

							}.bind(this));
						}
						else if(def.TipoDato == "FECHA"){
							editModals.openDate(defName,defValue,function(value){
								var name = $(element).attr("data-name");
								inventario.selected.CamposValor[name] = value;
								inventario.selected.dirty = true;
								inventario.selected.parseCampos();
								this.set("campos",inventario.selected.Campos);
								this.view.find(".properties-list").data("kendoMobileListView").refresh();
								inventario.dataSource.sync();

							}.bind(this));
						}
						else if(def.TipoDato == "NUMÉRICO"){
							editModals.openNumber(defName,defValue,function(value){
								var name = $(element).attr("data-name");
								inventario.selected.CamposValor[name] = value;
								inventario.selected.dirty = true;
								inventario.selected.parseCampos();
								this.set("campos",inventario.selected.Campos);
								this.view.find(".properties-list").data("kendoMobileListView").refresh();
								inventario.dataSource.sync();

							}.bind(this));
						}
						else if(def.TipoDato == "TEXTO" && def.ListaValores){
							var ds = new kendo.data.DataSource();
							var data = def.ListaValores.split(",").map(function(e,i){ return {Id:i,Text:e}})
							ds.data(data);
							editModals.openSelectOne(defName,defValue,ds,function(value){
								var name = $(element).attr("data-name");
								inventario.selected.CamposValor[name] = value.Text;
								inventario.selected.dirty = true;
								inventario.selected.parseCampos();
								this.set("campos",inventario.selected.Campos);
								this.view.find(".properties-list").data("kendoMobileListView").refresh();
								inventario.dataSource.sync();

							}.bind(this));
						}
						else if(def.TipoDato == "FICHERO"){
							editModals.openFile(defName,function(defValue){
								console.log(value);
								if(!value) return;
								var name = $(element).attr("data-name");
								var id = $(element).attr("data-definition");

								if(value.size > 8 * 1024 * 1024){
									alert(T("views.incidencias.edit.bigimage"));
									return;
								}
								if(!inventario.selected.files){
									inventario.selected.files = new Array();
								}
								inventario.selected.files.push({
									file: value,
									field: id,
									fieldName:  name,
								});
								inventario.selected.CamposValor[name] = value;
								inventario.selected.dirty = true;
								inventario.dataSource.sync();
								return;
								inventario.selected.CamposValor[name] = value;
								inventario.selected.dirty = true;
								inventario.selected.parseCampos();
								this.set("campos",inventario.selected.Campos);
								this.view.find(".properties-list").data("kendoMobileListView").refresh();

							}.bind(this));
						}
					}
				}.bind(this));
			}.bind(this));
		},
		drawMarker: function(lat,lng,index){
			this.map.removeMarkers();
			this.map.addMarker({
				lat: lat,
				lng: lng
			});
		},
		onViewShow: function(event){
			console.log("Show view: inventory-detailsElemento");
			console.log("Detail:",inventario.selected);

			this.view.find(".buttongroup").data("kendoMobileButtonGroup").select(0);
			this.view.find(".buttongroup-content").hide().eq(0).show();

			inventario.selected.parseCampos();
			this.set("model",inventario.selected);
			this.set("campos",inventario.selected.Campos);

			this.view.find(".zona").css("display",inventario.selected.Zona ? "" : "none");
			this.view.find(".ruta").css("display",inventario.selected.Ruta ? "" : "none");
			this.view.find(".espacioUrbano").css("display",inventario.selected.EspacioUrbano ? "" : "none");
			this.view.find(".puntoRecogida").css("display",inventario.selected.CamposValor["Punto Recogida"] ? "" : "none");
			this.view.find(".contenedores").css("display",inventario.selected.IdTipoElemento == 1 ? "" : "none");


			if(inventario.selected.Latitud && inventario.selected.Longitud){
				this.view.find(".km-tabstrip li:last-child").removeClass("km-state-disabled");
				this.map.removeOverlays();
				setTimeout(this.map.refresh.bind(this.map),1000);
				this.drawMarker(inventario.selected.Latitud,inventario.selected.Longitud,1);
				this.map.map.setCenter(new google.maps.LatLng(inventario.selected.Latitud,inventario.selected.Longitud));
				this.map.refresh();
			}
			else{
				this.view.find(".km-tabstrip li:last-child").addClass("km-state-disabled");
			}
			this.checkPermissions();
		},
		checkPermissions: function(){
			this.view.find(".inventory-buttons").css("display",permissionManager.hasPermission("Inventario.ModificarInventario") ? "" : "none");
		},
		onEspacioClick: function(event){
			inventario.setFilter({Jerarquia:{IdEspacioUrbano:inventario.selected.IdEspacioUrbano}});
			window.app.kendoApp.navigate("#inventory-results");
		},
		onZonaClick: function(event){
			inventario.setFilter({Jerarquia:{IdZona:inventario.selected.IdZona}});
			window.app.kendoApp.navigate("#inventory-results");
		},
		onRutaClick: function(event){
			inventario.setFilter({Jerarquia:{IdRuta:inventario.selected.IdRuta}});
			window.app.kendoApp.navigate("#inventory-results");
		},
		onPuntoRecogidaClick: function(event){
			inventario.dataSource.page(1);
			inventario.setFilter({IdElemento:inventario.selected.CamposValor["Punto Recogida"]});
			window.app.kendoApp.navigate("#inventory-results");
		},
		onContenedoresClick: function(event){
			var p = {
				IdTipoElementoCampo: 13,
				Valor: inventario.selected.IdElemento
			}
			inventario.dataSource.page(1);
			inventario.setFilter({Propiedades:[p]});
			window.app.kendoApp.navigate("#inventory-results");
		},
		onReadTag: function (event) {
			event.preventDefault();
			editModals.openTagReader(function(tag){
				this.model.set("TAG",tag);
				inventario.dataSource.sync();
			}.bind(this))
		},
		onViewHide: function(event){
			console.log("Hide view: inventory-detailsElemento" );
		}
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"app/models/localizacion",
	"kendo/kendo.core",
	"app/services/tagReader",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo.repeater",
	"kendo/kendo.autocomplete"
], function(j,Incidencia,incidencias,Location,kendo,tagReader) {
	'use strict'
	var closure1,closure2;
	var vm = {
		location: new Location(),
		selectedDevice: null,
		onViewInit: function(e){
			console.log(e.view.id + " init");
			//incidencias.selected = new Incidencia();
			//incidencias.selected.Localizaciones = new Array();
			this.view = e.view.content;
		},
		onViewShow: function(e){
			this.createNew();
			this.relist();
			this.footer = e.sender.footer;
			this.footer.hide();

			var reader = tagReader.getReader();
			if(!tagReader.isReady()){
				this.text(T("views.incidencias.selectByRFID.connectFail"));
			}
			else{
				this.text(T("views.incidencias.selectByRFID.waiting"));
				reader.scannedError.add(closure1 = this.onError.bind(this));
				reader.scanned.add(closure2 = this.onScan.bind(this));
				reader.readTag();
			}

		},
		onViewHide: function(e){
			var reader = tagReader.getReader();
			if(reader){
				reader.scannedError.remove(closure1);
				reader.scanned.remove(closure2);
			}
		},
		onError: function(message){
			this.error(message);
		},
		onScan: function(tag){
			this.text(T("views.incidencias.selectByRFID.tag") + tag);
			this.location.Codigo = tag;
			this.footer.show();
		},
		text: function(text){
			this.view.find(".label").removeClass("error");
			this.view.find(".label").text(text);
		},
		error: function(text){
			this.view.find(".label").addClass("error");
			this.view.find(".label").text(text);
		},
		relist: function(){
			/*$("#addressList").kendoRepeater({
				dataSource: incidencias.selected.Localizaciones,
				template: this.temp
			});
			$("#addressList a").click(this.removeDir.bind(this));*/
		},
		onNext: function(){
			incidencias.selected.Localizaciones.push(this.location);
		},
		createNew: function(){
			var loc = new Location();
			loc.IdTipoLocalizacion = 4;
			this.set("location",loc);
		}
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,incidencias,kendo) {
	'use strict'
	var vm = {
		source: ["una","dos","tres","cuatro"],
		onViewInit: function(e){
			console.log(e.view.id + " init");
			incidencias.list();
		},
		onViewShow: function(e){

		},
		onSelect: function(e){
			kendoConsole.log("event :: click (" + e.item.text() + ")", false, "#mobile-listview-events");
		}
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"app/models/localizacion",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo.repeater",
	"gmaps"
], function(j,Incidencia,incidencias,Location,kendo) {
	var vm = {
		addMode: false,
		onViewInit: function(e){
			console.log(e.view.id + " init");
			//incidencias.selected = new Incidencia();
			//incidencias.selected.Localizaciones = new Array();
			this.template = kendo.template($("#marker-template").html());
			this.map = new GMaps({
				div:"#select-map",
				zoom: 18,
				zoomControl: false, //{style:google.maps.ZoomControlStyle.LARGE},
				mapTypeControl: false,
				streetViewControl: false,
				panControl: false,
				lat: 40.416705,
				lng: -3.703582,
				draggable: true,
				click:this.onMapClick.bind(this)

			});
			this.map.map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById("select-map-search-box"));
			this.map.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(document.getElementById("select-map-buttons"));
			var input = document.getElementById('select-map-search');
			var options = {
				types: ['geocode'],
				componentRestrictions: {country: 'es'}
			};
			var autocomplete = new google.maps.places.Autocomplete(input, options);
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
				var place = autocomplete.getPlace();
				console.log(place);
				this.map.map.setCenter(place.geometry.location);
			}.bind(this));

			if(!Modernizr.csscalc || !Modernizr.cssvwunit){
				$('#select-map').height($(document).height() -110).width("100%");
			}
		},
		onMapClick: function(e){
			/*if(this.currentExpandedMarker){
			 $(this.currentExpandedMarker).find(".options").css("visibility","hidden");
			 }*/
			if(this.addMode){
				console.log(e.latLng);

				var location = new Location({
					IdTipoLocalizacion: 1,
					Map_Latitud: e.latLng.lat(),
					Map_Longitud: e.latLng.lng()
				});
				var index = incidencias.selected.Localizaciones.length;
				incidencias.selected.Localizaciones.push(location);
				this.drawMarker(e.latLng.lat(),e.latLng.lng(),index);
				this.switchAdd();
			}
		},
		drawMarker: function(lat,lng,index){
			this.map.drawOverlay({
				lat: lat,
				lng: lng,
				content: this.template({index:index}),
				click: function(e){
					if(this.currentExpandedMarker){
						$(this.currentExpandedMarker.el).find(".options").css("visibility","hidden");
					}
					if(this.currentExpandedMarker && this.currentExpandedMarker.el == e.el){
						this.currentExpandedMarker = null;
						return;
					}
					this.currentExpandedMarker = e;
					var options = $(e.el).find(".options");
					options.css("visibility",options.css("visibility")== "visible" ? "hidden" : "visible");
					$(e.el).find(".button").click(this.removeDir.bind(this));
				}.bind(this)
			});
		},
		onViewShow: function(e){
			this.map.removeOverlays();
			var total = 0;
			if(incidencias.selected && incidencias.selected.Localizaciones){
				var bounds = new google.maps.LatLngBounds();
				incidencias.selected.Localizaciones.forEach(function(it,index){
					if(it.IdTipoLocalizacion == 1){
						total++;
						bounds.extend(new google.maps.LatLng(it.Map_Latitud,it.Map_Longitud));
						this.drawMarker(it.Map_Latitud,it.Map_Longitud,index);
					}
				}.bind(this));
			}
			if(total > 0){
				this.map.map.fitBounds(bounds);
			}
			else{
				this.center();
			}
			this.createNew();
		},
		center: function(e){
			console.log("cetner");
			if(e)e.preventDefault();
			window.app.kendoApp.pane.loader.show();
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(this.showPosition.bind(this), onError);
			} else {
				onError();
			}

			function onError() {
				window.app.kendoApp.pane.loader.hide();
				console.error('Geolocation failed: '+error.message);
				alert(T("views.incidencias.selectLocation.geoloc-error"));
			}
		},
		showPosition: function(position){
			window.app.kendoApp.pane.loader.hide();
			this.map.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
		},
		switchAdd: function(e){
			if(e)e.preventDefault();
			this.addMode = !this.addMode;
			$("#map-switch-add").find("img").attr("src",this.addMode ? "content/imgs/map-add-location-selected.svg" : "content/imgs/map-add-location.svg")
		},
		relist: function(){
		},
		addDir: function(e){
			e.preventDefault();
			console.log("Añadida direccion");
			incidencias.selected.Localizaciones.push(this.location);
			this.createNew();
			this.relist();
		},
		removeDir: function(e){
			var i = $(e.currentTarget).parent().parent().attr("data-index");
			console.log("Removing at index",i);
			incidencias.selected.Localizaciones.splice(i,1);
			this.currentExpandedMarker.setMap(null);
		},
		createNew: function(){
			var loc = new Location();
			loc.IdTipoLocalizacion = 1;
			this.set("location",loc);
		},
		showHelp: function(e){
			e.preventDefault();
			$("#modal-mapHelp").kendoMobileModalView("open");
		}
	};
	return kendo.observable(vm);
});

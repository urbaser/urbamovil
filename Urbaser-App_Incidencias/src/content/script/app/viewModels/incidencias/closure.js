/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,Incidencia,incidencias,kendo) {
	'use strict'
	var vm = {
		model: new Incidencia(),
		onViewInit: function(e){
			console.log(e.view.id + " init");
			this.view = e.view.content;
			if(navigator.camera){
				$("#add-closure-image").click(this.onAddImageClick.bind(this));
				this.view.find("[type=file]").hide();
			}
		},
		onViewShow: function(e){
			this.set("model",incidencias.selected);
			try{
				$(this.view).find(".form-file img").removeClass("visible");
			}
			catch(error){
				console.log(error);
			}
		},
		imageChange: function(event){
			event.preventDefault();
			console.log("Image change")
			if(event.target.files[0].size > 8 * 1024 * 1024){
				alert(T("views.incidencias.closure.bigimage"));
				return;
			}
			this.model.closuretempfile = event.target.files[0];
			$(this.view).find(".form-file img").addClass("visible");
		},
		onAddImageClick: function(e){
			e.preventDefault();
			if(navigator.camera){
				navigator.camera.getPicture(this.onCameraSuccess.bind(this), null, {
					quality : 75,
					destinationType : Camera.DestinationType.FILE_URL,
					sourceType : Camera.PictureSourceType.CAMERA,
					encodingType: Camera.EncodingType.JPEG,
					targetWidth: 1000,
					targetHeight: 1000,
					correctOrientation: true,
					saveToPhotoAlbum: false
				});
			}
			else{
				alert(T("views.incidencias.edit.select-unsupported"));
			}
		},
		onCameraSuccess: function(uri){
			this.model.closuretempurifile = uri;
			$(this.view).find(".form-file img").addClass("visible");
		},
		send: function(e){
			if(this.model.closuretempfile || this.model.closuretempurifile){
				incidencias.selected.dirty = true;
				incidencias.selected.cargandoImagenCierre = true;
			}
			incidencias.dataSource.sync();
			incidencias.selected = null;
			window.app.kendoApp.navigate("#home");
			if(!window.app.online){
				$("#modal-offline").kendoMobileModalView("open");
			}
		}
	};
	return kendo.observable(vm);
});

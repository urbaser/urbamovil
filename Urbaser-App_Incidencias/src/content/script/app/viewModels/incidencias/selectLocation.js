/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/services/BTSerialBridge",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,bt) {
	'use strict'
	var vm = {

		onViewInit: function(e){
			console.log(e.view.id + " init");
			this.view = e.view.content;
		},
		onViewShow: function(e){
			this.view.find(".selectByRFID").css("display",bt.supported ? null : "none");
		}
	};
	return kendo.observable(vm);
});

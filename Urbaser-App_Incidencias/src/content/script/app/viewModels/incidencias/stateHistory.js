/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/estadosIncidencia",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,estadosIncidencia,kendo) {
	'use strict'
	var vm = {
		source: estadosIncidencia.dataSource,
		onViewInit: function(e){
			console.log(e.view.id + " init");
			//incidencias.list();
			//incidencias.listed.add(function(){
			//	incidencias.all.forEach(function(i,d){
			//		this.source.push(i);
			//	}.bind(this));
			//}.bind(this));
		},
		onViewShow: function(e){
			estadosIncidencia.dataSource.read();
		},
		onSelect: function(e){
			console.log(e);
		}
	};
	return kendo.observable(vm);
});

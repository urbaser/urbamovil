/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"app/models/incidencia",
	"app/models/incidencias",
	"app/ui/buttons",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"jQuery",
	"kendo/kendo.core",

], function(Incidencia,incidencias,buttons) {
	'use strict'
	var vm = {
		onViewInit: function(e){
			incidencias.dataSource.bind("sync",this.onSync);
			incidencias.dataSource.bind("change",this.onChange);
		},
		onViewShow: function(e){
			$('.user-info .name').text(window.app.user.nombreCompleto);
			$(".footer-contract").show();
			if(window.app.isCitizenApp){
				if($(".incidencias-list-button .label").length > 0){
					$(".incidencias-list-button .label").text(T("views.home.index.listCitizen"));
				}
				else{
					$(".incidencias-list-button").text(T("views.home.index.listCitizen"));
				}
			}
			setTimeout(buttons.fix,1000);
		},
		onSync: function(){
			var c = window.app.updatePendingCount();
			console.log("Sync: " + c);
		},
		onChange: function(){
			var c = window.app.updatePendingCount();
			console.log("Change: " + c);
		},
		onCreate: function(){
			var inc = new Incidencia();
			inc.IdEstado = 1;
			inc.Localizaciones = new Array();
			incidencias.select(inc);
		}
	};
	return kendo.observable(vm);
});

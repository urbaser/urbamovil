/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencias",
	'app/services/permissionManager',
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,incidencias,permissionManager,kendo) {
	'use strict'
	function onSelectedChange(){
		$("#detail-container").empty().append(this.template(incidencias.selected));
	}
	var vm = {
		source: function(){
			return incidencias.selected;
		},
		onViewInit: function(e){
			try{
				console.log(e.view.id + " init");
				this.template = kendo.template($("#detailTemplate").html());
			}
			catch(error){
				console.log(error.stack);
				throw error;
			}
		},
		onViewShow: function(e){
			try{
			    console.log("Cargando imagen: " + incidencias.selected.CargandoImagen);

				if(incidencias.selected){
				    incidencias.selected.hideHistory = window.app.isCitizenApp || !permissionManager.hasPermission("Incidencias.HistoricoIncidencias");
					$("#detail-container").empty().append($(this.template(incidencias.selected)).i18n());
					$("#detail-container").find(".imagen-incidencia.normal").load();
					incidencias.selected.bind("change",onSelectedChange.bind(this));
				}
				else{
					e.preventDefault();
					setTimeout(function(){window.app.kendoApp.replace("#incidencias")},100);
				}
			}
			catch(error){
				console.log(error.stack);
				throw error;
			}
		},
		openModal: function(){
			console.log("openModal")
			$("#modal-changeState").kendoMobileModalView("open");
		}
	};
	return kendo.observable(vm);
});

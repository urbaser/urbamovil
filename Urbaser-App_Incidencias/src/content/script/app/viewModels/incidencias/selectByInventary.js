/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"app/models/inventario",
	"app/models/localizacion",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,Incidencia,incidencias,inventario,Location,kendo) {
	'use strict'
	var vm = {
		inventary:inventario,
		location: new Location(),
		onViewInit: function(e){
			console.log(e.view.id + " init");
			//incidencias.selected = new Incidencia();
			//incidencias.selected.Localizaciones = new Array();
			this.temp = $("#address-list-inventary-template").html();
			this.view = e.view.content;
			this.view.find(".select-distritos").prepend("<option disabled selected>" + T("models.inventario.select-district") + "</option>").change(this.onDistritoChange.bind(this));
			this.view.find(".select-espacios").prepend("<option disabled selected>" + T("models.inventario.select-space") + "</option>").change(this.onEspacioChange.bind(this));
			this.view.find(".select-zonas").prepend("<option disabled selected>" + T("models.inventario.select-zone") + "</option>").change(this.onZonaChange.bind(this));
			//this.view.find(".select-rutas").prepend("<option disabled selected>" + T("models.inventario.select-route") + "</option>").change(this.onRutaChange.bind(this));
			this.view.find(".select-tipos").prepend("<option disabled selected>" + T("models.inventario.select-type") + "</option>").change(this.onTipoChange.bind(this));
			this.view.find(".select-elementos").prepend("<option disabled selected>" + T("models.inventario.select-element") + "</option>").change(this.onElementoChange.bind(this));
			this.createNew();
			inventario.distritos.bind("change",this.selectCheck.bind(this));
			inventario.espaciosUrbanos.bind("change",this.selectCheck.bind(this));
			inventario.zonas.bind("change",this.selectCheck.bind(this));
			inventario.rutas.bind("change",this.selectCheck.bind(this));
			inventario.tipos.bind("change",this.selectCheck.bind(this));
			inventario.elementos.bind("change",this.selectCheck.bind(this));
		},
		selectCheck: function(e){
			var parent = e.sender == inventario.distritos ? "select-distritos" :
				e.sender == inventario.espaciosUrbanos ? "select-espacios" :
					e.sender == inventario.zonas ? "select-zonas" :
						e.sender == inventario.rutas ? "select-rutas" :
							e.sender == inventario.tipos ? "select-tipos" : "select-elementos";
			//console.log(e);
			//$("." + parent).val(0).find("option:first-child").attr("disabled","");
		},
		onDistritoChange: function(){
			this.location.Inv_IdDistrito = this.location.Distrito.IdDistrito;
			this.inventary.location = this.location;
			inventario.espaciosUrbanos.params = this.location.Inv_IdDistrito;
			inventario.espaciosUrbanos.fetch();
			this.view
				.find(".select-espacios")
				.removeAttr("disabled")
				.parent()
				.removeClass("disabled");
			this.view
				.find(".select-zonas")
				.attr("disabled","")
				.empty()
				.parent()
				.addClass("disabled");
			this.view
				.find(".select-rutas")
				.attr("disabled","")
				.empty()
				.parent()
				.addClass("disabled");
			this.location.Inv_IdEspacioUrbano = 0;
			this.location.Inv_IdZona = 0;
			this.location.Inv_IdRuta = 0;
		},
		onEspacioChange: function(){
			this.location.Inv_IdEspacioUrbano = this.location.EspacioUrbano.IdEspacioUrbano;
			this.inventary.location = this.location;
			inventario.zonas.params = this.location.Inv_IdEspacioUrbano;
			inventario.rutas.params = this.location.Inv_IdEspacioUrbano;
			inventario.zonas.fetch();
			inventario.rutas.fetch();
			this.view.find(".select-zonas")
				.removeAttr("disabled")
				.parent()
				.removeClass("disabled");
			this.view.find(".select-rutas")
				.removeAttr("disabled")
				.parent()
				.removeClass("disabled");
		},
		onZonaChange: function(){
			if(this.location.Zona.IdZona){
				this.location.Inv_IdZona = this.location.Zona.IdZona;
				this.location.set("Inv_IdRuta",0);
				this.location.set("Ruta",null);
			}
			if(this.location.Zona.IdRuta){
				this.location.Inv_IdRuta = this.location.Zona.IdRuta;
				this.location.set("Inv_IdZona",0);
				this.location.set("Zona",null);
			}
			//this.checkIfElement();
		},
		onTipoChange: function(){
			this.location.Inv_IdTipoElemento = this.location.Tipo.IdTipoElemento;
			this.checkIfElement();
			this.view.find(".select-elementos")
				.removeAttr("disabled")
				.parent()
				.removeClass("disabled");
		},
		onRutaChange: function(){
			this.location.Inv_IdRuta = this.location.Ruta.IdRuta;
			this.view.find(".select-zonas").val(0);
			//this.checkIfElement();
		},
		onElementoChange: function(){
			this.location.Inv_IdElemento = this.location.Elemento.IdElemento;
		},
		checkIfElement: function(){
			inventario.elementosLista.params = {
				IdTipoElemento: this.location.Tipo.IdTipoElemento,
				Jerarquia: {
					IdZona: this.location.Zona,
					IdRuta: this.location.Ruta
				}
			};
			inventario.elementosLista.fetch();
		},
		onViewShow: function(e){
			this.createNew();
			this.relist();
			this.view.find(".error").removeClass("error");
		},
		relist: function(){
			$("#addressListInventary").kendoRepeater({
				dataSource: incidencias.selected.Localizaciones,
				template: this.temp
			});
			$("#addressListInventary a").click(this.removeDir.bind(this));
		},
		addDir: function(e){
			e.preventDefault();
			if(!this.validate()) return;
			console.log("Añadida direccion");
			incidencias.selected.Localizaciones.push(this.location);
			this.createNew();
			this.relist();
			this.view.find(".error").removeClass("error");
		},
		removeDir: function(e){
			e.preventDefault();
			var uid = $(e.currentTarget).attr("data-uid");
			console.log("remove",uid);
			var item = $.grep(incidencias.selected.Localizaciones, function(e){ return e.uid == uid; });
			var index = incidencias.selected.Localizaciones.indexOf(item[0]);
			if(index > -1){
				incidencias.selected.Localizaciones.splice(index,1);
				this.relist();
			}
		},
		validate: function(e){
			var valid = true;
			var view = this.view;
			this.view.find(".error").removeClass("error");
			function checkSelect(name){
				if(!view.find(name).val()){
					view.find(name).addClass("error");
					valid = false;
				}
			}
			checkSelect(".select-distritos");
			checkSelect(".select-espacios");
			if(!valid && e){
				e.preventDefault();
				alert(T("views.incidencias.selectByInventary.incomplete"));
			}
			return valid;
		},
		createNew: function(){
			var loc = new Location();
			loc.IdTipoLocalizacion = 3;
			this.inventary.location = loc;
			this.set("location",loc);
		}
	};
	return kendo.observable(vm);
});

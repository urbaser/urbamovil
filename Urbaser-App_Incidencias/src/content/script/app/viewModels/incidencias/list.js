/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencias",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,incidencias,kendo) {
	'use strict'
	var vm = {
		source: incidencias.dataSource,
		onViewInit: function(e){
			console.log(e.view.id + " init");
			//incidencias.list();
			//incidencias.listed.add(function(){
			//	incidencias.all.forEach(function(i,d){
			//		this.source.push(i);
			//	}.bind(this));
			//}.bind(this));
		},
		onViewShow: function(e){
			incidencias.dataSource.page(1);
			if(window.app.pendingOperationsCount() == 0){
				incidencias.dataSource.fetch();
			}
			if(incidencias.filter){

				window.app.changeTitle("incidencias/" + incidencias.filter.Nombre);
			}
		},
		onSelect: function(e){
			console.log(e);
			incidencias.select(e.dataItem);
			window.app.kendoApp.navigate("#detail");
		}
	};
	return kendo.observable(vm);
});

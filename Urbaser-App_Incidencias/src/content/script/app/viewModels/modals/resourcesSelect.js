define([
	"jQuery",
	"app/models/masterTables",
	"app/models/planificacion",
	"app/models/planificaciones",
	"app/models/planificacionRecursos",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,tables,planificacion,planificaciones,recursos,kendo) {
	'use strict'
	var dataChangeEvent;
	var changeSelectEvent;
	var vm = {
		model: {},
		selectedResource: null,
		resources: [],

		onViewInit: function(event){
			console.log("Init view: modals-resourcesSelect");

		},
		onViewShow: function(event){
			console.log("Show view: modals-resourcesSelect");
			this.set("model",planificaciones.selected);

			$(".changeResourceType").change(changeSelectEvent = this.changeResourceType.bind(this));
			$("#modal-resourcesSelect .changeResourceType").prop('checked', false);
			$("#modal-resourcesSelect .form-select").hide();

			recursos.update(this.model.DatosServicio.IdTipoServicio);
			this.set("resources", recursos.categorias);

			/*planificacion.servicioRecursos.one("change", dataChangeEvent = this.checkResources.bind(this));
			planificacion.servicioRecursos.params = this.model.DatosServicio.IdTipoServicio;
			planificacion.servicioRecursos.read();*/
		},
		onViewHide: function(event){
			console.log("Hide view: modals-resourcesSelect" );

		},
		changeResourceType: function(event) {
			$("#modal-resourcesSelect .form-select").show();

			this.resources.filter({
				field: "IdTipoRecurso",
				operator: "eq",
				value: parseInt($(event.currentTarget).val())
			});

		},
		checkResources: function(e) {
			//this.set("resources", planificacion.servicioRecursos.Categorias);
		},
		onCloseCancel: function(){
			$("#modal-resourcesSelect").kendoMobileModalView("close");
			//planificacion.servicioRecursos.unbind("change",dataChangeEvent);
			$(".changeResourceType").unbind("change",changeSelectEvent);
		},
		onCloseSave: function(){
			$("#modal-resourcesSelect").kendoMobileModalView("close");
			var category = this.selectedResource.IdCategoriaRecurso;
			window.app.kendoApp.navigate("#planning-resourcesList?resourceCategory=" + category);
			//planificacion.servicioRecursos.unbind("change",dataChangeEvent);
			$(".changeResourceType").unbind("change",changeSelectEvent);
		}
	};
	return kendo.observable(vm);
});

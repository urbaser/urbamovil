define([
	"jQuery",
	"app/models/flota",
	"kendo/kendo.core",
	"kendo/kendo.mobile.modalview",

], function(j,flota,kendo,k) {
	'use strict'
	var vm = {
		onViewInit: function(event){

		},
		onViewShow: function(event){
			console.log("Show view: modals-vehiclesFilter");
		},
		onViewHide: function(event){
			console.log("Hide view: modals-vehiclesFilter" );
		},

	};
	return kendo.observable(vm);
});

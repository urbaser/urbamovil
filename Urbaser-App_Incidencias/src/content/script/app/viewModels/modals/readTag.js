/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"app/models/localizacion",
	"kendo/kendo.core",
	"app/services/tagReader",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo.repeater",
	"kendo/kendo.autocomplete"
], function(j,Incidencia,incidencias,Location,kendo,tagReader) {
	'use strict'
	var closure1,closure2,currentTag;
	var vm = {
		onViewInit: function(event){
			this.view = $(event.sender.element);
		},
		onViewShow: function(e){
			currentTag = null;

			var reader = tagReader.getReader();
			if(!tagReader.isReady()){
				this.text(T("views.incidencias.selectByRFID.connectFail"));
				this.view.find(".cancel").hide();
			}
			else{
				this.text(T("views.incidencias.selectByRFID.waiting"));
				reader.scannedError.add(closure1 = this.onError.bind(this));
				reader.scanned.add(closure2 = this.onScan.bind(this));
				reader.readTag();
			}

		},
		onViewHide: function(e){
			var reader = tagReader.getReader();
			if(reader){
				reader.scannedError.remove(closure1);
				reader.scanned.remove(closure2);
			}
		},
		onError: function(message){
			this.error(message);
		},
		onScan: function(tag){
			this.text(T("views.incidencias.selectByRFID.tag") + tag);
			currentTag = tag;
		},
		text: function(text){
			this.view.find(".label").removeClass("error");
			this.view.find(".label").text(text);
		},
		error: function(text){
			this.view.find(".label").addClass("error");
			this.view.find(".label").text(text);
		},
		onClose: function(){
			$("#modals-readTag").kendoMobileModalView("close");
		},
		onSubmit: function(){
			$("#modals-readTag").kendoMobileModalView("close");
			if(this.onReadHandler && currentTag){
				this.onReadHandler(currentTag);
				currentTag = null;
			}

		}
	};
	return kendo.observable(vm);
});

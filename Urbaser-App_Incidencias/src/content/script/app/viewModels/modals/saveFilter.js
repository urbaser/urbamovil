/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/filtro",
	"app/models/filtros",
	"app/viewModels/filtrosDrawer",
	"kendo/kendo.core",
	"kendo/kendo.mobile.modalview",
], function(j,Filter,filtros,filtrosDrawer,kendo) {
	'use strict'
	var vm = {
		filtro: new Filter(),
		onViewInit: function(e){
			//console.log(e.view.id + " init");
		},
		onViewShow: function(e){
			console.log("save filter show");
			this.set("filtro",filtros.selected);
		},
		onSave: function(e){
			e.preventDefault();
			filtros.dataSource.add(this.filtro);
			filtros.dataSource.sync();
			filtrosDrawer.showList();
			$("#modal-save-filter").kendoMobileModalView("close");
		}
	};
	return kendo.observable(vm);
});

define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: modals-editFieldDate");
			this.view = $(event.sender.element);
		},
		onViewShow: function(event){
			console.log("Show view: modals-editFieldDate");
		},
		onViewHide: function(event) {
			console.log("Hide view: modals-editFieldDate");
		},
		onCloseCancel: function(){
			$("#modal-editFieldFile").kendoMobileModalView("close");
		},
		onCloseSave: function(){
			$("#modal-editFieldFile").kendoMobileModalView("close");
			if(this.change){
				this.change(this.view.find("[type=file]")[0].files[0]);
			}
		}
	};
	return kendo.observable(vm);
});

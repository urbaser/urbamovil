define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,kendo) {
	'use strict'
	var vm = {
		value: "",
		onViewInit: function(event){
			console.log("Init view: modals-editFieldNumber");
			this.view = $(event.sender.element);
		},
		onViewShow: function(event){
			console.log("Show view: modals-editFieldNumber");
		},
		onViewHide: function(event) {
			console.log("Hide view: modals-editFieldNumber");
		},
		onCloseCancel: function(){
			$("#modal-editFieldNumber").kendoMobileModalView("close");
		},
		onCloseSave: function(){
			$("#modal-editFieldNumber").kendoMobileModalView("close");
			if(this.change){
				this.change(this.value);
			}
		}
	};
	return kendo.observable(vm);
});

define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,kendo) {
	'use strict'
	var vm = {
		value: "",
		onViewInit: function(event){
			console.log("Init view: modals-editFieldInput");
			this.view = $(event.sender.element);
		},
		onViewShow: function(event){
			console.log("Show view: modals-editFieldInput");
		},
		onViewHide: function(event) {
			console.log("Hide view: modals-editFieldInput");
		},
		onCloseCancel: function(){
			$("#modal-editFieldInput").kendoMobileModalView("close");
		},
		onCloseSave: function(){
			$("#modal-editFieldInput").kendoMobileModalView("close");
			if(this.change){
				this.change(this.value);
			}
		}
	};
	return kendo.observable(vm);
});

define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: modals-editFieldBoolean");
		},
		onViewShow: function(event){
			console.log("Show view: modals-editFieldBoolean");
		},
		onViewHide: function(event){
			console.log("Hide view: modals-editFieldBoolean" );
		},
		onCloseCancel: function(){
			$("#modal-editFieldBoolean").kendoMobileModalView("close");
		},
		onCloseSave: function(){
			$("#modal-editFieldBoolean").kendoMobileModalView("close");
		}
	};
	return kendo.observable(vm);
});

define([
	"jQuery",
	"../../models/planificaciones",
	"../../models/planificacion",
	"app/models/masterTables",
	"app/helpers/buttongroup",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,planificaciones,planificacion,tables,buttonGroup,kendo) {
	'use strict'
	var errorHandler;
	var changeHandler;
	var vm = {
		model: new new planificacion.planificacion(),
		tables: tables,
		onViewInit: function(event){
			console.log("Init view: planning-createStep5");
			this.view = event.view.element;
			this.view.find(".save-planning").click(this.onSave.bind(this));
			this.template = kendo.template($("#planning-review-template").html());
		},
		onViewShow: function(event){
			console.log("Show view: planning-createStep5");
			this.view.find(".review-container").empty().append($(this.template(planificaciones.selected)).i18n());
			buttonGroup.apply(this.view);
		},
		onViewHide: function(event){
			console.log("Hide view: planning-createStep5" );
		},
		onSave: function(e){
			e.preventDefault();
			var tipoSync = (planificaciones.selected.IdOT == "") ? "CREATE" : "SAVE";
			if (planificaciones.selected.DatosServicio.IdServicio == 0 || planificaciones.selected.DatosServicio.IdServicio == undefined) planificaciones.selected.DatosServicio.IdServicio = null;
			if(!planificaciones.selected.id){
				planificaciones.dataSource.add(planificaciones.selected);
			}
			planificaciones.dataSource.one("error", errorHandler = function(e) {
				alert("Error al modificar OT: "+e.xhr.responseJSON);
				if(!planificaciones.selected.id){
					planificaciones.dataSource.remove(planificaciones.selected);
				}
				planificaciones.dataSource.unbind("error", errorHandler);
				planificaciones.dataSource.unbind("change", changeHandler);
			});
			planificaciones.dataSource.one("change", changeHandler = function(e) {
				if (tipoSync == "CREATE") {
					if (planificaciones.dataSource.get(planificaciones.selected.IdOT) != null) {
						alert("Se ha añadido la orden de trabajo correctamente.");
					}
				}
				else {
					alert("Se ha modificado la orden de trabajo correctamente.");
				}
				planificaciones.dataSource.unbind("error", errorHandler);
				planificaciones.dataSource.unbind("change", changeHandler);
				planificaciones.selected = null;
				window.app.kendoApp.pane.history = [];
				kendo.history.locations = [];
				window.app.kendoApp.navigate("#home");
			});
			planificaciones.dataSource.sync();
			//planificaciones.selected = null;
			//window.app.kendoApp.pane.history = [];
			//kendo.history.locations = [];
			//window.app.kendoApp.navigate("#home");
			if(!window.app.online){
				$("#modal-offline").kendoMobileModalView("open");
			}
		}
	};
	return kendo.observable(vm);
});

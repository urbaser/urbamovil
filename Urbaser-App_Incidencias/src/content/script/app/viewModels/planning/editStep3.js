define([
	"jQuery",
	"../../models/planificacion",
	"../../models/planificaciones",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo/kendo.mobile.listview"

], function(j,planificacion,planificaciones,kendo) {
	'use strict'
	var vm = {
		model: new planificacion.planificacion(),
		onViewInit: function(event){
			console.log("Init view: planning-createStep3");
			this.view = event.view.element;
			this.temp = $("#location-list-template").html();
			$(document).on("click", "#planning-editStep3 .remove-location", this.removeLocation.bind(this));
			$(document).on("click", ".open-locations-select", this.addLocation.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: planning-createStep3");
			this.set("model",planificaciones.selected);
			this.view.find(".planning-details-list").kendoRepeater({
				dataSource: this.model.Localizacion,
				template: this.temp
			});

			if (this.model.DatosServicio.IdServicio == null || this.model.DatosServicio.IdServicio == 0) {
				if (planificaciones.selected.Localizacion.length == 0) {
					this.view.find(".planning-noResources").show();
				}
				else {
					this.view.find(".planning-noResources").hide();
				}
				this.view.find(".remove-location").show();
				this.view.find(".resources-plus").show();
			}
			else {
				this.view.find(".planning-noResources").hide();
				this.view.find(".remove-location").hide();
				this.view.find(".resources-plus").hide();
			}

		},
		onViewHide: function(event){
			console.log("Hide view: planning-createStep3" );
		},
		addLocation: function(event) {
			if (this.model.Localizacion.length > 0) {
				alert("Sólo se puede añadir una localización");
				event.preventDefault();
			}
		},
		removeLocation: function(event) {
			event.preventDefault();
			if (this.model.DatosServicio.IdServicio == null || this.model.DatosServicio.IdServicio == 0) {
				var element = $($(event.currentTarget).parents(".planning-details-list-item")[0]);
				var index = $("#planning-editStep3 .planning-details-list-item").index(element);
				planificaciones.selected.Localizacion.splice(index,1);
				this.view.find(".planning-details-list").kendoRepeater({
					dataSource: this.model.Localizacion,
					template: this.temp
				});
				console.log("remove: "+index);

				if (planificaciones.selected.Localizacion.length == 0) {
					this.view.find(".planning-noResources").show();
				}
				else {
					this.view.find(".planning-noResources").hide();
				}
			}

		}
	};
	return kendo.observable(vm);
});

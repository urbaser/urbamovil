define([
	"jQuery",
	"../../models/planificaciones",
	"../../models/planificacion",
	"app/models/masterTables",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,planificaciones,planificacion,tables,kendo) {
	'use strict'
	var changeEvent;
	var changeEvent2;
	var vm = {
		model: new new planificacion.planificacion(),
		services: planificacion.servicios,
		service: {},
		tables: tables,
		editable: true,
		isFirst: false,
		onViewInit: function(event){
			console.log("Init view: planning-create");
			this.view = event.view.element;
			this.view.find(".goToStep2").click(this.goToStep2.bind(this));


		},
		onViewShow: function(event){
			console.log("Show view: planning-create");
			this.view.find(".error").removeClass("error");
			if (planificaciones.selected == null) {
				this.isFirst = false;
				console.log("Creando nueva planificación");

				this.set("model", new new planificacion.planificacion());
				this.set("service",{});

				this.model.IdEstadoOT = 1;
				this.model.Localizacion = new Array();
				this.model.DatosServicio = new new planificacion.planificacionDatosServicio();
				this.model.Recursos = new Array();
				this.model.Tareas = new Array();
				this.model.Instalacion = null;
				this.model.CodigoOT = null;
				this.model.Observaciones = "";
				planificaciones.select(this.model);
				this.set("editable", true);
				this.view.find(".servicio-fecha").val("");
				this.enableField(".select-tiposServicio");
				this.disableField(".select-servicios");
			}
			else{
				this.isFirst = true;
				console.log("Editando planificacion",planificaciones.selected);
				this.set("service",{});
				this.set("model",planificaciones.selected);

				if (this.model.IdOT == null || this.model.IdOT == 0) {
					this.set("editable", true);
					this.enableField(".select-tiposServicio");
					this.disableField(".select-servicios");
				} else {
					this.set("editable", false);
					this.disableField(".select-tiposServicio");
					this.disableField(".select-servicios");
				}
				this.view.find(".servicio-fecha").val(this.model.FechaOT ? kendo.toString(new Date(this.model.FechaOT), "yyyy-MM-dd") : "");

				if (this.model.DatosServicio.IdServicio) {
					this.services.one("change", changeEvent2 = this.checkServices.bind(this));
					this.services.params = this.model.DatosServicio.IdTipoServicio;
					this.services.read();
				}
			}

			if (this.model.IdOT == null || this.model.IdOT == 0) {
				this.view.find(".edit-only").hide();
			} else {
				this.view.find(".edit-only").show();
			}

			//this.view.find(".select-provincias").prepend("<option disabled selected>" + "Selecciona tipo" + "</option>");
			//this.view.find(".select-provincias").prepend("<option disabled>" + T("views.incidencias.edit.select-provincia") + "</option>");
			//this.view.find(".select-provincias").prepend("<option disabled selected>" + T("models.inventario.select-type") + "</option>");

		},
		onViewHide: function(event){
			console.log("Hide view: planning-create" );
			//this.services.unbind("change",changeEvent);
			//this.services.unbind("change",changeEvent2);

		},
		tiposServicioChange: function(e){
			var idTipoServicio = parseInt(e.currentTarget.value);
			var view = this.view;

			//this.services.unbind("change",changeEvent);
			this.services.one("change", changeEvent = this.checkServices.bind(this));
			this.services.params = idTipoServicio;
			this.services.read();

			var datosServicio = new new planificacion.planificacionDatosServicio();
			datosServicio.IdTipoServicio = parseInt(e.currentTarget.value);
			datosServicio.TipoServicio = e.currentTarget.selectedOptions[0].innerHTML;
			this.set("model.DatosServicio", datosServicio);
		},
		serviciosChange: function(e) {
			var idServicio = parseInt(e.currentTarget.value);

			/*if (this.model.instalacion() == "SmarTools") {
			 this.service.IdContratoAdmv = this.model.DatosServicio.IdContratoAdmv;
			 this.service.ContratoAdmv = this.model.DatosServicio.ContratoAdmv;
			 this.service.IdFaseCreacion = this.model.DatosServicio.IdFaseCreacion;
			 this.service.FaseCreacion = this.model.DatosServicio.FaseCreacion;
			 this.service.IdTipoActividad = this.model.DatosServicio.IdTipoActividad;
			 this.service.TipoActividad = this.model.DatosServicio.TipoActividad;
			 }*/

			if (idServicio == 0) {
				this.model.Recursos = [];
				this.model.Tareas = [];
			}
			else {
				this.model.Recursos = this.service.Recursos;
				this.model.Tareas = this.service.Tareas;
			}
			delete this.service.Tareas;
			delete this.service.Recursos;
			this.set("model.DatosServicio", this.service);


		},
		checkServices: function(e){
			var nuevoServicio = {
				IdServicio: 0,
				Servicio: "-Nuevo servicio-",
				IdTurno: 0,
				IdPeriodo: 0,
				IdCalendario: 0,
				IdTipoServicio: this.model.DatosServicio.IdTipoServicio,
				TipoServicio: this.model.DatosServicio.TipoServicio,
				FechaInicio: null,
				FechaFin: null
			};

			if (this.model.instalacion() == "SmarTools") {
				nuevoServicio.IdContratoAdmv = 0;
				nuevoServicio.IdFaseCreacion = 0;
				nuevoServicio.IdTipoActividad = 0;
			} else {
				nuevoServicio.IdProvincia = 0;
				nuevoServicio.IdMunicipio = 0;
			}
			this.services.insert(0,nuevoServicio);

			if (this.editable == true) {
				this.enableField(".select-servicios");
			}

			this.set("service", planificaciones.selected.DatosServicio);

			if (this.isFirst == false) {
				this.view.find(".select-servicios option:first").prop("selected", "selected");
				this.view.find(".select-servicios").trigger("change");
			} else {
				this.isFirst = false;
			}
		},

		disableField: function(field) {
			this.view.find(field).attr("disabled","").parent().addClass("disabled");
		},
		enableField: function(field) {
			this.view.find(field).removeAttr("disabled").parent().removeClass("disabled");
		},
		validate: function(e){
			var valid = true;
			var view = this.view;
			this.view.find(".error").removeClass("error");
			function checkSelect(name){
				if(!view.find(name).val()){
					view.find(name).addClass("error");
					valid = false;
				}
			}
			function checkDate(name){
				if(kendo.parseDate(new Date(view.find(name).val()), "yyyy-MM-dd HH:mm") == null){
					view.find(name).addClass("error");
					valid = false;
				}
			}
			checkSelect(".servicio-fecha");
			checkDate(".servicio-fecha");
			checkSelect(".select-servicios");

			if(!valid){
				alert(T("views.incidencias.edit.incomplete"));
			}
			return valid;
		},

		goToStep2: function(e) {
			var valid = this.validate();
			if (!valid)
			{
				e.preventDefault();
				return;
			}/*
			 if (this.model.DatosServicio.IdServicio != 0) {
			 this.model.DatosServicio.Servicio = null;
			 this.model.DatosServicio.IdPeriodo = null;
			 this.model.DatosServicio.IdTurno = null;
			 }*/
			var fechaOT = $(this.view.find(".servicio-fecha")[0]).val();
			//planificaciones.selected.FechaOT = kendo.parseDate(fechaOT, "dd/MM/yyyy HH:mm");
			if (kendo.parseDate(fechaOT, "yyyy-MM-dd HH:mm") != null) planificaciones.selected.FechaOT = kendo.parseDate(fechaOT, "yyyy-MM-dd HH:mm");
			else if (kendo.parseDate(fechaOT, "yyyy-MM-dd")) planificaciones.selected.FechaOT = kendo.parseDate(fechaOT, "yyyy-MM-dd");


		}
	};
	return kendo.observable(vm);
});

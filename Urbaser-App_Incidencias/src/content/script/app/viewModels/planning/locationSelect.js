define([
	"jQuery",
	"../../models/planificacionLocalizacion",
	"../../models/planificaciones",
	"../../models/planificacion",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,planificacionLocalizacion,planificaciones,planificacion,kendo) {
	'use strict'
	var changeZonas;
	var changeRutas;
	var vm = {
		model: {},
		zonas: [],
		rutas: [],
		onViewInit: function(event){
			console.log("Init view: planning-tasksSelect");
			this.view = event.view.element;
			this.view.find(".add-location").click(this.addLocation.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: planning-tasksSelect");
			this.set("model", {});
			planificacion.zonasAdmin.one("change", changeZonas = this.populateZonas.bind(this));
			planificacion.zonasAdmin.params = planificaciones.selected.DatosServicio.IdContratoAdmv;
			planificacion.zonasAdmin.read();
			this.disableField(".select-rutasSector");
		},
		onViewHide: function(event){
			console.log("Hide view: planning-tasksSelect" );
			planificacion.zonasAdmin.unbind("change", changeZonas);
			planificacion.rutasSector.unbind("change", changeRutas);
		},
		zonaAdminChange: function(e) {
			planificacion.rutasSector.one("change", changeRutas = this.populateRutas.bind(this));
			planificacion.rutasSector.params = parseInt(e.currentTarget.value);
			planificacion.rutasSector.read();
			this.enableField(".select-rutasSector");
		},
		rutaSectorChange: function(e){

		},
		populateZonas: function(e) {
			this.set("zonas", planificacion.zonasAdmin);
		},
		populateRutas: function(e) {
			this.set("rutas", planificacion.rutasSector);
		},
		disableField: function(field) {
			this.view.find(field).attr("disabled","").empty().parent().addClass("disabled");
			this.view.find(field).find("option:selected").prop("selected", false);
		},
		enableField: function(field) {
			this.view.find(field).removeAttr("disabled").parent().removeClass("disabled");
			this.view.find(field).find("option:selected").prop("selected", false);
		},
		reset: function(e) {

			//if (!$(this.view.find(".select-elementos")[0].firstChild).is(":disabled")) this.view.find(".select-elementos").prepend("<option disabled selected>" + T("models.inventario.select-element") + "</option>").prop("disabled", true);
		},
		validate: function(e){
			var valid = true;
			var view = this.view;
			this.view.find(".error").removeClass("error");
			function checkSelect(name){
				if(!view.find(name).val() || view.find(name).val() == 0){
					view.find(name).addClass("error");
					valid = false;
				}
			}
			checkSelect(".select-zonas");
			checkSelect(".select-rutasSector");

			if(!valid){
				alert(T("views.incidencias.edit.incomplete"));
			}
			return valid;
		},
		addLocation: function(e) {
			e.preventDefault();
			if (!this.validate()) {
				return;
			}
			var localizacion =  new planificacionLocalizacion();
			localizacion.IdZona = this.model.ZonaAdmv.IdZonaAdministrativa;
			localizacion.Zona = this.model.ZonaAdmv.ZonaAdministrativa;
			localizacion.IdRutaSector = this.model.RutaSector.IdRutaSector;
			localizacion.RutaSector = this.model.RutaSector.Nombre;

			planificaciones.selected.Localizacion.push(localizacion);

			window.app.kendoApp.navigate("#:back");
		}
	};
	return kendo.observable(vm);
});

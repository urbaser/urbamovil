define([
	"app/models/masterTables",
	"app/models/planificacion",
	"app/models/planificaciones",
	"app/models/inventario",
	"app/models/inventarios",
	"jQuery",
	"app/helpers/buttongroup",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(tables,planificacion,planificaciones,inventario,inventories,j, buttonGroup, kendo) {
	'use strict'
	var vm = {
		provincias: [],
		municipios: [],
		tiposServicio: [],
		inventory: inventario,
		elementos: [],
		tiposElementos: [],
		model: {},
		onViewInit: function(event){
			console.log("Init view: planning-search");
			this.view = event.view.element;
			buttonGroup.apply(this.view);
			this.view.find(".send-query").click(this.sendQuery.bind(this));
			this.view.find(".reset").click(this.resetForm.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: planning-search");
			this.set("model", {});
			this.set("provincias", tables.provincias);
			this.set("municipios", tables.municipios);
			this.set("tiposElementos", tables.tiposElementos);
			this.set("tiposServicio", tables.tiposServicio);
			this.checkProvincias();
			this.checkTiposElementos();
			this.checkTiposServicios();
		},
		onViewHide: function(event){
			console.log("Hide view: planning-search" );
			this.municipios.filter([]);
			if (this.provincias.at(0) && this.provincias.at(0).IdProvincia == 0) this.provincias.remove(this.provincias.at(0));
			if (this.municipios.at(0) && this.municipios.at(0).IdMunicipio == 0) this.municipios.remove(this.municipios.at(0));
			if (this.inventory.distritos.at(0) && this.inventory.distritos.at(0).IdDistrito == 0) this.inventory.distritos.remove(this.inventory.distritos.at(0));
			if (this.inventory.espaciosUrbanos.at(0) && this.inventory.espaciosUrbanos.at(0).IdEspacioUrbano == 0) this.inventory.espaciosUrbanos.remove(this.inventory.espaciosUrbanos.at(0));
			if (this.inventory.zonas.at(0) && this.inventory.zonas.at(0).Id == 0) this.inventory.zonas.remove(this.inventory.zonas.at(0));
			if (this.tiposElementos.at(0) && this.tiposElementos.at(0).IdTipoElemento == 0) this.tiposElementos.remove(this.tiposElementos.at(0));
			if (this.elementos.at(0) && this.elementos.at(0).IdElemento == 0) this.elementos.remove(this.elementos.at(0));
			if (this.tiposServicio.at(0) && this.tiposServicio.at(0).IdTipoServicio == 0) this.tiposServicio.remove(this.tiposServicio.at(0));
		},
		checkProvincias: function(e) {
			if (this.view.find(".select-provincias option:first").val() == 0) return;
			var nuevaProvincia = {
				NombreProvincia: "-Todas-",
				IdProvincia: 0
			};
			this.provincias.insert(0,nuevaProvincia);
			/*this.view.find(".select-provincias").prepend($('<option>', {
				text: "-Todas-",
				value: 0
			}));*/
		},
		checkMunicipios: function(e) {
			if (this.view.find(".select-municipios option:first").val() == 0) return;
			var nuevo = {
				NombreMunicipio: "-Todos-",
				IdMunicipio: 0
			};
			this.municipios.insert(0,nuevo);
		},
		checkDistritos: function(e) {
			if (this.view.find(".select-distritos option:first").val() == 0) return;
			var nuevo = {
				Distrito: "-Todos-",
				IdDistrito: 0
			};
			this.inventory.distritos.insert(0,nuevo);
		},
		checkEspacios: function(e) {
			if (this.view.find(".select-espacios option:first").val() == 0) return;
			var nuevo = {
				EspacioUrbano: "-Todos-",
				IdEspacioUrbano: 0
			};
			this.inventory.espaciosUrbanos.insert(0,nuevo);
		},
		checkZonas: function(e) {
			if (this.view.find(".select-zonas option:first").val() == 0) return;
			var nuevo = {
				Nombre: "-Todas-",
				Id: 0
			};
			this.inventory.zonas.insert(0,nuevo);
		},
		checkTiposElementos: function(e) {
			if (this.view.find(".select-tiposElementos option:first").val() == 0) return;
			var nuevo = {
				Tipo: "-Todos-",
				IdTipoElemento: 0
			};
			this.tiposElementos.insert(0,nuevo);
		},
		checkElementos: function(e) {
			this.set("elementos", inventario.elementosLista);
			if (this.elementos.at(0).IdElemento == 0) this.elementos.remove(this.elementos.at(0));
			var nuevo = {
				NombreElemento: "-Todos-",
				IdElemento: 0
			};
			this.elementos.insert(0,nuevo);
		},
		checkTiposServicios: function(e) {
			if (this.view.find(".select-tiposServicio option:first").val() == 0)  return;
			var nuevo = {
				TipoServicio: "-Todos-",
				IdTipoServicio: 0
			};
			this.tiposServicio.insert(0,nuevo);
		},

		reset: function(e) {
			this.view.find(".")
		},
		tiposServicioChange: function(e) {
			//this.model.IdTipoServicio = parseInt(e.currentTarget.value);
		},
		provinciaChange: function(e){
			if (parseInt(e.currentTarget.value) > 0) {
				this.municipios.one("change", this.checkMunicipios.bind(this));
				this.municipios.filter({
					field: "IdProvincia",
					operator: "eq",
					value: parseInt(e.currentTarget.value)
				});

				this.enableField(".select-municipios");
			}
			else {
				this.disableField(".select-municipios");

			}
			this.disableField(".select-distritos");
			this.disableField(".select-espacios");
			this.disableField(".select-zonas");

		},
		municipioChange: function(e){
			if (parseInt(e.currentTarget.value) > 0) {
				this.inventory.distritos.one("change", this.checkDistritos.bind(this));
				this.inventory.distritos.filter({
					field: "IdMunicipio",
					operator: "eq",
					value: parseInt(e.currentTarget.value)
				});
				this.enableField(".select-distritos");
			}
			else {
				this.disableField(".select-distritos");
			}

			this.disableField(".select-espacios");
			this.disableField(".select-zonas");
		},
		distritoChange: function(e) {
			if (parseInt(e.currentTarget.value) > 0) {
				this.inventory.espaciosUrbanos.one("change", this.checkEspacios.bind(this));
				this.inventory.espaciosUrbanos.params = parseInt(e.currentTarget.value);
				this.inventory.espaciosUrbanos.read();
				this.enableField(".select-espacios");
			} else {
				this.disableField(".select-espacios");
			}
			this.disableField(".select-zonas");
		},
		espacioUrbanoChange: function(e){
			if (parseInt(e.currentTarget.value) > 0) {
				this.inventory.zonas.one("change", this.checkZonas.bind(this));
				this.inventory.zonas.params = parseInt(e.currentTarget.value);
				this.inventory.zonas.read();

				this.enableField(".select-zonas");
			}
			else {
				this.disableField(".select-zonas");
			}

		},

		zonaChange: function(e){

			this.enableField(".select-zonas");
		},
		tipoElementoChange: function(e) {
			if (parseInt(e.currentTarget.value) > 0) {
				this.updateElements();
			} else {
				this.disableField(".select-elementos");
			}
		},
		elementoChange: function(e) {
			//this.model.IdElemento = parseInt(e.currentTarget.value);
		},
		updateElements: function() {
			var filtro = {};
			filtro.Jerarquia = {};
			if (this.model.TipoElemento) filtro.IdTipoElemento = this.model.TipoElemento.IdTipoElemento;
			if (this.model.Provincia) filtro.Jerarquia.IdProvincia = this.model.Provincia.IdProvincia;
			if (this.model.Municipio) filtro.Jerarquia.IdMunicipio = this.model.Municipio.IdMunicipio;
			if (this.model.Distrito) filtro.Jerarquia.IdDistrito = this.model.Distrito.IdDistrito;
			if (this.model.EspacioUrbano) filtro.Jerarquia.IdEspacioUrbano = this.model.EspacioUrbano.IdEspacioUrbano;

			if (this.model.ZonaRuta) {
				if (this.model.ZonaRuta.IdZona) {
					filtro.Jerarquia.IdZona = this.model.ZonaRuta.IdZona;
				}
				if (this.model.ZonaRuta.IdRuta) {
					filtro.Jerarquia.IdRuta = this.model.ZonaRuta.IdRuta;
				}
			}
			this.model.Elemento = null;

			this.view.find(".select-elementos")
				.removeAttr("disabled")
				.empty()
				.prepend("<option disabled selected>" + T("app.loading") + "</option>")
				.parent()
				.removeClass("disabled");

			inventario.elementosLista.one("change", this.checkElementos.bind(this));
			inventario.elementosLista.params = filtro;
			inventario.elementosLista.read();

		},

		disableField: function(field) {
			this.view.find(field).attr("disabled","").parent().addClass("disabled");
			//this.view.find(field).find("option:selected").prop("selected", false)
		},
		enableField: function(field) {
			this.view.find(field).removeAttr("disabled").parent().removeClass("disabled");
			this.view.find(field).find("option:first").prop("selected", "selected");
			//this.view.find(field).find("option:selected").prop("selected", false)
		},
		sendQuery: function() {
			var filtro = {};

			var fechaDesde = $(this.view.find(".fecha-desde")[0]).val();
			if (fechaDesde) {
				if (kendo.parseDate(fechaDesde, "yyyy-MM-dd HH:mm") != null) filtro.FechaDesde = kendo.parseDate(fechaDesde, "yyyy-MM-dd HH:mm");
				else if (kendo.parseDate(fechaDesde, "yyyy-MM-dd")) filtro.FechaDesde = kendo.parseDate(fechaDesde, "yyyy-MM-dd");
			}

			var fechaHasta = $(this.view.find(".fecha-hasta")[0]).val();
			if (fechaHasta) {
				if (kendo.parseDate(fechaHasta, "yyyy-MM-dd HH:mm") != null) filtro.FechaHasta = kendo.parseDate(fechaHasta, "yyyy-MM-dd HH:mm");
				else if (kendo.parseDate(fechaHasta, "yyyy-MM-dd")) filtro.FechaHasta = kendo.parseDate(fechaHasta, "yyyy-MM-dd");
			}

			filtro.IdentificadorOT = this.model.IdentificadorOT;
			filtro.IdentificadorServicio = this.model.IdentificadorServicio;
			if (this.model.TipoServicio && this.model.TipoServicio.IdTipoServicio != 0) filtro.IdTipoServicio = this.model.TipoServicio.IdTipoServicio;
			filtro.DescripcionServicio = this.model.DescripcionServicio;

			if (this.model.Provincia && this.model.Provincia.IdProvincia != 0) filtro.IdProvincia = this.model.Provincia.IdProvincia;
			if (this.model.Municipio && this.model.Municipio.IdMunicipio != 0) filtro.IdMunicipio = this.model.Municipio.IdMunicipio;
			if (this.model.EspacioUrbano && this.model.EspacioUrbano.IdEspacioUrbano != 0) filtro.IdEspacioUrbano = this.model.EspacioUrbano.IdEspacioUrbano;
			if (this.model.Elemento && this.model.Elemento.IdElemento != 0) filtro.IdElemento = this.model.Elemento.IdElemento;


			if (this.model.ZonaRuta && this.model.ZonaRuta.IdZona) {
				filtro.IdZona = this.view.find(".select-zonas").val();
			}
			if (this.model.ZonaRuta && this.model.ZonaRuta.IdRuta) {
				filtro.IdRutaSector = this.view.find(".select-zonas").val();
			}


			planificaciones.setFilter(filtro, true);
		},
		resetForm: function(e) {
			e.preventDefault();
			this.set("model", {});
		}
	};
	return kendo.observable(vm);
});

define([
	"jQuery",
	"../../models/planificaciones",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,planificaciones,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: planning-index");
			this.view = event.view.element;
			this.view.find(".planning-edit").click(function(e) {
				planificaciones.select(null);
			});
		},
		onViewShow: function(event){

			console.log("Show view: planning-index");
		},
		onViewHide: function(event){
			console.log("Hide view: planning-index" );
		}
	};
	return kendo.observable(vm);
});

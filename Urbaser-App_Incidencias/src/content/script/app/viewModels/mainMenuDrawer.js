/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(kendo) {
	'use strict'

	var vm = {
		onViewInit: function(e){
			console.log(e.sender.content.id + " init");
			this.view = e.sender.element;
			this.scroller = e.sender.scroller;
		},
		onViewShow: function(e){
			app.kendoApp.view().element.addClass("darked");
			if(window.app.user != null){
				e.view.header.find(".user-info .name").text(window.app.user.nombreCompleto).show();
				if(window.app.user.currentContract != null){
					e.view.header.find(".contract").text(window.app.user.currentContract.Nombre);
				}
			}
			e.view.footer.find(".version").text("Version: " + app.version);
		},
		onViewHide: function(e){
			$("[data-role=view]").removeClass("darked");
		}
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/ciudadano",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,user,kendo) {
	'use strict'
	var vm = {
		source: user.contracts,
		onViewInit: function(e){
			console.log(e.view.id + " init");
		},
		onViewShow: function(e){
			$('.user-info .name').text(user.nombreCompleto);
			if(user.currentContract != null){
				console.log(".contract-item[data-id=" + user.currentContract.IdContrato+"]");
				$('.contract-item span').removeClass("selected");
				$(".contract-item[data-id=" + user.currentContract.IdContrato+"] span").addClass("selected");
			}
			$(".footer-contract").hide();
		},
		onSelect: function(e){
			console.log("contract selected",e);
			user.selectContract(e.dataItem);
			window.app.kendoApp.navigate("#home");
			if(window.app.user){
				$(".footer-contract a").text(window.app.user.currentContract.Nombre);
			}

		},
		onBound: function(){
			console.log("A");
		}
	};
	return kendo.observable(vm);
});

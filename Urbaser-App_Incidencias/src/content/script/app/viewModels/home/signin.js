/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"app/models/ciudadano",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"jQuery",
	"kendo/kendo.core",
	"parsleyjs"
], function(user) {
	'use strict'
	var vm = {
		profile: kendo.observable({}),
		onViewInit: function(e){
			this.view = e.view.content;
			this.parsley = e.view.content.find("form").parsley({
				errorClass: "error"
			});
		},
		onViewShow: function(e){
			if(window.app.user){
				window.app.user.clear();
			}
			//var v = this.view;
			//this.view.find("input").focus(function(e) {
			//	var container = $(v),
			//		scrollTo = $(this);
            //
			//	setTimeout((function() {
			//		window.app.kendoApp.scroller().scrollTo(0,-(scrollTo.offset().top - container.offset().top + container.scrollTop())+50)
			//	}), 500);
            //
			//});
		},
		onViewHide: function(){
		},
		validate: function(e){
			var valid = true;
			if(!this.view.find("#select-address-address").val()){
				this.view.find("#select-address-address").addClass("error");
				valid = false;
			}
			if(!valid && e){
				e.preventDefault();
				alert(T("views.incidencias.selectByAddress.incomplete"));
			}
			return valid;
		},
		send: function(e){
			e.preventDefault();
			if(!this.parsley.validate()){
				alert(T("views.home.signin.incomplete"));
				return false;
			}
			user.registered.addOnce(function(e){
				window.app.kendoApp.pane.loader.hide();
				window.app.kendoApp.navigate("#citizenPass");
			}.bind(this));
			user.registeredFailed.addOnce(function(message){
				alert(message);
				window.app.kendoApp.pane.loader.hide();
			}.bind(this));


			console.log("send: " + this.user);
			user.register({
				Nombre:this.profile.get("Nombre"),
				Apellido1:this.profile.get("Apellido1"),
				Apellido2:this.profile.get("Apellido2"),
				Telefono:this.profile.get("Telefono"),
				Email:this.profile.get("Email")
			});

			window.app.kendoApp.pane.loader.show();
		}
	};

	return kendo.observable(vm);
});

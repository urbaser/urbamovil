/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"jQuery",
	"kendo/kendo.core",
], function() {
	'use strict'
	var vm = {
		user: "",
		password: "",
		onViewInit: function(e){
			this.view = e.view.content;
		},
		onViewShow: function(e){
			if(window.app.user.ticket){
				window.app.user.clear();
			}
			//var v = this.view;
			//this.view.find("input").focus(function(e) {
			//	var container = $(v),
			//		scrollTo = $(this);
            //
			//	setTimeout((function() {
			//		window.app.kendoApp.scroller().scrollTo(0,-(scrollTo.offset().top - container.offset().top + container.scrollTop())+50)
			//	}), 500);
            //
			//});
		},
		onViewHide: function(){
		},
		send: function(e){
			e.preventDefault();
			window.app.user.loggedIn.addOnce(function(e){
				window.app.kendoApp.pane.loader.hide();
				window.app.kendoApp.navigate("#contracts");
			}.bind(this));
			window.app.user.loginFailed.addOnce(function(e){
				alert(T("views.home.login.incorrect"));
				window.app.kendoApp.pane.loader.hide();
			}.bind(this));
			if(!this.user || !this.password){
				console.log(this.user , this.password)
				alert(T("views.home.login.incomplete"));
				console.log("error")
			}
			else{
				console.log("send: " + this.user);
				window.app.user.user = this.user;
				window.app.user.password = this.password;
				window.app.user.login();

				window.app.kendoApp.pane.loader.show();
			}
		}
	};

	return kendo.observable(vm);
});

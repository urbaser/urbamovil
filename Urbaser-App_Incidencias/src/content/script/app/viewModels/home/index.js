define([
	"app/models/shortcuts",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"kendo.repeater"

], function(shortcuts,permissionManager,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: home-index");
			this.view = event.view.element;
		},
		onViewShow: function(event){
			console.log("Show view: home-index");
			$("#homeShortcuts").kendoRepeater({
				dataSource: shortcuts.dataSource,
				template: $("#home-shortcuts-template").html()
			});
		},
		onViewHide: function(event){
			console.log("Hide view: home-index" );
		}
	};
	return kendo.observable(vm);
});

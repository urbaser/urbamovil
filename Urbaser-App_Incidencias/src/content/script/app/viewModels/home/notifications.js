/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"app/config",
	"kendo/kendo.core",
	"app/models/user",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,Incidencia,incidencias,config,kendo,user) {
	'use strict'

	var vm = {
		source: incidencias.dataSource,
		onViewInit: function(e){
			console.log(e.view.id + " init");
			//incidencias.list();
			//incidencias.listed.add(function(){
			//	incidencias.all.forEach(function(i,d){
			//		this.source.push(i);
			//	}.bind(this));
			//}.bind(this));
		},
		onViewShow: function(e){
			var offlines = incidencias.dataSource.offlineData();
			var dataSource = new kendo.data.DataSource({
				data: offlines.filter(function(it){return it.hasOwnProperty("__state__")}),
				schema: {
					model: Incidencia
				}
			});
			this.set("source",dataSource);
			//$('.lang-selection').hide();
		},
		onSelect: function(e){
			console.log(e);
			incidencias.select(e.dataItem);
			window.app.kendoApp.navigate("#detail");
		}
	};
	return kendo.observable(vm);
});

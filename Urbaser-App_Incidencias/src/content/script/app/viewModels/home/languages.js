/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/incidencia",
	"app/models/incidencias",
	"app/config",
	"kendo/kendo.core",
	"app/models/user",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,Incidencia,incidencias,config,kendo,user) {
	'use strict'

	var vm = {
		langs: config.globalization.supportedLanguagesNames,
		onViewInit: function(e){
			console.log(e.view.id + " init");
		},
		onViewShow: function(e){
		},
		onLangSelect: function(e){
			user.preferences.lang = e.dataItem.code;
			user.saveLocal();
			if(confirm(T("views.home.notifications.lang",{lang: e.dataItem.title}))){
				location.href = location.href.substr(0,location.href.indexOf("#"));
			}
		}
	};
	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"app/models/ciudadano",
	"kendo/kendo.data",
	"kendo/kendo.binder",
	"jQuery",
	"kendo/kendo.core",
], function(user) {
	'use strict'
	var vm = {
		user: "",
		password: "",
		onViewInit: function(e){
			this.view = e.view.content;
		},
		onViewShow: function(e){
			if(window.app.user){
				window.app.user.clear();
			}
			//var v = this.view;
			//this.view.find("input").focus(function(e) {
			//	var container = $(v),
			//		scrollTo = $(this);
            //
			//	setTimeout((function() {
			//		window.app.kendoApp.scroller().scrollTo(0,-(scrollTo.offset().top - container.offset().top + container.scrollTop())+50)
			//	}), 500);
            //
			//});
		},
		onViewHide: function(){
		},
		send: function(e){
			e.preventDefault();
			user.gotPassword.addOnce(function(e){
				window.app.kendoApp.pane.loader.hide();
				window.app.kendoApp.navigate("#citizenPass?thanks=0");
			}.bind(this));
			user.getPasswordFailed.addOnce(function(message){
				alert(message);
				window.app.kendoApp.pane.loader.hide();
			}.bind(this));
			if(!this.user){
				alert(T("views.home.login.incomplete"));
				console.log("error")
			}
			else{
				console.log("send: " + this.user);
				user.user = this.user;
				user.saveLocal();
				user.getPassword();

				window.app.kendoApp.pane.loader.show();
			}
		}
	};

	return kendo.observable(vm);
});

/**
 * Created by 7daysofrain on 13/3/15.
 */
define([
	"jQuery",
	"app/models/masterTables",
	"app/models/inventario",
	"app/models/filtro",
	"app/models/filtros",
	"app/models/filtroCampo",
	"app/models/incidencias",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,tables,inventary,Filtro,filtros,FiltroCampo,incidencias,kendo) {
	'use strict'
	function createFilterField(){
		return kendo.observable({
			IdProvincia:0,
			IdMunicipio:0,
			IdEstado:0,
			IdGrupo:0,
			IdTipo:0,
			IdDescripcion:0,
			FromDate: null,
			ToDate: null,
			Days: null,
			IdPrioridad:0,
			Address: null,
			IdTipoElemento: 0,
			IdElemento: 0
		});
	}
	var vm = {
		tables: tables,
		inventary: inventary,
		currentFilter: new Filtro(),
		currentFilterField: kendo.observable({
			IdProvincia:0,
			IdMunicipio:0,
			IdEstado:0,
			IdGrupo:0,
			IdTipo:0,
			IdDescripcion:0,
			FromDate: null,
			ToDate: null,
			Days: null,
			IdPrioridad:0,
			Address: null,
			IdTipoElemento: 0,
			IdElemento: 0
		}),
		filtros: [],
		onViewInit: function(e){
			console.log(e.sender.content.id + " init");
			this.view = e.sender.element;
			this.scroller = e.sender.scroller;
			this.temp = $("#filter-field-list-template").html();
			$("#filter-add-tip").click(this.onTipClose.bind(this));
			$("#filter-save-tip").click(this.onTipClose.bind(this));
			this.relist();
			this.createFilter();
		},
		onViewShow: function(e){
			this.set("filtros",filtros.dataSource);
			$("#incidencias").addClass("blurred");
			if(this.view.find(".select-grupos option:first-child").attr("value")){
				this.view.find(".select-grupos").prepend("<option disabled>" + T("views.incidencias.edit.select-group") + "</option>");
				this.view.find(".select-tipos").prepend("<option disabled>" + T("views.incidencias.edit.select-tipo") + "</option>");
				this.view.find(".select-descripciones").prepend("<option disabled>" + T("views.incidencias.edit.select-descripcion") + "</option>");
				this.view.find(".select-provincias").prepend("<option disabled>" + T("views.incidencias.edit.select-provincia") + "</option>");
				this.view.find(".select-municipios").prepend("<option disabled>" + T("views.incidencias.edit.select-municipio") + "</option>");
				this.view.find(".select-prioridades").prepend("<option disabled>" + T("views.incidencias.edit.select-prioridad") + "</option>");
				this.view.find(".select-estados").prepend("<option disabled>" + T("views.incidencias.edit.select-estado") + "</option>");

			}
			this.currentFilterField = createFilterField();
			if(!window.app.user.preferences.filterHelp1){
				$("#filter-add-tip").addClass("visible");
				window.app.user.preferences.filterHelp1 = true;
				window.app.user.saveLocal();
			}
			if(filtros.dataSource.total() == 0){
				filtros.dataSource.fetch();
			}
		},
		onViewHide: function(e){
			$("#incidencias").removeClass("blurred");
			$("#filter-add-tip").removeClass("visible");
			$("#filter-save-tip").removeClass("visible");
		},
		onTipoFiltroChange: function(e){
			$(".filter-box .values > *")
				.hide()
				.parent()
				.find("[data-option='" + (this.selectedFilterType = $(e.currentTarget).val()) + "']")
				.show();

			this.view.find(".values option:first-child").attr("selected",true);
		},
		provinciaChange: function(e){
			tables.municipios.filter({
				field: "IdProvincia",
				operator: "eq",
				value:this.currentFilterField.IdProvincia
			});
			this.view.find(".select-municipios").removeAttr("disabled").prepend("<option disabled selected>Selecciona municipio</option>");
		},
		grupoChange: function(e){
			tables.tipos.filter({
				field: "IdGrupoIncidencia",
				operator: "eq",
				value:this.currentFilterField.IdGrupo
			});

			this.view.find(".select-tipos").removeAttr("disabled").prepend("<option disabled selected>Selecciona un tipo</option>");
			this.view.find(".select-descripciones").attr("disabled","").empty().prepend("<option disabled selected>Selecciona descripción</option>");
		},
		tipoChange: function(e){
			tables.descripciones.filter({
				field: "IdTipoIncidencia",
				operator: "eq",
				value:this.currentFilterField.IdTipo
			});
			this.view.find(".select-descripciones").removeAttr("disabled").prepend("<option disabled selected>Selecciona descripción</option>");
		},
		tipoElementoChange: function(e){
			inventary.elementosLista.params = {IdTipoElemento:this.currentFilterField.IdTipoElemento}
			inventary.elementosLista.read();
		},
		createFilter: function(){

			console.log("Creating new filter");
			this.set("currentFilter",new Filtro());
			this.currentFilter.FiltrosCampo = new Array();
			filtros.select(this.currentFilter);
			incidencias.setFilter(null);
		},
		onCreateFilter: function(e){
			e.preventDefault();
			if(this.currentFilter == null || this.currentFilter.IdFiltroUsuario){
				this.createFilter();
			}
			console.log("creating filter: " + this.selectedFilterType);
			var f = new FiltroCampo();
			f.NombreCampo = this.selectedFilterType;
			switch(this.selectedFilterType){
				case "ESTADO_INCIDENCIA":
					f.Tipo = "INTEGER";
					f.Valor = this.view.find(".select-estados").val();
					break;
				case "GRUPO_TIPO_DESCRIPCION":
					f.Tipo = "INTEGER";
					f.Valor = 	this.view.find(".select-grupos").val() + "," +
								this.view.find(".select-tipos").val() + "," +
								this.view.find(".select-descripciones").val()
					break;
				case "FECHA_DESDE":
					f.Tipo = "DATE";
					f.Valor = new Date($("#filter-date-from").val());
					break;
				case "FECHA_HASTA":
					f.Tipo = "DATE";
					f.Valor = new Date($("#filter-date-to").val());
					break;
				case "ULTIMOS_DIAS":
					f.Tipo = "INTEGER";
					f.Valor = $("#filter-days").val();
					break;
				case "CONTRATO":
					f.Tipo = "INTEGER";
					f.Valor = "";
					break;
				case "PRIORIDAD":
					f.Tipo = "INTEGER";
					f.Valor = this.view.find(".select-prioridades").val();
					break;
				case "PROVINCIA/MUNICIPIO":
					f.Tipo = "INTEGER";
					f.Valor = 	this.view.find(".select-provincias").val() + "," +
								this.view.find(".select-municipios").val()
					break;
				case "DIRECCION":
					f.Tipo = "STRING";
					f.Valor = $("#filter-address").val();
					break;
				case "ELEMENTO_INVENTARIO":
					f.Tipo = "INTEGER";
					f.Valor =  	this.view.find(".select-tipos").val() + "," +
								this.view.find(".select-elementos").val()
					break;
			}
			this.currentFilter.FiltrosCampo.push(f);
			this.view.find(".filter-box").removeClass("visible").addClass("hidden");
			this.relist();
		},
		onCancelCreateFilter: function(e){
			e.preventDefault();
			this.view.find(".filter-box").removeClass("visible").addClass("hidden");
		},
		onCreateFilterField: function(e){
			e.preventDefault();
			if(this.currentFilter == null){
				this.createFilter();
			}
			this.currentFilterField = createFilterField();
			$(".tipbox").removeClass("visible");
			this.view.find(".filter-box").removeClass("hidden").addClass("visible");
			this.scroller.scrollTo(0,0);
			$("#filtrosDrawer .drawer-content").addClass("show-fields");
			this.view.find(".button-cancel,.button-save").addClass("show");
			this.view.find(".filter-box select").val(0);
			this.view.find(".filter-box input").val("");
			if(!window.app.user.preferences.filterHelp2){
				$("#filter-save-tip").addClass("visible");
				window.app.user.preferences.filterHelp2 = true;
				window.app.user.saveLocal();
			}
			this.view.find(".select-tipo-filtro option:first-child").attr("selected",true);
			this.relist();
		},
		removeFilterField: function(e){
			e.preventDefault();
			var uid = $(e.currentTarget).attr("data-uid");
			console.log("remove",uid);
			var item = $.grep(this.currentFilter.FiltrosCampo, function(e){ return e.uid == uid; });
			var index = this.currentFilter.FiltrosCampo.indexOf(item[0]);
			if(index > -1){
				this.currentFilter.FiltrosCampo.splice(index,1);
				this.relist();
			}
		},
		removeFilter: function(e){
			e.preventDefault();
			$("#modal-confirmDeleteFilter").kendoMobileModalView("open");
		},
		onTipClose: function(e){
			e.preventDefault();
			$(".tipbox").removeClass("visible");
		},
		relist: function(){
			$("#filterFieldList").kendoRepeater({
				dataSource: this.currentFilter.FiltrosCampo,
				template: this.temp
			});
			$("#filterFieldList a").click(this.removeFilterField.bind(this));
		},
		onSaveFilter: function(e){
			e.preventDefault();
			$("#modal-save-filter").kendoMobileModalView("open");
		},
		onCancelFilter: function(e){
			e.preventDefault();
			this.scroller.scrollTo(0,0);
			this.createFilter();
			this.createFilter();
			this.relist();
			$("#filtrosDrawer .drawer-content").removeClass("show-fields");
			this.view.find(".button-cancel,.button-save").removeClass("show");
		},
		onFiltroSelect: function(e){

			// Cancel if delete tapped
			if(e.target.parent().hasClass("close")){
				filtros.select(e.dataItem);
				return;
			}
			$(e.sender.element).find(".icon").removeClass("selected");
			if(incidencias.filter == e.dataItem){
				incidencias.setFilter(null);
				window.app.changeTitle("Incidencias");
				console.log(e);
			}
			else{
				incidencias.setFilter(e.dataItem);
				window.app.changeTitle("incidencias/" + e.dataItem.Nombre);
				$(e.item).find(".icon").addClass("selected");
			}

			$("#filtrosDrawer").data("kendoMobileDrawer").hide();
		},
		showList: function(){
			this.currentFilter = null;
			this.scroller.scrollTo(0,0);
			$("#filtrosDrawer .drawer-content").removeClass("show-fields");
			this.view.find(".button-cancel,.button-save").removeClass("show");
		}
	};
	return kendo.observable(vm);
});

define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: floats-index");
			this.view = event.view.element;
		},
		onViewShow: function(event){
			console.log("Show view: floats-index");
		},
		onViewHide: function(event){
			console.log("Hide view: floats-index" );
		}
	};
	return kendo.observable(vm);
});

define([
	"jQuery",
	"app/models/flota",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,flota,kendo) {
	'use strict'
	var vm = {
		model: {},
		onViewInit: function(event){
			console.log("Init view: floats-vehicleDetails");
			this.view = event.view.element;
			this.template = kendo.template($("#vehicle-details-template").html());
			$(document).on("click", "#floats-vehicleDetails .goToSignals", this.goToSignals.bind(this));
			$(document).on("click", "#floats-vehicleDetails .goToRoutes", this.goToRoutes.bind(this));
			$(document).on("click", "#floats-vehicleDetails .goToAlarms", this.goToAlarms.bind(this));
		},
		onViewShow: function(event){
			console.log("Show view: floats-vehicleDetails");
			this.set("model", flota.vehiculos.selected);
			this.view.find(".vehicle-details-container").empty().append($(this.template(this.model)).i18n());
		},
		onViewHide: function(event){
			console.log("Hide view: floats-vehicleDetails" );
		},
		goToSignals: function(event) {
			event.preventDefault();
			flota.signals.filter = null;
			window.app.kendoApp.navigate("#floats-signals");
		},
		goToAlarms: function(event) {
			event.preventDefault();
			window.app.kendoApp.navigate("#floats-alarms");
		},
		goToRoutes: function(event) {
			event.preventDefault();
			window.app.kendoApp.navigate("#floats-routes");
		}
	};
	return kendo.observable(vm);
});

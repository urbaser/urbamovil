/**
 * Created by 7daysofrain on 16/2/16.
 */
define([
		"app/models/inventoryMasterTables"
], function(inventoryMasterTables)
{
	var obj = {
		loadInventory: function(force){
			if(!inventoryMasterTables.hasData || force){
				console.log("Loading inventory tables");
				inventoryMasterTables.update(app.user.currentContract);
			}
		}
	};
	return obj;
});

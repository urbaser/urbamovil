/**
 * Created by 7daysofrain on 21/5/15.
 */
define(['jQuery','modernizr','modernizrFeatures/css-vwunit'], function()
{
	// Background position 4 values
	var elem = document.createElement('a'),
		eStyle = elem.style,
		val = "right 10px bottom 10px";

	Modernizr.addTest('bgpositionfourvalues', function(){
		eStyle.cssText = "background-position: "+val+";";
		return (eStyle.backgroundPosition === val) ? true : false;
	});
	console.log("Moderniz background position 4 values: " + Modernizr.bgpositionfourvalues);
	// Background position 4 values
	var elem = document.createElement('a'),
		eStyle = elem.style,
		val = "1";

	Modernizr.addTest('flexgrow', function(){
		eStyle.cssText = "flex-grow: "+val+";-webkit-flex-grow:" + val + ";";
		return (eStyle.flexGrow === val) ? true : false;
	});
	console.log("Moderniz Flex grow: " + Modernizr.flexgrow);

	// CSS Calc
	Modernizr.addTest('csscalc', function() {
		var prop = 'width:';
		var value = 'calc(10px);';
		var el = document.createElement('div');

		el.style.cssText = prop + Modernizr._prefixes.join(value + prop);

		return !!el.style.length;
	});
	var obj = {

	};
	return obj;
});

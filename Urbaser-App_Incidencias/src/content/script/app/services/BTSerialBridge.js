/**
 * Created by 7daysofrain on 5/8/15.
 */
define([
	'app/services/BTSerialBlackBerry',
	'app/services/BTSerialAndroid',
	'app/services/BTSerialNull',

], function(bb,android,nil)
{
	var os = !window.cordova ? "web" : window.device.platform;
	return os == "blackberry10" ? bb : "android" ? android : nil;
});

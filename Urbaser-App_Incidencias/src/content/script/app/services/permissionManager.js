define([], function()
{
	var obj = {
		hasPermission: function(permission){
			var roles = app.user.profile.Roles;
			var permissionRequest = permission.split(".");
			var level1 = permissionRequest[0];
			var level2 = permissionRequest[1];
			if(!roles.hasOwnProperty(level1)){
				return false;
			}
			else if(!level2){
				return true;
			}
			else{
				return roles[level1][level2] === true;
			}

		}
	};
	return obj;
});

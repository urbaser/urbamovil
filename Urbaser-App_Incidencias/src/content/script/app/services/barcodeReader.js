define([
	"signals",
], function(signals)
{

	var obj = {
		scanned: new signals.Signal(),
		scannedError: new signals.Signal(),
		readTag: function(){
			cordova.plugins.barcodeScanner.scan(
				this.onBarcodeReceived.bind(this),
				this.error.bind(this),
				{
					"preferFrontCamera" : false, // iOS and Android
					"showFlipCameraButton" : true, // iOS and Android
					"prompt" : T("app.barcodePrompt"), // supported on Android only
					"formats" : "QR_CODE", // default: all but PDF_417 and RSS_EXPANDED
					"orientation" : "portrait" // Android only (portrait|landscape), default unset so it rotates with the device
				}
			);
		},
		onBarcodeReceived: function(barcode){
			console.log("Barcode read",barcode);
			this.scanned.dispatch(barcode.text);
		},
		error: function(msg){
			this.scannedError.dispatch(msg);
		}
	};
	return obj;
});

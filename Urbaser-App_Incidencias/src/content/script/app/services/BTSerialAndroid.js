/**
 * Created by 7daysofrain on 30/7/15.
 */
define(['signals'], function(signals)
{
	var obj = {
		supported: true,
		init: function(){

			console.log('Bluetooth initialised');
			this.bluetoothInitialised = true;
			this.deviceConnected = false;
			this.btScanning = false;
			this.btScanned = false;
		},
		scanned: new signals.Signal(),
		scannedError: new signals.Signal(),
		scan: function(){
			// check to see if Bluetooth is turned on.
			// this function is called only
			//if isEnabled(), below, returns success:
			var listPorts = function() {
				console.log('Started Scanning ... please wait ...');
				this.btScanning = true;
				bluetoothSerial.list(
					function(results) {
						this.btScanning = false;
						console.log(results);
						this.btScanned = true;
						console.log('XXXX json.devices.length ' + results.length);
						this.devices = new Array();
						if (results.length > 0) {
							for (var i = 0; i < results.length; i++) {
								var thisDevice = {};
								thisDevice.name = results[i].name;
								thisDevice.address = results[i].address;
								this.devices.push(thisDevice);
								console.log('XXXX Name: ' + results[i].name);
								console.log('XXXX Address: ' + results[i].address);
							}
							this.scanned.dispatch(this.devices);
						} else {
							this.scannedError.dispatch(T("services.btserial.devicenotfound"));
						}
					}.bind(this),
					function(error) {
						this.btScanning = false;
						console.log(error);
						this.scannedError.dispatch(error);
					}.bind(this)
				);
			}.bind(this);

			// if isEnabled returns failure, this function is called:
			var notEnabled = function() {
				this.scannedError.dispatch("Bluetooth no está disponible.")
			}

			// check if Bluetooth is on:
			bluetoothSerial.isEnabled(
				listPorts,
				notEnabled
			);
		},
		connected: new signals.Signal(),
		connectedError: new signals.Signal(),
		connect: function(deviceAddress){
			var bluetoothAddress = deviceAddress || this.selectedDevice;

			bluetoothSerial.connect(
				bluetoothAddress,  // device to connect to
				function(result){
					this.deviceConnected = true;
					console.log('Connected to device');
					this.connected.dispatch();
					bluetoothSerial.subscribe('\r', this.connectionCallback.bind(this));
				}.bind(this),
				function(error){
					console.error('Failed to connect to device: ' + error);
					this.connectedError.dispatch(error);
					this.connect();
				}.bind(this)
			);
		},
		connectionCallback: function(data) {
			if (data.charCodeAt(0) == 36) {
				console.log('XXXX data: ' + data);
				if(data.indexOf(36) != data.lastIndexOf(36)){
					var buff = data.substring(5,json.data.indexOf(13))
				}
				else{
					var buff = data.substring(5)
				}
				if(buff.length == 17){
					this.rfidReceived.dispatch(buff.substr(0,17));
				}
				else{
					this.rfidError.dispatch();
				}
			} else {
				console.log('>> ' + data);
			}
		},
		disconnected: new signals.Signal(),
		disconnect: function(){
			bluetoothSerial.unsubscribe();
			this.deviceConnected = false;
			this.disconnected.dispatch();
		},
		rfidReceived: new signals.Signal(),
		rfidError: new signals.Signal()
	};
	return obj;
});

define([
	'app/models/user',
	'app/services/rfidReader',
	'app/services/nfcReader',
	'app/services/barcodeReader'
], function(user,rfid,nfcReader,qrReader)
{
	var obj = {
		isReady: function(){
			return user.preferences.tagReaderDevice != null;
		},
		getReader: function(){
			if(this.isReady()){
				switch(user.preferences.tagReaderDevice){
					case "htl-lite":
						return rfid;
					case "nfc":
						return nfcReader;
					case "qr":
						return qrReader;
					default:
						throw new Error("Lector no reconocido: ",user.preferences.tagReaderDevice);
				}
			}
			else{
				alert("No tienes configurado ningún lector de tags. Puedes hacerlo desde el menu lateral.");
			}
		}
	};
	return obj;
});

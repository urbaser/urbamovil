define([
	'app/models/user',
	"signals",
], function(user,signals)
{

	var obj = {
		scanned: new signals.Signal(),
		scannedError: new signals.Signal(),
		readTag: function(){
			window.nfc.addNdefListener(
				this.onNFCReceived.bind(this),
				this.onConnect.bind(this),
				this.onConnectError.bind(this)
			);
			window.nfc.addTagDiscoveredListener(
				this.onNFCReceived.bind(this),
				this.onConnect.bind(this),
				this.onConnectError.bind(this)
			);
		},
		onConnect: function(){
			console.log("rfid reader connected");
		},
		onConnectError: function(){
			this.error(T("views.incidencias.selectByRFID.connectFail"));
		},
		onNFCReceived: function(nfc){
			console.log("NFC Tag read",nfc.tag);
			var id = window.nfc.bytesToHexString(nfc.tag.id);
			console.log("ID Decoded",id);
			this.scanned.dispatch(id);
		},
		error: function(msg){
			this.scannedError.dispatch(msg);
		}
	};
	return obj;
});

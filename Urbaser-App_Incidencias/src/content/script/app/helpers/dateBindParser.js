define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"
], function(j,kendo) {
	var obj = {
		init: function(){
			alert("ini");
			kendo.data.binders.date = kendo.data.Binder.extend({
				init: function (element, bindings, options) {
					kendo.data.Binder.fn.init.call(this, element, bindings, options);

					this.format = $(element).data("format");
				},
				refresh: function () {
					var data = this.bindings["date"].get();
					if (data) {
						$(this.element).val(kendo.toString(new Date(data), this.format));
					}
				}
			});
		}
	}

	return obj;
});

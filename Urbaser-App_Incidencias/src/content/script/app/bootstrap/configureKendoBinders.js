define([

	"kendo/kendo.mobile.application",
], function(kendo)
{
	var obj = {
		process: function(app){
			return new Promise(function (resolve, reject) {
				kendo.data.binders.date = kendo.data.Binder.extend({
					init: function (element, bindings, options) {
						kendo.data.Binder.fn.init.call(this, element, bindings, options);

						this.format = $(element).data("format");
					},
					refresh: function () {
						var data = this.bindings["date"].get();
						if (data) {
							$(this.element).val(kendo.toString(new Date(data), this.format));
						}
					}
				});
				resolve(app);
			});
		}
	};
	return obj;
});

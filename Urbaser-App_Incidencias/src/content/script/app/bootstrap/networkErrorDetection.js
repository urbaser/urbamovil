define([], function()
{
	var obj = {
		process: function (app) {
			return new Promise(function (resolve, reject) {

				// Global network error handler
				$(document).ajaxError(function( event, jqxhr, settings, thrownError ) {
					// Session timeout detection
					if ( jqxhr.status == 401 ) {
						window.app.kendoApp.navigate("#login");
					}
					// Session timeout detection
					else if ( jqxhr.status == 405 ) {
						alert(T("app.nopermission"));
					}
				});
				resolve(app);
			});
		}
	};
	return obj;
});

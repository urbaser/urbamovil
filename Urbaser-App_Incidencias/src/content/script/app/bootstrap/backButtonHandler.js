define([], function()
{
	var obj = {
		process: function(app){
			return new Promise(function (resolve, reject) {
				document.addEventListener("backbutton", function(ev){
					// TODO Implementar Router propio
					//this.kendoApp.router.navigate("#:back");
					ev.preventDefault();
				}.bind(this), false);
				resolve(app);
			});
		}
	};
	return obj;
});

define([
	"app/models/incidencias"
], function(incidencias)
{
	var obj = {
		process: function(app){
			return new Promise(function (resolve, reject) {

				// Online / Offline handling
				document.addEventListener("offline", function(){
					console.log("offline");
					app.online = false;
					incidencias.offline();
					$("#modal-offline").kendoMobileModalView("open");
				}, false);
				document.addEventListener("online", function(){
					console.log("online");
					app.online = true;
					incidencias.online();
				}, false);
				if(navigator.connection){
					console.log("Connection type: " + navigator.connection.type);
					if(navigator.connection.type == "none"){
						app.online = false;
					}
				}
				resolve(app);
			});
		}
	};
	return obj;
});

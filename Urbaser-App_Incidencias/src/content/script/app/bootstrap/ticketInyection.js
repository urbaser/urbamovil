/**
 * Created by 7daysofrain on 11/2/16.
 */
define([], function()
{
	var obj = {
		process: function(app){
			return new Promise(function (resolve, reject) {
				$.ajaxSetup({
					beforeSend: function(xhr) {
						if(window.app.user.ticket){
							xhr.setRequestHeader("Ticket",window.app.user.ticket);
						}
					}
				});
				console.log("Ticket inyection configured");
				resolve(app);
			});
		}
	};
	return obj;
});

define([
	"i18next",
], function(i18next)
{
	var resolver,rejecter,app;
	var obj = {
		process: function(application){
			return new Promise(function (resolve, reject) {
				app = application;
				resolver = resolve;
				rejecter = reject;
				function getBrowserLocale(){
					return window.navigator.language;
				}
				if(app.user.preferences.lang){
					app.detectedLanguage = app.user.preferences.lang;
					this.i18n();
				}
				else if(navigator.globalization){
					navigator.globalization.getPreferredLanguage(
						function (language) {
							app.detectedLanguage = language.value;
							this.i18n();
						}.bind(this),
						function () {
							app.detectedLanguage = getBrowserLocale();
							this.i18n();
						}.bind(this)
					);
				}
				else{
					app.detectedLanguage = getBrowserLocale();
					this.i18n();
				}
			}.bind(this));
		},
		i18n: function(){
			var supported = app.config.globalization.supports(app.detectedLanguage);
			var lng = supported ? app.config.globalization.map(app.detectedLanguage) : app.config.globalization.defaultLanguage;
			this.selectedLanguage = lng;
			console.log("i18n localizating, supported: " + supported + ", detected language: " + this.detectedLanguage + ", selected language: " + lng);
			i18next.init({
					debug: true,
					fallbackLng: false,
					lng: lng,
					useLocalStorage: false
				},
				function(t) {
					console.log("i18n init");
					$("body").i18n();
					resolver(app);
					window.T = t;
				}.bind(this));
		}
	};
	return obj;
});

define([], function()
{
	var obj = {
		process: function(app){
			return new Promise(function (resolve) {
				// Error detection
				window.onerror = function (message, filename, linenumber, char, ex) {
					console.error(ex  || message);
					if(message.indexOf("#!") > -1){
						history.back();
					}
					if (app.debugMode) {
						var msg = (message + "<br>" + filename + ":" + linenumber + "<br>" + ex.stack);
						var frag = $("<div>" + msg + "</div>")
							.css("background-color", "red")
							.css("font-size", "16px")
							.css("position", "absolute")
							.css("top", "0")
							.css("left", "0")
							.css("width", "100%")
							.css("word-wrap", "break-word")
							.css("z-index", 999999999);
						setTimeout(function(){
							frag.click(function () {
								$(this).remove();
							})
						},1);
						$(document.documentElement)
							.append(frag);
					}
					return !app.debugMode; // The exception is handled, don't show to the user.
				};
				resolve(app);
			});
		}
	};
	return obj;
});

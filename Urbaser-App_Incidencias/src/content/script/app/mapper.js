define([
	"app/viewModels/incidencias/index",
	"app/viewModels/incidencias/list",
	"app/viewModels/incidencias/edit",
	"app/viewModels/incidencias/detail",
	"app/viewModels/incidencias/locationsMap",
	"app/viewModels/incidencias/selectLocation",
	"app/viewModels/incidencias/selectByAddress",
	"app/viewModels/incidencias/selectByInventary",
	"app/viewModels/incidencias/selectByMap",
	"app/viewModels/incidencias/selectByRFID",
	"app/viewModels/incidencias/resume",
	"app/viewModels/incidencias/stateHistory",
	"app/viewModels/incidencias/closure",
	"app/viewModels/inventory/index",
	"app/viewModels/inventory/search",
	"app/viewModels/inventory/results",
	"app/viewModels/inventory/detailsElemento",
	"app/viewModels/inventory/create",
	"app/viewModels/inventory/createContenedor",
	"app/viewModels/inventory/createPuntoRecogida",
	"app/viewModels/inventory/createPapelera",
	"app/viewModels/inventory/createElemento",
	"app/viewModels/inventory/readTag",
	"app/viewModels/planning/index",
	"app/viewModels/planning/search",
	"app/viewModels/inventory/mapsearch",
	"app/viewModels/planning/results",
	"app/viewModels/planning/details",
	"app/viewModels/planning/edit",
	"app/viewModels/planning/editService",
	"app/viewModels/planning/editStep2",
	"app/viewModels/planning/editStep2B",
	"app/viewModels/planning/editStep3",
	"app/viewModels/planning/editStep4",
	"app/viewModels/planning/editStep5",
	"app/viewModels/planning/resourcesList",
	"app/viewModels/planning/locationSelect",
	"app/viewModels/planning/tasksSelect",
	"app/viewModels/floats/index",
	"app/viewModels/floats/vehicles",
	"app/viewModels/floats/vehicleDetails",
	"app/viewModels/floats/routes",
	"app/viewModels/floats/signals",
	"app/viewModels/floats/signalDetails",
	"app/viewModels/floats/alarms",
	"app/viewModels/modals/changeState",
	"app/viewModels/modals/offline",
	"app/viewModels/modals/saveFilter",
	"app/viewModels/modals/confirmDeleteFilter",
	"app/viewModels/modals/selectBTDevice",
	"app/viewModels/modals/editFieldSelect",
	"app/viewModels/modals/editFieldBoolean",
	"app/viewModels/modals/editFieldNumber",
	"app/viewModels/modals/editFieldInput",
	"app/viewModels/modals/editFieldDate",
	"app/viewModels/modals/editFieldFile",
	"app/viewModels/modals/resourcesSelect",
	"app/viewModels/modals/alarmMessage",
	"app/viewModels/modals/vehiclesFilter",
	"app/viewModels/modals/readTag",
	"app/viewModels/home/index",
	"app/viewModels/home/contracts",
	"app/viewModels/home/login",
	"app/viewModels/home/languages",
	"app/viewModels/home/notifications",
	"app/viewModels/home/tagReaderSelector",
	"app/viewModels/filtrosDrawer",
	"app/viewModels/mainMenuDrawer"
	
], function()
{
	return {
		views: {
			incidencias: {
				index: require("app/viewModels/incidencias/index"),
				list: require("app/viewModels/incidencias/list"),
				edit: require("app/viewModels/incidencias/edit"),
				detail: require("app/viewModels/incidencias/detail"),
				locationsMap: require("app/viewModels/incidencias/locationsMap"),
				selectLocation: require("app/viewModels/incidencias/selectLocation"),
				selectByInventary: require("app/viewModels/incidencias/selectByInventary"),
				selectByAddress: require("app/viewModels/incidencias/selectByAddress"),
				selectByMap: require("app/viewModels/incidencias/selectByMap"),
				selectByRFID: require("app/viewModels/incidencias/selectByRFID"),
				resume: require("app/viewModels/incidencias/resume"),
				stateHistory: require("app/viewModels/incidencias/stateHistory"),
				closure: require("app/viewModels/incidencias/closure"),
			},
			home: {
				index: require("app/viewModels/home/index"),
				login: require("app/viewModels/home/login"),
				contracts: require("app/viewModels/home/contracts"),
				languages: require("app/viewModels/home/languages"),
				notifications: require("app/viewModels/home/notifications"),
				tagReaderSelector: require("app/viewModels/home/tagReaderSelector"),
			},
			inventory: {
				index: require("app/viewModels/inventory/index"),
				search: require("app/viewModels/inventory/search"),
				mapsearch: require("app/viewModels/inventory/mapsearch"),
				results: require("app/viewModels/inventory/results"),
				detailsElemento: require("app/viewModels/inventory/detailsElemento"),
				create: require("app/viewModels/inventory/create"),
				createContenedor: require("app/viewModels/inventory/createContenedor"),
				createPuntoRecogida: require("app/viewModels/inventory/createPuntoRecogida"),
				createPapelera: require("app/viewModels/inventory/createPapelera"),
				createElemento: require("app/viewModels/inventory/createElemento"),
				readTag: require("app/viewModels/inventory/readTag"),
			},
			planning: {
				index: require("app/viewModels/planning/index"),
				search: require("app/viewModels/planning/search"),
				results: require("app/viewModels/planning/results"),
				details: require("app/viewModels/planning/details"),
				edit: require("app/viewModels/planning/edit"),
				editService: require("app/viewModels/planning/editService"),
				editStep2: require("app/viewModels/planning/editStep2"),
				editStep2B: require("app/viewModels/planning/editStep2B"),
				editStep3: require("app/viewModels/planning/editStep3"),
				editStep4: require("app/viewModels/planning/editStep4"),
				editStep5: require("app/viewModels/planning/editStep5"),
				resourcesList: require("app/viewModels/planning/resourcesList"),
				locationSelect: require("app/viewModels/planning/locationSelect"),
				tasksSelect: require("app/viewModels/planning/tasksSelect")
			},
			floats: {
				index: require("app/viewModels/floats/index"),
				vehicles: require("app/viewModels/floats/vehicles"),
				vehicleDetails: require("app/viewModels/floats/vehicleDetails"),
				routes: require("app/viewModels/floats/routes"),
				signals: require("app/viewModels/floats/signals"),
				signalDetails: require("app/viewModels/floats/signalDetails"),
				alarms: require("app/viewModels/floats/alarms")
			},
			modals: {
				changeState: require("app/viewModels/modals/changeState"),
				offline: require("app/viewModels/modals/offline"),
				saveFilter: require("app/viewModels/modals/saveFilter"),
				confirmDeleteFilter: require("app/viewModels/modals/confirmDeleteFilter"),
				selectBTDevice: require("app/viewModels/modals/selectBTDevice"),
				editFieldSelect: require("app/viewModels/modals/editFieldSelect"),
				editFieldBoolean: require("app/viewModels/modals/editFieldBoolean"),
				editFieldInput: require("app/viewModels/modals/editFieldInput"),
				editFieldDate: require("app/viewModels/modals/editFieldDate"),
				editFieldNumber: require("app/viewModels/modals/editFieldNumber"),
				editFieldFile: require("app/viewModels/modals/editFieldFile"),
				resourcesSelect: require("app/viewModels/modals/resourcesSelect"),
				alarmMessage: require("app/viewModels/modals/alarmMessage"),
				readTag: require("app/viewModels/modals/readTag")
			},
			filtrosDrawer: require("app/viewModels/filtrosDrawer"),
			mainMenuDrawer: require("app/viewModels/mainMenuDrawer")
		},
		layouts: {
		}
	};
});

require.config({
	baseUrl: "content/script",
	paths: {
		jQuery: "../components/jquery/dist/jquery",
		jquery: "../components/jquery/dist/jquery",
		kendo: "../components/kendo-ui/src/js",
		refr3sh: "../components/refr3sh-jslib/src/refr3sh",
		"TweenMax": "../components/gsap/src/uncompressed/TweenMax",
		"TweenLite": "../components/gsap/src/uncompressed/TweenLite",
		"TimelineMax": "../components/gsap/src/uncompressed/TimelineMax",
		"signals": "../components/signals/dist/signals",
		"kendo.repeater": "vendor/kendo.repeater",
		"gmaps": "../components/gmaps/gmaps",
		"i18next": "../components/i18next/i18next.amd.withJQuery",
		"polyfill": "../components/polyfill/dist/polyfill",
		"modernizr": "../components/modernizr/modernizr",
		"modernizrFeatures": "../components/modernizr/feature-detects",
		"parsleyjs": "../components/parsleyjs/dist/parsley"


	},
	shim: {
		jQuery: {
			exports: "jQuery"
		},
		kendo: {
			deps: ["jQuery"]
		},
		"kendo.repeater": {
			deps: ["kendo/kendo.core"]
		},
		"parsleyjs": {
			deps: ["jQuery"]
		}
	}
});


// Function.bind polyfill
if (!Function.prototype.bind) {
	Function.prototype.bind = function(oThis) {
		if (typeof this !== 'function') {
			// closest thing possible to the ECMAScript 5
			// internal IsCallable function
			throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
		}

		var aArgs   = Array.prototype.slice.call(arguments, 1),
			fToBind = this,
			fNOP    = function() {},
			fBound  = function() {
				return fToBind.apply(this instanceof fNOP
						? this
						: oThis,
					aArgs.concat(Array.prototype.slice.call(arguments)));
			};

		fNOP.prototype = this.prototype;
		fBound.prototype = new fNOP();

		return fBound;
	};
}

var app;
function run() {
	require(["jQuery",
		"app/citizenApp",
		"app/models/ciudadano",
		"app/citizenMapper",
		"app/config",
		"kendo/kendo.core",
		"kendo/kendo.mobile.application",
		"kendo.repeater"
	], function (j,application,user,mapper,config) {
		window.app = app = application;
		window.app.user = user;
		window.app.viewModels = mapper;
		window.app.config = config;
		app.init();
	});
}


if(window.cordova){
	console.log("Cordova environment detected");
	document.addEventListener('deviceready', function(){
		run();
	});
}
else{
	console.log("Cordova environment NOT detected");
	run();
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace RepositorioUrbamovil.Cliente
{
    public class AreaTICJavaScriptSerializer : JavaScriptSerializer
    {
        public List<KeyValuePair<string, string>> GetKeyValuePair(string jSonText)
        {
            List<KeyValuePair<string, string>> lstResultado = new List<KeyValuePair<string, string>>();

            //separar la cadena jSon para tener los objetos
            MatchCollection mcElementosLista = Regex.Matches(jSonText, @"(?<=\{)(.*?)(?=\})", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);

            foreach (Match mElemento in mcElementosLista)
            {
                //separar por clave : valor
                MatchCollection mcRelacionClavesValores = Regex.Matches(mElemento.Value, @"(?<=(^|\,)).*?(?=(\,|$))", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
                foreach (Match mClaveValor in mcRelacionClavesValores)
                {
                    //separar cadena por ':' y asignar valores
                    string[] strSeparador = { "\":\"", "\":null" };
                    string[] strClaveValor = mClaveValor.Value.Split(strSeparador, StringSplitOptions.None);
                    if (strClaveValor.Length == 2)
                    {
                        string strClave = strClaveValor[0];
                        string strValor = strClaveValor[1];

                        if (strClave.StartsWith("\""))
                            strClave = strClave.Substring(strClave.IndexOf("\"") + "\"".Length);

                        if (strClave.EndsWith("\""))
                            strClave = strClave.Substring(0, strValor.IndexOf("\""));

                        if (strValor.StartsWith("\""))
                            strValor = strValor.Substring(strValor.IndexOf("\"") + "\"".Length);

                        if (strValor.EndsWith("\""))
                            strValor = strValor.Substring(0, strValor.IndexOf("\""));

                        if (strValor.Length == 0)
                            strValor = "null";

                        KeyValuePair<string, string> kvpAux = new KeyValuePair<string, string>(strClave, strValor);
                        lstResultado.Add(kvpAux);
                    }
                }
            }
            return lstResultado;
        }

        public List<KeyValuePair<string, string>> GetKeyValuePair(object objectValue)
        {
            try
            {
                List<KeyValuePair<string, string>> lstResultado = new List<KeyValuePair<string, string>>();
                System.Reflection.PropertyInfo[] propiedadesObjeto = objectValue.GetType().GetProperties();
                foreach (System.Reflection.PropertyInfo propiedad in propiedadesObjeto)
                {
                    //obtener el nombre y valor de la propiedad a añadir en el Diccionario
                    string strValor = "";
                    object o = propiedad.GetValue(objectValue, null);
                    if (o != null)
                        strValor = o.ToString();
                    if (strValor.Length == 0)
                    {
                        if (propiedad.PropertyType == typeof(string)) strValor = " ";
                        else strValor = "null";
                    }
                    
                    KeyValuePair<string, string> kvpAux = new KeyValuePair<string, string>(propiedad.Name, strValor);
                    lstResultado.Add(kvpAux);
                }

                return lstResultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

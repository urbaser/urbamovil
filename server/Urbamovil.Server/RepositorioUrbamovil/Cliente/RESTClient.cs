﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioUrbamovil.Cliente
{
    /// <summary>
    /// Representa la parte del cliente que hará las solicitudes al servicio REST
    /// Referencia: http://www.areatic.net/2013/04/crear-un-cliente-rest-con-visual-studio.html
    /// </summary>
    /// <typeparam name="T">Tipo de datos que se espera como respuesta</typeparam>
    public class RESTClient<T>
    {
        #region Propiedades

        private Uri _BaseURL;
        private string _MediaTypeHeader = "application/json";
        private HttpClient _Client;
        private string _ServiceName = string.Empty;

        #endregion

        #region Constructora

        public RESTClient(string BaseURL, string pServiceName)
        {
            _BaseURL = new Uri(BaseURL);
            _ServiceName = pServiceName;

            _Client = new HttpClient();
            _Client.BaseAddress = _BaseURL;
            _Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_MediaTypeHeader));
        }

        #endregion

        #region GET

        public List<T> GetElements()
        {
            try
            {
                List<T> result;
                HttpResponseMessage response = _Client.GetAsync(string.Format("{0}", _ServiceName)).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<T>>().Result;
                }
                else
                {
                    throw new Exception(string.Format("{0}: No se ha podido conectar al servicio.", response.StatusCode), new Exception(response.ReasonPhrase)); ;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<T> GetElements(Guid id)
        {
            try
            {
                List<T> result;
                HttpResponseMessage response = _Client.GetAsync(string.Format("{0}?id={1}", _ServiceName, id)).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<T>>().Result;
                }
                else
                {
                    throw new Exception(string.Format("{0}: No se ha podido conectar al servicio.", response.StatusCode), new Exception(response.ReasonPhrase)); ;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<T> GetElements(Guid id, Guid value)
        {
            try
            {
                List<T> result;
                HttpResponseMessage response = _Client.GetAsync(string.Format("{0}?id={1}&value={2}", _ServiceName, id, value)).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<T>>().Result;
                }
                else
                {
                    throw new Exception(string.Format("{0}: No se ha podido conectar al servicio.", response.StatusCode), new Exception(response.ReasonPhrase)); ;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T Get(string dominio, string login, string pass)
        {
            try
            {
                T result;
                string api = string.Format("{0}?dominio={1}&login={2}&pass={3}", _ServiceName, dominio, login, pass);
                HttpResponseMessage response = _Client.GetAsync(api).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<T>().Result;
                }
                else
                {
                    throw new Exception(string.Format("{0}: No se ha podido conectar al servicio.", response.StatusCode), new Exception(response.ReasonPhrase)); ;
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T Get(Guid id)
        {
            try
            {
                T result;
                string api = string.Format("{0}?id={1}", _ServiceName, id);
                HttpResponseMessage response = _Client.GetAsync(api).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<T>().Result;
                }
                else
                {
                    throw new Exception(string.Format("{0}: No se ha podido conectar al servicio.", response.StatusCode), new Exception(response.ReasonPhrase)); ;
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T Get(Guid id, Guid value)
        {
            try
            {
                T result;
                string api = string.Format("{0}?id={1}&value={2}", _ServiceName, id, value);
                HttpResponseMessage response = _Client.GetAsync(api).Result;

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<T>().Result;
                }
                else
                {
                    throw new Exception(string.Format("{0}: No se ha podido conectar al servicio.", response.StatusCode), new Exception(response.ReasonPhrase)); ;
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region POST

        public void Post(T pValue, Guid id)
        {
            try
            {
                List<KeyValuePair<string, string>> oListTest = new AreaTICJavaScriptSerializer().GetKeyValuePair(pValue);
                FormUrlEncodedContent content = new FormUrlEncodedContent(oListTest);

                HttpResponseMessage response = _Client.PostAsync(string.Format("{0}?id={1}", _ServiceName, id), content).Result;

                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(string.Format("{0}: No se ha podido conectar al servicio.", response.StatusCode), new Exception(response.ReasonPhrase)); ;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region PUT

        public void Put(T pValue, Guid id)
        {
            try
            {
                List<KeyValuePair<string, string>> oListTest = new AreaTICJavaScriptSerializer().GetKeyValuePair(pValue);
                FormUrlEncodedContent content = new FormUrlEncodedContent(oListTest);

                HttpResponseMessage response = _Client.PutAsync(string.Format("{0}?id={1}", _ServiceName, id), content).Result;

                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(string.Format("{0}: No se ha podido conectar al servicio.", response.StatusCode), new Exception(response.ReasonPhrase));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region DELETE

        public void Delete(Guid value, Guid id)
        {
            HttpResponseMessage response = _Client.DeleteAsync(string.Format("{0}?value={1}&id={2}", _ServiceName, value, id)).Result;
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(string.Format("{0}: No se ha podido conectar al servicio.", response.StatusCode), new Exception(response.ReasonPhrase)); ;
            }
        }

        #endregion
    }
}

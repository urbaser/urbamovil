﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using NegocioComunUrbamovil;

namespace WSUrbamovil
{
    // Nota: para obtener instrucciones sobre cómo habilitar el modo clásico de IIS6 o IIS7, 
    // visite http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            LogManager.AddTextLog("INICIO DEL SERVICIO REST DE URBAMOVIL.");
        }

        void Application_End(object sender, EventArgs e)
        {
            LogManager.AddExceptionLog(new Exception("FINALIZANDO EL SERVICIO DE URBAMOVIL"));
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
        }

        void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        void Application_EndRequest(object sender, EventArgs e)
        {

        }
    }
}
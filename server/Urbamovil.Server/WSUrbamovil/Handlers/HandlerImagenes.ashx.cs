﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NegocioUrbamovil.Manager;
using NegocioUrbamovil.Modelo;
using System.Configuration;
using System.Drawing;

namespace WSUrbamovil.Handlers
{
    /// <summary>
    /// Descripción breve de HandlerImagenes
    /// </summary>
    public class HandlerImagenes : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string id = string.Empty;
            string v = string.Empty;
            
            if (context.Request.QueryString["id"] != null && context.Request.QueryString["v"] != null)
            {
                id = context.Request.QueryString["id"].ToString();
                
                var managerUsuario = new UsuarioManager();

                AppSettingsReader appReader = new AppSettingsReader();

                int horas = int.Parse(appReader.GetValue("KEY_HORASCADUCIDADTICKET", typeof(string)).ToString());

                Guid IdTicket = new Guid(id);

                Usuarios usuario = managerUsuario.ObtenerUsuarioPorTicket(IdTicket);

                var managerTicketUsuario = new TicketUsuarioManager();

                if (usuario != null)
                {
                    managerTicketUsuario.AumentarCaducidadTicket(IdTicket, horas);
                }
                else
                {
                    throw new Exception("El usuario no es correcto.");
                }

                TicketUsuario ticket = managerTicketUsuario.Obtener(IdTicket);

                if (!ticket.IdContrato.HasValue)
                {
                    throw new Exception("El contrato no esta asociado a ningun usuario existente.");
                }

                var managerContratos = new ContratosManager();

                Contratos contrato = managerContratos.ObtenerContrato(ticket.IdContrato.Value);

                if (contrato == null)
                {
                    throw new Exception("El contrato asociado al usuario es incorrecto");
                }

                v = context.Request.QueryString["v"].ToString();
            }
            else if (context.Request.QueryString["Inc"] != null)
            {
                v = context.Request.QueryString["Inc"].ToString();
            }
            else
            {
                throw new ArgumentException("Parámetros incorrectos");
            }

            Guid IdIncidencia = new Guid(v);

            IncidenciasManager manager = new IncidenciasManager();

            Incidencias incidencia = manager.ObtenerIncidencia(IdIncidencia);

            if (incidencia != null)
            {
                if (incidencia.Imagen != null)
                {
                    byte[] imageBuffer = incidencia.Imagen;

                    context.Response.ContentType = "image/jpg";
                    context.Response.OutputStream.Write(imageBuffer, 0, imageBuffer.Length);
                }
            }
            else
            {
                throw new Exception("La incidencia no existe.");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
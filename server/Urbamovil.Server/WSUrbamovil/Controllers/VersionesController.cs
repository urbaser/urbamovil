﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Manager;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class VersionesController : BaseController
    {
        // GET api/versiones?c=codigo&s=sistema&i=idioma
        [HttpGet]
        public HttpResponseMessage GetVersion(string c, string s, int i)
        {
            try
            {
                Versiones v = managerVersiones.ObtenerVersion(c, s);

                if (v == null) throw HttpError(HttpStatusCode.NotFound, "La versión no existe");

                Mensajes mensaje = managerMensajes.ObtenerMensaje(i, v.IdEstadoVersion);

                clsVersiones version = new clsVersiones();
                version.IdVersion = v.IdVersion;
                version.Codigo = v.Codigo;
                version.Sistema = v.Sistema;
                version.IdEstadoVersion = v.IdEstadoVersion;
                version.FechaInicio = v.FechaInicio;
                version.FechaFin = v.FechaFin;
                version.Mensaje = mensaje.Texto;

                return HttpOK<clsVersiones>(version);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        [HttpGet]
        public HttpResponseMessage GetVersion(string c, string s, string l)
        {
            try
            {
                Versiones v = managerVersiones.ObtenerVersion(c, s);

                if (v == null) throw HttpError(HttpStatusCode.NotFound, "La versión no existe");

                int i = 0;

                switch(l)
                {
                    case ISO_Idiomas.ES: i = (int)Idioma.ES;
                        break;
                    case ISO_Idiomas.EN: i = (int)Idioma.EN;
                        break;
                    case ISO_Idiomas.FR: i = (int)Idioma.FR;
                        break;
                    default:
                        throw HttpError(HttpStatusCode.BadRequest, "El parámetro idioma es incorrecto. Verifique que el formato sea ISO.");
                }

                Mensajes mensaje = managerMensajes.ObtenerMensaje(i, v.IdEstadoVersion);

                clsVersiones version = new clsVersiones();
                version.IdVersion = v.IdVersion;
                version.Codigo = v.Codigo;
                version.Sistema = v.Sistema;
                version.IdEstadoVersion = v.IdEstadoVersion;
                version.FechaInicio = v.FechaInicio;
                version.FechaFin = v.FechaFin;
                version.Mensaje = mensaje.Texto;

                return HttpOK<clsVersiones>(version);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

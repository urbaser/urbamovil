﻿using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class IncidenciaHistoricoEstadoController : BaseController
    {
        // GET api/incidenciahistoricoestado
        [HttpGet]
        public List <clsIncidenciaHistoricoEstado> Get(Guid id, Guid idIncidencia)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                List<Incidencias_HistoricoEstado> list = managerIncHistEstado.Obtener(idIncidencia);
                List<clsIncidenciaHistoricoEstado> result = null;

                if(list != null && list.Count > 0)
                {
                    result = new List<clsIncidenciaHistoricoEstado>();
                    
                    foreach(var item in list)
                    {
                        clsIncidenciaHistoricoEstado h = new clsIncidenciaHistoricoEstado();
                        h.IdIncidencia = item.IdIncidencia;
                        h.IdEstado = item.IdEstado;
                        h.Fecha = item.Fecha;
                        h.IdInstalacion = item.IdInstalacion;
                        h.Observaciones = item.Observaciones;
                        h.Usuario = item.Usuario;

                        result.Add(h);
                    }
                }

                return result;
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                throw hex;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw HttpError(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
        
        // PUT api/incidenciahistoricoestado/5
        [HttpPut]
        public void Put(Guid id, clsIncidenciaHistoricoEstado historico)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Incidencias_HistoricoEstado entidad = new Incidencias_HistoricoEstado();
                entidad.IdIncidencia = Guid.NewGuid();
                entidad.IdEstado = historico.IdEstado;
                entidad.Fecha = DateTime.Now;
                entidad.IdInstalacion = historico.IdInstalacion;
                entidad.Observaciones = historico.Observaciones;
                entidad.Usuario = historico.Usuario;

                managerIncHistEstado.Guardar(entidad);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                throw hex;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw HttpError(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

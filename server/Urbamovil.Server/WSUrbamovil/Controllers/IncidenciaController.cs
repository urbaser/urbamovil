﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class IncidenciaController : BaseController
    {
        // GET api/incidencia?id=ticket
        [HttpGet]
        public HttpResponseMessage Get(Guid Id, clsFiltroUsuario filtro)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(Id);

                Contratos contrato = ComprobarValidezContrato(Id);

                TiposPerfilUsuario perfil = managerPerfil.ObtenerPerfil(TipoPerfiles.CIUDADANO);
                bool esCiudadano = perfil.IdPerfil == usuario.IdPerfil;

                List<Incidencias> list = null;

                if (filtro != null)
                {
                    FiltroIncidencia filtroIncidencia = filtro.toFiltroIncidencia();

                    if (esCiudadano)
                    {
                        filtroIncidencia.IdContrato = null;
                        filtroIncidencia.IdUsuarioAlta = usuario.IdUsuario;
                    }
                    else
                    {
                        filtroIncidencia.IdContrato = contrato.IdContrato;
                        filtroIncidencia.IdUsuarioAlta = null;
                    }

                    list = managerIncidencias.ObtenerIncidencias(filtroIncidencia);
                }
                else
                {
                    if (esCiudadano)
                    {
                        list = managerIncidencias.ObtenerIncidenciasPorUsuario(usuario.IdUsuario);
                    }
                    else
                    {
                        list = managerIncidencias.ObtenerIncidenciasPorContrato(contrato.IdContrato);
                    }
                    
                }

                List<clsIncidencias> result = new List<clsIncidencias>();

                if (list != null && list.Count > 0)
                {
                    foreach (Incidencias item in list)
                    {
                        clsIncidencias incidencia = new clsIncidencias();
                        incidencia.IdIncidencia = item.IdIncidencia;
                        incidencia.IdGrupo = item.IdGrupo;
                        incidencia.IdTipo = item.IdTipo;
                        incidencia.IdDescripcion = item.IdDescripcion;
                        incidencia.IdPrioridad = item.IdPrioridad;
                        incidencia.IdEstado = item.IdEstado;
                        incidencia.IdProvincia = item.IdProvincia;
                        incidencia.IdMunicipio = item.IdMunicipio;
                        incidencia.Imagen = item.Imagen != null ? ToAbsolutePath(string.Format(pathHandler, Id, item.IdIncidencia.ToString())) : string.Empty;
                        incidencia.ImagenCierre = item.ImagenCierre != null ? ToAbsolutePath(string.Format(pathHandlerCierre, Id, item.IdIncidencia.ToString())) : string.Empty;
                        incidencia.CargandoImagen = item.CargandoImagen;
                        incidencia.CargandoImagenCierre = item.CargandoImagenCierre;
                        incidencia.Observaciones = item.Observaciones;
                        incidencia.Sincronizada = item.Sincronizada;
                        incidencia.IdOperacionDestino = item.IdOperacionDestino;
                        incidencia.CodigoIncidencia = item.CodigoIncidencia;
                        incidencia.IdUsuarioAlta = item.IdUsuarioAlta;
                        incidencia.FechaAlta = item.FechaAlta.ToUTC();
                        //incidencia.FechaUltimaModificacion = item.FechaUltimaModificacion;

                        var localizaciones = managerLocalizacionesIncidencias.ObtenerPorIncidencia(item.IdIncidencia);
                        foreach (Localizaciones_Incidencia l in localizaciones)
                        {
                            clsLocalizacionesIncidencias localizacion = new clsLocalizacionesIncidencias();
                            localizacion.IdLocalizacion = l.IdLocalizacion;
                            localizacion.IdIncidencia = l.IdIncidencia;
                            localizacion.IdTipoLocalizacion = l.IdTipoLocalizacion;
                            localizacion.Map_Latitud = l.Map_Latitud;
                            localizacion.Map_Longitud = l.Map_Longitud;
                            localizacion.Dir_Calle = l.Dir_Calle;
                            localizacion.Dir_Numero = l.Dir_Numero;
                            localizacion.Inv_IdDistrito = l.Inv_IdDistrito;
                            localizacion.Inv_IdEspacioUrbano = l.Inv_IdEspacioUrbano;
                            localizacion.Inv_IdZona = l.Inv_IdZona;
                            localizacion.Inv_IdRuta = l.Inv_IdRuta;
                            localizacion.Inv_IdArea = l.Inv_IdArea;
                            localizacion.Inv_IdTipoElemento = l.Inv_IdTipoElemento;
                            localizacion.Inv_IdElemento = l.Inv_IdElemento;
                            localizacion.Codigo = l.Codigo;
                            localizacion.IdInstalacion = l.IdInstalacion;
                            localizacion.Descripcion = GetDescripcion(l);

                            incidencia.Localizaciones.Add(localizacion);
                        }

                        result.Add(incidencia);
                    }
                }

                return HttpOK<List<clsIncidencias>>(result);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // GET api/incidencia?id=ticket&f=IdFiltro
        [HttpGet]
        public HttpResponseMessage Get(Guid id, Guid f)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                TiposPerfilUsuario perfil = managerPerfil.ObtenerPerfil(TipoPerfiles.CIUDADANO);
                bool esCiudadano = perfil.IdPerfil == usuario.IdPerfil;

                FiltroUsuario filtro = managerFiltroUsuario.ObtenerFiltro(f);

                if (filtro == null || filtro.IdUsuario != usuario.IdUsuario)
                {
                    throw HttpError(HttpStatusCode.Unauthorized, "Error al intentar recuperar el filtro.");
                }

                clsFiltroUsuario filtroUsuario = new clsFiltroUsuario();
                filtroUsuario.IdFiltroUsuario = filtro.IdFiltroUsuario;
                filtroUsuario.Nombre = filtro.Nombre;
                filtroUsuario.FiltrosCampo = JSONManager.DeserializeJSON<List<FiltroCampo>>(filtro.ObjetoJSON);

                FiltroIncidencia filtroIncidencia = filtroUsuario.toFiltroIncidencia();
                
                if (esCiudadano)
                {
                    filtroIncidencia.IdUsuarioAlta = usuario.IdUsuario;
                    filtroIncidencia.IdContrato = null;
                }
                else
                {
                    filtroIncidencia.IdUsuarioAlta = null;
                    filtroIncidencia.IdContrato = contrato.IdContrato;
                }

                List<Incidencias> list = managerIncidencias.ObtenerIncidencias(filtroIncidencia);
                List<clsIncidencias> result = new List<clsIncidencias>();

                if (list != null && list.Count > 0)
                {
                    foreach (Incidencias item in list)
                    {
                        clsIncidencias incidencia = new clsIncidencias();
                        incidencia.IdIncidencia = item.IdIncidencia;
                        incidencia.IdGrupo = item.IdGrupo;
                        incidencia.IdTipo = item.IdTipo;
                        incidencia.IdDescripcion = item.IdDescripcion;
                        incidencia.IdPrioridad = item.IdPrioridad;
                        incidencia.IdEstado = item.IdEstado;
                        incidencia.IdProvincia = item.IdProvincia;
                        incidencia.IdMunicipio = item.IdMunicipio;
                        incidencia.Imagen = item.Imagen != null ? ToAbsolutePath(string.Format(pathHandler, id, item.IdIncidencia.ToString())) : string.Empty;
                        incidencia.ImagenCierre = item.ImagenCierre != null ? ToAbsolutePath(string.Format(pathHandlerCierre, id, item.IdIncidencia.ToString())) : string.Empty;
                        incidencia.CargandoImagen = item.CargandoImagen;
                        incidencia.CargandoImagenCierre = item.CargandoImagenCierre;
                        incidencia.Observaciones = item.Observaciones;
                        incidencia.Sincronizada = item.Sincronizada;
                        incidencia.IdOperacionDestino = item.IdOperacionDestino;
                        incidencia.CodigoIncidencia = item.CodigoIncidencia;
                        incidencia.IdUsuarioAlta = item.IdUsuarioAlta;
                        incidencia.FechaAlta = item.FechaAlta.ToUTC();
                        //incidencia.FechaUltimaModificacion = item.FechaUltimaModificacion;

                        var localizaciones = managerLocalizacionesIncidencias.ObtenerPorIncidencia(item.IdIncidencia);
                        foreach (Localizaciones_Incidencia l in localizaciones)
                        {
                            clsLocalizacionesIncidencias localizacion = new clsLocalizacionesIncidencias();
                            localizacion.IdLocalizacion = l.IdLocalizacion;
                            localizacion.IdIncidencia = l.IdIncidencia;
                            localizacion.IdTipoLocalizacion = l.IdTipoLocalizacion;
                            localizacion.Map_Latitud = l.Map_Latitud;
                            localizacion.Map_Longitud = l.Map_Longitud;
                            localizacion.Dir_Calle = l.Dir_Calle;
                            localizacion.Dir_Numero = l.Dir_Numero;
                            localizacion.Inv_IdDistrito = l.Inv_IdDistrito;
                            localizacion.Inv_IdEspacioUrbano = l.Inv_IdEspacioUrbano;
                            localizacion.Inv_IdZona = l.Inv_IdZona;
                            localizacion.Inv_IdRuta = l.Inv_IdRuta;
                            localizacion.Inv_IdArea = l.Inv_IdArea;
                            localizacion.Inv_IdTipoElemento = l.Inv_IdTipoElemento;
                            localizacion.Inv_IdElemento = l.Inv_IdElemento;
                            localizacion.Codigo = l.Codigo;
                            localizacion.IdInstalacion = l.IdInstalacion;
                            localizacion.Descripcion = GetDescripcion(l);

                            incidencia.Localizaciones.Add(localizacion);
                        }

                        result.Add(incidencia);
                    }
                }

                return HttpOK<List<clsIncidencias>>(result);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // POST api/incidencia?id=ticket
        [HttpPost]
        public HttpResponseMessage Post(Guid id, clsIncidencias incidencia)
        {
            string json = JSONManager.ConvertObjectToJSON<clsIncidencias>(incidencia);
            LogManager.AddTextLog("POST INCIDENCIA  " + json);

            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                incidencia.Sincronizada = false;
                incidencia.IdOperacionDestino = (int)Operacion.UPDATE;

                int? estadoInicial = managerIncidencias.ObtenerIncidencia(incidencia.IdIncidencia).IdEstado;

                Incidencias updateIncidencia = new Incidencias();
                updateIncidencia.IdIncidencia = incidencia.IdIncidencia;
                updateIncidencia.IdGrupo = incidencia.IdGrupo;
                updateIncidencia.IdTipo = incidencia.IdTipo == 0 ? null : incidencia.IdTipo;
                updateIncidencia.IdDescripcion = incidencia.IdDescripcion == 0 ? null : incidencia.IdDescripcion;
                updateIncidencia.IdPrioridad = incidencia.IdPrioridad;
                updateIncidencia.IdEstado = incidencia.IdEstado == 0 ? null : incidencia.IdEstado;
                updateIncidencia.IdProvincia = incidencia.IdProvincia;
                updateIncidencia.IdMunicipio = incidencia.IdMunicipio;
                if (incidencia.CargandoImagen) updateIncidencia.Imagen = null;
                updateIncidencia.CargandoImagen = incidencia.CargandoImagen;
                if (incidencia.CargandoImagenCierre) updateIncidencia.ImagenCierre = null;
                updateIncidencia.CargandoImagenCierre = incidencia.CargandoImagenCierre;
                updateIncidencia.Observaciones = incidencia.Observaciones;
                updateIncidencia.Sincronizada = incidencia.Sincronizada;
                updateIncidencia.IdOperacionDestino = incidencia.IdOperacionDestino;
                updateIncidencia.CodigoIncidencia = incidencia.CodigoIncidencia;
                updateIncidencia.IdUsuarioAlta = incidencia.IdUsuarioAlta;
                updateIncidencia.IdInstalacion = contrato.IdInstalacion;
                updateIncidencia.IdContrato = contrato.IdContrato;
                updateIncidencia.FechaAlta = incidencia.FechaAlta;
                updateIncidencia.FechaUltimaModificacion = DateTime.Now;

                managerIncidencias.ActualizarIncidencia(updateIncidencia);

                if (incidencia.Localizaciones != null)
                {
                    List<Localizaciones_Incidencia> entidades = new List<Localizaciones_Incidencia>();

                    foreach (clsLocalizacionesIncidencias l in incidencia.Localizaciones)
                    {
                        Localizaciones_Incidencia localizacion = new Localizaciones_Incidencia();
                        localizacion.IdLocalizacion = Guid.NewGuid();
                        localizacion.IdIncidencia = incidencia.IdIncidencia;
                        localizacion.IdTipoLocalizacion = l.IdTipoLocalizacion;
                        localizacion.Map_Latitud = l.Map_Latitud;
                        localizacion.Map_Longitud = l.Map_Longitud;
                        localizacion.Dir_Calle = l.Dir_Calle;
                        localizacion.Dir_Numero = l.Dir_Numero;
                        localizacion.Inv_IdDistrito = l.Inv_IdDistrito;
                        localizacion.Inv_IdEspacioUrbano = l.Inv_IdEspacioUrbano;
                        localizacion.Inv_IdZona = (l.Inv_IdZona == 0) ? null : l.Inv_IdZona;
                        localizacion.Inv_IdRuta = (l.Inv_IdRuta == 0) ? null : l.Inv_IdRuta;
                        localizacion.Inv_IdArea = l.Inv_IdArea;
                        localizacion.Inv_IdTipoElemento = l.Inv_IdTipoElemento;
                        localizacion.Inv_IdElemento = l.Inv_IdElemento;
                        localizacion.Codigo = l.Codigo;
                        localizacion.IdInstalacion = contrato.IdInstalacion;

                        entidades.Add(localizacion);
                    }

                    managerLocalizacionesIncidencias.Guardar(incidencia.IdIncidencia, entidades);
                }

                Incidencias_HistoricoEstado h = new Incidencias_HistoricoEstado();
                h.IdIncidencia = updateIncidencia.IdIncidencia;
                h.IdEstado = updateIncidencia.IdEstado.Value;
                h.Fecha = updateIncidencia.FechaUltimaModificacion;
                h.IdInstalacion = updateIncidencia.IdInstalacion;
                h.Observaciones = "Actualizada";
                h.Usuario = string.Format("{0} {1} {2}", usuario.Nombre, usuario.Apellido1, usuario.Apellido2);

                managerIncHistEstado.Guardar(h);

                return HttpOK<clsIncidencias>(incidencia);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // PUT api/incidencia?id=ticket
        [HttpPut]
        public HttpResponseMessage Put(Guid id, clsIncidencias incidencia)
        {
            string json = JSONManager.ConvertObjectToJSON<clsIncidencias>(incidencia);
            LogManager.AddTextLog("PUT INCIDENCIA  " + json);

            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                incidencia.Sincronizada = false;
                incidencia.IdOperacionDestino = (int)Operacion.INSERT;

                Incidencias nuevaIncidencia = new Incidencias();
                nuevaIncidencia.IdIncidencia = Guid.NewGuid();
                nuevaIncidencia.IdGrupo = incidencia.IdGrupo;
                nuevaIncidencia.IdTipo = incidencia.IdTipo == 0 ? null : incidencia.IdTipo;
                nuevaIncidencia.IdDescripcion = incidencia.IdDescripcion == 0 ? null : incidencia.IdDescripcion;
                nuevaIncidencia.IdPrioridad = incidencia.IdPrioridad;
                nuevaIncidencia.IdEstado = incidencia.IdEstado == 0 ? null : incidencia.IdEstado;
                nuevaIncidencia.IdProvincia = incidencia.IdProvincia;
                nuevaIncidencia.IdMunicipio = incidencia.IdMunicipio;
                nuevaIncidencia.CargandoImagen = incidencia.CargandoImagen;
                nuevaIncidencia.CargandoImagenCierre = incidencia.CargandoImagenCierre;
                nuevaIncidencia.Observaciones = incidencia.Observaciones;
                nuevaIncidencia.Sincronizada = incidencia.Sincronizada;
                nuevaIncidencia.IdOperacionDestino = incidencia.IdOperacionDestino;
                nuevaIncidencia.CodigoIncidencia = incidencia.CodigoIncidencia;
                nuevaIncidencia.IdUsuarioAlta = usuario.IdUsuario;
                nuevaIncidencia.IdInstalacion = contrato.IdInstalacion;
                nuevaIncidencia.IdContrato = contrato.IdContrato;
                nuevaIncidencia.FechaAlta = incidencia.FechaAlta;
                nuevaIncidencia.FechaUltimaModificacion = DateTime.Now;

                managerIncidencias.GuardarIncidencia(nuevaIncidencia);

                if (incidencia.Localizaciones != null)
                {
                    foreach (clsLocalizacionesIncidencias l in incidencia.Localizaciones)
                    {
                        Localizaciones_Incidencia localizacion = new Localizaciones_Incidencia();
                        localizacion.IdLocalizacion = Guid.NewGuid();
                        localizacion.IdIncidencia = nuevaIncidencia.IdIncidencia;
                        localizacion.IdTipoLocalizacion = l.IdTipoLocalizacion;
                        localizacion.Map_Latitud = l.Map_Latitud;
                        localizacion.Map_Longitud = l.Map_Longitud;
                        localizacion.Dir_Calle = l.Dir_Calle;
                        localizacion.Dir_Numero = l.Dir_Numero;
                        localizacion.Inv_IdDistrito = l.Inv_IdDistrito;
                        localizacion.Inv_IdEspacioUrbano = l.Inv_IdEspacioUrbano;
                        localizacion.Inv_IdZona = (l.Inv_IdZona == 0) ? null : l.Inv_IdZona;
                        localizacion.Inv_IdRuta = (l.Inv_IdRuta == 0) ? null : l.Inv_IdRuta;
                        localizacion.Inv_IdArea = l.Inv_IdArea;
                        localizacion.Inv_IdTipoElemento = l.Inv_IdTipoElemento;
                        localizacion.Inv_IdElemento = l.Inv_IdElemento;
                        localizacion.Codigo = l.Codigo;
                        localizacion.IdInstalacion = contrato.IdInstalacion;

                        managerLocalizacionesIncidencias.Guardar(localizacion);
                    }
                }

                Incidencias_HistoricoEstado h = new Incidencias_HistoricoEstado();
                h.IdIncidencia = nuevaIncidencia.IdIncidencia;
                h.IdEstado = nuevaIncidencia.IdEstado.Value;
                h.Fecha = nuevaIncidencia.FechaUltimaModificacion;
                h.IdInstalacion = nuevaIncidencia.IdInstalacion;
                h.Observaciones = "Pendiente";
                h.Usuario = string.Format("{0} {1} {2}", usuario.Nombre, usuario.Apellido1, usuario.Apellido2);

                managerIncHistEstado.Guardar(h);

                incidencia.IdIncidencia = nuevaIncidencia.IdIncidencia;
                incidencia.IdUsuarioAlta = usuario.IdUsuario;

                return HttpOK<clsIncidencias>(incidencia);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // DELETE api/incidencia?value=idIncidencia&id=ticket
        [HttpDelete]
        public HttpResponseMessage Delete(Guid value, Guid id)
        {
            try
            {
                ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                Incidencias incidencia = managerIncidencias.ObtenerIncidencia(value);
                incidencia.Sincronizada = false;
                incidencia.IdOperacionDestino = (int)Operacion.DELETE;

                managerIncidencias.ActualizarIncidencia(incidencia);

                return HttpOK("Eliminación realizada con éxito");
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                throw hex;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw HttpError(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        [NonAction]
        private string GetDescripcion(Localizaciones_Incidencia incidencia)
        {
            TipoLocalizacion tipo = (TipoLocalizacion)incidencia.IdTipoLocalizacion;
            string Descripcion = string.Empty;

            switch (tipo)
            {
                case TipoLocalizacion.MAPA:
                    if (incidencia.Map_Latitud.HasValue && incidencia.Map_Longitud.HasValue)
                    {
                        Descripcion = string.Format("({0} , {1})", incidencia.Map_Latitud.Value, incidencia.Map_Longitud.Value);
                    }
                    break;
                case TipoLocalizacion.DIRECCION:
                    if (!string.IsNullOrWhiteSpace(incidencia.Dir_Calle) && incidencia.Dir_Numero.HasValue)
                    {
                        Descripcion = string.Format("{0}, {1}", incidencia.Dir_Calle, incidencia.Dir_Numero.Value);
                    }
                    break;
                case TipoLocalizacion.INVENTARIO:
                    if (incidencia.Inv_IdElemento.HasValue)
                    {
                        Descripcion = incidencia.Elementos.NombreElemento;
                    }
                    else if (incidencia.Inv_IdTipoElemento.HasValue)
                    {
                        Descripcion = incidencia.Localizaciones_TiposElemento.Tipo;
                    }
                    else if (incidencia.Inv_IdRuta.HasValue)
                    {
                        Descripcion = incidencia.Localizaciones_Rutas.Ruta;
                    }
                    else if (incidencia.Inv_IdZona.HasValue)
                    {
                        Descripcion = incidencia.Localizaciones_Zonas.Zona;
                    }
                    else if (incidencia.Inv_IdEspacioUrbano.HasValue)
                    {
                        Descripcion = incidencia.Localizaciones_EspaciosUrbanos.EspacioUrbano;
                    }
                    else if (incidencia.Inv_IdDistrito.HasValue)
                    {
                        Descripcion = incidencia.Localizaciones_Distritos.Distrito;
                    }
                    break;
                case TipoLocalizacion.TAG:
                    Descripcion = incidencia.Codigo;
                    break;
                default:
                    Descripcion = string.Empty;
                    break;
            }

            return Descripcion;
        }
    }
}

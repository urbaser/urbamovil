﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Manager;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;
using WSUrbamovil.Encriptacion;

namespace WSUrbamovil.Controllers
{
    public class UsuarioController : BaseController
    {
        // GET api/usuario?login=Login&pass=Pass
        [HttpGet]
        public HttpResponseMessage Get(string login, string pass)
        {
            try
            {
                clsUsuarios usuario = null;

                string passEncripted = Encriptado.EncriptarMD5(pass);
                var user = managerUsuario.ComprobarUsuario(login, passEncripted);

                if (user != null)
                {
                    TicketUsuario ticket = managerTicketUsuario.GenerarTicketPorUsuario(user.IdUsuario, horas);

                    usuario = new clsUsuarios();
                    usuario.IdUsuario = user.IdUsuario;
                    usuario.Nombre = user.Nombre;
                    usuario.Apellido1 = user.Apellido1;
                    usuario.Apellido2 = user.Apellido2;
                    usuario.Direccion = user.Direccion;
                    usuario.Empresa = user.Empresa;
                    usuario.Email = user.Email;
                    usuario.Telefono = user.Telefono;
                    usuario.TelefonoMovil = user.TelefonoMovil;
                    usuario.Login = user.Login;
                    usuario.IdPerfil = user.IdPerfil;
                    usuario.NombrePerfil = user.IdPerfil.HasValue ? user.TiposPerfilUsuario.Nombre : string.Empty;
                    usuario.Permisos = new clsPermisos(){
                        NuevaIncidencia = user.IdPerfil.HasValue ? user.TiposPerfilUsuario.NuevaIncidencia : false,
                        ListadoIncidencias = user.IdPerfil.HasValue ? user.TiposPerfilUsuario.ListadoIncidencias : false,
                        ModificarIncidencia = user.IdPerfil.HasValue ? user.TiposPerfilUsuario.ModificarIncidencia : false,
                        HistoricoIncidencias = user.IdPerfil.HasValue ? user.TiposPerfilUsuario.HistoricoIncidencias : false,
                        CambioEstadoIncidencia = user.IdPerfil.HasValue ? user.TiposPerfilUsuario.CambioEstadoIncidencia : false,
                        ConsultarIncidencia = user.IdPerfil.HasValue ? user.TiposPerfilUsuario.ConsultarIncidencia : false
                    };
                    usuario.IPDireccion = user.IPDireccion;
                    usuario.PreferenciasJSON = user.PreferenciasJSON;
                    usuario.FechaRegistro = user.FechaRegistro;
                    usuario.TicketSesion = ticket.Ticket;
                    usuario.IdIdioma = user.IdIdioma;
                    usuario.CodigoIdioma = user.Idiomas.Codigo;
                    usuario.Activo = user.Activo;
                    usuario.FechaValidezCC = user.FechaValidezCC;
                    usuario.ContraClave = user.ContraClave;

                    List<FiltroUsuario> filtros = managerFiltroUsuario.ObtenerFiltrosPorUsuario(user.IdUsuario);
                    if (filtros != null && filtros.Count > 0)
                    {
                        usuario.Filtros = new List<clsFiltroUsuario>();
                        foreach (var f in filtros)
                        {
                            clsFiltroUsuario filtro = new clsFiltroUsuario();
                            filtro.IdFiltroUsuario = f.IdFiltroUsuario;
                            filtro.Nombre = f.Nombre;
                            filtro.FiltrosCampo = JSONManager.DeserializeJSON<List<FiltroCampo>>(f.ObjetoJSON);

                            usuario.Filtros.Add(filtro);
                        }
                    }

                    // comentado 20150512 no es necesaria la tabla de idiomas por el momento
                    //List<Idiomas> idiomas = managerIdiomas.ObtenerIdiomas();
                    //if (idiomas != null && filtros.Count > 0)
                    //{
                    //    usuario.Idiomas = new List<clsIdiomas>();
                    //    foreach (var i in idiomas)
                    //    {
                    //        clsIdiomas idioma = new clsIdiomas();
                    //        idioma.IdIdioma = i.IdIdioma;
                    //        idioma.Idioma = i.Idioma;
                    //        idioma.Codigo = i.Codigo;
                    //        idioma.PorDefecto = i.IdIdiomaDefecto;

                    //        usuario.Idiomas.Add(idioma);
                    //    }
                    //}
                }
                else
                {
                    return HttpErrorMessage(HttpStatusCode.Unauthorized, "El usuario no existe en el sistema.");
                }
                
                return HttpOK<clsUsuarios>(usuario);
            }
            catch(HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

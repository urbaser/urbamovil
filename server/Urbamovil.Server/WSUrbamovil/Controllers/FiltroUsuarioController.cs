﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class FiltroUsuarioController : BaseController
    {
        // GET api/filtrousuario?id=ticket
        [HttpGet]
        public HttpResponseMessage Get(Guid id)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                List<FiltroUsuario> list = managerFiltroUsuario.ObtenerFiltrosPorUsuario(usuario.IdUsuario);

                List<clsFiltroUsuario> result = new List<clsFiltroUsuario>();
                foreach (var item in list)
                {
                    clsFiltroUsuario f = new clsFiltroUsuario();
                    f.IdFiltroUsuario = item.IdFiltroUsuario;
                    f.Nombre = item.Nombre;
                    f.FiltrosCampo = JSONManager.DeserializeJSON<List<FiltroCampo>>(item.ObjetoJSON);

                    result.Add(f);
                }

                return HttpOK<List<clsFiltroUsuario>>(result);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // POST api/filtrousuario?id=ticket
        [HttpPost]
        public HttpResponseMessage Post(Guid id, clsFiltroUsuario filtro)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                FiltroUsuario updateFiltro = new FiltroUsuario();
                updateFiltro.IdFiltroUsuario = filtro.IdFiltroUsuario;
                updateFiltro.IdUsuario = usuario.IdUsuario;
                updateFiltro.Nombre = filtro.Nombre;
                updateFiltro.ObjetoJSON = JSONManager.ConvertObjectToJSON<List<FiltroCampo>>(filtro.FiltrosCampo);

                managerFiltroUsuario.ActualizarFiltro(updateFiltro);

                return HttpOK<clsFiltroUsuario>(filtro);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // PUT api/filtrousuario?id=ticket
        [HttpPut]
        public HttpResponseMessage Put(Guid id, clsFiltroUsuario filtro)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                FiltroUsuario nuevoFiltro = new FiltroUsuario();
                nuevoFiltro.IdFiltroUsuario = Guid.NewGuid();
                nuevoFiltro.IdUsuario = usuario.IdUsuario;
                nuevoFiltro.Nombre = filtro.Nombre;
                nuevoFiltro.ObjetoJSON = JSONManager.ConvertObjectToJSON<List<FiltroCampo>>(filtro.FiltrosCampo);

                managerFiltroUsuario.GuardarFiltro(nuevoFiltro);

                filtro.IdFiltroUsuario = nuevoFiltro.IdFiltroUsuario;

                return HttpOK<clsFiltroUsuario>(filtro);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // DELETE api/filtrousuario?value=idFiltro&id=ticket
        [HttpDelete]
        public HttpResponseMessage Delete(Guid value, Guid id)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                managerFiltroUsuario.EliminarFiltro(value);

                return HttpOK("Eliminación realizada correctamente");
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                throw hex;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw HttpError(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

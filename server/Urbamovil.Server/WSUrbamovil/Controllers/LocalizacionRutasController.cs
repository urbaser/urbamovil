﻿using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class LocalizacionRutasController : BaseController
    {
        // GET api/localizacionrutas?id=ticket&v=IdEspacioUrbano
        [HttpGet]
        public HttpResponseMessage Get(Guid id, int v)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                List<Localizaciones_Rutas> LocalizacionRutas = managerMaestras.ObtenerRutas(contrato.IdInstalacion, v);
                List<clsRutas> result = new List<clsRutas>();

                foreach (var item in LocalizacionRutas)
                {
                    clsRutas ruta = new clsRutas();
                    ruta.IdRuta = item.IdRuta;
                    ruta.Ruta = item.Ruta;
                    ruta.IdEspacioUrbano = item.IdEspacioUrbano;
                    ruta.IdInstalacion = item.IdInstalacion;

                    result.Add(ruta);
                }

                return HttpOK<List<clsRutas>>(result);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

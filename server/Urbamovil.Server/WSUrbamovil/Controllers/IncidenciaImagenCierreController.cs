﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Configuration;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class IncidenciaImagenCierreController : BaseController
    {
        [HttpPost]
        public HttpResponseMessage Post(Guid id, Guid value)
        {
            try
            {
                LogManager.AddTextLog("POST ACTUALIZACION DE IMAGEN DE CIERRE DE LA INCIDENCIA " + value.ToString());

                AppSettingsReader appReader = new AppSettingsReader();

                int iW = (int)appReader.GetValue("KEY_IMG_MAXWIDTH", typeof(int));
                int iH = (int)appReader.GetValue("KEY_IMG_MAXHEIGHT", typeof(int));

                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                Incidencias incidencia = managerIncidencias.ObtenerIncidencia(value);

                if (incidencia == null)
                {
                    return HttpErrorMessage(HttpStatusCode.NotFound, "La incidencia no existe.");
                }

                var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;

                if (file != null && file.ContentLength > 0)
                {
                    byte[] data = new byte[file.ContentLength];

                    file.InputStream.Read(data, 0, file.ContentLength);

                    incidencia.ImagenCierre = ImagenUtil.ImageResize(data, iW, iH);
                    incidencia.CargandoImagenCierre = false;

                    managerIncidencias.ActualizarIncidencia(incidencia);
                }
                else
                {
                    return HttpErrorMessage(HttpStatusCode.NoContent, "Contenido no valido");
                }

                string url = ToAbsolutePath(string.Format(pathHandlerCierre, id, value));

                return HttpOK(url);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Manager;
using NegocioUrbamovil.Modelo;
using System.Configuration;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class ContratosController : BaseController
    {
        // GET api/contratos?id=ticket
        [HttpGet]
        public HttpResponseMessage GetContratos(Guid id)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                TiposPerfilUsuario perfil = managerPerfil.ObtenerPerfil(TipoPerfiles.CIUDADANO);

                if (usuario.IdPerfil == perfil.IdPerfil)
                {
                    managerUsuariosContratos.EliminarPorUsuario(usuario.IdUsuario);

                    var list = managerContratos.ObtenerContratos();

                    managerUsuariosContratos.Guardar(usuario.IdUsuario, list);
                }

                List<Contratos> lContratos = managerUsuariosContratos.ObtenerContratosPorTicket(id, horas).Select(e => e.Contratos).ToList();
                
                List<clsContratos> contratos = lContratos.Select(c =>
                                                    new clsContratos()
                                                    {
                                                        IdContrato = c.IdContrato,
                                                        IdInstalacion = c.IdInstalacion,
                                                        Nombre = c.Nombre
                                                    }).ToList();
                return HttpOK<List<clsContratos>>(contratos);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }

        // POST api/contratos
        public void Post([FromBody]string value)
        {
        }

        // PUT api/contratos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/contratos/5
        public void Delete(int id)
        {
        }
    }
}

﻿using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class LocalizacionEspacioUrbanoController : BaseController
    {
        // GET api/localizacionespaciourbano?id=ticket&v=IdDistrito
        [HttpGet]
        public HttpResponseMessage Get(Guid id, int v)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(id);

                Contratos contrato = ComprobarValidezContrato(id);

                List<Localizaciones_EspaciosUrbanos> LocalizacionEspaciosUrbanos = managerMaestras.ObtenerEspaciosUrbanos(contrato.IdInstalacion, v);
                List<clsEspaciosUrbanos> result = new List<clsEspaciosUrbanos>();

                foreach (var item in LocalizacionEspaciosUrbanos)
                {
                    clsEspaciosUrbanos espacioUrbano = new clsEspaciosUrbanos();
                    espacioUrbano.IdEspacioUrbano = item.IdEspacioUrbano;
                    espacioUrbano.EspacioUrbano = item.EspacioUrbano;
                    espacioUrbano.IdDistrito = item.IdDistrito;
                    espacioUrbano.IdInstalacion = item.IdInstalacion;

                    result.Add(espacioUrbano);
                }

                return HttpOK<List<clsEspaciosUrbanos>>(result);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

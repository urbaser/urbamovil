﻿using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSUrbamovil.Clases;

namespace WSUrbamovil.Controllers
{
    public class TablasMaestrasController : BaseController
    {
        // GET api/tablasmaestras?id=ticket&IdContrato=contrato
        [HttpGet]
        public HttpResponseMessage Get(Guid Id, Guid IdContrato)
        {
            try
            {
                Usuarios usuario = ComprobarValidezTicket(Id);

                if (!managerUsuariosContratos.ExisteContrato(IdContrato, usuario.IdUsuario))
                {
                    throw HttpError(HttpStatusCode.NotFound, "No existe el contrato para el usuario especificado.");
                }

                Contratos contrato = managerContratos.ObtenerContrato(IdContrato);

                if (contrato == null)
                {
                    throw HttpError(HttpStatusCode.NotFound, "No existe el contrato especificado");
                }

                managerTicketUsuario.GuardarContratoEnTicket(Id, IdContrato);

                List<Incidencia_Grupos> grupos = managerMaestras.ObtenerIncidenciaGrupos(contrato.IdInstalacion);
                List<Incidencia_Tipos> tipos = managerMaestras.ObtenerIncidenciaTipos(contrato.IdInstalacion);
                List<Incidencia_Descripciones> descripciones = managerMaestras.ObtenerIncidenciaDescripciones(contrato.IdInstalacion);
                List<Incidencia_Estados> estados = managerMaestras.ObtenerIncidenciaEstados(contrato.IdInstalacion);
                List<Incidencia_Prioridades> prioridades = managerMaestras.ObtenerIncidenciaPrioridades(contrato.IdInstalacion);
                List<Provincias> provincias = managerMaestras.ObtenerProvincias(contrato.IdInstalacion);
                List<Municipios> municipios = managerMaestras.ObtenerMunicipios(contrato.IdInstalacion);

                clsTablasMaestras maestras = new clsTablasMaestras();

                foreach(var item in grupos)
                {
                    clsGrupo grupo = new clsGrupo();
                    grupo.GuidGrupoIncidencia = item.GuidGrupoIncidencia;
                    grupo.IdGrupo = item.IdGrupo;
                    grupo.GrupoIncidencia = item.GrupoIncidencia;
                    grupo.IdInstalacion = item.IdInstalacion;

                    maestras.Grupos.Add(grupo);
                }

                foreach (var item in tipos)
                {
                    clsTipo tipo = new clsTipo();
                    tipo.GuidTipoIncidencia = item.GuidTipoIncidencia;
                    tipo.IdTipo = item.IdTipo;
                    tipo.IdGrupoIncidencia = item.IdGrupoIncidencia;
                    tipo.TipoIncidencia = item.TipoIncidencia;
                    tipo.IdInstalacion = item.IdInstalacion;

                    maestras.Tipos.Add(tipo);
                }

                foreach (var item in descripciones)
                {
                    clsDescripcion desc = new clsDescripcion();
                    desc.GuidDescripcionIncidencia = item.GuidDescripcionIncidencia;
                    desc.IdDescripcion = item.IdDescripcion;
                    desc.IdTipoIncidencia = item.IdTipoIncidencia;
                    desc.DescripcionIncidencia = item.DescripcionIncidencia;
                    desc.IdInstalacion = item.IdInstalacion;

                    maestras.Descripciones.Add(desc);
                }

                foreach (var item in estados)
                {
                    clsEstado estado = new clsEstado();
                    estado.GuidEstadoIncidencia = item.GuidEstadoIncidencia;
                    estado.IdEstado = item.IdEstado;
                    estado.EstadoIncidencia = item.EstadoIncidencia;
                    estado.Observaciones = item.Observaciones;
                    estado.IdInstalacion = item.IdInstalacion;

                    maestras.Estados.Add(estado);
                }
 
                foreach (var item in prioridades)
                {
                    clsPrioridad prioridad = new clsPrioridad();
                    prioridad.IdPrioridad = item.IdPrioridad;
                    prioridad.Prioridad = item.Prioridad;
                    prioridad.IdInstalacion = item.IdInstalacion;

                    maestras.Prioridades.Add(prioridad);
                }

                foreach (var item in provincias)
                {
                    clsProvincia provincia = new clsProvincia();
                    provincia.IdProvincia = item.IdProvincia;
                    provincia.NombreProvincia = item.NombreProvincia;
                    provincia.IdInstalacion = item.IdInstalacion;

                    maestras.Provincias.Add(provincia);
                }

                foreach (var item in municipios)
                {
                    clsMunicipio municipio = new clsMunicipio();

                    municipio.IdMunicipio = item.IdMunicipio;
                    municipio.NombreMunicipio = item.NombreMunicipio;
                    municipio.IdProvincia = item.IdProvincia;
                    municipio.IdInstalacion = item.IdInstalacion;

                    maestras.Municipios.Add(municipio);
                }

                return HttpOK<clsTablasMaestras>(maestras);
            }
            catch (HttpResponseException hex)
            {
                LogManager.AddExceptionLog(hex);
                return hex.Response;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return HttpErrorMessage(HttpStatusCode.InternalServerError, "Error al realizar la solicitud");
            }
        }
    }
}

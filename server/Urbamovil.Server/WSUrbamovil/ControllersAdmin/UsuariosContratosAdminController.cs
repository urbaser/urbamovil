﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.ControllersAdmin
{
    public class UsuariosContratosAdminController : BaseAdminController
    {
        // GET api/usuarioscontratosadmin?id=ticket&value=user
        [HttpGet]
        public List<clsUsuarios_Contratos> Get(Guid id, Guid value)
        {
            try
            {
                List<clsUsuarios_Contratos> result = null;

                if (ComprobarUsuarioAdministrador(id))
                {
                    List<Usuarios_Contratos> list = managerUsuariosContratos.ObtenerContratosPorUsuario(value);

                    result = list.Select(uc => new clsUsuarios_Contratos {  Id = uc.Id, 
                                                                            IdContrato = uc.IdContrato, 
                                                                            IdUsuario = uc.IdUsuario,   
                                                                            NombreContrato = uc.Contratos.Nombre }).ToList();
                }

                return result;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }

        // PUT api/usuarioscontratosadmin?id=ticket
        [HttpPut]
        public void Put(Guid id, clsUsuarios_Contratos value)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    Usuarios_Contratos entidad = new Usuarios_Contratos();
                    entidad.Id = Guid.NewGuid();
                    entidad.IdUsuario = value.IdUsuario;
                    entidad.IdContrato = value.IdContrato;

                    managerUsuariosContratos.Guardar(entidad);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }

        // POST api/usuarioscontratosadmin?id=ticket
        [HttpPost]
        public void Post(Guid id, clsUsuarios_Contratos value)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    Usuarios_Contratos entidad = new Usuarios_Contratos();
                    entidad.Id = value.Id;
                    entidad.IdUsuario = value.IdUsuario;
                    entidad.IdContrato = value.IdContrato;

                    managerUsuariosContratos.Actualizar(entidad);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }

        // DELETE api/usuarioscontratosadmin?value=value&id=ticket
        [HttpDelete]
        public void Delete(Guid value, Guid id)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    managerUsuariosContratos.EliminarPorUsuario(value);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.ControllersAdmin
{
    public class CambioAdminController : BaseAdminController
    {
        // POST: api/CambioAdmin
        [HttpPost]
        public void Post(Guid id, clsPass pass)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    string newPass = Encriptacion.Encriptado.EncriptarMD5(pass.Pass);

                    managerUsuario.CambiarPass(pass.IdUsuario, newPass);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;
using WSUrbamovil.Encriptacion;

namespace WSUrbamovil.ControllersAdmin
{
    public class UsuarioAdminController : BaseAdminController
    {
        // GET api/usuarioadmin?id=ticket
        [HttpGet]
        public List<clsUsuarios> Get(Guid id)
        {
            try
            {
                List<clsUsuarios> result = null;

                if (ComprobarUsuarioAdministrador(id))
                {
                    var usuarios = managerUsuario.ObtenerUsuarios();

                    result = usuarios.Select(u =>
                                new clsUsuarios()
                                {
                                    IdUsuario = u.IdUsuario,
                                    Nombre = u.Nombre,
                                    Apellido1 = u.Apellido1,
                                    Apellido2 = u.Apellido2,
                                    Direccion = u.Direccion,
                                    Empresa = u.Empresa,
                                    Email = u.Email,
                                    Telefono = u.Telefono,
                                    TelefonoMovil = u.TelefonoMovil,
                                    Login = u.Login,
                                    IdPerfil = u.IdPerfil,
                                    NombrePerfil = u.IdPerfil.HasValue ? u.TiposPerfilUsuario.Nombre : string.Empty,
                                    IPDireccion = u.IPDireccion,
                                    PreferenciasJSON = u.PreferenciasJSON,
                                    FechaRegistro = u.FechaRegistro,
                                    IdIdioma = u.IdIdioma,
                                    Activo = u.Activo,
                                    FechaValidezCC = u.FechaValidezCC,
                                    ContraClave = u.ContraClave
                                }).ToList();
                }

                return result;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }

        // GET api/usuarioadmin?id=ticket&value=idUsuario
        [HttpGet]
        public clsUsuarios Get(Guid id, Guid value)
        {
            try
            {
                clsUsuarios usuario = null;

                if (ComprobarUsuarioAdministrador(id))
                {
                    Usuarios _user = managerUsuario.ObtenerUsuarioPorId(value);
                    usuario = new clsUsuarios();
                    usuario.IdUsuario = _user.IdUsuario;
                    usuario.Nombre = _user.Nombre;
                    usuario.Apellido1 = _user.Apellido1;
                    usuario.Apellido2 = _user.Apellido2;
                    usuario.Direccion = _user.Direccion;
                    usuario.Empresa = _user.Empresa;
                    usuario.Email = _user.Email;
                    usuario.Telefono = _user.Telefono;
                    usuario.TelefonoMovil = _user.TelefonoMovil;
                    usuario.Login = _user.Login;
                    usuario.IdPerfil = _user.IdPerfil;
                    usuario.NombrePerfil = _user.IdPerfil.HasValue ? _user.TiposPerfilUsuario.Nombre : string.Empty;
                    usuario.IPDireccion = _user.IPDireccion;
                    usuario.PreferenciasJSON = _user.PreferenciasJSON;
                    usuario.FechaRegistro = _user.FechaRegistro;
                    usuario.IdIdioma = _user.IdIdioma;
                    usuario.Activo = _user.Activo;
                    usuario.FechaValidezCC = _user.FechaValidezCC;
                    usuario.ContraClave = _user.ContraClave;
                }

                return usuario;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }

        // POST api/usuarioadmin?id=ticket
        [HttpPost]
        public void Post(Guid id, clsUsuarios usuario)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    Usuarios nuevoUsuario = new Usuarios();

                    nuevoUsuario.IdUsuario = usuario.IdUsuario;
                    nuevoUsuario.Nombre = usuario.Nombre;
                    nuevoUsuario.Apellido1 = usuario.Apellido1;
                    nuevoUsuario.Apellido2 = usuario.Apellido2;
                    nuevoUsuario.Direccion = usuario.Direccion;
                    nuevoUsuario.Empresa = usuario.Empresa;
                    nuevoUsuario.Email = usuario.Email;
                    nuevoUsuario.Telefono = usuario.Telefono;
                    nuevoUsuario.TelefonoMovil = usuario.TelefonoMovil;
                    nuevoUsuario.Login = usuario.Login;
                    nuevoUsuario.IdPerfil = usuario.IdPerfil;
                    nuevoUsuario.IPDireccion = usuario.IPDireccion;
                    nuevoUsuario.PreferenciasJSON = usuario.PreferenciasJSON;
                    nuevoUsuario.IdIdioma = usuario.IdIdioma;
                    nuevoUsuario.Activo = usuario.Activo;

                    managerUsuario.ActualizarUsuario(nuevoUsuario);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }

        // PUT api/usuarioadmin?id=ticket
        [HttpPut]
        public void Put(Guid id, clsUsuarios nuevoUsuario)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    Usuarios usuario = new Usuarios();
                    usuario.IdUsuario = nuevoUsuario.IdUsuario;
                    usuario.Nombre = nuevoUsuario.Nombre;
                    usuario.Apellido1 = nuevoUsuario.Apellido1;
                    usuario.Apellido2 = nuevoUsuario.Apellido2;
                    usuario.Direccion = nuevoUsuario.Direccion;
                    usuario.Empresa = nuevoUsuario.Empresa;
                    usuario.Email = nuevoUsuario.Email;
                    usuario.Telefono = nuevoUsuario.Telefono;
                    usuario.TelefonoMovil = nuevoUsuario.TelefonoMovil;
                    usuario.Login = nuevoUsuario.Login;
                    usuario.Pass = Encriptado.EncriptarMD5(nuevoUsuario.Pass);
                    usuario.IdPerfil = nuevoUsuario.IdPerfil;
                    usuario.IPDireccion = nuevoUsuario.IPDireccion;
                    usuario.PreferenciasJSON = nuevoUsuario.PreferenciasJSON;
                    usuario.IdIdioma = nuevoUsuario.IdIdioma;
                    usuario.FechaRegistro = DateTime.Now;
                    usuario.Activo = nuevoUsuario.Activo;

                    managerUsuario.GuardarUsuario(usuario);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // DELETE api/usuarioadmin?value=value&id=ticket
        [HttpDelete]
        public void Delete(Guid value, Guid id)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    managerFiltroUsuario.EliminarFiltrosPorUsuario(value);

                    managerUsuariosContratos.EliminarPorUsuario(value);

                    managerTicketUsuario.EliminarPorUsuario(value);
                    
                    managerUsuario.EliminarUsuario(value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.ControllersAdmin
{
    public class ContratosAdminController : BaseAdminController
    {
        // GET api/contratosadmin?id=ticket
        [HttpGet]
        public List<clsContratos> Get(Guid id)
        {
            try
            {
                List<clsContratos> result = null;

                if (ComprobarUsuarioAdministrador(id))
                {
                    List<Contratos> list = managerContratos.ObtenerContratos();
                    result = new List<clsContratos>();

                    foreach (var item in list)
                    {
                        clsContratos contrato = new clsContratos();
                        contrato.IdContrato = item.IdContrato;
                        contrato.Nombre = item.Nombre;
                        contrato.IdGEO = item.IdGEO;
                        contrato.IdInstalacion = item.IdInstalacion;
                        contrato.NombreInstalacion = item.Instalaciones.Nombre;

                        result.Add(contrato);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }

        // GET api/contratosadmin?id=ticket&value=IdContrato
        [HttpGet]
        public clsContratos Get(Guid id, Guid value)
        {
            try
            {
                clsContratos result = null;

                if (ComprobarUsuarioAdministrador(id))
                {
                    Contratos contrato = managerContratos.ObtenerContrato(value);

                    result = new clsContratos();
                    result.IdContrato = contrato.IdContrato;
                    result.Nombre = contrato.Nombre;
                    result.IdGEO = contrato.IdGEO;
                    result.IdInstalacion = contrato.IdInstalacion;
                    result.NombreInstalacion = contrato.Instalaciones.Nombre;
                }

                return result;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }

        // POST api/contratosadmin?id=ticket
        [HttpPost]
        public void Post(Guid id, clsContratos contrato)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    Contratos updateContrato = new Contratos();
                    updateContrato.IdContrato = contrato.IdContrato;
                    updateContrato.Nombre = contrato.Nombre;
                    updateContrato.IdGEO = contrato.IdGEO;
                    updateContrato.IdInstalacion = contrato.IdInstalacion;

                    managerContratos.ActualizarContrato(updateContrato);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }

        // PUT api/contratosadmin?id=ticket
        [HttpPut]
        public void Put(Guid id, clsContratos contrato)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    Contratos nuevoContrato = new Contratos();
                    nuevoContrato.IdContrato = Guid.NewGuid();
                    nuevoContrato.Nombre = contrato.Nombre;
                    nuevoContrato.IdGEO = contrato.IdGEO;
                    nuevoContrato.IdInstalacion = contrato.IdInstalacion;

                    managerContratos.GuardarContrato(nuevoContrato);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }

        // DELETE api/contratosadmin?value=value&id=ticket
        [HttpDelete]
        public void Delete(Guid value, Guid id)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    managerUsuariosContratos.EliminarPorContrato(value);

                    managerContratos.EliminarContrato(value);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }
    }
}

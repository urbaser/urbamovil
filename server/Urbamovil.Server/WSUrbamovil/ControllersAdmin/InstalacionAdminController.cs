﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using WSUrbamovil.Clases;

namespace WSUrbamovil.ControllersAdmin
{
    public class InstalacionAdminController : BaseAdminController
    {
        // GET api/instalacionadmin?id=ticket
        [HttpGet]
        public List<clsInstalaciones> Get(Guid id)
        {
            try
            {
                List<clsInstalaciones> result = null;

                if (ComprobarUsuarioAdministrador(id))
                {
                    List<Instalaciones> list = managerInstalaciones.ObtenerInstalaciones();
                    result = new List<clsInstalaciones>();

                    foreach (var item in list)
                    {
                        clsInstalaciones instalacion = new clsInstalaciones();
                        instalacion.IdInstalacion = item.IdInstalacion;
                        instalacion.Nombre = item.Nombre;
                        instalacion.URL = item.URL;
                        instalacion.URL_MAPA_AGOL = item.URL_MAPA_AGOL;
                        instalacion.IdTipoInstalacion = item.IdTipoInstalacion;
                        instalacion.NombreTipoInstalacion = item.TiposInstalacion.Nombre;

                        result.Add(instalacion);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }

        // GET api/instalacionadmin?id=ticket&value=idInstalacion
        [HttpGet]
        public clsInstalaciones Get(Guid id, Guid value)
        {
            try
            {
                clsInstalaciones result = null;

                if (ComprobarUsuarioAdministrador(id))
                {
                    Instalaciones instalacion = managerInstalaciones.ObtenerInstalacion(value);

                    result = new clsInstalaciones();
                    result.IdInstalacion = instalacion.IdInstalacion;
                    result.Nombre = instalacion.Nombre;
                    result.URL = instalacion.URL;
                    result.URL_MAPA_AGOL = instalacion.URL_MAPA_AGOL;
                    result.IdTipoInstalacion = instalacion.IdTipoInstalacion;
                    result.NombreTipoInstalacion = instalacion.TiposInstalacion.Nombre;
                }

                return result;
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                return null;
            }
        }

        // POST api/instalacionadmin?id=ticket
        [HttpPost]
        public void Post(Guid id, clsInstalaciones instalacion)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    Instalaciones updateInstalacion = new Instalaciones();
                    updateInstalacion.IdInstalacion = instalacion.IdInstalacion;
                    updateInstalacion.Nombre = instalacion.Nombre;
                    updateInstalacion.URL = instalacion.URL;
                    updateInstalacion.URL_MAPA_AGOL = instalacion.URL_MAPA_AGOL;
                    updateInstalacion.IdTipoInstalacion = instalacion.IdTipoInstalacion;

                    managerInstalaciones.ActualizarInstalacion(updateInstalacion);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }

        // PUT api/instalacionadmin?id=ticket
        [HttpPut]
        public void Put(Guid id, clsInstalaciones value)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    Instalaciones nuevaInstalacion = new Instalaciones();
                    nuevaInstalacion.IdInstalacion = value.IdInstalacion;
                    nuevaInstalacion.Nombre = value.Nombre;
                    nuevaInstalacion.URL = value.URL;
                    nuevaInstalacion.URL_MAPA_AGOL = value.URL_MAPA_AGOL;
                    nuevaInstalacion.IdTipoInstalacion = value.IdTipoInstalacion;

                    managerInstalaciones.GuardarInstalacion(nuevaInstalacion);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }

        // DELETE api/instalacionadmin?value=idInstalacion&id=ticket
        [HttpDelete]
        public void Delete(Guid value, Guid id)
        {
            try
            {
                if (ComprobarUsuarioAdministrador(id))
                {
                    List<Contratos> listContratos = managerContratos.ObtenerContratoPorInstalacion(value);
                    
                    foreach (Contratos contrato in listContratos)
                    {
                        managerUsuariosContratos.EliminarPorContrato(contrato.IdContrato);

                        managerContratos.EliminarContrato(contrato.IdContrato);
                    }

                    managerInstalaciones.EliminarInstalacion(value);
                }
            }
            catch (Exception ex)
            {
                LogManager.AddExceptionLog(ex);
                throw ex;
            }
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FRMImagenes.aspx.cs" Inherits="WSUrbamovil.Forms.FRMImagenes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>INCIDENCIAS IMAGENES</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Añadir imagenes a las incidencias</h1>
        <fieldset>
            <legend>Guardar Imagenes</legend>
            <div>
                <div>
                    <asp:Label ID="lblIncidencia" runat="server" Text="Incidencia: "/>
                    <asp:DropDownList ID="ddlIncidencia" runat="server" AutoPostBack="false" DataTextField="IdIncidencia" DataValueField="IdIncidencia" />
                </div>
                <br />
                <div>
                    <asp:Label ID="lblImagen" runat="server" Text="Imagen: "  />
                    <asp:FileUpload ID="fuImagen" runat="server" AllowMultiple="false" />
                </div>
                <br />
                 <div>
                    <asp:Label ID="lblImagenCierre" runat="server" Text="Imagen cierre: "  />
                    <asp:FileUpload ID="fuImagenCierre" runat="server" AllowMultiple="false" />
                </div>
            </div>
        </fieldset>
        <div class="divBotones">
            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
            <asp:Button ID="btnMostrar" runat="server" Text="Mostrar imagenes" OnClick="btnMostrar_Click" />
        </div>
        <br />
        <br />
        <asp:Image ID="Imagen" runat="server" Visible="false"/>
        <br />
        <asp:Image ID="ImagenCierre" runat="server" Visible="false"/>
    </form>
</body>
</html>

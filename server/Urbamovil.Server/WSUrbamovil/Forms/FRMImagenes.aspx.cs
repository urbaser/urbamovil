﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WSUrbamovil.Clases;

using NegocioUrbamovil.Manager;
using NegocioUrbamovil.Modelo;


namespace WSUrbamovil.Forms
{
    public partial class FRMImagenes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.CargarDatos();
            }
        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            AppSettingsReader appReader = new AppSettingsReader();

            int iW = (int)appReader.GetValue("KEY_IMG_MAXWIDTH", typeof(int));
            int iH = (int)appReader.GetValue("KEY_IMG_MAXHEIGHT", typeof(int));

            Guid IdIncidencia = new Guid(ddlIncidencia.SelectedValue);

            IncidenciasManager manager = new IncidenciasManager();

            Incidencias incidencia = manager.ObtenerIncidencia(IdIncidencia);

             this.fuImagen.PostedFile.SaveAs(@"d:\temp\img.jpg");

            if (incidencia != null)
            {
                int lenght = this.fuImagen.PostedFile.ContentLength;
                byte[] myData = new byte[lenght];
                this.fuImagen.PostedFile.InputStream.Read(myData, 0, lenght);
                incidencia.Imagen = ImagenUtil.ImageResize(myData, iW, iH); ;

                int lenghtCierre = this.fuImagenCierre.PostedFile.ContentLength;
                byte[] myDataCierre = new byte[lenghtCierre];
                this.fuImagenCierre.PostedFile.InputStream.Read(myDataCierre, 0, lenghtCierre);
                incidencia.ImagenCierre = myDataCierre;

                manager.ActualizarIncidencia(incidencia);
            }
        }

        private void CargarDatos()
        {
            IncidenciasManager manager = new IncidenciasManager();

            List<Incidencias> list = manager.Obtener();

            ddlIncidencia.DataSource = list;
            ddlIncidencia.DataBind();
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            this.Imagen.Visible = true;
            this.Imagen.ImageUrl = ResolveUrl("~/Handlers/HandlerImagenes.ashx?Inc=" + ddlIncidencia.SelectedValue);
            
            this.ImagenCierre.Visible = true;
            this.ImagenCierre.ImageUrl = ResolveUrl("~/Handlers/HandlerImagenesCierre.ashx?Inc=" + ddlIncidencia.SelectedValue);
        }
    }
}
﻿using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioUrbamovil.Manager
{
    public class MaestrasManager : BaseManager
    {
        public MaestrasManager() : base() { }

        public MaestrasManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public List<Incidencia_Grupos> ObtenerIncidenciaGrupos(Guid IdInstalacion)
        {
            return Contexto.Incidencia_Grupos.Where(g => g.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Incidencia_Tipos> ObtenerIncidenciaTipos(Guid IdInstalacion)
        {
            return Contexto.Incidencia_Tipos.Where(g => g.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Incidencia_Descripciones> ObtenerIncidenciaDescripciones(Guid IdInstalacion)
        {
            return Contexto.Incidencia_Descripciones.Where(g => g.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Incidencia_Estados> ObtenerIncidenciaEstados(Guid IdInstalacion)
        {
            return Contexto.Incidencia_Estados.Where(g => g.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Incidencia_Prioridades> ObtenerIncidenciaPrioridades(Guid IdInstalacion)
        {
            return Contexto.Incidencia_Prioridades.Where(p => p.IdInstalacion == IdInstalacion).ToList();
        }
        
        public List<Provincias> ObtenerProvincias(Guid IdInstalacion)
        {
            return Contexto.Provincias.Where(p => p.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Municipios> ObtenerMunicipios(Guid IdInstalacion)
        {
            return Contexto.Municipios.Where(p => p.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Elementos> ObtenerElementos(Guid IdInstalacion)
        {
            return Contexto.Elementos.Where(e => e.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Localizaciones_TiposElemento> ObtenerTiposElementos(Guid IdInstalacion)
        {
            return Contexto.Localizaciones_TiposElemento.Where(t => t.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Localizaciones_EspaciosUrbanos> ObtenerEspaciosUrbanos(Guid IdInstalacion)
        {
            return Contexto.Localizaciones_EspaciosUrbanos.Where(t => t.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Localizaciones_EspaciosUrbanos> ObtenerEspaciosUrbanos(Guid IdInstalacion, int IdDistrito)
        {
            return Contexto.Localizaciones_EspaciosUrbanos.Where(t => t.IdInstalacion == IdInstalacion && t.IdDistrito == IdDistrito).ToList();
        }

        public List<Localizaciones_Rutas> ObtenerRutas(Guid IdInstalacion)
        {
            return Contexto.Localizaciones_Rutas.Where(t => t.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Localizaciones_Rutas> ObtenerRutas(Guid IdInstalacion, int IdEspacioUrbano)
        {
            return Contexto.Localizaciones_Rutas.Where(t => t.IdInstalacion == IdInstalacion && t.IdEspacioUrbano == IdEspacioUrbano).ToList();
        }

        public List<Localizaciones_Zonas> ObtenerZonas(Guid IdInstalacion)
        {
            return Contexto.Localizaciones_Zonas.Where(t => t.IdInstalacion == IdInstalacion).ToList();
        }

        public List<Localizaciones_Zonas> ObtenerZonas(Guid IdInstalacion, int IdEspacioUrbano)
        {
            return Contexto.Localizaciones_Zonas.Where(t => t.IdInstalacion == IdInstalacion && t.IdEspacioUrbano == IdEspacioUrbano).ToList();
        }

        public List<Localizaciones_Distritos> ObtenerDistritos(Guid IdInstalacion)
        {
            return Contexto.Localizaciones_Distritos.Where(t => t.IdInstalacion == IdInstalacion).ToList();
        }
    }
}

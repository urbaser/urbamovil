﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioUrbamovil.Modelo;

namespace NegocioUrbamovil.Manager
{
    public class HistoricoIncidenciasManager : BaseManager
    {
        public HistoricoIncidenciasManager() : base() { }

        public HistoricoIncidenciasManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public void Guardar(HistoricoIncidencias entidad)
        {
            try
            {
                Contexto.HistoricoIncidencias.Add(entidad);

                Contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Guardar(Guid IdIncidencia, int IdOperacion, string objectJSON)
        {
            try
            {
                HistoricoIncidencias entidad = new HistoricoIncidencias();
                entidad.Id = Guid.NewGuid();
                entidad.IdIncidencia = IdIncidencia;
                entidad.IdOperacion = IdOperacion;
                entidad.Fecha = DateTime.Now;
                entidad.EntidadJSON = objectJSON;

                Contexto.HistoricoIncidencias.Add(entidad);

                Contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

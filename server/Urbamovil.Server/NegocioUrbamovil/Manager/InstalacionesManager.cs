﻿using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioUrbamovil.Manager
{
    public class InstalacionesManager : BaseManager
    {
        public InstalacionesManager() : base() { }

        public InstalacionesManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }
         
        public List<Instalaciones> ObtenerInstalaciones()
        {
            try
            {
                return Contexto.Instalaciones.Include("TiposInstalacion").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Instalaciones ObtenerInstalacion(Guid IdInstalacion)
        {
            try
            {
                Instalaciones instalacion = Contexto.Instalaciones.Include("TiposInstalacion").FirstOrDefault(i => i.IdInstalacion == IdInstalacion);

                if (instalacion == null)
                {
                    throw new Exception("ERROR: la instalación con dicho identificador no existe en el sistema");
                }

                return instalacion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarInstalacion(Instalaciones nuevaInstalacion)
        {
            try
            {
                Instalaciones instalacion = ObtenerInstalacion(nuevaInstalacion.IdInstalacion);
                
                if (instalacion != null)
                {
                    instalacion.Nombre = nuevaInstalacion.Nombre;
                    instalacion.URL = nuevaInstalacion.URL;
                    instalacion.URL_MAPA_AGOL = nuevaInstalacion.URL_MAPA_AGOL;
                    instalacion.IdTipoInstalacion = nuevaInstalacion.IdTipoInstalacion;

                    Contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GuardarInstalacion(Instalaciones nuevaInstalacion)
        {
            try
            {
                if (!ExisteInstalacion(nuevaInstalacion.IdInstalacion))
                {
                    Contexto.Instalaciones.Add(nuevaInstalacion);

                    Contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarInstalacion(Guid IdInstalacion)
        {
            try
            {
                Instalaciones instalacion = ObtenerInstalacion(IdInstalacion);

                Contexto.Instalaciones.Remove(instalacion);

                Contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ExisteInstalacion(Guid IdInstalacion)
        {
            return Contexto.Instalaciones.FirstOrDefault(i => i.IdInstalacion == IdInstalacion) != null;
        }
    }
}

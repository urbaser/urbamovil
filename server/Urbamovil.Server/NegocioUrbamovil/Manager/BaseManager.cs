﻿using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioUrbamovil.Manager
{
    public class BaseManager
    {
        protected DbUrbamovil_DevEntities Contexto = null;

        public BaseManager()
        {
            this.Contexto = new DbUrbamovil_DevEntities();
        }

        public BaseManager(DbUrbamovil_DevEntities Contexto)
        {
            this.Contexto = Contexto;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioUrbamovil.Modelo;

namespace NegocioUrbamovil.Manager
{
    public class TipoInstalacionManager : BaseManager
    {
        public TipoInstalacionManager() : base() { }

        public TipoInstalacionManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public List<TiposInstalacion> ObtenerTiposInstalaciones()
        {
            return Contexto.TiposInstalacion.ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioUrbamovil.Modelo;

namespace NegocioUrbamovil.Manager
{
    public class FiltroUsuarioManager : BaseManager
    {
        public FiltroUsuarioManager() : base() { }

        public FiltroUsuarioManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public FiltroUsuario ObtenerFiltro(Guid IdFiltro)
        {
            try
            {
                FiltroUsuario filtro = Contexto.FiltroUsuario.FirstOrDefault(f => f.IdFiltroUsuario == IdFiltro);

                if (filtro == null)
                {
                    throw new Exception("ERROR: el filtro del usuario es inexistente.");
                }

                return filtro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FiltroUsuario> ObtenerFiltrosPorUsuario(Guid IdUsuario)
        {
            try
            {
                List<FiltroUsuario> list = Contexto.FiltroUsuario.Where(f => f.IdUsuario == IdUsuario).ToList();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarFiltro(FiltroUsuario nuevoFiltro)
        {
            try
            {
                FiltroUsuario filtro = ObtenerFiltro(nuevoFiltro.IdFiltroUsuario);

                filtro.Nombre = nuevoFiltro.Nombre;
                filtro.ObjetoJSON = nuevoFiltro.ObjetoJSON;

                Contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GuardarFiltro(FiltroUsuario nuevoFiltro)
        {
            try
            {
                if (!ExisteFiltro(nuevoFiltro.IdFiltroUsuario))
                {
                    Contexto.FiltroUsuario.Add(nuevoFiltro);

                    Contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarFiltrosPorUsuario(Guid IdUsuario)
        {
            try
            {
                List<FiltroUsuario> listFiltros = Contexto.FiltroUsuario.Where(f => f.IdUsuario == IdUsuario).ToList();

                foreach (FiltroUsuario filtro in listFiltros)
                {
                    Contexto.FiltroUsuario.Remove(filtro);
                }

                Contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarFiltro(Guid IdFiltro)
        {
            try
            {
                FiltroUsuario filtro = ObtenerFiltro(IdFiltro);

                Contexto.FiltroUsuario.Remove(filtro);

                Contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ExisteFiltro(Guid IdFiltro)
        {
            return Contexto.FiltroUsuario.FirstOrDefault(f => f.IdFiltroUsuario == IdFiltro) != null;
        }
    }
}

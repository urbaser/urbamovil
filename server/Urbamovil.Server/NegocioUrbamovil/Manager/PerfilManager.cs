﻿using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioUrbamovil.Manager
{
    public class PerfilManager : BaseManager
    {
        public PerfilManager() : base() { }

        public PerfilManager(DbUrbamovil_DevEntities Context) : base(Context) { }

        public List<TiposPerfilUsuario> ObtenerPerfiles()
        {
            return Contexto.TiposPerfilUsuario.ToList();
        }

        public TiposPerfilUsuario ObtenerPerfil(Guid IdPerfil)
        {
            return Contexto.TiposPerfilUsuario.FirstOrDefault(p => p.IdPerfil == IdPerfil);
        }

        public TiposPerfilUsuario ObtenerPerfil(string Desc)
        {
            return Contexto.TiposPerfilUsuario.FirstOrDefault(p => p.Nombre.Equals(Desc));
        }
    }
}

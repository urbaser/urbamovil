﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioUrbamovil.Modelo;

namespace NegocioUrbamovil.Manager
{
    public class UsuariosContratosManager : BaseManager
    {
        public UsuariosContratosManager() : base() { }

        public UsuariosContratosManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public Usuarios_Contratos ObtenerPorId(Guid Id)
        {
            try
            {
                Usuarios_Contratos entidad = Contexto.Usuarios_Contratos.FirstOrDefault(uc => uc.Id == Id);

                if (entidad == null)
                {
                    throw new Exception("ERROR: entidad inexistente");
                }

                return entidad;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Usuarios_Contratos> ObtenerContratosPorUsuario(Guid IdUsuario)
        {
            try
            {
                List<Usuarios_Contratos> contratos = Contexto.Usuarios_Contratos.Include("Contratos").Include("Usuarios")
                                                    .Where(uc => uc.IdUsuario == IdUsuario).ToList();

                return contratos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Usuarios_Contratos> ObtenerContratosPorTicket(Guid Ticket, int horas)
        {
            try
            {
                UsuarioManager managerUsuario = new UsuarioManager();

                Usuarios usuario = managerUsuario.ObtenerUsuarioPorTicket(Ticket, horas);

                List<Usuarios_Contratos> contratos = ObtenerContratosPorUsuario(usuario.IdUsuario);

                return contratos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Actualizar(Usuarios_Contratos updateEntidad)
        {
            Usuarios_Contratos entidad = ObtenerPorId(updateEntidad.Id);

            if (entidad != null)
            {
                entidad.IdContrato = updateEntidad.IdContrato;
                entidad.IdUsuario = updateEntidad.IdUsuario;

                Contexto.SaveChanges();
            }
        }

        public void Guardar(Usuarios_Contratos entidad)
        {
            if (!Existe(entidad.Id))
            {
                Contexto.Usuarios_Contratos.Add(entidad);

                Contexto.SaveChanges();
            }
        }

        public void Guardar(Guid IdUsuario, List<Contratos> contratos)
        {
            foreach(var item in contratos)
            {
                Usuarios_Contratos entidad = new Usuarios_Contratos();
                entidad.Id = Guid.NewGuid();
                entidad.IdUsuario = IdUsuario;
                entidad.IdContrato = item.IdContrato;

                Contexto.Usuarios_Contratos.Add(entidad);
            }

            Contexto.SaveChanges();
        }

        public void Eliminar(Guid Id)
        {
            Usuarios_Contratos entidad = ObtenerPorId(Id);

            if(entidad != null)
            {
                Contexto.Usuarios_Contratos.Remove(entidad);

                Contexto.SaveChanges();
            }
        }

        public void EliminarPorUsuario(Guid IdUsuario)
        {
            List<Usuarios_Contratos> entidades = Contexto.Usuarios_Contratos.Where(uc => uc.IdUsuario == IdUsuario).ToList();

            if (entidades != null && entidades.Count > 0)
            {
                foreach (var entidad in entidades)
                {
                    Contexto.Usuarios_Contratos.Remove(entidad);
                }

                Contexto.SaveChanges();
            }
        }

        public void EliminarPorContrato(Guid IdContrato)
        {
            List<Usuarios_Contratos> entidades = Contexto.Usuarios_Contratos.Where(uc => uc.IdContrato == IdContrato).ToList();

            foreach (var entidad in entidades)
            {
                Contexto.Usuarios_Contratos.Remove(entidad);
            }

            Contexto.SaveChanges();
        }

        public bool ExisteContrato(Guid IdContrato, Guid IdUsuario)
        {
            Usuarios_Contratos contrato = Contexto.Usuarios_Contratos.FirstOrDefault(c => c.IdContrato == IdContrato && c.IdUsuario == IdUsuario);

            return contrato != null;
        }

        private bool Existe(Guid Id)
        {
            return Contexto.Usuarios_Contratos.FirstOrDefault(u => u.Id == Id) != null;
        }
    }
}

﻿using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioUrbamovil.Manager
{
    public class ElementosManager : BaseManager
    {
        public ElementosManager() : base() { }

        public ElementosManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public List<Elementos> ObtenerElementos(Guid IdInstalacion, int IdTipo)
        {
            return Contexto.Elementos.Where(e => e.IdInstalacion == IdInstalacion && e.IdTipoElemento == IdTipo).ToList();
        }
    }
}

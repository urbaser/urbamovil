﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NegocioUrbamovil.Modelo;

namespace NegocioUrbamovil.Manager
{
    public class VersionesManager : BaseManager
    {
        public VersionesManager() : base() { }

        public VersionesManager(DbUrbamovil_DevEntities Context) : base(Context) { }

        public Versiones ObtenerVersion(string Codigo, string Sistema)
        {
            var version = Contexto.Versiones.Include("EstadoVersion").FirstOrDefault(v => v.Codigo.Equals(Codigo) && v.Sistema.Equals(Sistema));

            return version;
        }
    }
}

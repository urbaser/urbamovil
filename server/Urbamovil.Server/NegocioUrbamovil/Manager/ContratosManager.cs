﻿using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioUrbamovil.Manager
{
    public class ContratosManager : BaseManager
    {
        public ContratosManager() : base() { }

        public ContratosManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public List<Contratos> ObtenerContratos()
        {
            return Contexto.Contratos.ToList();
        }

        public List<Contratos> ObtenerContratoPorInstalacion(Guid IdInstalacion)
        {
            return Contexto.Contratos.Where(c => c.IdInstalacion == IdInstalacion).ToList();
        }

        public Contratos ObtenerContrato(Guid IdContrato)
        {
            try
            {
                Contratos contrato = Contexto.Contratos.FirstOrDefault(c => c.IdContrato == IdContrato);

                if(contrato == null)
                {
                    throw new Exception("ERROR: contrato no existente.");
                }

                return contrato;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarContrato(Contratos contrato)
        {
            Contratos updateContrato = ObtenerContrato(contrato.IdContrato);

            updateContrato.Nombre = contrato.Nombre;
            updateContrato.IdGEO = contrato.IdGEO;
            updateContrato.IdInstalacion = contrato.IdInstalacion;

            Contexto.SaveChanges();
        }

        public void EliminarContrato(Guid IdContrato)
        {
            Contratos contrato = ObtenerContrato(IdContrato);

            Contexto.Contratos.Remove(contrato);

            Contexto.SaveChanges();
        }

        public void GuardarContrato(Contratos nuevoContrato)
        {
            try
            {
                if (!ExiteContrato(nuevoContrato.IdContrato))
                {
                    Contexto.Contratos.Add(nuevoContrato);

                    Contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ExiteContrato(Guid IdContrato)
        {
            return Contexto.Contratos.FirstOrDefault(c => c.IdContrato == IdContrato) != null;
        }
    }
}

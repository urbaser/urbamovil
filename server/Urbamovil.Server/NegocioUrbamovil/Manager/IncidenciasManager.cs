﻿using NegocioComunUrbamovil;
using NegocioUrbamovil.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioUrbamovil.Manager
{
    public class IncidenciasManager : BaseManager
    {
        public IncidenciasManager() : base() { }

        public IncidenciasManager(DbUrbamovil_DevEntities Contexto) : base(Contexto) { }

        public List<Incidencias> Obtener()
        {
            return Contexto.Incidencias.OrderByDescending(i => i.FechaAlta).ToList();
        }

        public Incidencias ObtenerIncidencia(Guid IdIncidencia)
        {
            try
            {
                Incidencias incidencia = Contexto.Incidencias.FirstOrDefault(i => i.IdIncidencia == IdIncidencia);

                if (incidencia == null)
                {
                    throw new Exception("ERROR: la incidencia que se quiere obtener no existe");
                }

                return incidencia;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Incidencias> ObtenerIncidencias(FiltroIncidencia filtro)
        {
            var list = Contexto.Incidencias.AsQueryable();

            if (filtro.HasValue())
            {
                if (filtro.IdUsuarioAlta.HasValue)
                {
                    list = list.Where(i => i.IdUsuarioAlta == filtro.IdUsuarioAlta.Value);
                }
                if (filtro.IdContrato.HasValue)
                {
                    list = list.Where(i => i.IdContrato.HasValue && i.IdContrato.Value == filtro.IdContrato.Value);
                }
                if (filtro.IdEstado.HasValue)
                {
                    list = list.Where(i => i.IdEstado.HasValue && i.IdEstado.Value == filtro.IdEstado.Value);
                }
                if (filtro.IdGrupo.HasValue)
                {
                    list = list.Where(i => i.IdGrupo == filtro.IdGrupo.Value);
                }
                if (filtro.IdTipo.HasValue)
                {
                    list = list.Where(i => i.IdTipo == filtro.IdTipo.Value);
                }
                if (filtro.IdDescripcion.HasValue)
                {
                    list = list.Where(i => i.IdDescripcion == filtro.IdDescripcion.Value);
                }
                if (filtro.FechaDesde.HasValue)
                {
                    list = list.Where(i => i.FechaAlta >= filtro.FechaDesde.Value);
                }
                if (filtro.FechaHasta.HasValue)
                {
                    list = list.Where(i => i.FechaAlta <= filtro.FechaHasta.Value);
                }
                if (filtro.IdPrioridad.HasValue)
                {
                    list = list.Where(i => i.IdPrioridad == filtro.IdPrioridad.Value);
                }
                if (filtro.IdProvincia.HasValue)
                {
                    list = list.Where(i => i.IdProvincia == filtro.IdProvincia.Value);
                }
                if (filtro.IdMunicipio.HasValue)
                {
                    list = list.Where(i => i.IdMunicipio == filtro.IdMunicipio.Value);
                }
                if (!string.IsNullOrWhiteSpace(filtro.Direccion))
                {
                    list = list.Where(i => i.Localizaciones_Incidencia.Any(l => l.Dir_Calle.Contains(filtro.Direccion)));
                }
                if (filtro.IdTipoElemento.HasValue)
                {
                    list = list.Where(i => i.Localizaciones_Incidencia.Any(l => l.Inv_IdTipoElemento.HasValue && l.Inv_IdTipoElemento.Value == filtro.IdTipoElemento));
                }
                if (filtro.IdElemento.HasValue)
                {
                    list = list.Where(i => i.Localizaciones_Incidencia.Any(l => l.Inv_IdElemento.HasValue && l.Inv_IdElemento.Value == filtro.IdElemento));
                }
            }

            return list.OrderByDescending(i => i.FechaAlta).ToList();
        }

        public List<Incidencias> ObtenerIncidenciasPorUsuario(Guid IdUsuario)
        {
            return Contexto.Incidencias.Where(i => i.IdUsuarioAlta == IdUsuario)
                            .OrderByDescending(i => i.FechaAlta)
                            .ToList();
        }

        public List<Incidencias> ObtenerIncidenciasPorContrato(Guid IdContrato)
        {
            return Contexto.Incidencias.Where(i => i.IdContrato == IdContrato)
                            .OrderByDescending(i => i.FechaAlta)
                            .ToList();
        }

        public void ActualizarIncidencia(Incidencias nuevaIncidencia)
        {
            try
            {
                Incidencias incidencia = ObtenerIncidencia(nuevaIncidencia.IdIncidencia);

                incidencia.IdGrupo = nuevaIncidencia.IdGrupo;
                incidencia.IdTipo = nuevaIncidencia.IdTipo;
                incidencia.IdDescripcion = nuevaIncidencia.IdDescripcion;
                incidencia.IdPrioridad = nuevaIncidencia.IdPrioridad;
                incidencia.IdEstado = nuevaIncidencia.IdEstado;
                incidencia.IdProvincia = nuevaIncidencia.IdProvincia;
                incidencia.IdMunicipio = nuevaIncidencia.IdMunicipio;
                incidencia.CodigoIncidencia = nuevaIncidencia.CodigoIncidencia;
                incidencia.Imagen = nuevaIncidencia.Imagen;
                incidencia.ImagenCierre = nuevaIncidencia.ImagenCierre;
                incidencia.Observaciones = nuevaIncidencia.Observaciones;
                incidencia.Sincronizada = nuevaIncidencia.Sincronizada;
                incidencia.IdOperacionDestino = nuevaIncidencia.IdOperacionDestino;
                incidencia.FechaUltimaModificacion = DateTime.Now;

                Contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GuardarIncidencia(Incidencias nuevaIncidencia)
        {
            try
            {
                if (!ExisteIncidencia(nuevaIncidencia.IdIncidencia))
                {
                    Contexto.Incidencias.Add(nuevaIncidencia);

                    Contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarIncidencia(Guid IdIncidencia)
        {
            try
            {
                Incidencias incidencia = ObtenerIncidencia(IdIncidencia);

                Contexto.Incidencias.Remove(incidencia);

                Contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ExisteIncidencia(Guid IdIncidencia)
        {
            return Contexto.Incidencias.FirstOrDefault(i => i.IdIncidencia == IdIncidencia) != null;
        }
    }
}

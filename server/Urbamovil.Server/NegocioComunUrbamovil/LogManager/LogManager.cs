﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class LogManager
    {
        public static void AddTextLog(string text)
        {
            AppSettingsReader appSet = new AppSettingsReader();
            string pathFile = appSet.GetValue("KEY_PATH_LOG", typeof(string)).ToString();

            AddTextLog(pathFile, text);
        }

        private static void AddTextLog(string pathFile, string text)
        {
            string basePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Remove(0, 6) + "/";
            string logPath = Path.GetFullPath(basePath + pathFile);
            try
            {

                StreamWriter _escritor = null;
                _escritor = File.AppendText(String.Format(logPath, DateTime.Now.ToString("yyyyMMdd")));
                _escritor.WriteLine(DateTime.Now.ToString());
                _escritor.WriteLine("---------------------------------------");
                _escritor.WriteLine(text);
                _escritor.WriteLine("---------------------------------------");
                _escritor.Flush();
                _escritor.Close();
            }
            catch { 
            //existe posibilidad de bloqueo del archivo, lo genero en otro archivo
                try
                {
                    StreamWriter _escritor = null;
                    _escritor = File.AppendText(String.Format(logPath, DateTime.Now.ToString("yyyyMMdd_hhmmss")));
                    _escritor.WriteLine(DateTime.Now.ToString());
                    _escritor.WriteLine("---------------------------------------");
                    _escritor.WriteLine(text);
                    _escritor.WriteLine("---------------------------------------");
                    _escritor.Flush();
                    _escritor.Close();
                }
                catch
                {
                    //en caso contrario se obvia el errror
                }
            }
        }

        public static void AddExceptionLog(Exception ex)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(ex.Message);
            if (!string.IsNullOrWhiteSpace(ex.StackTrace)) { sb.AppendLine(ex.StackTrace); }

            if (ex.InnerException != null)
            {
                sb.AppendLine("-- Excepcion Interna --");
                sb.AppendLine(ex.InnerException.Message);
                if (!string.IsNullOrWhiteSpace(ex.InnerException.StackTrace)) { sb.AppendLine(ex.InnerException.StackTrace); }
            }

            AddTextLog(sb.ToString());
        }
    }
}

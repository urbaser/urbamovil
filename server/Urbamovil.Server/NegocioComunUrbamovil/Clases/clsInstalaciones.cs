﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsInstalaciones
    {
        public Guid IdInstalacion { get; set; }
        public string Nombre { get; set; }
        public string URL { get; set; }
        public string URL_MAPA_AGOL { get; set; }
        public Guid IdTipoInstalacion { get; set; }
        public string NombreTipoInstalacion { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsDistritos
    {
        public int IdDistrito { get; set; }
        public string Distrito { get; set; }
        public Guid IdInstalacion { get; set; }
    }
}

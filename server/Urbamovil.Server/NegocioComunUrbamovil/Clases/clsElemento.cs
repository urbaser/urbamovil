﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsElemento
    {
        public int IdElemento { get; set; }
        public string NombreElemento { get; set; }
        public Guid IdInstalacion { get; set; }
        public int IdTipoElemento { get; set; }
        public int? IdZona { get; set; }
        public int? IdRuta { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsFiltroUsuario
    {
        public Guid IdFiltroUsuario { get; set; }
        public string Nombre { get; set; }
        public List<FiltroCampo> FiltrosCampo { get; set; }

        public clsFiltroUsuario() {
            FiltrosCampo = new List<FiltroCampo>();
        }

        public FiltroIncidencia toFiltroIncidencia()
        {
            FiltroIncidencia filtro = new FiltroIncidencia();

            foreach (var f in FiltrosCampo)
            {
                filtro.SetCampo(f);
            }

            return filtro;
        }
    }
}

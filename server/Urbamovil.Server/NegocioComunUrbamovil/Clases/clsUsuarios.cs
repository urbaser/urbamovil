﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsUsuarios
    {
        public Guid IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Direccion { get; set; }
        public string Empresa { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
        public Guid? IdPerfil { get; set; }
        public string NombrePerfil { get; set; }
        public clsPermisos Permisos { get; set; }
        public string IPDireccion { get; set; }
        public string TelefonoMovil { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string PreferenciasJSON { get; set; }
        public Guid? TicketSesion { get; set; }
        public int IdIdioma { get; set; }
        public string CodigoIdioma { get; set; }
        public bool Activo { get; set; }
        public DateTime? FechaValidezCC { get; set; }
        public string ContraClave { get; set; }

        public List<clsFiltroUsuario> Filtros { get; set; }
        public List<clsIdiomas> Idiomas { get; set; }

        public clsUsuarios()
        {
            this.Pass = null;
            this.TicketSesion = null;
            this.Filtros = null;
            this.ContraClave = string.Empty;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(IdUsuario.ToString());
            sb.AppendLine("Nombre: " + Nombre);
            sb.AppendLine("Apellido1: " + Apellido1);
            sb.AppendLine("Apellido2: " + Apellido2);
            sb.AppendLine("Dirección: " + Direccion);
            sb.AppendLine("Empresa: " + Empresa);
            sb.AppendLine("Email: " + Email);
            sb.AppendLine("Login: " + Login);

            return sb.ToString();
        }
    }

    public class clsPermisos
    {
        public bool NuevaIncidencia { get; set; }
        public bool ModificarIncidencia { get; set; }
        public bool ListadoIncidencias { get; set; }
        public bool HistoricoIncidencias { get; set; }
        public bool CambioEstadoIncidencia { get; set; }
        public bool ConsultarIncidencia { get; set; }
    }
}

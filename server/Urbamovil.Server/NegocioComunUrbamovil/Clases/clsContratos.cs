﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsContratos
    {
        public Guid IdContrato { get; set; }
        public string Nombre { get; set; }
        public Guid? IdGEO { get; set; }
        public Guid IdInstalacion { get; set; }
        public string NombreInstalacion { get; set; }
    }
}

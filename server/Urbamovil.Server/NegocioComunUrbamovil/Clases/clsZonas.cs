﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsZonas
    {
        public int IdZona { get; set; }
        public string Zona { get; set; }
        public Guid IdInstalacion { get; set; }
        public int IdEspacioUrbano { get; set; }
    }
}

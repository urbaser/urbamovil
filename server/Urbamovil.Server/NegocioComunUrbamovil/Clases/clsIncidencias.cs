﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class clsIncidencias
    {
        public Guid IdIncidencia { get; set; }
        public int IdGrupo { get; set; }
        public int? IdTipo { get; set; }
        public int? IdDescripcion { get; set; }
        public int IdPrioridad { get; set; }
        public int? IdEstado { get; set; }
        public int IdProvincia { get; set; }
        public int IdMunicipio { get; set; }
        public string Imagen { get; set; }
        public bool CargandoImagen { get; set; }
        public string ImagenCierre { get; set; }
        public bool CargandoImagenCierre { get; set; }
        public string Observaciones { get; set; }
        public bool Sincronizada { get; set; }
        public int IdOperacionDestino { get; set; }
        public Guid IdUsuarioAlta { get; set; }
        public DateTime FechaAlta { get; set; }
        //public DateTime FechaUltimaModificacion { get; set; }
        public string CodigoIncidencia { get; set; }
        public List<clsLocalizacionesIncidencias> Localizaciones { get; set; }
        
        public clsIncidencias() 
        {
            Localizaciones = new List<clsLocalizacionesIncidencias>();
        }
    }
}

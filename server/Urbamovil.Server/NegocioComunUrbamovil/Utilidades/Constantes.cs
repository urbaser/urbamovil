﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class Campos
    {
        public const string ESTADO_INCIDENCIA = "ESTADO_INCIDENCIA";
        public const string GRUPO_TIPO_DESCRIPCION = "GRUPO_TIPO_DESCRIPCION";
        public const string FECHA_DESDE = "FECHA_DESDE";
        public const string FECHA_HASTA = "FECHA_HASTA";
        public const string ULTIMOS_DIAS = "ULTIMOS_DIAS";
        public const string CONTRATO = "CONTRATO";
        public const string PRIORIDAD = "PRIORIDAD";
        public const string PROVINCIA_MUNICIPIO = "PROVINCIA_MUNICIPIO";
        public const string DIRECCION = "DIRECCION";
        public const string ELEMENTO_INVENTARIO = "ELEMENTO_INVENTARIO";
        
    }

    public class Tipos
    {
        public const string DATE = "DATE";
        public const string INTEGER = "INTEGER";
        public const string GUID = "GUID";
        public const string TEXTO = "TEXTO";
    }

    public class TipoPerfiles
    {
        public const string ADMINISTRADOR = "Administrador";
        public const string CIUDADANO = "Ciudadano";
        public const string NORMAL = "Normal";
    }

    public class ISO_Idiomas
    {
        public const string ES = "es-es";
        public const string EN = "en-en";
        public const string FR = "fr-fr";
    }
}

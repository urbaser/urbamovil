﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class Util
    {
        public static string GenerateCode(int n)
        {
            StringBuilder sb = new StringBuilder(n);
            Random rnd = new Random();

            for (int i=0;i<n;i++)
            {
                int s = rnd.Next(2);
                char c = s == 0 ? (char)(rnd.Next(48, 58)) : (char)(rnd.Next(67, 91));

                sb.Append(c);
            }

            return sb.ToString();
        }
    }
}

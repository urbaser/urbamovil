﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NegocioComunUrbamovil
{
    public class JSONManager
    {
        public static string ConvertObjectToJSON<T> (T obj)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);

            return json.ToString();
        }

        public static T DeserializeJSON<T>(string json)
        {
            T obj = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);

            return obj;
        }
    }
}

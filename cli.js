/**
 * Created by 7daysofrain on 2/2/16.
 */
var fs = require('fs');
var Q = require('q');
var Mustache = require("mustache");

var args = process.argv.splice(2);
var command = args.shift();

String.prototype.splice = function( idx, rem, s ) {
	return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
};

switch(command){
	case "create-view":
		createView.apply(this,args);
		break;
	case "remove-view":
		removeView.apply(this,args);
		break;
	default:
		console.error("Unrecognized command " + command);
}
function removeView(id){
	var parts = id.split(".");
	var ipath = "views/";
	var vpath = __dirname + "/src/views/";
	var vmpath = __dirname + "/src/content/script/app/viewModels/";
	while(parts.length > 1){
		var part = parts.shift();
		vpath += part + "/";
		vmpath += part + "/";
		ipath += part + "/";
	}
	fs.unlink(vpath + parts + ".html");
	fs.unlink(vmpath + parts + ".js");

	// Update main.html

	var main = fs.readFileSync(__dirname + "/src/main.html").toString();
	// Seguro que esto se podría mejorar con un parser de dom, pero en fin...
	main = main.replace("<!-- build:include " + ipath + parts + ".html" + " --><!-- /build -->","")
	fs.writeFileSync(__dirname + "/src/main.html",main);

	console.log("Succesfully removed view " + id);
}
function createView(id){
	var data = {
		id:id,
		viewId: id.replace(/\./g,"-")
	};

	var view = fs.readFileSync(__dirname + '/templ/view.html').toString();
	view = Mustache.render(view, data);
	var viewModel = fs.readFileSync(__dirname + '/templ/viewmodel.js').toString();
	viewModel = Mustache.render(viewModel, data);

	var parts = id.split(".");
	var ipath = "views/";
	var vpath = __dirname + "/src/views/";
	var vmpath = __dirname + "/src/content/script/app/viewModels/";
	while(parts.length > 1){
		var part = parts.shift();
		vpath += part + "/";
		vmpath += part + "/";
		ipath += part + "/";
		if(!fs.existsSync(vpath)){
			fs.mkdir(vpath);
		}
		if(!fs.existsSync(vmpath)){
			fs.mkdir(vmpath);
		}
	}

	fs.writeFileSync(vpath + parts + ".html",view);
	fs.writeFileSync(vmpath + parts + ".js",viewModel);

	// Update main.html

	var main = fs.readFileSync(__dirname + "/src/main.html").toString();
	// Seguro que esto se podría mejorar con un parser de dom, pero en fin...
	var index = main.indexOf('<!-- mark -->') + 13;
	var main = main.splice(index,0,"\n\t<!-- build:include " + ipath + parts + ".html" + " --><!-- /build -->");
	fs.writeFileSync(__dirname + "/src/main.html",main);

	console.log("Succesfully created view " + id);
}


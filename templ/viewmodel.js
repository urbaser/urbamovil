define([
	"jQuery",
	"kendo/kendo.core",
	"kendo/kendo.data",
	"kendo/kendo.binder"

], function(j,kendo) {
	'use strict'
	var vm = {
		onViewInit: function(event){
			console.log("Init view: {{viewId}}");
			this.view = event.view.element;
		},
		onViewShow: function(event){
			console.log("Show view: {{viewId}}");
		},
		onViewHide: function(event){
			console.log("Hide view: {{viewId}}" );
		}
	};
	return kendo.observable(vm);
});
